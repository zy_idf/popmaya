<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('profile_menu_top'))
{
	function profile_menu_top()
	{	
		$CI =& get_instance();
		$CI->load->model('member/m_member');
    	$uid = $CI->session->userdata('user_id');
    	$id=$CI->uri->segment(4);
    	if(strcmp($CI->uri->segment(3),"playlist")==0 || strcmp($id,"")==0 || strcmp($id,"playlist_detail")==0){
			$id=$uid;
		}
		if(!is_numeric($id)){
			$id=$uid;
		}
		//nisa: ini dikomen soalnya kalo menu top kan selalu dirinya
    	//if($id=='') //my profile
		//{
			$row=$CI->m_member->get_user_profile($uid);
		//}
		//else //other member profile
		//{
			//$row=$CI->m_member->get_user_profile($id);
		//}

		$name=$row->up_name;

    	//setting data
		if($uid==$id)
		{
			$user_id=$uid;
			$image_path=$CI->m_member->get_profile_path($uid);
		}
		else
		{
			$user_id=$row->up_uid;
			$image_path=$CI->m_member->get_profile_path($row->up_uid);
			
			$my_user_id=$uid;
			$my_name=$CI->m_member->get_user_profile($uid)->up_name;
		}

//  		$genres=$CI->m_member->get_genre();
		$unread_messages=$CI->m_member->get_count_unread_messages($user_id);		
		$count_friend_request=$CI->m_member->get_count_friend_request($user_id);
		$count_notification=$CI->m_member->get_count_notification($user_id);

		if($CI->session->userdata('user_id')){
			echo '
				<div class="profile-right">';
					if($CI->session->userdata('user_id')==$uid){ 
					echo '
     					<div class="profile-dropmenu">'.anchor('member/profile/'.$user_id, $name);
	  				}else {
	  					echo '
						<div class="profile-dropmenu">'.anchor('member/profile/'.$my_user_id, $my_name);
	 				}
        echo '
         <ul>';
         	if($CI->session->userdata('group')=='admin'){
				echo '<li>'.anchor(site_url().'track/manage', 'Track CMS').'</li>
				<li>'.anchor(site_url().'admin/manage', 'Admin CMS').'</li>';
    		}
    		echo '
             <li>'.anchor(site_url().'member/use_as_page', 'Use as Page').'</li>
             <li>'.anchor(site_url().'member/profile', 'Profile').'</li>
             <li><a href="#">Setting</a></li>
             <li>'.anchor('member/profile_edit', 'Edit Profile').'</li>
             <li>'.anchor('home/logout', 'Logout').'</li>
         </ul>
     					</div>
     					<span>&nbsp;|&nbsp;</span>
						<span class="noti_container">
						<a href="'.base_url().'member/friend_request">
						'.image_asset('star_ico.png', '', array('alt'=>'favorite')).'
				 		<span class="noti_bubble">
						<!-- nisa 25 Des friend request notification -->
						';
						if($count_friend_request){
							echo $count_friend_request;
						}
					echo '	
						</span>        		
						</a>
						</span>
         	<span class="noti_container">
             <a href="'.site_url().'member/message/box/inbox">'.image_asset('message_ico.png', '', array('alt'=>'notification')).'</a>
             	<span class="noti_bubble">';
				if($unread_messages>0){
					echo $unread_messages;
				} 
			echo '	
				</span>
         	</span>   
     		<span class="noti_container">
				<a href="'.base_url().'member/notification">
			'.image_asset('circle_ico.png', '', array('alt'=>'refresh')).'
				</a>
		 		<span class="noti_bubble">
				<!-- nisa 25 Des self notification --> ';
				if($count_notification){ 
					echo $count_notification;
				}
			echo '
				</span>        		
			</span>
 				</div>';
		}		
	}
}

if(!function_exists('sidebar_left_suggest_song')){
	function sidebar_left_suggest_song($up_uid='', $user_id='')
    {
    	//music sidebar, my song list
    	$CI =& get_instance();
    	$CI->config->load('my_config');
		$CI->load->model('member/m_member');
    	
    	//music sidebar, suggest song drop down menu
  		$fav_genre=$CI->m_member->get_fav_genre($user_id);
  		$fav_genre=explode(", ", $fav_genre);
  		$genres=$CI->m_member->get_genre();
  		$i=0;
  		$fav_genre_display='';

		if($fav_genre[0]) //have favourite genre
		{
			foreach($genres as $g)
			{
				if($g->g_id==$fav_genre[$i])
				{
					$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$i++;
				}
			}	
		}
		else //do not have favourite genre
		{
			foreach($genres as $g)
			{
				$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
				$i++;
			}				
		}	
		$suggest_song_option=$fav_genre_display;
		$genre_id=$fav_genre_display[0]['id'];

  		$limit=$CI->config->item('limit_sidebar_suggest_song_list');		
  		$song_by_genre=$CI->m_member->get_song_by_genre($genre_id, $limit);

    	echo '
    	Suggest song by<br />
         <select name="song_genre" id="song_genre">';
			foreach($suggest_song_option as $s) {
            	echo '<option value="'.$s['id'].'">'.$s['name'].'</option>';
			}
		echo '	
         </select>';
		if($song_by_genre) {
         	echo '<ul>';
				foreach($song_by_genre as $s) {
				echo '
              		<li>
                  		<a href="#">'.image_asset('thumb_song_list1.jpg', '', array('alt'=>'ERK', 'class'=>'thumb-list')).'</a>
                  		<h4><a href="#">'.$s->artist.'</a></h4>
                  		<span>'.$s->title.'</span>
                  		<div class="button-share">
                     	<a href="#" title="Play">'.image_asset('play_ico.png', '', array('alt'=>'play')).'</a>
                     	<a href="#" title="Download">
                     	'.image_asset('download_ico.png', '', array('alt'=>'download')).'</a>
                     	<a href="#" title="Share">'.image_asset('share_ico.png', '', array('alt'=>'share')).'</a>
                     	<a href="#" class="last" title="Add">'.image_asset('add_ico.png', '', array('alt'=>'add')).'</a>
                  		</div>
              		</li>';
				}
          echo '</ul>';
		} else {
			echo '<br />No song for this genre';
		} 
    }	
}

if(!function_exists('sidebar_left_profile'))
{
	function sidebar_left_profile($image_path='', $user_id='', $name, $gender, $city, $country, $ispagephoto)
    {	

    	$CI =& get_instance();

    	echo '
    	<div class="profile-picture">
     		'.image_profile($image_path.'/'.$user_id.'.jpg', array('alt'=>'profile')).'
      	</div>
      <div class="profiler">
          <h2>'.$name.'</h2>
          <span>'.$gender.'</span>
          <span>'.$city.' - '.$country.'</span>
      </div>';
      if($user_id!=$CI->session->userdata('user_id')){
      	$icon_img=image_asset('icon_add_white.png');

      	// belom ada pengecekan dia udh request apa belom
        // harus ditambahkan
      	echo anchor(site_url().'member/add_friend/'.$user_id, $icon_img.' add friend', 'class="uiButton m_side_profile"').'<br class="clear"/>';
      }
      echo '
      <div class="profile-link">
          <ul>';
           
			$nv_actv='active';
			$cls1=$cls2=$cls3=$cls4=$cls5=$cls6='';
			switch($CI->uri->segment(4)){
				case 'info' : $cls1=$nv_actv; break;
				case 'status' : $cls2=$nv_actv; break;
				case 'voice' : $cls3=$nv_actv; break;
				case 'photo' : $cls4=$nv_actv; break;
				case 'video' : $cls5=$nv_actv; break;
				case 'note' : $cls6=$nv_actv; break;
			}
			
			if($ispagephoto == true){
				$cls4=$nv_actv;
				$cls1="";
			}
		
			//$user_id = ($this->uri->segment(3)=="") ? $this->session->userdata('user_id') : $this->uri->segment(3);
			echo '
			    <li><a href="'.site_url().'member/profile_info/'.$user_id.'" class="info '.$cls1.'" title="Info">Info</a></li>
			    <li><a href="'.site_url().'member/profile/'.$user_id.'/status" class="status '.$cls2.'" title="Status">Status</a></li>
			    <li><a href="'.site_url().'member/profile/'.$user_id.'/voice" class="voice '.$cls3.'" title="Voice">Voice</a></li>
			    <li><a href="'.site_url().'member/photo" class="photo '.$cls4.'" title="Photo">Photo</a></li>
			    <li><a href="'.site_url().'member/profile/'.$user_id.'/video" class="video '.$cls5.'" title="Video">Video</a></li>
			    <li><a href="'.site_url().'member/profile/'.$user_id.'/note" class="note '.$cls6.'" title="Note">Note</a></li>
          </ul>
      </div>';
    }
}

if(!function_exists('my_friend_list'))
{
	function my_friend_list($up_uid='', $user_id='')
    {
    	//music sidebar, my song list
    	$CI =& get_instance();
		$CI->config->load('my_config');
		$CI->load->model('member/m_member');
    	
    	$limit_sidebar_friend_list=$CI->config->item('limit_sidebar_friend_list');
    	$friends=$CI->m_member->get_friends($up_uid, $limit_sidebar_friend_list);

    	echo '
    	<h3>My Friend List</h3>
          <ul>';
			foreach($friends as $row) { 
	        	echo '<li>'.anchor("member/profile/".$up_uid, image_profile_thumb($row->up_uid.'.jpg', array('alt'=>$row->up_name, 'class'=>'thumb-list'))).'
	                  	<h4>'.anchor("member/profile/".$row->up_uid, $row->up_name).'</h4>
	                  	<span>'.$row->up_city.'</span>
	              </li>';
			}
		echo '		
          </ul>
          <span>'.anchor('member/friend_list/'.$user_id, 'View All', array('title'=>'View all', 'class'=>'more')).'</span>
          <div class="clear"></div>';
    }   
}

if ( ! function_exists('my_song_list')) // ^zy. 4 Jan
{
    function my_song_list($up_uid='')
    {
    	//music sidebar, my song list
    	$CI =& get_instance();
		$limit=$CI->config->item('limit_sidebar_fave_song_list');
		$CI->load->model('member/m_member');
		$song_list=$CI->m_member->get_fav_song($up_uid, $limit);	
    	
    	echo '
    	
          <h3>My Song List</h3>';
			if($song_list){
		echo '<ul>';
				foreach($song_list as $s) {
				//print_r($s);
				echo '<li>
							<!-- Tambahan Adi 27 Dec 2011 - link song detail -->
	                        <a href="#"><img src="'.site_url().'/assets/images/general/layout/thumb_song_list1.jpg" alt="ERK" class="thumb-list"/></a>
	                        <h4>'.anchor(site_url().'track/detail/'.$s->fs_id, $s->title).'</h4>
	                        <span>'.$s->artist.'</span>
	                        <div class="button-share">
	                          '.anchor(site_url().'assets/media/sample.mp3', image_asset('play_ico.png', '', array('alt'=>'play')), array('title'=>'Play', 'class'=>'btn_play')).'
	                          <!-- Tambahan Adi 27 Dec 2011 - link download music -->
							  <a href='.site_url().'member/download_music/sample.mp3 title="Download">
							  	<img src="'.site_url().'/assets/images/download_ico.png" alt="download"/></a>
	                          <a href="#" title="Share" class="hover_share_side"><img src="'.site_url().'/assets/images/share_ico.png" alt="share"/></a>
	                          <a href="'.site_url().'track/add_to_playlist/'.$s->fs_id.'" class="last add_to_playlist" title="Add">
	                          	<img src="'.image_asset_url('add_ico.png').'" alt="add"/></a>
	                        </div>
	                      </li>';
				} 
				
		echo '</ul>';
			} else {
		    	echo 'Your list is empty.<br />
		          	Find your Fav music here.';
			}
    }

	/* Nisa 18 Jan 2012. Buat bikin hour ago */
	function compare_dates($param)	 
	{ 
		$now=date("Y-m-d h:i:s"); 
		$date1 = new DateTime($now); 
	    $date2 = new DateTime($param); 
	    $interval = $date1->diff($date2); 
	    $years = $interval->format('%y'); 
	    $months = $interval->format('%m'); 
	    $days = $interval->format('%d'); 
		$hour = $interval->format('%h');
		$minutes = $interval->format('%i');
		$second = $interval->format('%s');
	    if($years!=0)
		{ 
	        $ago = $years.' year ago'; 
	    }
		else
	    {
			if($months!=0)
			{
				$ago = $months.' month ago'; 
			}
			else
			{
				if($days!=0)
				{
					$ago = $days.' days ago'; 
				}
				else
				{
					if($hour!=0)
					{
						$ago = $hour.' hour ago'; 
					}
					else
					{
						if($minutes!=0)
						{
							$ago = $minutes.' minutes ago';
						}
						else
						{
							$ago = $second.' second ago';
						}
					}
				}
			}
	    } 
	    return $ago;
	}
}

?>