<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override'] = '';

$route['mobile/profile'] = 'mobile/profile/index';
$route['mobile/profile/([0-9]+)'] = 'mobile/profile/index/$1';

$route['mobile/photo'] = 'mobile/photo/index';
$route['mobile/photo/([0-9]+)'] = 'mobile/photo/index/$1';

$route['mobile/post'] = 'mobile/post/index';
$route['mobile/post/([0-9]+)'] = 'mobile/post/index/$1';

$route['mobile/note'] = 'mobile/note/index';
$route['mobile/note/([0-9]+)'] = 'mobile/note/index/$1';

$route['mobile/video'] = 'mobile/video/index';
$route['mobile/video/([0-9]+)'] = 'mobile/video/index/$1';

$route['mobile/voice'] = 'mobile/voice/index';
$route['mobile/voice/([0-9]+)'] = 'mobile/voice/index/$1';

/* End of file routes.php */
/* Location: ./application/config/routes.php */