<?php
class Global_model extends CI_Model {
   
   
   	function get_image_ads() // ads
	{
		$query = $this->db->get_where('tr_ads', array('position' => 'ads'));
			$this->db->order_by('RAND()','asc');
			$this->db->limit(1);
		return $query->result();		
	}
    
	function notify_app($mode, $whr, $arrNotif){ // ^zy
		if($mode=='edit'){
	    	$this->db->where($whr);
	    	if($this->db->update('tr_notification', $arrNotif)){
	    		$app_ok=1;
	    	}else{
	    		$app_ok=0;
	    	}
	    }else{
	    	$this->db->insert('tr_notification', $arrNotif);
	    	if($this->db->insert_id()){
	    		$app_ok=$this->db->insert_id();
	    	}else{
	    		$app_ok=0;
	    	}
	    }

	    return $app_ok;

	}
		function update_read($mid){ // ^zy
	    	$this->db->where(array('ID'=>$mid));
	    	$this->db->update('tr_message', array('isread'=>1));
	    }

	    function admin_tools(){ // ^zy
	      $this->db->select('*');
	      $queat=$this->db->get('ms_artist_tools');
	      return $queat->result();
	    }

	    function update_ms_artist_tools($fieldItem, $valTbl){ // ^zy
	      $this->db->where(array('title'=>$fieldItem));
	      if($this->db->update('ms_artist_tools', array('status'=>$valTbl))){
	        return 1;
	      }else{ return 0; }
	    }

	    function limit_profile(){ // ^zy
	      $this->db->select('*');
	      $this->db->where(array('belongs_to'=>'profile'));  
	      $quelp=$this->db->get('ms_limit');
	      return $quelp->result();
	    }

	    function update_ms_limit($fieldItem, $valTbl){ // ^zy
	      $this->db->where(array('title'=>$fieldItem));
	      if($this->db->update('ms_limit', array('info'=>$valTbl))){
	        return 1;
	      }else{ return 0; }
	    }

	    function tarif_mobile(){ // ^zy
	      $this->db->select('*');
	      $this->db->where(array('belongs_to'=>'mobile_premium'));  
	      $quelp=$this->db->get('ms_tarif');
	      return $quelp->result();
	    }

	    function update_ms_tarif($fieldItem, $valTbl){ // ^zy
	      $this->db->where(array('title'=>$fieldItem));
	      if($this->db->update('ms_tarif', array('info'=>$valTbl))){
	        return 1;
	      }else{ return 0; }
	    }
	
   function log_user($action, $item_id)
   {
      $arr=array('action'=>$action, 'item_id'=>$item_id, 'user_id'=>$this->session->userdata('user_id'), 'level_id'=>$this->session->userdata('level_id'));
      $this->db->insert('history_logs', $arr);
   }
   
}
?>