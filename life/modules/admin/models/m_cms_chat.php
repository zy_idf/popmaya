<?php // ^zy
class M_Cms_Chat extends CI_Model 
{
	function get_paged_list($limit, $offset = 0, $whr, $likey){

	  	$this->db->select('a.*');
      	$this->db->from('tr_chat_room a');

      	if($whr){ 
			$this->db->where($whr);
		}
		if($likey){ 
			$this->db->like($likey);
		}
     	$this->db->limit($limit, $offset);
   		$query = $this->db->get();
   		return $query->result();
	}

	function get_room_paged_list($limit, $offset = 0, $whr, $likey){

	  	$this->db->select('a.*, b.up_name');
      	$this->db->from('tr_chat a');
      	$this->db->join('tr_user_profile b', 'a.c_sender_id=b.up_uid');

      	if($whr){ 
			$this->db->where($whr);
		}
		if($likey){ 
			$this->db->like($likey);
		}
     	$this->db->limit($limit, $offset);
   		$query = $this->db->get();
   		return $query->result();
	}

	function count_searches($whr, $likey){
		$this->db->select('a.*');
      	$this->db->from('tr_chat_room a');

      	if($whr){ 
			$this->db->where($whr);
		}
		if($likey){ 
			$this->db->like($likey);
		}
     	$this->db->limit($limit, $offset);
   		$query = $this->db->get();
   		return $query->result();
	}

	function count_all($whr){
		$this->db->select('cr_id');
      	$this->db->from('tr_chat_room');
      	if($whr){ 
			$this->db->where($whr);
		}
   		$query = $this->db->get();
   		return $query->num_rows();
	}
	function count_all_mssg($whr){
		$this->db->select('c_id');
      	$this->db->from('tr_chat');
      	if($whr){ 
			$this->db->where($whr);
		}
   		$query = $this->db->get();
   		return $query->num_rows();
	}

}
?>