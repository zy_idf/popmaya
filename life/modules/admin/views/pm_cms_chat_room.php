
<?php // ^zy
	$target=site_url().'admin/chat/new_room';
	$attr='';
	$check1=$check2='';
	$val=$val2='';
	$mode="add";
	$item_id='';
	if($item){
		$val=$item->cr_title;
		$val2=$item->cr_max_num_ppl;
		$mode='edit';
		$item_id=$item->cr_id;
	}
	echo '<h3>'.strtoupper($mode).' Chat Room</h3>';
	$hidden=array('mode'=>$mode, 'item_id'=>$item_id);
	echo form_open($target, $attr, $hidden);
	echo '<table class="new_chat_room">';
	echo '<tr><td>'.form_label('Title ').'</td><td>'.form_input(array('name'=>'item_title', 'id'=>'item_title', 'value'=>$val)).'</td></tr>';
	echo '<tr><td>'.form_label('Max. People ').'</td><td>'.form_input(array('name'=>'max_ppl', 'id'=>'max_ppl', 'value'=>$val2)).'</td></tr>';
	echo '<tr><td></td><td>'.form_submit(array('name'=>'submit', 'value'=>'Save')).'</td></tr>';
	echo '<tr><td colspan="2">&nbsp;</td></tr>';
	echo form_close();
?>
<table class="list_song">
	<tr><td colspan="4"><?= $pagination ?></td></tr>
	<tr>
		<!--<th></th>--><th>No</th><th>Room Name</th><th>Max. People</th><th>Status</th><th></th>
	</tr>
	<?php
		
		if(count($chat_rooms)>0){
			$i=$start_no;
			foreach ($chat_rooms as $items => $item) {
				$cls='';
				if(($i%2)==1){
					$cls='class="odd"';
				}
				echo '
			<tr '.$cls.'>
				<!--<td>'.form_checkbox('cb_'.$i).'</td>-->
				<td>'.$i.'</td>
				<td>'.$item->cr_title.'</td>
				<td>'.$item->cr_max_num_ppl.'</td>
				<td>'; echo ($item->cr_status==1) ? 'Active':'Not Active'; echo '</td>
				<td>';
					echo anchor(site_url().'admin/chat/room/'.$item->cr_id, 'Lihat Chat');

					echo ' &middot; '.anchor(site_url().'admin/chat/index/'.$item->cr_id, 'Edit');
					//echo anchor('#', 'Delete', array('class'=>'conf_item', 'title'=>'Hapus chat room ini?'));
					
					if($item->cr_status){ $rec_title="Deactivate"; }else{ $rec_title="Activate"; }
					echo ' &middot; '.anchor('admin/chat/set_status/'.$item->cr_id, $rec_title);

					echo '
				</td>
			</tr>
				';
				$i++;
			}
		}else{
			echo '<tr><td colspan="7"><h2>No item.</h2></td></tr>';
		}
	?>
</table>
