<?php // ^zy
	$target=site_url().'admin/song/search';
	$attr=$hidden='';
	$check1=$check2='';
	$val='';
	$check1='checked';
	if($this->uri->segment(4)){
		if($this->uri->segment(4)=="artist"){
			$check1='checked';
		}else{
			$check2="checked";
		}
		$val=urldecode($this->uri->segment(5));
	}
	echo form_open($target, $attr, $hidden);
	echo form_radio(array('name'=>'tipeSearch', 'value'=>'artist', 'checked'=>$check1)).' Artist ';
	echo ' &nbsp; '.form_radio(array('name'=>'tipeSearch', 'value'=>'album', 'checked'=>$check2)).' Album <br />';
	echo form_input(array('name'=>'item_title', 'id'=>'item_title', 'value'=>$val));
	echo form_submit(array('name'=>'submit', 'value'=>'Search'));
	echo form_close();
?>
<br />
<?= $pagination ?>
<table class="list_song">
	<tr>
		<!--<th></th>--><th>No</th><th>Artist</th><th>Album</th><th>Nama</th><th class="w270">Info</th><th>Lama</th><th></th>
	</tr>
	<?php
		
		if(count($list_search)>0){
			$i=$start_no;
			foreach ($list_search as $items => $item) {
				$cls='';
				if(($i%2)==1){
					$cls='class="odd"';
				}
				echo '
			<tr '.$cls.'>
				<!--<td>'.form_checkbox('cb_'.$i).'</td>-->
				<td>'.$i.'</td>
				<td>'.$item->artist_name.'</td>
				<td>'.$item->album_title.'</td>
				<td>'.$item->title.'</td>
				<td>'.$item->post.'</td>
				<td>'.$item->time_length.'</td>
				<td>';
					if($item->ispromo){ $rec_title="Reset"; }else{ $rec_title="Recommend"; }
					echo anchor('admin/song/recommend/'.$item->s_id, $rec_title, array('class'=>'conf_item')).'</td>
			</tr>
				';
				$i++;
			}
		}else{
			echo '<tr><td colspan="7"><h2>No item.</h2></td></tr>';
		}
	?>
</table>
