<div class="form_add">
	<table>
	<?php 
		echo form_open_multipart('admin/manage/add_banner/'.$banner_type); 
		echo '<tr><td>Image title</td><td>'.form_input(array('name'=>$banner_type.'_title', 'value'=>'')).'</td></tr>';
		echo '<tr><td>Upload image</td><td><input type="file" name="banner_'.$banner_type.'_image" size="34" /></td></tr>';
		echo '<tr><td>Url</td><td>'.form_input(array('name'=>$banner_type.'_url', 'value'=>'')).'</td></tr>';
		echo '<tr><td>Online</td><td>'.form_dropdown('on_stat_'.$banner_type,array('0' => 'offline','1' => 'online'), '1').'</td></tr>';
		echo '<tr><td>Script</td><td>'.form_textarea(array('name'=>$banner_type.'_script', 'value'=>'')).'</td></tr>';
		echo '<tr><td></td><td>'.form_submit(array('name'=>'submit', 'value'=>'Submit')).'</td></tr>';
		echo form_close();
	?>
	</table>
</div>
<table class="list_song" style="font-size:11px;">
	<tr>
		<!--<th></th>--><th>No</th><th>Title</th><th>Url</th><th class="w270">Script</th><th>offline</th><th>online</th><th></th>
	</tr>
	<?php
		
		if(count($banner_data)>0){
			$i=1;
			foreach ($banner_data as $banner) {
				$cls='';
				if(($i%2)==1){
					$cls='class="odd"';
				}
				echo '
			<tr '.$cls.'>
				<td>'.$i.'</td>
				<td>'.$banner->title.'</td>
				<td>'.$banner->url.'</td>
				<td>'.$banner->script.'</td>
				<td>'.$banner->offline.'</td>
				<td>'.$banner->online.'</td>
				<td>'.anchor('admin/manage/edit_banner/'.$banner->ID.'/'.$banner_type, 'Edit').' &middot; '.anchor('admin/manage/delete_banner/'.$banner->ID.'/'.$banner_type, 'Delete', array('class'=>'delConf')).'</td>
			</tr>
				';
				$i++;
			}
		}
	?>
</table>