<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Song extends MX_Controller 
{ // ^zy
  private $limit = 20;
	function __construct(){
   		parent::__construct();

   		$this->load->library('form_validation');

		  $this->user_id=$this->session->userdata('user_id');
   		
      $this->load->model('m_cms_song', '', TRUE);

      $this->template->set_master_template('admin_cms');
      $this->template->add_region('ttl');
      $this->template->add_region('content');

      $this->output->enable_profiler(TRUE);
   	}
  
    function index(){
      
        $this->template->write('head_title', 'Song Recommendation');
        $data['foot_js']=$data['warn_js']='';

        /* pagination */ 
        $uri_segment = 4;
        if($this->uri->segment($uri_segment)==''){ $offset=0; }else{ $offset=$this->uri->segment($uri_segment); }
        $data['start_no']= $offset+1;

        $this->load->library('pagination');
        $config['base_url'] = site_url().'/'.$this->uri->segment(2).'/'.$this->uri->segment(3);
//        $config['total_rows'] = $this->m_track->count_all('');
        $config['per_page'] = $this->limit;
        $config['uri_segment'] = $uri_segment;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        /* pagination.end */

        $data['list_search']=$this->m_cms_song->get_paged_list($this->limit, $offset, '', '');
        $this->template->write('ttl', 'Song Recommendation');
        $this->template->write_view('content', 'pm_cms_recommend_song', $data, '');
        $this->template->render();       
    }

    function search(){
        $tipes=$this->input->post('tipeSearch');
        $item_title = $this->input->post('item_title');
        redirect(site_url().'admin/song/list_search/'.$tipes.'/'.$item_title);
    }

    function list_search(){
      
        $this->template->write('head_title', 'Song Recommendation');
        $data['foot_js']=$data['warn_js']='';
        $data['foot_script']='
          $(".conf_item").click(function(){
            var url = $(this).attr("href");
            var ttl = $(this).attr("title");
            if(confirm(ttl)) {
              location.href=url;
            } else {
              return false;
            }   
          });
        ';
        if($this->uri->segment(4)=="artist"){
          $sTerm=array('b.artist_name'=>urldecode($this->uri->segment(5)));  
        } else{
          $sTerm=array('c.title'=>urldecode($this->uri->segment(5)));  
        }       

        /* pagination */ 
        $uri_segment = 6;
        if($this->uri->segment($uri_segment)==''){ $offset=0; }else{ $offset=$this->uri->segment($uri_segment); }
        $data['start_no']= $offset+1;

        $this->load->library('pagination');
        if($this->uri->segment('6')==''){ $urlVal='a'; }else{
          $urlVal=$this->uri->segment('5');
        }
        
        $config['base_url'] = site_url().'/admin/'.$this->uri->segment(2).'/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$urlVal;

        $config['total_rows'] = $this->m_cms_song->count_searches('', $sTerm);
        $config['per_page'] = $this->limit;
        $config['uri_segment'] = $uri_segment;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        /* pagination.end */

        $data['list_search']=$this->m_cms_song->get_paged_list($this->limit, $offset, '', $sTerm);

        $this->template->write('ttl', 'Song Recommendation');
        $this->template->write_view('content', 'pm_cms_recommend_song', $data, '');
        $this->template->render();       
    }
	 
    function recommend($sid){
      $querec=$this->db->get_where('tr_song', array('s_id'=>$sid));
      if($querec->row()->ispromo==1){
        $rec_val=0;
      }else{
        $rec_val=1;
      }
      $this->db->where(array('s_id'=>$sid));
      if($this->db->update('tr_song', array('ispromo'=>$rec_val))){
        set_flash('warn', 'success', 'Data Updated');
      }else{
        set_flash('warn', 'notice', 'Data Not Updated');
      }
      redirect($_SERVER['HTTP_REFERER']);
    }
		
}