<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Fans extends MX_Controller 
{
	var $uid;
	
   	function __construct()
	{
   		parent::__construct();

   		$this->load->library('form_validation');
   		$this->load->model('m_fans', '', TRUE);
   		$this->load->model('member/m_member', '', TRUE);

		$this->uid=$this->session->userdata('user_id');

   		//$this->output->enable_profiler(TRUE);
   	}

	//------------------------------------------------------------------------------------------------------------------------frontpage
   	function index()
	{	
		
		$data['foot_js']=css_asset('plugin/sliderStyle.css').js_asset('jquery.nivo.slider.pack.js');
	    $data['foot_script']='
			var sics = $(".share_icons"); sics.hide();
		    $(\'#slider\').nivoSlider({speed:5000});

		    $("#tab ul li.middle a").addClass("active").show(); 
 	        $("#tab2").show();
		    ';	      
			
			$this->config->load('my_config');
			$limit_sidebar_friend_list=$this->config->item('limit_sidebar_friend_list');
			
			if($this->uid){
				$row=$this->m_member->get_user_profile($this->uid);
				$data['user_id']=$this->uid;
				$data['up_uid']=$row->up_uid; // ^zy 4 jan
				$data['name']=$row->up_name;
				$data['alias']=$row->up_alias;	
				$data['unread_messages']=$this->m_member->get_count_unread_messages($this->uid);		
				$data['count_friend_request']=$this->m_member->get_count_friend_request($this->uid);
				$data['count_notification']=$this->m_member->get_count_notification($this->uid);

				$data['gender_code']=$row->up_gender;
				$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
				$data['city']=$row->up_city;
				$data['country']=$row->up_country;
				$data['friends']=$this->m_member->get_friends($this->uid, $limit_sidebar_friend_list);
			}
			

			//Autthor	: Firman
			//Date		: 2011 Dec 15
			//Desc		: Load fans list and fans page amount	
			$data['fans']=$this->m_fans->get_fans_list($this->uri->segment(3)/10);
			/* pagination */ 
			$uri_segment = 3;
			if($this->uri->segment($uri_segment)==''){ $offset=0; }else{ $offset=$this->uri->segment($uri_segment); }
			$data['start_no']= $offset+1;

			$this->load->library('pagination');
			$config['base_url'] = site_url().'fans/index/';
			$data['jml_member']=$config['total_rows'] = $this->m_fans->get_num_fans();
			$config['per_page'] = 10;
			$config['uri_segment'] = $uri_segment;
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			/* pagination.end */
			
			//Suggest Song --> duplicate form member
			//music sidebar, suggest song drop down menu	
			$fav_genre=$this->m_fans->get_fav_genre($this->uid);
			$fav_genre=explode(", ", $fav_genre);
			$fav_artist=$this->m_fans->get_fav_artist($this->uid);
			$fav_artist=explode(", ", $fav_artist);
			
			$genres=$this->m_fans->get_genre();
			$artists=$this->m_fans->get_artist();
			$i=0;
			$fav_genre_display='';
			$fav_genre_string='';
			$fav_artist_display='';
			$fav_artist_string='';

			if($fav_genre[0]) //have favourite genre
			{
				foreach($genres as $g)
				{
					if($g->g_id==$fav_genre[$i])
					{
						$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
						$fav_genre_string .= $g->g_name.',';
						$i++;
					}
				}	
			}
			else //do not have favourite genre
			{
				foreach($genres as $g)
				{
					$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$fav_genre_string .= $g->g_name.',';
					$i++;
				}				
			}	
			  $data['suggest_song_option']=$fav_genre_display;
				$data['fav_genre_string']=$fav_genre_string;
				
			$i=0;	
			if($fav_artist[0]) //have favourite artist
			{
				foreach($artists as $a)
				{
					if($a->ID==$fav_artist[$i])
					{
						$fav_artist_display[$i]=array('id'=>$a->ID, 'name'=>$a->artist_name);
						$fav_artist_string .= $a->artist_name.',';
						$i++;
					}
				}	
			}
			else //do not have favourite genre
			{
				foreach($artists as $a)
				{
					$fav_artist_display[$i]=array('id'=>$a->ID, 'name'=>$a->artist_name);
					$fav_artist_string .= $a->artist_name.',';
					$i++;
				}				
			}	
			
				$data['fav_artist_string']=$fav_artist_string;

			  $genre_id=$fav_genre_display[0]['id'];
			$limit=$this->config->item('limit_sidebar_suggest_song_list');		
			$data['song_by_genre']=$this->m_fans->get_song_by_genre($genre_id, $limit);
			//end duplicate suggest song

			if($this->input->post('submit')){
				
				$data['fans']=$this->m_fans->get_fans_list_criteria($this->uri->segment(3)/10,$this->input->post('gender'),$this->input->post('age1'),$this->input->post('age2'));
			}
		
			
			//load ads data
			$data['ads_data']=$this->m_fans->get_image_ads();
			//render view
			$this->template->write('head_title', 'Artist');
			$this->template->write('keywords', '');
	      	$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
		  	$this->template->write_view('sidebar_left', 'pm_side_fans', $data, '');
			$this->template->write_view('middle_content', 'pm_fans_idx', $data, '');
			$this->template->write_view('sidebar_right', 'pm_fans_side_right', $data, '');
			$this->template->render();			      			

   	}


	
}
?>