Chat List
<br />
<br />

Room: <?= $room->cr_title; ?>
<br />
Created by: <?= $room->artist_name; ?>
<br />
People in this room: <?= $room->cr_max_num_ppl; ?>
<br />
Created on: <?= $room->cr_created_on; ?>
<br />
<br />

<?php if($this->session->flashdata('message')) {?>
	<?= $this->session->flashdata('message'); ?>
<?php } ?>

<?= form_open('chat/chat_add_process/'.$this->uri->segment(3)); ?>
	Kirim Chat<br />
	<?= form_label('Kirim Chat', 'chat'); ?>
	<?= form_input('chat', ''); ?><br />
	<?= form_submit('submit', "Kirim"); ?>
<?= form_close(); ?>

<br />
<br />

<ul>
	<?php foreach($chats as $c) { ?>
		<li>
			<?= $c->c_mssg; ?>
			<br />
			Created by: <?= $c->up_name; ?>
			<br />
			Created on: <?= $c->c_created_on; ?>
		</li>
	<?php } ?>
</ul>