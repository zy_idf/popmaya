<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Asyncro extends MX_Controller 
{ // ^zy
	
   	function __construct()
	{
   		parent::__construct();

		$this->user_id=$this->session->userdata('user_id');

   		$this->load->model('m_chat', '', TRUE);
		$this->load->model('member/m_member', '', TRUE);
/*		
		//if not logged in, redirect them to the login page
		if (!$this->ion_auth->logged_in())
		{
			redirect('home', 'refresh');
		}		
*/		
   		//$this->output->enable_profiler(TRUE);
   	}

   	function cek_rooms(){ // ^zy
   		// get recent updated date from db
		$last_update = $this->m_chat->get_last_updated_room();
		echo $last_update;
   	}

   	function get_rooms(){ // ^zy. berhubung ini kemaren katanya chatroom maks 7
   		$roomies = $this->m_chat->get_all_rooms();
		echo json_encode($roomies);
   	}

   	function get_messages(){ // ^zy
   		$messages = $this->m_chat->get_messages($this->input->post('room_id'));
		echo json_encode($messages);
   	}
   	
	function post_message(){ // ^zy
	  	$upd = $this->m_chat->insert_chat($this->input->post('room_id'), $this->session->userdata('user_id'), $this->input->post('mssg_ctt'));
   		if($upd){
   			echo $this->input->post('room_id');
   		}else{
   			echo 0;
   		}
	}
	function get_username(){ // ^zy
	  return $this->m_chat->get_username($this->session->userdata('user_id'));
	}
	function update_room(){ // ^zy
	   	$room_id=$this->input->post('room_no');
         $person=$this->session->userdata('user_id');
         $tipe_upd=$this->input->post('tipe_qr');
         $ppls=$this->m_chat->get_chatters($room_id);
         if($ppls!='' && $ppls!='[]'){
            if($tipe_upd=='add'){
               $arrd=json_decode($ppls, true);
               if(array_key_exists($person, $arrd)){
                  $arrd[$person]=date('Y-m-d H:i:s');
                  $ppl=$arrd;
                  $this->m_chat->update_room($room_id, json_encode($ppl));
               }      
            }
            if($tipe_upd=='del'){
               $arrd=json_decode($ppls, true);
               if(array_key_exists($person, $arrd)){
                  unset($arrd[$person]);
                  $this->m_chat->update_room($room_id, json_encode($arrd));
               }
            }            
         }else{
            // insert to table
            $ppl=array($person=>date('Y-m-d H:i:s'));
            $this->m_chat->update_room($room_id, json_encode($ppl));
         }
         $ppls=$this->m_chat->get_chatters($room_id);
         $arrd=json_decode($ppls, true);
         echo count($arrd);
	}
   function get_last_mssg_time(){ // ^zy
      $room_id = $this->input->get('room_id');
      $this->db->select('c_created_on');
      $this->db->from('tr_chat');
      $this->db->where(array('c_room_id'=>$room_id));
      $this->db->order_by('c_created_on', 'desc');
      $que_glm=$this->db->get();
      echo $que_glm->row()->c_created_on; 
   }
}
?>