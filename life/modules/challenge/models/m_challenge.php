<?php
class M_challenge extends CI_Model 
{
	//------------------------------------------------------------------------------------------------------------------------challenge
	/* Nisa. 22 Des 2011. Mengambi list photo challenge */
	function get_challenges()
	{
		$query=$this->db->select('tr_photo.*, tr_user_profile.up_name')		
			->from('tr_photo')
			->join('tr_user_profile', 'tr_user_profile.up_uid=tr_photo.memberID')
			->order_by('view', 'desc')
			->get();
		return $query->result();	
	}
	
	/* Nisa. 22 Des 2011. Mengambi sebuah photo challenge */
	function get_challenge($id)
	{
		$query=$this->db->select('tr_photo.*, tr_user_profile.up_name')		
			->from('tr_photo')
			->join('tr_user_profile', 'tr_user_profile.up_uid=tr_photo.memberID')
			->where('ID', $id)
			->get();
		return $query->row();	
	}
	
	/* Nisa. 22 Des 2011. Membuat photo challenge */
	function create_challenge($user_id, $title, $image, $description)
	{
		$data=array('memberID'=>$user_id
			, 'title'=>$title
			, 'image'=>$image
			, 'description'=>$description
			, 'post'=>'0'
			, 'rate'=>'0.00'
			, 'view'=>'0'
			, 'band'=>'1'
		);
		$this->db->insert('tr_photo', $data);
		
		return $this->db->insert_id();
	}
	
	/* Nisa. 22 Des 2011. Menambahkan counter viewed pada sebuah photo challenge */	
	function add_challenge_viewed($id)
	{
		$row=$this->get_challenge($id);
		$view=$row->view;
		
		$data=array('view'=>$view+1);
		$this->db->where('ID', $id);
		$this->db->update('tr_photo', $data);
	}
	
	//------------------------------------------------------------------------------------------------------------------------photo	
	/* Nisa. 22 Des 2011. Mengambi list photo dari sebuah photo challenge */
	function get_photos($id)
	{
		$query=$this->db->select('tr_photo_challenge.*, tr_user_profile.up_name')
			->from('tr_photo_challenge')
			->join('tr_user_profile', 'tr_user_profile.up_uid=tr_photo_challenge.memberID')
			->where('challengeID', $id)
			->where('status', '1')
			->order_by('tr_photo_challenge.created_on')
			->get();
		return $query->result();
	}
	
	/* Nisa. 22 Des 2011. Menambah sebuah photo dari photo challenge */
	function add_photo($user_id, $challenge_id)
	{
		//isi photo id
		$query=$this->db->order_by('photoID', 'desc')->limit(1)->get_where('tr_photo_challenge', array('challengeID'=>$challenge_id));
		$photo_id=now()+$user_id+$query->row()->photoID+1;
		
		//insert ke tr_photo_challenge
		$data=array('memberID'=>$user_id
			, 'challengeID'=>$challenge_id
			, 'photoID'=>$photo_id
			, 'rank'=>'0'
			, 'status'=>'1'
			, 'post'=>''
		);
		$this->db->insert('tr_photo_challenge', $data);		
		$insert_id=$this->db->insert_id();
		
		//tambah jumlah post di tr_photo
		$row=$this->get_challenge($challenge_id);
		$post=$row->post;		
		$data=array('post'=>$post+1);
		$this->db->where('ID', $challenge_id);
		$this->db->update('tr_photo', $data);
		
		return $insert_id;
	}
	
	/* Nisa. 22 Des 2011. Mengurangi sebuah photo dari photo challenge karena file gagal di upload */
	function add_photo_cancel($user_id, $challenge_id, $insert_id)
	{
		//delete row di tr_photo_challenge
		$this->db->delete('tr_photo_challenge', array('ID' => $insert_id));
		
		 //kurangi jumlah post di tr_photo
		$row=$this->get_challenge($challenge_id);
		$post=$row->post;
		$data=array('post'=>$post-1);
		$this->db->where('ID', $challenge_id);
		$this->db->update('tr_photo', $data);		
	}
}
?>