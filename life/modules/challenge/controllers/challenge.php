<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Challenge extends MX_Controller 
{
	/* Nisa. 22 Des 2011. Atribut user id, diisi di konstruktor dengan session */	
	var $user_id;
	
	//------------------------------------------------------------------------------------------------------------------------constructor
	/* Nisa. 22 Des 2011. Konstruktor kelas challenge controller */	
   	function __construct()
	{
   		parent::__construct();

		$this->user_id=$this->session->userdata('user_id');

   		$this->load->model('m_challenge', '', TRUE);
		$this->load->model('member/m_member', '', TRUE);
		
		//if not logged in, redirect them to the login page
		if (!$this->ion_auth->logged_in())
		{
			redirect('home', 'refresh');
		}		
		
   		//$this->output->enable_profiler(TRUE);
   	}

	//------------------------------------------------------------------------------------------------------------------------index
	/* Nisa. 22 Des 2011. Method index, menampilkan list dari photo contest */		
	function index()
	{
		//top content
		$data['unread_messages']=$this->m_member->get_count_unread_messages($this->user_id);		
		$data['count_friend_request']=$this->m_member->get_count_friend_request($this->user_id);
		$data['count_notification']=$this->m_member->get_count_notification($this->user_id);
		$data['user_id']=$this->user_id;
		$data['name']=$this->m_member->get_user_name($this->user_id);
		
		//middle content
		$data['challenge']=$this->m_challenge->get_challenges();
		//end middle content
		
		//right content
		$data['recommend_friends']=$this->m_member->get_recommendation($this->user_id, 5);
				
		//render view
		$this->template->write('head_title', 'Challenge');
		$this->template->write('keywords', '');
      	$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
	  	$this->template->write_view('sidebar_left', 'pm_challenge_side', $data, '');
		$this->template->write_view('middle_content', 'pm_challenge_idx', $data, '');
		$this->template->write_view('sidebar_right', 'general/sidebar_profile_right', $data, '');
		$this->template->render();	      					
	}
	
	//------------------------------------------------------------------------------------------------------------------------challenge
	/* Nisa. 22 Des 2011. Tambah photo challenge */		
	function challenge_add()
	{
		//top content
		$data['unread_messages']=$this->m_member->get_count_unread_messages($this->user_id);		
		$data['count_friend_request']=$this->m_member->get_count_friend_request($this->user_id);
		$data['count_notification']=$this->m_member->get_count_notification($this->user_id);
		$data['user_id']=$this->user_id;
		$data['name']=$this->m_member->get_user_name($this->user_id);
		
		//middle content
		//end middle content
		
		//right content
		$data['recommend_friends']=$this->m_member->get_recommendation($this->user_id, 5);
				
		//render view
		$this->template->write('head_title', 'Challenge');
		$this->template->write('keywords', '');
      	$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
	  	$this->template->write_view('sidebar_left', 'pm_challenge_side', $data, '');
		$this->template->write_view('middle_content', 'pm_challenge_add', $data, '');
		$this->template->write_view('sidebar_right', 'general/sidebar_profile_right', $data, '');
		$this->template->render();		
	}

	/* Nisa. 22 Des 2011. Proses tambah photo challenge */		
	function challenge_add_process()
	{
		$this->load->library('form_validation');
		$this->load->helper('form');
   		
		//validation rules
		$this->form_validation->set_rules('title', 'Judul', 'required');
		$this->form_validation->set_rules('image', 'Image', 'required');
		$this->form_validation->set_rules('description', 'Deskripsi', 'required');
		
		//validation message
    	$this->form_validation->set_message('required', '%s is required.');
		
		if ($this->form_validation->run() == true)
		{
			$insert_id=$this->m_challenge->create_challenge($this->user_id
				, $this->input->post('title')
				, $this->input->post('image')
				, $this->input->post('description')
				);
				
			//file upload
			$error="";
			$year=date('Y');
			$month=date('m');
			$day=date('d');

			//create directory	
			$dir="./assets/images/challenge/".$year."/";
			if(!is_dir($dir)) {mkdir($dir, 0755);}
			$dir="./assets/images/challenge/".$year."/".$month."/";
			if(!is_dir($dir)) {mkdir($dir, 0755);}
			$dir="./assets/images/challenge/".$year."/".$month."/".$day;
			if(!is_dir($dir)) {mkdir($dir, 0755);}

			$config['upload_path']="./assets/images/challenge/".$year."/".$month."/".$day;
			$config['file_name']=$insert_id;
			$config['allowed_types']='jpg';
			$config['max_size']='300';
			$config['max_width']='950';
			$config['max_height']='950';
			$config['overwrite']=TRUE;

			//upload
			$this->load->library('upload', $config);

			if (!$this->upload->do_upload())
			{
				$error=array('error' => $this->upload->display_errors());
			}	
			else
			{
				$data=array('upload_data'=>$this->upload->data());

				//thumbnailining
		        $thumb_config['width']=184;
		        $thumb_config['height']=184;
		        $thumb_config['master_dim']='width';
		        $thumb_config['create_thumb']=TRUE;
		        $thumb_config['maintain_ratio']=TRUE;
		       	$thumb_config['source_image']="./assets/images/challenge/".$year."/".$month."/".$day."/".$insert_id.".jpg";

		        $this->load->library('image_lib', $thumb_config);	

		        if (!$this->image_lib->resize())
		        {
					$error="Can not create photo thumbnail";
		        }
				//end thumbniling
			}
			//end upload
			
			$this->session->set_flashdata('message', $error);
			redirect('challenge');
		}
		else
		{
			$this->session->set_flashdata('message', validation_errors()."<br />Buat photo challenge gagal");
			redirect('challenge/challenge_add');														
		}
	}
	
	/* Nisa. 22 Des 2011. Lihat photo challenge */
	function challenge_view($id)
	{
		//top content
		$data['unread_messages']=$this->m_member->get_count_unread_messages($this->user_id);		
		$data['count_friend_request']=$this->m_member->get_count_friend_request($this->user_id);
		$data['count_notification']=$this->m_member->get_count_notification($this->user_id);
		$data['user_id']=$this->user_id;
		$data['name']=$this->m_member->get_user_name($this->user_id);
		
		//middle content
		$this->m_challenge->add_challenge_viewed($id);
		$data['challenge']=$this->m_challenge->get_challenge($id);		
		$data['photos']=$this->m_challenge->get_photos($id);
		//end middle content
		
		//right content
		$data['recommend_friends']=$this->m_member->get_recommendation($this->user_id, 5);
				
		//render view
		$this->template->write('head_title', 'Challenge');
		$this->template->write('keywords', '');
      	$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
	  	$this->template->write_view('sidebar_left', 'pm_challenge_side', $data, '');
		$this->template->write_view('middle_content', 'pm_challenge_photo', $data, '');
		$this->template->write_view('sidebar_right', 'general/sidebar_profile_right', $data, '');
		$this->template->render();		
	}
	
	//------------------------------------------------------------------------------------------------------------------------photo
	function photo_add_process($id)
	{	
		$insert_id=$this->m_challenge->add_photo($this->user_id
			, $id
			);
		
		$error="";
		$challenge=$this->m_challenge->get_challenge($id);
		$ch_date=substr($challenge->created_on, 0, 10);
		$ch_date=explode('-', $ch_date);
		$year=$ch_date[0];
		$month=$ch_date[1];
		$day=$ch_date[2];

		//create directory	
		$dir="./assets/images/challenge/".$year."/";
		if(!is_dir($dir)) {mkdir($dir, 0755);}
		$dir="./assets/images/challenge/".$year."/".$month."/";
		if(!is_dir($dir)) {mkdir($dir, 0755);}
		$dir="./assets/images/challenge/".$year."/".$month."/".$day;
		if(!is_dir($dir)) {mkdir($dir, 0755);}
		$dir="./assets/images/challenge/".$year."/".$month."/".$day."/".$id;
		if(!is_dir($dir)) {mkdir($dir, 0755);}

		$config['upload_path']="./assets/images/challenge/".$year."/".$month."/".$day."/".$id;
		$config['file_name']=$insert_id;
		$config['allowed_types']='jpg';
		$config['max_size']='300';
		$config['max_width']='950';
		$config['max_height']='950';
		$config['overwrite']=TRUE;

		$this->load->library('upload', $config);

		//upload
		if (!$this->upload->do_upload())
		{
			$error=$this->upload->display_errors();
			
			//jika gagal, hapus lagi row nya, kurangin lagi jumlah postnya
			$this->m_challenge->add_photo_cancel($this->user_id
				, $id
				, $insert_id
				);
		}	
		else
		{
			//thumbnailining
	        $thumb_config['width']=184;
	        $thumb_config['height']=184;
	        $thumb_config['master_dim']='width';
	        $thumb_config['create_thumb']=TRUE;
	        $thumb_config['maintain_ratio']=TRUE;
	       	$thumb_config['source_image']="./assets/images/challenge/".$year."/".$month."/".$day."/".$id."/".$insert_id.".jpg";

	        $this->load->library('image_lib', $thumb_config);	

	        if (!$this->image_lib->resize())
	        {
				$error=$this->upload->display_errors();
	        }
			//end thumbniling
		}
		//end upload
		
		$this->session->set_flashdata('message', $error);
		redirect('challenge/challenge_view/'.$id);
	}
}
?>