<div id="sidebar">
      <div class="profile-picture">
          <?php
            if(file_exists('./assets/images/profile/0000/00/00/'.$user_id.'.jpg')){
              echo image_asset('profile/0000/00/00/'.$user_id.'.jpg');
            }else{
              echo image_asset('profile-pict.jpg', '', array('alt'=>'profile'));
            }

             
          ?>
      </div>
      <div class="profiler">
          <h2><?= $name; ?></h2>
          <span><?= $gender; ?></span>
          <span><?= $city; ?> - <?= $country; ?></span>
      </div>  
      
      <div class="list-left">              
              <h3>My Song List</h3>
              <p class="bottom">Your List is Empty.<br/><a href="#" title="Find Brand">Find Your Fav Music Here</a></p>
              <div class="divider">&nbsp;</div>
          </div>
          <div class="list-left">
              <span>Suggest Song By</span>
              <div class="styled-select-small">
                  <select title="Select one">
                    <option>Genre</option>
                    <option>Blue</option>
                    <option>Red</option>
                    <option>Green</option>
                    <option>Yellow</option>
                    <option>Brown</option>
                  </select>
              </div>
              <ul>
                  <li>
                      <a href="#"><img src="assets/images/general/layout/thumb_song_list1.jpg" alt="ERK" class="thumb-list"/></a>
                      <h4><a href="#">ERK</a></h4>
                      <span>Dibuang Sayang</span>
                      <div class="button-share"><a href="#" title="Play"><img src="assets/images/play_ico.png" alt="play"/></a><a href="#" title="Download"><img src="assets/images/download_ico.png" alt="download"/></a><a href="#" title="Share"><img src="assets/images/share_ico.png" alt="share"/></a><a href="#" class="last" title="Add"><img src="assets/images/add_ico.png" alt="add"/></a></div>
                  </li>
                  <li>
                      <a href="#"><img src="assets/images/general/layout/thumb_song_list2.jpg" alt="Paramore" class="thumb-list"/></a>
                      <h4><a href="#">Paramore</a></h4>
                      <span>The Only Exception</span>
                      <div class="button-share"><a href="#" title="Play"><img src="assets/images/play_ico.png" alt="play"/></a><a href="#" title="Download"><img src="assets/images/download_ico.png" alt="download"/></a><a href="#" title="Share"><img src="assets/images/share_ico.png" alt="share"/></a><a href="#" class="last" title="Add"><img src="assets/images/add_ico.png" alt="add"/></a></div>
                  </li>
                  <li>
                      <a href="#"><img src="assets/images/general/layout/thumb_song_list3.jpg" alt="Cold Play" class="thumb-list"/></a>
                      <h4><a href="#">Cold Play</a></h4>
                      <span>Fix You</span>
                      <div class="button-share"><a href="#" title="Play"><img src="assets/images/play_ico.png" alt="play"/></a><a href="#" title="Download"><img src="assets/images/download_ico.png" alt="download"/></a><a href="#" title="Share"><img src="assets/images/share_ico.png" alt="share"/></a><a href="#" class="last" title="Add"><img src="assets/images/add_ico.png" alt="add"/></a></div>
                  </li>
                  <li>
                      <a href="#"><img src="assets/images/general/layout/thumb_song_list4.jpg" alt="Uthopia" class="thumb-list"/></a>
                      <h4><a href="#">Uthopia</a></h4>
                      <span>Hujan</span>
                      <div class="button-share"><a href="#" title="Play"><img src="assets/images/play_ico.png" alt="play"/></a><a href="#" title="Download"><img src="assets/images/download_ico.png" alt="download"/></a><a href="#" title="Share"><img src="assets/images/share_ico.png" alt="share"/></a><a href="#" class="last" title="Add"><img src="assets/images/add_ico.png" alt="add"/></a></div>
                  </li>
                  <li>
                      <a href="#"><img src="assets/images/general/layout/thumb_song_list5.jpg" alt="SID" class="thumb-list"/></a>
                      <h4><a href="#">SID</a></h4>
                      <span>Kuat Kita Bersinar</span>
                      <div class="button-share"><a href="#" title="Play"><img src="assets/images/play_ico.png" alt="play"/></a><a href="#" title="Download"><img src="assets/images/download_ico.png" alt="download"/></a><a href="#" title="Share"><img src="assets/images/share_ico.png" alt="share"/></a><a href="#" class="last" title="Add"><img src="assets/images/add_ico.png" alt="add"/></a></div>
                  </li>
              </ul>
              <div class="pagination">
                  <a href="#" class="first">Previous</a>
                  <a href="#" class="last">Next</a>
              </div>
              <div class="clear"></div>
          </div>
  

  </div>
