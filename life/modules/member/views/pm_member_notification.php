<!-- all style edited by gilangaramadan 26Des11 -->
<div class="grid_8">
	<div class="head-title">
		<h2>Notification <?= $count_notification; ?></h2>
	</div>
	<div class="grid_8" style="margin-left: 10px;">
		<div class="clear"></div>
				
		<div class="grid_7" style="width: 460px; margin-top: 10px;">
		<h6 style="margin-bottom: 10px;"><span class="abuMonyet">New Notification</span></h6>
		<hr />
		</div>				
		<div class="'clear"></div>
		
		<!--
		dr_notification_type:
		1	commented on your post
		2	also commented
		3	likes your post		
		4	posts on your wall
		5	responds on your wall
		6	share your status		
		7	accepted your friend request

		//dua ini tidak diimplementasikan disini karena ditangani oleh notification friend dan notification message
		8	sent friend request
		9	sent a message

		tr_notification:
		n_id
		n_type
		n_itemid
		n_subitemid
		n_fromuserid
		n_foruserid
		n_isread
		n_datetime
		n_who
		-->
				
		<?php if($notification) { ?>					
			<?php foreach($notification as $n) { ?>
				<div class="grid_7" style="width: 460px;">
					<p>
						<?php
						$path=substr($n->ul_createdon, 0, 10);
						$path=str_replace('-', '/', $path);
						$path=$path."/".$n->up_uid."_thumb.jpg";
						?>
						
						<?php if(is_file("./assets/images/profile/$path")) { ?>
							<?= image_asset("profile/$path",'',array('class'=>'left', 'style'=>'margin-top:0px;', 'width'=>'20')); ?>
						<?php } else { ?>
							<?= image_asset("profile/thumbnail/what.jpg",'',array('class'=>'left', 'style'=>'margin-top:0px;', 'width'=>'20')); ?>
						<?php } ?>						

						<span class="ijoText"><b><?= anchor('member/profile/'.$n->up_uid, $n->up_name); ?></b></span> 

						<?php if(($n->n_type==1)||($n->n_type==2)||($n->n_type==3)) { ?>
							<?php
							$n->nt_name=str_replace('post', '', $n->nt_name);
							?>
							<?= $n->nt_name; ?> 

							<span class="ijoText"><b><?= anchor('member/notification_detail/wall/'.$n->n_itemid, 'post'); ?></b></span>. <br />  
							<span style="margin-top: 3px;"><?= image_asset('status_ico.png',''); ?></span><span class="small biru"> <?= $n->n_datetime; ?></span>
							
						<?php } else if(($n->n_type==4)||($n->n_type==5)) { ?>
							<?php
							$n->nt_name=str_replace('wall', '', $n->nt_name);
							?>
							<?= $n->nt_name; ?> 

							<span class="ijoText"><b><?= anchor('member/notification_detail/wall/'.$n->n_itemid, 'wall'); ?></b></span>. <br />  
							<span style="margin-top: 3px;"><?= image_asset('status_ico.png',''); ?></span><span class="small biru"> <?= $n->n_datetime; ?></span>
							
						<?php } else if($n->n_type==6) { ?>
							<?php
							$n->nt_name=str_replace('status', '', $n->nt_name);
							?>
							<?= $n->nt_name; ?> 
							
							<span class="ijoText"><b><?= anchor('member/notification_detail/wall/'.$n->n_itemid, 'Status'); ?></b></span>. <br />  
							<span style="margin-top: 3px;"><?= image_asset('status_ico.png',''); ?></span><span class="small biru"> <?= $n->n_datetime; ?></span>
							
						<?php } else if($n->n_type==7) { ?>
							<?= $n->nt_name; ?> 
							<br />														
							<span style="margin-top: 3px;"><?= image_asset('status_ico.png',''); ?></span><span class="small biru"> <?= $n->n_datetime; ?></span>
							
						<?php } ?>					
						
					</p>
					<hr />
				</div>
			<?php } ?>			
		<?php } else { ?>
			<div class="grid_7" style="width: 460px;">
				<p>No new notification</p>
			</div>
		<?php } ?>


		<div class="grid_7" style="width: 460px; margin-top: 10px;">
		<h6 style="margin-bottom: 10px;"><span class="abuMonyet">All Notification</span></h6>
		<hr />
		</div>
		
		
		<?php if($all_notification) { ?>					
			<?php foreach($all_notification as $n) { ?>		
				<div class="grid_7" style="width: 460px;">
				<p>
					<?php
					$path=substr($n->ul_createdon, 0, 10);
					$path=str_replace('-', '/', $path);
					$path=$path."/".$n->up_uid."_thumb.jpg";
					?>
					
					<?php if(is_file("./assets/images/profile/$path")) { ?>
						<?= image_asset("profile/$path",'',array('class'=>'left', 'style'=>'margin-top:0px;', 'width'=>'20')); ?>
					<?php } else { ?>
						<?= image_asset("profile/thumbnail/what.jpg",'',array('class'=>'left', 'style'=>'margin-top:0px;', 'width'=>'20')); ?>
					<?php } ?>						

					<span class="ijoText"><b><?= anchor('member/profile/'.$n->up_uid, $n->up_name); ?></b></span> 

					<?php if(($n->n_type==1)||($n->n_type==2)||($n->n_type==3)) { ?>
						<?php
						$n->nt_name=str_replace('post', '', $n->nt_name);
						?>
						<?= $n->nt_name; ?> 

						<span class="ijoText"><b><?= anchor('member/notification_detail/wall/'.$n->n_itemid, 'post'); ?></b></span>. <br />  
						<span style="margin-top: 3px;"><?= image_asset('status_ico.png',''); ?></span><span class="small biru"> <?= $n->n_datetime; ?></span>
						
					<?php } else if(($n->n_type==4)||($n->n_type==5)) { ?>
						<?php
						$n->nt_name=str_replace('wall', '', $n->nt_name);
						?>
						<?= $n->nt_name; ?> 
						
						<span class="ijoText"><b><?= anchor('member/notification_detail/wall/'.$n->n_itemid, 'wall'); ?></b></span>. <br />  
						<span style="margin-top: 3px;"><?= image_asset('status_ico.png',''); ?></span><span class="small biru"> <?= $n->n_datetime; ?></span>
						
					<?php } else if($n->n_type==6) { ?>
						<?php
						$n->nt_name=str_replace('status', '', $n->nt_name);
						?>
						<?= $n->nt_name; ?> 
						
						<span class="ijoText"><b><?= anchor('member/notification_detail/wall/'.$n->n_itemid, 'status'); ?></b></span>. <br />  
						<span style="margin-top: 3px;"><?= image_asset('status_ico.png',''); ?></span><span class="small biru"> <?= $n->n_datetime; ?></span>
						
					<?php } else if($n->n_type==7) { ?>
						<?= $n->nt_name; ?> 
						<br />
						<span style="margin-top: 3px;"><?= image_asset('status_ico.png',''); ?></span><span class="small biru"> <?= $n->n_datetime; ?></span>
						
					<?php } ?>					
				</p>
				<hr />
				</div>
			<?php } ?>			
		<?php } else { ?>
			<div class="grid_7" style="width: 460px;">
				<p>No new notification</p>
			</div>
		<?php } ?>
				
	</div>
</div>