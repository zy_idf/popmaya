<h1 class="profile_name_title"><?= $name; ?></h1>
<div id="tab">
	<ul>
       	<li class="middle"><a href="#tab2">Info</a></li>
    </ul>
</div>
<!--<div id="fans-of"><label>Fans of : </label><?= $fav_artist_string; ?></div>-->
<div class="profile-title">
	<h2>Basic Info</h2>
</div>
<?php //to whomever wrote below code, please use table for tabular data. rgds, ^zy. ?>
<div class="profile-row">
	<label>Full Name</label> :
	<div class="data"><?= $name; ?></div>
</div>
<div class="profile-row">
	<label>Nick Name</label> :
	<div class="data"><?= $alias; ?></div>
</div>
<div class="profile-row">
	<label>Mobile Number</label> :
	<div class="data"><?= $phone; ?></div>
</div>
<div class="profile-row">
	<label>Gender</label> :
	<div class="data"><?= $gender; ?></div>
</div>
<div class="profile-row">
	<label>Birthday</label> :
	<div class="data"><?= date('d M Y', strtotime($birthday)); ?></div>
</div>
<div class="profile-row">
	<label>Status</label> :
	<div class="data"><?= $status; ?></div>
</div>
<div class="profile-row">
	<label>Location</label> :
	<div class="data"><?= $state.'/'.$country; ?></div>
</div>
<div class="profile-row">
	<label>Address</label> :
	<div class="data"><?= $address; ?></div>
</div>
<div class="profile-row">
	<label>ZIP / Postal Code</label> :
	<div class="data"><?= $zip_code; ?></div>
</div>
<div class="profile-title">
	<h2>Work and Education</h2>
</div>
<div class="profile-row">
	<label>Occupation</label> :
	<div class="data"><?= $occupation; ?></div>
</div>
<div class="profile-row">
	<label>Companies</label> :
	<div class="data"><?= $companies; ?></div>
</div>
<div class="profile-row">
	<label>School</label> :
	<div class="data"><?= $school; ?></div>
</div>
<div class="profile-title">
	<h2>Favorite Information</h2>
</div>
<div class="profile-row">
	<label>Favorite Genre</label> :
	<div class="data"><?= $fav_genre_string; ?></div>
</div>
<div class="profile-row">
	<label>Favorite Artist</label> :
	<div class="data"><?= $fav_artist_string; ?></div>
</div>