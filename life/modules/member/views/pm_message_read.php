<div class="head-title">
	<span class="mssg_ttl">
        Read Message
    </span>
</div>
      
<div id="track_list">
    <div class="mssg_control">
        <?php 
            $mssg_sbjt='Untitled';
            if(strlen($mssg_read->subject)>1){
                $mssg_sbjt = $mssg_read->subject;
            }
        
    	   echo anchor(site_url().'member/message/box/inbox', 'Back to Inbox')
        ?>
        <br />
        <h3>Subject : </h3><?= $mssg_sbjt ?>
        <h3>Message : </h3>
        <?= nl2br($mssg_read->message); ?>
        <hr />
        <strong>Reply</strong>
        <hr />
        <?php
            $path_to=site_url().'member/message/send';
            $attributes=array('id'=>'new_message');
            $hidden=array('friend_id'=>$reply_to_id); // harus get member id
            echo form_open($path_to, $attributes, $hidden); ?>
        <!--<label for="to member">To</label><?= $reply_to ?>-->
        <table class="new_msg_tbl">
        <!--<tr>
            <td><?= form_label('To'); ?></td>
            <td></td>
        </tr> -->
        
        <tr>
            <td><?= form_label('Subject'); ?></td>
            <td>
                <input type="text" name="mssg_subject" placeholder="Subject" value="RE: <?= $mssg_sbjt ?>"/>
            </td>
        </tr>
        <tr>
            <td><?= form_label('Message'); ?></td>
            <td>
                <textarea name="mssg_text" class="w250 h50" ></textarea>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <?= form_submit(array('name'=>'submit', 'value'=>'Reply', 'id'=>"btn_submit")); ?>
                <?= anchor(site_url().'message/box/inbox', 'Cancel', 'class="btn_gnrl orange lowpad"'); ?>
            </td>
        </tr>
        </table>
        <?= form_close(); ?>

        
    </div>      

    <div class="clear"></div>
</div>      
<br class="clear"/>