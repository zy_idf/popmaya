<div class="head-title">
  <span class="mssg_ttl">
    <?php if($this->uri->segment(4)!='trash'){echo 'Message'; } ?>
    <?= $msg_title; ?> <em><?= $total_rows; ?></em>
  </span>
</div>
      
      <div id="track_list">
          <div class="mssg_control">
          <?php 
            $actv1=$actv2=$actv3=$actv4='';
            switch($this->uri->segment(4)){
                case 'new' : $actv1=' active_green'; break;
                case 'inbox' : $actv2=' active_green'; break;
                case 'sent' : $actv3=' active_green'; break;
                case 'trash' : $actv4=' active_green'; break;
            }
        
            echo anchor(site_url().'member/message/box/new', 'Send New Message', 'class="btn_gnrl orange'.$actv1.' mr"');
            echo '<span class="pad_ctr">&nbsp;</span>';
            echo anchor(site_url().'member/message/box/inbox', 'Inbox', 'class="btn_gnrl orange'.$actv2.' mr"');
            echo anchor(site_url().'member/message/box/sent', 'Sent Item', 'class="btn_gnrl orange'.$actv3.' mr"');
            echo anchor(site_url().'member/message/box/trash', 'Trash', 'class="btn_gnrl orange'.$actv4.'"');
        ?>
          </div>
          <?php if($this->uri->segment(4)!='trash'){ ?>
            <div class="sort-form">
              <label for="category-music">Inbox</label>
              <select id="category-music">
                  <option>All</option>
                  <option>Deleted</option>
              </select>
            </div>
          <?php }else{ 
          echo '
            <ul class="panel_trash">
              <li><a href="#" title="Restore deleted messages" class="link_restore" >Restore</a></li>
              <li ><a href="#" title="Delte messages" class="link_delete" >Delete</a></li>
              <li><a href="#" title="Empty trash" class="link_empty" >Empty</a></li>
            </ul>';
                } 
           ?>
          
          <table class="mssg_list">
            <tr class="first_line">
              <th><input type="checkbox" name="select_all" value="" id="select_all"></th>
              <th class="sel_all">Select All</th>
              <th><span class="mssg_main_action"><?= anchor('#', 'Mark as Unread')?>
              <?= anchor('#', 'Mark as Read')?>
              <?= anchor('#', 'Mark as Trash')?></span></th>
            </tr>
            <?php
              if(count($list_messages)==0){
                echo '<tr><td colspan="4"><h2>No message</h2></td></tr>';
              }
              foreach($list_messages as $item) {  // tinggal ganti jadi foreach
            ?>
                 <tr>
                    <td><input type="checkbox" name="cb_item_id" value="" class="cb_item_id"></td>
                    <td class="sender_img"><?= image_asset('general/profile_th.png', '', array('alt'=>$item->up_name)); ?></td>
                    <td>
                      <div class="time_sent"><?= $item->timestamp?><!-- 2 jam yang lalu -->
                      <br />
                      <?= anchor(site_url().'member/message/delete_item/'.$item->ID, 'Delete', 'class="conf_del" title="Hapus item ini?"'); ?>
                      </div>
                      <?= anchor(site_url().'member/profile/'.$item->up_uid, $item->up_name)?><br />
                      <?php
                        $item_subj='Untitled';
                        if(strlen($item->subject)>1){
                          $item_subj=$item->subject;
                        }
                        echo anchor('member/message/box/read/'.$item->message_contentID, $item_subj, 'class="mssg_more"'); ?>
                    </td>
                  </tr>
            <?php
              }
            ?>
          </table>
          
          <div class="clear"></div>
          <?= $pagination ?>
          <!--
          Showing 1 of 4 Page.
          <ul class="pagination_r">
            <li><?= anchor('#', '1'); ?></li>
            <li><?= anchor('#', '2'); ?></li>
            <li><?= anchor('#', '3'); ?></li>
          </ul>
          -->
          <div class="clear"></div>
      </div>