<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Application Photo List</title>
<link rel="stylesheet" type="text/css" media="all" href="<?= site_url(); ?>assets/css/reset.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?= site_url(); ?>assets/css/text.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?= site_url(); ?>assets/css/940_16_0_0.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?= site_url(); ?>assets/css/style.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?= site_url(); ?>assets/css/sliderStyle.css" />
<script type="text/javascript" src="<?= site_url(); ?>assets/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="<?= site_url(); ?>assets/js/custom.js"></script>
<script type="text/javascript" src="<?= site_url(); ?>js/jquery.nivo.slider.pack.js"></script>
<!--script type="text/javascript">
	$(document).ready(function() {
		$("#tab ul li:first a").removeClass("active");
	}
</script-->
</head>
<body>
    <div id="header">
        <div class="container_16 main-header">
            <div class="grid_3 logo"><h1><a href="#"><img src="images/pop_maya_logo.png" alt="pop maya"/></a></h1></div>
            <div class="grid_8" id="nav">
                <ul>
                    <li><a href="#">Artist</a></li>
                    <li><a href="#">Band</a></li>
                    <li><a href="#" class="active">Fans</a></li>
                    <li><a href="#">Songs</a></li>
                </ul>                
                <form action="#" method="post" id="form">
                    <input type="text" name="csearch" placeholder="Search" class="text-input" title="search"/><input type="image" src="images/search_ico.png" alt="Search" class="submit" />
                </form>     
            </div>
            <div class="grid_4 profile-menu">
                <div class="profile-right">
                    <div class="profile-dropmenu"><a href="#">Alam Mvalswe</a>
                        <ul>
                            <li><a href="#">Profile</a></li>
                            <li><a href="#">Setting</a></li>
                            <li><a href="#">Logout</a></li>
                        </ul>
                    </div><span>&nbsp;|&nbsp;</span><span><a href="#"><img src="images/star_ico.png" alt="favorite"/></a></span>
                        <span class="noti_container">
                            <a href="#"><img src="images/message_ico.png" alt="notif" /></a>
                            <span class="noti_bubble">10</span>
                        </span>   
                    <span><a href="#"><img src="images/circle_ico.png" alt="refresh"/></a></span>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
<div class="container_16" id="wrapper">  
  <div class="grid_3" id="left-column">
      <div id="sidebar">
          <div class="profile-picture">
              <img src="images/erk_pict.jpg" alt="irawan"/>
          </div>
          <div class="profiler">
              <h2>Efek Rumah Kaca</h2>
              <span>Group/Duo</span>
              <span>INDONESIA</span>
              <form method="post" action="#">
                <input type="submit" value="Become Fans" class="submit" />
              </form>
          </div>  
          <div class="profile-link">
              <ul>
                  <li><a href="#" class="info" title="Info">Info</a></li>
                  <li><a href="#" class="status" title="Status">Status</a></li>
                  <li><a href="#" class="voice" title="Voice">Voice</a></li>
                  <li><a href="#" class="photo active" title="Photo">Photo</a></li>
                  <li><a href="#" class="video" title="Video">Video</a></li>
                  <li><a href="#" class="note" title="Note">Note</a></li>
              </ul>
          </div>
          <div class="list-left">
            <div  id="album-track">
              <h3>Album List</h3>
              <form method="post" action="#">
                <input type="submit" value="Upload" class="upload" />
              </form>
              <div class="divider">&nbsp;</div>
              <a href="#"><img src="images/thumb_b_list1.jpg" alt="ERK" class="thumb-list"/></a>
              <h4><a href="#">Efek Rumah Kaca Jendela</a></h4>
              <a href="#" class="edit">(edit album)</a>
              <div class="clear"></div>
              <ul>
                  <li>
                      <span>Jalang</span>
                      <div class="button-share"><a href="#" title="Play"><img src="images/play_ico.png" alt="play"/></a><a href="#" title="Download"><img src="images/download_ico.png" alt="download"/></a><a href="#" title="Share"><img src="images/share_ico.png" alt="share"/></a><a href="#" class="last" title="Add"><img src="images/add_ico.png" alt="add"/></a></div>
                  </li>
                  <li>
                      <span>Jatuh Cinta itu</span>
                      <div class="button-share"><a href="#" title="Play"><img src="images/play_ico.png" alt="play"/></a><a href="#" title="Download"><img src="images/download_ico.png" alt="download"/></a><a href="#" title="Share"><img src="images/share_ico.png" alt="share"/></a><a href="#" class="last" title="Add"><img src="images/add_ico.png" alt="add"/></a></div>
                  </li>
                  <li>
                      <span>Belanja terus sampa..</span>
                      <div class="button-share"><a href="#" title="Play"><img src="images/play_ico.png" alt="play"/></a><a href="#" title="Download"><img src="images/download_ico.png" alt="download"/></a><a href="#" title="Share"><img src="images/share_ico.png" alt="share"/></a><a href="#" class="last" title="Add"><img src="images/add_ico.png" alt="add"/></a></div>
                  </li>
                  <li>
                      <span>Desember</span>
                      <div class="button-share"><a href="#" title="Play"><img src="images/play_ico.png" alt="play"/></a><a href="#" title="Download"><img src="images/download_ico.png" alt="download"/></a><a href="#" title="Share"><img src="images/share_ico.png" alt="share"/></a><a href="#" class="last" title="Add"><img src="images/add_ico.png" alt="add"/></a></div>
                  </li>

              </ul>
              <span><a href="#" title="More song" class="more">More songs from this album</a></span>
              <div class="clear"></div>
            </div>
          </div>          
      </div>
  </div>
  <!-- end .grid_3 -->
  <div class="grid_8" id="middle-column">
    <div class="head-title">
              <span><strong>Photo Competition</strong></span>
      </div>
      <div id="tab">
          <div class="banner-mid"><img src="images/banner_competition.jpg" alt="Photo Competition" title="Photo Competition"/></div>
      </div>
      <div id="preview-container">
          <a href="#" title="close" class="close">close</a>
          <div class="slide">
              <div class="bt-prev">
                  <a href="#" title="previous"></a>
              </div>
              <div class="photo">
                  <img src="" />Photo Preview
              </div>
              <div class="bt-next">
                  <a href="#" title="next"></a>
              </div>
          </div>
          <div class="comment">              
              <ul>
                  <li class="photo-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
                  <li class="photo-bt bt">
                      <a href="#" class="ico like" title="Like this">123</a>
                      <a href="#" class="ico comment" title="View Comments">123</a>
                      <a href="#" class="ico flag" title="Flag this Photo">Flag</a>
                      <a href="#" class="ico flag" title="Share this Photo">Share</a>
                      <div class="photo-share">
                        <div class="url">
                            <form>
                                <input type="text" value="http://popmaya.com/abcd123" class="text-input"/>
                                <input type="button" value="Copy" class="button-copy" />
                            </form>
                        </div>
                        <div class="button">
                            <a href="#" class="ico gplus" title="Share to Google Plus">&nbsp;</a>
                            <div class="ico gplus2">123</div>
                            <a href="#" class="ico twit" title="Share to Twitter">&nbsp;</a>
                            <a href="#" class="ico fb" title="Share to Facebook">&nbsp;</a>
                            <div class="ico twit2">123</div>
                        </div>  
                      </div>
                  </li>                  
              </ul>              
          </div>
          <div class="comment">
              <ul>
                  <li class="thumb"><a href="#"><img src="images/thumb_song_list1.jpg" class="thumb-list" /></a></li>
                  <li class="name"><a href="#">Efek Rumah Kaca</a></li>
                  <li class="desc">Single terbaru ERK, tribute untuk pejuang yang hilang pada mei 98, Cekidot! Single terbaru ERK, tribute untuk pejuang yang hilang pada mei 98, Cekidot!</li>                  
                  <li class="status">5 hours ago</li>
              </ul>
          </div>                  
          <div class="comment">
              <ul>
                  <li class="thumb"><a href="#"><img src="images/thumb_song_list1.jpg" class="thumb-list" /></a></li>
                  <li class="name"><a href="#">Efek Rumah Kaca</a></li>
                  <li class="desc">Single terbaru ERK, tribute untuk pejuang yang hilang pada mei 98, Cekidot! Single terbaru ERK, tribute untuk pejuang yang hilang pada mei 98, Cekidot!</li>
                  <li class="status">5 hours ago</li>
              </ul>
          </div>
          <div class="comment">
              <ul>
                  <li class="thumb"><a href="#"><img src="images/adam_thumb.jpg" class="thumb-list" /></a></li>
                  <li>
                      <form method="post" action="#">
                          <textarea>Write a comment</textarea><br />
                          <input type="submit" title="Submit" value="Submit" class="submit" />
                      </form>
                  </li>
              </ul>
          </div>
      </div>
      <div id="load_tab">
          <div class="head-title">
              <span><strong>Photo List</strong></span>
          </div>
          <ul class="photos">
              <li class="item">
                  <div class="thumb"><img src="images/thumb1.jpg" class="thumb-list" /></div>
                  <div class="author">by : <a href="#">Albert Michael</a></div>
                  <div class="title">My Self</div>
                  <div class="desc">
                      Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                  </div>
                  <span><a href="#" title="More" class="more">More</a></span>
                  <a href="#" class="ico like" title="Like this">123</a>
              </li>
              <li class="item">
                  <div class="thumb"><img src="images/thumb1.jpg" class="thumb-list" /></div>
                  <div class="author">by : <a href="#">Albert Michael</a></div>
                  <div class="title">My Self</div>
                  <div class="desc">
                      Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                  </div>
                  <span><a href="#" title="More" class="more">More</a></span>
                  <a href="#" class="ico like" title="Like this">123</a>
              </li>
          </ul>
          <div class="pagination">
              <a href="#" class="first">Previous</a><a href="#">1</a><a href="#">2</a><a href="#">3</a><a href="#">4</a><a href="#">5</a><a href="#">6</a><a href="#">7</a><a href="#" class="last">Next</a>
          </div>
        <div class="clear"></div>
      </div>      
  </div>
  <div class="grid_4" id="right-column">      
      <div class="sidebar-right">
          <div class="friend-page">
              <span class="pron">ADMIN TOOLS</span>
              <ul class="admin-menu">
                  <li>Use popmaya as</li>
                  <li class="sub"><a href="#">Efek Rumah Kaca</a></li>
                  <li><a href="#">Edit Profile</a></li>
                  <li><a href="#">Inbox (0)</a></li>
                  <li><a href="#">Upload Album/Song</a></li>
                  <li><a href="#">Applications</a></li>
                  <li><a href="#">Mobile Premium</a></li>
              </ul>              
          </div>
      </div>
      <div class="divider">&nbsp;</div>
      <div class="sidebar-right">
          <div class="friend-page">
              <span class="pron">FANS (345)<a href="#" class="more" title="View all">View all</a></span>
              <ul class="fans">
                  <li>
                      <a href="#"><img src="images/pham_thumb.jpg" alt="Phamella" title="Phamella" class="thumb-list"/></a>
                  </li>
                  <li>
                      <a href="#"><img src="images/des_thumb.jpg" alt="Dedes" title="Dedes" class="thumb-list"/></a>
                  </li>
                  <li>
                      <a href="#"><img src="images/rian_thumb.jpg" alt="Rian" title="Rian" class="thumb-list"/></a>
                  </li>
                  <li>
                      <a href="#"><img src="images/pham_thumb.jpg" alt="Phamella" title="Phamella" class="thumb-list"/></a>
                  </li>
                  <li>
                      <a href="#"><img src="images/des_thumb.jpg" alt="Dedes" title="Dedes" class="thumb-list"/></a>
                  </li>
                  <li>
                      <a href="#"><img src="images/rian_thumb.jpg" alt="Rian" title="Rian" class="thumb-list"/></a>
                  </li>
              </ul>
          </div>          
      </div>
      <div class="clear"></div>
      <div class="divider">&nbsp;</div>
  </div>
  <!-- end .grid_13 -->
  
    <!-- end .grid_16 -->
    <div class="clear">&nbsp;</div>
</div>
    <script type="text/javascript">
        $(window).load(function() {
                    $('#slider').nivoSlider({speed:5000});
                });
                
     </script>
<!-- end .container_16 -->
</body></html>