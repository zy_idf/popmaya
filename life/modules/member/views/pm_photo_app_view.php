<div class="head-title">
              <span><strong>Photo Competition</strong></span>
      </div>
      <div id="tab">
          <div class="banner-mid">
          <?= image_asset('banner_competition.jpg', '', array('alt'=>'Photo Competition')); ?>
          </div>
      </div>
      <div id="preview-container">
          <a href="#" title="close" class="close">close</a>
          <div class="slide">
              <div class="bt-prev">
                  <a href="#" title="previous"></a>
              </div>
              <div class="photo">
                  <img src="" />Photo Preview
              </div>
              <div class="bt-next">
                  <a href="#" title="next"></a>
              </div>
          </div>
          <div class="comment">              
              <ul>
                  <li class="photo-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
                  <li class="photo-bt bt">
                      <a href="#" class="ico like" title="Like this">123</a>
                      <a href="#" class="ico comment" title="View Comments">123</a>
                      <a href="#" class="ico flag" title="Flag this Photo">Flag</a>
                      <a href="#" class="ico flag" title="Share this Photo">Share</a>
                      <div class="photo-share">
                        <div class="url">
                            <form>
                                <input type="text" value="http://popmaya.com/abcd123" class="text-input"/>
                                <input type="button" value="Copy" class="button-copy" />
                            </form>
                        </div>
                        <div class="button">
                            <a href="#" class="ico gplus" title="Share to Google Plus">&nbsp;</a>
                            <div class="ico gplus2">123</div>
                            <a href="#" class="ico twit" title="Share to Twitter">&nbsp;</a>
                            <a href="#" class="ico fb" title="Share to Facebook">&nbsp;</a>
                            <div class="ico twit2">123</div>
                        </div>  
                      </div>
                  </li>                  
              </ul>              
          </div>
          <div class="comment">
              <ul>
                  <li class="thumb"><a href="#"><?= image_asset('thumb_song_list1.jpg', '', array('class'=>'thumb-list')); ?></a></li>
                  <li class="name"><a href="#">Efek Rumah Kaca</a></li>
                  <li class="desc">Single terbaru ERK, tribute untuk pejuang yang hilang pada mei 98, Cekidot! Single terbaru ERK, tribute untuk pejuang yang hilang pada mei 98, Cekidot!</li>                  
                  <li class="status">5 hours ago</li>
              </ul>
          </div>                  
          <div class="comment">
              <ul>
                  <li class="thumb"><a href="#">
                    <?= image_asset('thumb_song_list1.jpg', '', array('class'=>'thumb-list')); ?>
                    </a></li>
                  <li class="name"><a href="#">Efek Rumah Kaca</a></li>
                  <li class="desc">Single terbaru ERK, tribute untuk pejuang yang hilang pada mei 98, Cekidot! Single terbaru ERK, tribute untuk pejuang yang hilang pada mei 98, Cekidot!</li>
                  <li class="status">5 hours ago</li>
              </ul>
          </div>
          <div class="comment">
              <ul>
                  <li class="thumb"><a href="#">
                  <?= image_asset('adam_thumb.jpg', '', array('class'=>'thumb-list')); ?>
                  </a></li>
                  <li>
                      <form method="post" action="#">
                          <textarea>Write a comment</textarea><br />
                          <input type="submit" title="Submit" value="Submit" class="submit" />
                      </form>
                  </li>
              </ul>
          </div>
      </div>
      <div id="load_tab">
          <div class="head-title">
              <span><strong>Photo List</strong></span>
          </div>
          <ul class="photos">
              <li class="item">
                  <div class="thumb">
                  <?= image_asset('thumb1.jpg', '', array('class'=>'thumb-list')); ?>
                  </div>
                  <div class="author">by : <a href="#">Albert Michael</a></div>
                  <div class="title">My Self</div>
                  <div class="desc">
                      Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                  </div>
                  <span><a href="#" title="More" class="more">More</a></span>
                  <a href="#" class="ico like" title="Like this">123</a>
              </li>
              <li class="item">
                  <div class="thumb">
                  <?= image_asset('thumb1.jpg', '', array('class'=>'thumb-list')); ?>
                  </div>
                  <div class="author">by : <a href="#">Albert Michael</a></div>
                  <div class="title">My Self</div>
                  <div class="desc">
                      Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                  </div>
                  <span><a href="#" title="More" class="more">More</a></span>
                  <a href="#" class="ico like" title="Like this">123</a>
              </li>
          </ul>
          <div class="pagination">
              <a href="#" class="first">Previous</a><a href="#">1</a><a href="#">2</a><a href="#">3</a><a href="#">4</a><a href="#">5</a><a href="#">6</a><a href="#">7</a><a href="#" class="last">Next</a>
          </div>
        <div class="clear"></div>
      </div>      