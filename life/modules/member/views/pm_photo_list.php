<div class="share temp_share" style="display: block;">
	
	<img class="upload_photo" onclick="js:showUploadPhoto()" src="<?= image_asset_url('general/upload_photo.png'); ?>" /> &nbsp;
	<img class="create_album" onclick="js:showCreateAlbum()" src="<?= image_asset_url('general/create_album.png'); ?>" />
	<div class="share_photo_box">
	</div>
	
    <div class="clear"></div>
</div>
<div id="sharepost" class="small biru" style="margin:10px;">
<a href="#" class="hover_share" style='font-size:14px;'>Share</a>
<div class="hover_content">
	<!-- <?= image_asset('hover_content.jpg'); ?> -->
	<div class="url_shorten"><span class="url_val">Url shortener here</span><a href="#" class="copy_shorten">Copy</a></div>
	<div class="share_box">
		<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
			<a class="addthis_button_preferred_1"></a>
			<a class="addthis_button_preferred_2"></a>
			<a class="addthis_button_preferred_3"></a>
		</div>
	</div>
</div>
</div>
<hr>
<?= anchor(site_url()."member/photo", 'Back', 'class="back_green"'); ?>

<h3 class="ttl_photo_detail">Judul Album Foto (click to set as primary)</h3>
<ul id="photo-list">
<?php foreach($user_photo_album as $photo){ 
?>
	<li class="thumb">
		<a href="<?= site_url()."member/photo/change_avatar/".$photo->ID ?>"><?= image_asset("album/".$photo->image, '', array('alt'=>'')); ?></a>
		
		<!-- nisa 26 des -->

		<!-- di sini bagian pop up -->
		<div class="zyBox">
			<a href="#" class="close_zyBox">(X) Close</a>
			<span class="ijoText"><h6>MY PHOTO</h6></span>
			<?= image_asset("album/".$photo->image); ?>
			<?php $attr=array('class'=>'form_photo_comment'); ?>
			<?= form_open('member/photo/photo_comment_process', $attr); ?>
				<?= form_input('comment'); ?>
				<?= form_hidden('photo_id', $photo->ID); ?>
				<?= form_submit('submit', 'Comment'); ?>
			<?= form_close(); ?>
			
			<?php if($photo_comments) { ?>
				<ul class="list_photo_comment">
				<?php foreach($photo_comments as $c) { ?>
					<?php if($c->photoID == $photo->ID) { ?>
						<li><?= $c->up_name." - ".$c->comment; ?></li>
					<?php } ?>
				<?php } ?>
				</ul>
			<?php } ?>			
			
			<?php if($photo->like >= 1) { ?>
				<?= $photo->like; ?> member like this - 			
			<?php } ?>	
			
			<?php if(!$this->m_member->is_like_photo($this->session->userdata('user_id'), $photo->ID)) { ?>
				<?= anchor('member/photo/photo_like_process/'.$photo->ID, 'Like'); ?>
			<?php } else { ?>
				You like this
			<?php } ?>
			
		</div>
		
		<a href="#" class="preview_image">Preview</a>
		<!-- endPopup -->
		
		<?php if($photo->memberID == $this->session->userdata['user_id']) { ?>
			<?= anchor('member/photo/photo_delete_process/'.$photo->ID, 'Delete'); ?>
		<?php } ?>
		
		<!-- end nisa 26 des -->
		
		<div style="clear:both;"></div>
	</li>
<?php
} ?>
</ul>