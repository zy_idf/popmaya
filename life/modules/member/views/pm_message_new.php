<div class="head-title">
  <span class="mssg_ttl">Send New Message</span>
</div>
      
<div id="track_list">
    <div class="mssg_control">
        <?php 
            $actv1=$actv2=$actv3=$actv4='';
            switch($this->uri->segment(4)){
                case 'new' : $actv1=' active_green'; break;
                case 'inbox' : $actv2=' active_green'; break;
                case 'sent' : $actv3=' active_green'; break;
                case 'trash' : $actv4=' active_green'; break;
            }
        
            echo anchor(site_url().'member/message/box/new', 'Send New Message', 'class="btn_gnrl orange'.$actv1.' mr"');
            echo '<span class="pad_ctr">&nbsp;</span>';
            echo anchor(site_url().'member/message/box/inbox', 'Inbox', 'class="btn_gnrl orange'.$actv2.' mr"');
            echo anchor(site_url().'member/message/box/sent', 'Sent Item', 'class="btn_gnrl orange'.$actv3.' mr"');
            echo anchor(site_url().'member/message/box/trash', 'Trash', 'class="btn_gnrl orange'.$actv4.'"');
        ?>
    </div>
    <div class="mssg_control">
    <br /><br />
        <?php
            $path_to=site_url().'member/message/send';
            $attributes=array('id'=>'new_message');
            $hidden='';
            echo form_open($path_to, $attributes, $hidden);
        ?>
        <table class="new_msg_tbl">
        <tr>
            <td><?= form_label('To'); ?></td>
            <td>
                <select name="friend_id">
            <?php   foreach ($list_to as $item) { 
                        if($item->f_friendid!=$this->session->userdata('user_id')){
                            echo '<option value="'.$item->f_friendid.'">'.$item->up_name.'</option>';    
                        }
                    } 
            ?>
                </select>
            </td>
        </tr>
        <tr>
            <td><?= form_label('Subject'); ?></td>
            <td>
                <input type="text" class="w250" name="mssg_subject" placeholder="Subject" />
            </td>
        </tr>
        <tr>
            <td><?= form_label('Message'); ?></td>
            <td>
                <textarea name="mssg_text" class="w250 h50" ></textarea>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <?= form_submit(array('name'=>'submit', 'value'=>'Send Message', 'id'=>"btn_submit")); ?>
                <?= anchor(site_url().'message/box/inbox', 'Cancel', 'class="btn_gnrl orange lowpad"'); ?>
            </td>
        </tr>
        
        
        
        </table>
        <?= form_close(); ?>
    </div>      

    <div class="clear"></div>
</div>      