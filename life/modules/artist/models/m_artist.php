<?php
class M_Artist extends CI_Model 
{
	function getUserProfile($id)
	{
		$query=$this->db->select('tr_user_profile.*, tr_user_login.ul_email')
				->from('tr_user_profile')
				->join('tr_user_login', 'ul_id=up_uid')
				->where('up_uid', $id)
				->get();
		return $query->row();
	}
	
	/* Nisa. 21 Des 2011. Mengambil list artist yang dikelola oleh seorang member */
	function get_user_artist($user_id)
	{
		$query=$this->db->select('tr_member_artist.ID, tr_member_artist.artist_name')
			->from('tr_member_artist')
			->where('tr_member_artist.memberID', $user_id)
			->get();
		return $query->result();
	}
	
	
	//Author		: Firman
	//Date			: 2011 Dec 18
	//Desc			:get image ads
	function get_image_ads()
	{
		$query = $this->db->get_where('tr_ads', array('position' => 'ads'));
			$this->db->order_by('RAND()','asc');
			$this->db->limit(1);
		return $query->result();
	}
	
	//Author		: Adi 
	//Date			: 2011 Dec 19
	//Desc			: get artist data
	function getArtistProfile($id)
	{
		$query=$this->db->select('tr_member_artist.*')
				->from('tr_member_artist')
				->where('ID', $id)
				->get();
		return $query->row();
	}
	
	//Author		: Adi 
	//Date			: 2011 Dec 19
	//Desc			: get genre name
	function get_genre_name($id)
	{
		$query=$this->db->get_where('tr_genre', array('g_id'=>$id));
		$row=$query->row();
		if($row) return $row->g_name;
		else return "";
	}
	
	//Author		: Adi 
	//Date			: 2011 Dec 20
	//Desc			: save data artist
	function save_artist($data){
		$data_insert = array(
				'artist_name' => $data['a_name'],
				'title' => '',
				'memberID' => $data['user_id'],
				'pageID' => 0,
				'type' => $data['a_type'],
				'genre' => '',
				'status' => 0,
				'post' => 0,
				'ref' => ''
		);
		$this->db->insert('tr_member_artist',$data_insert);
		return $this->db->insert_id();
	}
	
	//Author		: Adi 
	//Date			: 2011 Dec 21
	//Desc			: save album song data
	function save_album_song($album){
		$this->db->insert('tr_album_song', $album);
	}
	
	//Author		: Adi 
	//Date			: 2011 Dec 22
	//Desc			: get album song
	function get_album_song($id){
		$query = $this->db->select('*')
			->from("tr_album_song")
			->where("as_id",$id)
			->get();
		return $query->row();
	}
	
	//Author		: Adi 
	//Date			: 2011 Dec 22
	//Desc			: get artist id from album song
	function get_artist_id($album_id){
		$query=$this->db->get_where('tr_album_song', array('as_id'=>$album_id));
		$row = $query->row();
		if($row) return $row->memberID;
		else return '';
	}
	
	//Author		: Adi 
	//Date			: 2011 Dec 22
	//Desc			: get album data
	function get_count_album_song($album_id)
	{
		$count=$this->db->select('*')
			->from('tr_song')
			->where('albumID', $album_id)
			->count_all_results();
		
		return $count;
	}
	
	//Author		: Adi 
	//Date			: 2011 Dec 22 (updated 7 jan 2012)
	//Desc			: get album data by artist
	function get_album_song_by_artist($artist_id, $limit='')
	{
		if($limit){
			$query=$this->db->select('tr_album_song.*')
					->from('tr_album_song')
					->where('memberID', $artist_id)
					->limit($limit)
					->get();
		}else{
				$query=$this->db->select('tr_album_song.*')
					->from('tr_album_song')
					->where('memberID', $artist_id)
					->get();
		}
		return $query->result();
	}
	
	//Author		: Adi 
	//Date			: 2011 Dec 23
	//Desc			: get album data
	function updateAlbum($album_id, $data)
	{
		$data_insert = array(
			'title' => $data['album_name'],
			'image' => $data['cover_album']
		);
		//$this->db->where('as_id', $album_id);
		$this->db->update('tr_album_song', $data_insert, array('as_id'=>$album_id)); 
	}
	
	//Author		: Adi 
	//Date			: 2011 Dec 23 (updated 7 jan 2012)
	//Desc			: get song by album
	function get_song_by_album($album_id, $limit='')
	{
		if($limit){
			$query = $this->db->select('*')
				->from("tr_song")
				->where("albumID",$album_id)
				->limit($limit)
				->get();
		}else{
			$query = $this->db->select('*')
				->from("tr_song")
				->where("albumID",$album_id)
				->get();
		}
		return $query->result();
	}
	
	//Author		: Adi 
	//Date			: 2011 Dec 23
	//Desc			: get song by album
	function update_song($s_id,$data)
	{
		$data_insert = array(
			'title' => $data['title'],
			'isprevonly' => $data['isprevonly'],
			'isdownload' => $data['isdownload'],
			'price' => $data['price']
		);
		$this->db->where('s_id', $s_id);
		$this->db->update('tr_song', $data_insert); 
	}
	//Author		: Adi 
	//Date			: 2011 Dec 26
	//Desc			: get song by artist
	function get_song_by_artist($artist_id,$limit='')
	{
		if($limit){
			$query = $this->db->select('*')
				->from("tr_song")
				->where("artist_id",$artist_id)
				->limit($limit)
				->get();
		}else{
			$query = $this->db->select('*')
				->from("tr_song")
				->where("artist_id",$artist_id)
				->get();
		}
		return $query->result();
	}
	
	//Author		: Firman (edit by adi 2012 jan 11)
	//Date			: 2011 Dec 27
	//Desc			: get artist list from database
	function get_artist_list($page,$id){
			$query = $this->db->select('*')
			->from("tr_member_artist")
			->join('tr_member_fans','tr_member_artist.ID = tr_member_fans.artist_id AND member_id='.$id,'left')
			->limit(10,$page)
			->get();
			
			
		return $query->result();
	}	
	
	//Author		: Firman
	//Date			: 2011 Dec 18
	//Desc			: get number of artist
	function get_num_artist(){
		$query=$this->db->get('tr_member_artist');
		return $query->num_rows();
	}
	
	function become_fans($data){
		$this->db->insert('tr_member_fans', $data);
	}
	
	function unfans($artist_id,$member_id){
		$this->db->where('artist_id', $artist_id);
		$this->db->where('member_id', $member_id);
		
		$this->db->delete('tr_member_fans'); 
	}
	
	//Author 		: Adi
	//Date			: 2011 Dec 29
	//Desc			: cek artist buatan member
	function artist_member($uid,$a_id){
		$query=$this->db->get_where('tr_member_artist', array('ID'=>$a_id, 'memberID'=>$uid));
		$row = $query->row();
		if($row) return true;
		else return false;
	}
	
	/* Adi. 9 Jan 2012. get status update */
	function get_status_update_artist($artist_id, $type='')
	{
		if($type=='')
		{
			$query=$this->db->select('tr_wall_artist.*')
				->from('tr_wall_artist')
				->where('to_artistID', $artist_id)
				->order_by('created', 'desc')
				->get();
		}
		else
		{
			$query=$this->db->select('tr_wall_artist.*')
				->from('tr_wall_artist')
				->where('to_artistID', $artist_id)
				->where('type', $type)
				->order_by('created', 'desc')
				->get();			
		}
		return $query->result();
	}
	
	/* Adi 9 Jan 2012. ambil komentar2 terhadap suatu status artist */
	function get_status_comment_artist($artist_id)
	{		
		$query=$this->db->select('tr_user_profile.up_uid, tr_user_profile.up_name, tr_user_profile.up_membersince, artist_name, tr_wall_artist_comment.*')
			->from('tr_wall_artist_comment')
			->join('tr_user_profile', 'tr_user_profile.up_uid=tr_wall_artist_comment.memberID')
			->join('tr_member_artist', 'tr_wall_artist_comment.artistID=tr_member_artist.ID')
			->join('tr_wall_artist', 'tr_wall_artist.ID=tr_wall_artist_comment.wall_artistID')
			->where('tr_wall_artist.artistID', $artist_id)
			->order_by('post', 'asc')
			->get();
		return $query->result();
	}
	
	//Adi 11 Jan 2012. get notif detail
	function get_member_fans($mf_id){
		$query=$this->db->select('*')
			->from('tr_member_fans')
			->where('mf_id',$mf_id)
			->get();
		return $query->result();
	}

	/* Adi 16 Jan 2012. share status artist*/
	function share_status_artist($artist, $user_id, $to_id, $textStatus, $ip_addr)
	{
		//insert wall item
		$data = array(
		   'memberID' => $user_id,
		   'to_artistID' => $to_id,
		   'artistID' => $artist,
		   'info' => $textStatus,
		   'type' => '1',
		   'host' => $ip_addr
		);
		$this->db->insert('tr_wall_artist', $data);
		
		/*
		//insert notification
		if($to_id!='0')
		{
			$wall_id=$this->db->insert_id();
			
			$data = array(
			   'n_type' => '4', //posts on your wall, lihat dr_notification_type
			   'n_itemid' => $wall_id,
			   'n_fromuserid' => $user_id,
			   'n_foruserid' => $to_id,
			   'n_isread' => '0'
			);
			$this->db->insert('tr_notification', $data);			
		}*/
	}
	
	/* Adi 16 Jan 2012. share note artist*/
	function share_note_artist($artist, $user_id, $to_id, $title, $note, $ip_addr)
	{
		$data = array(
		   'memberID' => $user_id,
		   'artistID' => $artist,
		   'to_artistID' => $to_id,
		   'title' => $title,
		   'info' => $note,
		   'created' => now(),
		   'type' => '4',
		   'host' => $ip_addr
		);

		$this->db->insert('tr_wall_artist', $data);
	}
	
	/* Adi. 17 Jan 2012. Cek udah pernah nge like suatu wall atau belum. Kembalian boolean */
	function is_artist_like_wall_artist($user_id, $wall_id)
	{
		$query=$this->db->select('*')
			->from('tr_wall_artist_like')
			->where('l_artist_id',$user_id)
			->where('l_wall_id',$wall_id)
			->get();
		//get_where('tr_wall_artist_like', array('l_member_id'=>$user_id, 'l_wall_id'=>$wall_id));
		if($query->row())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function is_member_like_wall_artist($user_id, $wall_id)
	{
		$query=$this->db->select('*')
			->from('tr_wall_artist_like')
			->where('l_member_id',$user_id)
			->where('l_wall_id',$wall_id)
			->get();
		//get_where('tr_wall_artist_like', array('l_member_id'=>$user_id, 'l_wall_id'=>$wall_id));
		if($query->row())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/* Adi 17 Jan 2012. aksi artist klik like suatu status artist */
	function artist_like_status($wall_id, $artist_id)
	{
		//insert tr_wall_item_like
		$data = array(
			'l_artist_id' => $artist_id,
		   'l_member_id' => '0',
		   'l_wall_id' => $wall_id,
		);
		$this->db->insert('tr_wall_artist_like', $data);				
		
		//get jumlah like
		$query=$this->db->get_where('tr_wall_artist', array('ID'=>$wall_id));
		$result=$query->row();
		$like=$result->like;
		
		//update jumlah like
		$data = array(
	        'like' => $like+1
		);
		$this->db->where('ID', $wall_id);
		$this->db->update('tr_wall_artist', $data);

		/*
		//cari tau siapa yg punya wall
		$query=$this->db->get_where('tr_wall_artist', array('ID'=>$wall_id));
		$result=$query->row();		
		$user_have_wall=$result->to_artistID;
		
		//insert notification kalau yg nge like komen itu yg bikin wall
		//kalau ngomenin wall sendiri ga ada notificationnya
		if($user_have_wall!=$user_id)
		{
			//insert notification
			$data = array(
			   'n_type' => '3',
			   'n_itemid' => $wall_id,
			   'n_fromuserid' => $user_id,
			   'n_foruserid' => $user_have_wall,
			   'n_isread' => '0'
			);
			$this->db->insert('tr_notification', $data);	
		}
		*/		
	}
	
	/* Adi 17 Jan 2012. aksi member klik like suatu status artist */
	function member_like_status($wall_id, $user_id)
	{
		//insert tr_wall_item_like
		$data = array(
			'l_artist_id' => '0',
		   'l_member_id' => $user_id,
		   'l_wall_id' => $wall_id,
		);
		$this->db->insert('tr_wall_artist_like', $data);				
		
		//get jumlah like
		$query=$this->db->get_where('tr_wall_artist', array('ID'=>$wall_id));
		$result=$query->row();
		$like=$result->like;
		
		//update jumlah like
		$data = array(
	        'like' => $like+1
		);
		$this->db->where('ID', $wall_id);
		$this->db->update('tr_wall_artist', $data);

		/*
		//cari tau siapa yg punya wall
		$query=$this->db->get_where('tr_wall_artist', array('ID'=>$wall_id));
		$result=$query->row();		
		$user_have_wall=$result->to_artistID;
		
		//insert notification kalau yg nge like komen itu yg bikin wall
		//kalau ngomenin wall sendiri ga ada notificationnya
		if($user_have_wall!=$user_id)
		{
			//insert notification
			$data = array(
			   'n_type' => '3',
			   'n_itemid' => $wall_id,
			   'n_fromuserid' => $user_id,
			   'n_foruserid' => $user_have_wall,
			   'n_isread' => '0'
			);
			$this->db->insert('tr_notification', $data);	
		}
		*/		
	}
	
	// Adi 19 Jan 2012. save album photo artist
	function save_album_artist($album){
		$this->db->insert('tr_album_artist', $album);
		
		return $this->db->insert_id();
	}
	
	// Adi 19 Jan 2012. save photo artist
	function save_photo_artist($photo){
		$this->db->insert('tr_photo_artist', $photo);
		
		//get jumlah foto dalam album
		$count_post=$this->db->select('*')
			->from('tr_photo_artist')
			->where('albumID', $photo['albumID'])
			->count_all_results();		

		//update jumlah foto
		$data = array(
	        'post' => $count_post
		);
		$this->db->where('ID', $photo['albumID']);
		$this->db->update('tr_album_artist', $data);		
	}
	
	function get_album_artist($user_id){
		$query = $this->db->select('*')
			->from("tr_album_artist")
			->where("artistID",$user_id)
			->get();
		return $query->result();
	}
	
	function get_photo_album_artist($album_id){
		$query = $this->db->select('*')
			->from("tr_photo_artist")
			->where("albumID",$album_id)
			->get();
		return $query->result();
	}
	
	function get_single_photo_artist($id){
		$query = $this->db->select('*')
			->from("tr_photo_artist")
			->where("ID",$id)
			->get();
		return $query->result();
	}

	/*
	Nama: Adi
	Tanggal: 20 Jan 2012
	Deskripsi: Update artist profile
	*/
	function update_artist_profile($data, $id)
	{
		$this->db->where('ID', $id);
		$this->db->update('tr_member_artist', $data);
	}	
	
	function get_artist_fans($artist_id){
		$query=$this->db->select('*')
			->from('tr_member_fans')
			->where('artist_id',$artist_id)
			->get();
		return $query->result();
	}
	
	function save_playlist($playlist){
		$this->db->insert('tr_playlist', $playlist);
		return $this->db->insert_id();
	}
	
	function get_one_playlist($playlist_id){
		$query = $this->db->select("*")
			->from("tr_playlist")
			->join("tr_song_playlist","tr_playlist.playlist_id = tr_song_playlist.playlist_id")
			->where("tr_song_playlist.playlist_id",$playlist_id)
			->get();
		return $query->result();
	}
	
	function get_all_song(){
		$query=$this->db->get('tr_song');
		return $query->result();		
	}
	
	function get_artist_playlist($artist_id){	
		//echo $user_id;
		$query = $this->db->select("*")
			->from("tr_playlist")
			->join("tr_song_playlist","tr_playlist.playlist_id = tr_song_playlist.playlist_id")
			->where("tr_playlist.artist_id",$artist_id)
			->get();
		return $query->result();
	}
	
	function get_specified_song($song_id){
		$query=$this->db->select('*')
				->from('tr_song')
				->where('s_id', $song_id)
				->get();
		return $query->row();
	}
	
	function get_specified_artist($artist_id){
		$query=$this->db->select('*')
				->from('tr_member_artist')
				->where('ID', $artist_id)
				->get();
		return $query->row();
	}
	
}
?>