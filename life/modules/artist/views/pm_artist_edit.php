	  <?= js_asset('jquery-1.3.2.min.js'); ?>
	<?= js_asset('jquery.imgareaselect.min.js'); ?>
  <?echo 'aaa  '.$large_photo_exists;?>
    <?php if($large_photo_exists && !$thumb_photo_exists){?>
	<?= js_asset('jquery.imgpreview.js'); ?>
    <script type="text/javascript">
    // <![CDATA[
        var thumb_width    = <?php echo $thumb_width ;?> ;
        var thumb_height   = <?php echo $thumb_height ;?> ;
        var image_width    = <?php echo $img['image_width'] ;?> ;
        var image_height   = <?php echo $img['image_height'] ;?> ;

        var auto_x1 = <?php echo $img['x1'] ;?> ;
        var auto_x2 = <?php echo $img['x2'] ;?> ;
        var auto_y1 = <?php echo $img['y1'] ;?> ;
        var auto_y2 = <?php echo $img['y2'] ;?> ;
    // ]]>
    </script>
    <?}?>

<!-- all style edited by gilangaramadan 25Des11 -->
<?php if($this->session->flashdata('message')) {?>
	<?= $this->session->flashdata('message'); ?>
<?php } ?>

<div id="editForm" class="grid_8">
<?= form_open(base_url()."artist/profile_edit_process", array('id'=>'register_form', 'class'=>'niceform'));?>
    <div class="head-title">
			<h2>Basic Information</h2>
	</div>
	<fieldset style="margin-top: 10px">
		
		<div>
			<label for="page_name">Page Name:</label>
			<input type="text" name="page_name" id="page_name" size="32" maxlength="128" value="<?= $page_name; ?>"/>
		</div>
		<div>
			<label for="name">Artist Name:</label> 
			<input type="text" name="name" id="name" size="32" maxlength="128" value="<?= $name; ?>"/>
		</div>
		<div>
			<label for="gb">Group/Band Member:</label>
			<input type="text" name="gb" id="gb" size="32" maxlength="128" value="<?= $gb; ?>"/>
			<div class="clear"></div>
		</div>
		
		<div>
			<label for="birth_date">Date of Birth:</label>
			<div class="seleksi grid_1">
			<select size="1" name="birth_month" id="birth_month">
				<option value="1" <?php if($birth_month=="1") echo"selected";?>>January</option>
				<option value="2" <?php if($birth_month=="2") echo"selected";?>>February</option>
				<option value="3" <?php if($birth_month=="3") echo"selected";?>>March</option>
				<option value="4" <?php if($birth_month=="4") echo"selected";?>>April</option>
				<option value="5" <?php if($birth_month=="5") echo"selected";?>>May</option>
				<option value="6" <?php if($birth_month=="6") echo"selected";?>>June</option>
				<option value="7" <?php if($birth_month=="7") echo"selected";?>>July</option>
				<option value="8" <?php if($birth_month=="8") echo"selected";?>>August</option>
				<option value="9" <?php if($birth_month=="9") echo"selected";?>>September</option>
				<option value="10" <?php if($birth_month=="10") echo"selected";?>>October</option>
				<option value="11" <?php if($birth_month=="11") echo"selected";?>>November</option>
				<option value="12" <?php if($birth_month=="12") echo"selected";?>>December</option>
			</select>
			</div>
			<div class="seleksi grid_1" style="width: 50px;">
			<select size="1" name="birth_date" style="width: 70px;">
				<?php for($i=1 ; $i<=31 ; $i++) {
					echo "<option value='".$i."'";
						if($birth_date==$i) echo"selected";
					echo ">".$i."</option>";
				} ?>
			</select>
			</div>
			<div class="seleksi grid_1">
			<select name="birth_year">
				<?php 
					$year_now=date("Y");			  
						for($i=1980 ; $i<=$year_now ; $i++) {
						echo "<option value='".$i."'";
							if($birth_year==$i) echo"selected";
						echo ">".$i."</option>";
				} ?>
			</select>
			</div>
		</div>
	</fieldset>
		<div>
			<div class="radio" style="margin-bottom: -25px;">
				<fieldset>
				<legend for="genre"><span>Genre:</span></legend>
				
				<div class="clear"></div>
				<div class="grid_5 checkbox" style="margin-bottom: 5px;">
					<?php $i=0;
					foreach($genres as $g) {?>
					<div class="grid_2">
						<input type="checkbox" name="genre[]" value="<?= $g->g_id?>" <?php if($genre[$i]==$g->g_id) { echo "checked"; if($i<count($genre)-1) {$i++;} }?>/>
						<label style="margin-top: 7px;" for="<?= $g->g_id?>"><span><?= $g->g_name;?></span></label>
					</div>
					<?php }?>
				</div>
				</fieldset>
			</div>
		</div>
		
	
	
	<div class="head-title">
		<h2>Detail Information</h2>
	</div>
	<fieldset style="margin-top: 10px">
		<div>
			<label for="label">Record Label:</label>
			<input type="text" name="label" id="label" size="32" maxlength="128" value="<?= $label; ?>"/>
		</div>
		<div>
			<label for="manager">Manager:</label>
			<input type="text" name="manager" id="manager" size="32" maxlength="128" value="<?= $manager; ?>"/>
		</div>
		<div>
			<label for="contact">Contact Number:</label>
				<input type="text" name="contact" id="contact" size="32" maxlength="20" value="<?= $contact;?>"/>
		</div>
		<div>
			<label for="content">Content Officer:</label>
			<input type="text" name="content" id="content" size="32" maxlength="128" value="<?= $content; ?>"/>
		</div>
		<div>
			<label for="link">Other Link/Website:</label>
				<input type="text" name="link" id="link" size="32" maxlength="20" value="<?= $link;?>"/>
		</div>
		<div>
			<label for="description">Group/Band Description:</label>
			<textarea name="description" id="description" cols="38" rows="5"><?= $description?></textarea>
		</div>
	</fieldset>
		
	<div class="clear"></div>
	<fieldset>
		<div class="clear"></div>
		<div class="prefix_2">
			<div class="grid_2">
				<button class="save_change" type="submit" name="submit" value="Save Changes" />
			</div>
			<div class="grid_1"> <!-- update gilangaramadan 26Des11 -->
				<button class="cancel_change" type="submit" name="cancel" value="Cancel" />
			</div>
		</div>
	</fieldset>
    <?= form_close();?>
	
	<div class="head-title">
		<h2>My Photo</h2>
	</div>
	<fieldset style="margin-top:10px;">
    <div class="grid_8" style="margin-left: 10px; margin-bottom: 10px;">
	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse nec lorem bibendum neque dignissim tincidunt. Pellentesque a tortor vel augue fringilla luctus
	</div>	
	<div class="clear"></div>
	<div class="prefix_2 grid_2" style="margin:0 0 10px 60px;">
	<?= form_open_multipart('artist/profile_edit'); ?>    
		<div id="file_browse_wrapper">
			<input type="file" name="userfile" id="file_browse" value="upload" />
		</div>
		<button class="save_change" type="submit" name="upload" value="Save Photo" />
	<?= form_close(); ?>
	</div>
	<div class="clear"></div>
	<div class="prefix_2 grid_3" style="margin:0 0 10px 40px;">
		<span class="ijoText" style="margin-left:35px;">Thumbnail save</span>
		<?= form_open('artist/profile_edit'); ?>    
		<div class="image-selector">
			<div class="frame">
				<?if($img['file_name']){?>
					<img src="<?php echo base_url() . $destination_medium . $img['file_name'];?>" id="thumbnail" alt="Create Thumbnail" />
				<?}else{?>
					<? echo image_artist($this->session->userdata['artist_id'], array('alt'=>$artist_name)); ?>
				<?}?>
			</div>

			<div class="frame-preview">
			  <span class="ijoText" style="margin-left:55px;">Preview</span>
			  <div id="preview">
					<?if($img['file_name']){?>
						<img src="<?php echo base_url() . $destination_medium .$img['file_name'];?>" style="position: relative; " alt="Thumbnail Preview" />
					<?}else{?>
						<? echo image_artist($this->session->userdata['artist_id'], array('alt'=>$artist_name)); ?>
					<?}?>
			  </div>
			  <input type="hidden" name="x1" value="0" id="x1" />
                <input type="hidden" name="y1" value="0" id="y1" />
                <input type="hidden" name="x2" value="0" id="x2" />
                <input type="hidden" name="y2" value="0" id="y2" />
                <input type="hidden" name="file_name" value="<?php echo $img['file_name'] ;?>" />
			</div>
		
		</div>
		<div class="grid_2" style="margin-left:18px;"> <!-- update gilangaramadan 26Des11 -->
			<button class="save_change" type="submit" name="upload_thumbnail" value="Save Thumbnail" />
		</div>
		<?= form_close();?>
		</fieldset>
		<div class="clear"></div>
	</div>
	
</body>
</html>