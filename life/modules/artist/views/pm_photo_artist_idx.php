<div class="share temp_share" style="display: block;">
	<!--
	<img class="upload_photo" onclick="js:showUploadPhoto()" src="<?= image_asset_url('general/upload_photo.png'); ?>" /> &nbsp;
	<img class="create_album" onclick="js:showCreateAlbum()" src="<?= image_asset_url('general/create_album.png'); ?>" />
	<div class="share_photo_box">
	</div>-->
	         <?= image_asset('general/upload_photo.png','',array('class'=>'upload_photo','onclick'=>'js:showUploadPhoto()')).' &nbsp; '.image_asset('general/create_album.png','', array('class'=>'create_album','onclick'=>'js:showCreateAlbum()')); ?>
              <div class="share_photo_box">
                  <div class="box_create_album">
					  <?= form_open_multipart('artist/photo_artist'); ?>
						  <?= image_asset('closebutton.png','',array('class'=>'btn_close','onclick'=>'js:showCreateAlbum()'))?>
						  <h4>Create Album</h4>
						  <input type="hidden" name="createalbum" value="create album" />
						  <div class="input">
							  <div class="row">
								  <label for="title">Title</label><input class="title" type="text" name="title"/>
							  </div>
							  <div class="row">
								  <label for="desc">
									  Description
								  </label>
								  <textarea name="desc"></textarea>
							  </div>
						  </div>
						  
						  <div class="action">
							    <!--<input type="button" class="button" value="Create Now" onclick="js:createAlbum()"/>-->
								<input type="submit" class="button" value="Create Now" />
						 </div>
					  </form>
                  </div>
                  <div class="box_upload_photo">
					  <?= form_open_multipart('artist/photo_artist'); ?>
                      <?= image_asset('closebutton.png','',array('class'=>'btn_close','onclick'=>'js:showUploadPhoto()'))?>
                      <h4>Upload Photo</h4>
                      <input type="hidden" name="savephoto" value="Save Photo" />
					  <div class="input">
                          <div class="row">
                          <label for="album">Select Album</label>
                          <select class="album" name="album">
							<?php foreach($artist_album as $album) { 
									echo '<option value="'.$album->ID.'">'.$album->title.'</option>';
							}?>
                          </select>
                          </div>
                          <div class="row">
                              <label for="photo">
                                  Select Photo
                              </label>
                              <div id="photo_file">       
                                    <noscript>          
                                        <p>Please enable JavaScript to use file uploader.</p>
                                        <!-- or put a simple form for upload here -->
                                    </noscript>  
									<input type="file" name="photo_album" size="20" />									
                                </div>
                          </div>
                          <div class="row">
                              <label for="desc">
                                  Description
                              </label>
                              <textarea class="desc" name="desc"></textarea>
                          </div>
                      </div>
                      <div class="action">
                          <input type="submit" class="button" value="Upload Now"/>
                      </div>
					  </form>
                  </div>
              </div>
    <div class="clear"></div>
</div>
<div id="album-list">
<?php foreach($artist_album as $album) { 
?>
	<ul class="album">
		<li class="thumb"><a href="<?= site_url().'artist/photo_artist/album/'.$album->title.'/'.$album->ID ?>"><img src="images/<?= $album->image ?>" alt="<?= $album->title ?>"/></a></li>
		<li class="title"><a href="<?= site_url().'artist/photo_artist/album/'.$album->title.'/'.$album->ID ?>"><h2><?= $album->title ?></h2></a></li>
		<li class="resume"><?= $album->post; ?> photos</li>

		<!-- nisa 26 des -->
		<?= anchor('artist/photo_artist/album_delete_process/'.$album->ID, 'Delete Album'); ?>
	</ul>
<?php
} ?>
</div>	