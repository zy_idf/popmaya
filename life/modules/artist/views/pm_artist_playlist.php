<input type="hidden" id='active_playlist' value='0' />
<?= $sound_js ?>
<script type="text/javascript">
	function copy_playlist(playlist_id){
		//alert("Copy playlist " + playlist_id); 
		var ajaxRequest; 

		try{
			// Opera 8.0+, Firefox, Safari
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			// Internet Explorer Browsers
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e){
					// Something went wrong
					alert("Your browser broke!");
					return false;
				}
			}
		}
		
		// Create a function that will receive data sent from the server
		ajaxRequest.onreadystatechange = function(){
			if(ajaxRequest.readyState == 4){
				var div = document.getElementById("copy_link_" + playlist_id);
				div.innerHTML = " | Playlist copied";
			}
			
		}
		ajaxRequest.open("POST", "<?= site_url()  ?>member/copy_playlist/"+playlist_id, true);
		ajaxRequest.send(null);
	}
	
	function set_active_playlist(id){
		document.getElementById('active_playlist').value = id;
	}
	
</script>
<div class="create-form">
	<?	if($is_my_playlist){?>
			<h2>Create New Playlist</h2>
			<?=form_open(base_url().'artist/create_playlist/'.$this->session->userdata['artist_id']);?>				
				<input type="text" name="playlist_name" placeholder="Input playlist name" title="name" class="text-input"/>
				<input name="create_playlist" type="submit" value="Create" />
				<!--
				<a href=""javascript:void this.submit();"" ><?= image_asset('create_btn.png', '',array('alt'=>'create_playlist')); ?></a>
				-->
			</form>
	<?  }else{
			echo "<h2>".$name." Playlist</h2>";
		} ?>
	
	<?  $id=0;
		$list = 1;
		//echo count($user_playlist);
		foreach($user_playlist as $playlist){ 
			if($is_my_playlist){
				$small_link = "<a href='".site_url()."artist/edit_playlist/".$this->session->userdata['artist_id'].'/'.$playlist->playlist_id."'> | edit playlist</a><a href='".site_url()."artist/delete_playlist/".$playlist->playlist_id."'> | delete playlist</a>";
			}else{
				$small_link = "<div id='copy_link_".$playlist->playlist_id."' style=''><a style='cursor:pointer;' onclick='copy_playlist(".$playlist->playlist_id.");'> | copy playlist</a></div>";
			}
		if($id==0){
			$id = $playlist->playlist_id;
			$text_area = "<textarea id='songlist_".$playlist->playlist_id."' style='visibility:hidden'>";
			echo "";
			echo "<hr><a href='".site_url()."member/profile/playlist_detail/".$playlist->playlist_id."'><h2>Playlist ".$playlist->playlist_name.$small_link."</a></h2>";;
			$table_playlist = "<table>";
		}
		
		//print_r($playlist);
		//echo "torotet ".$playlist->song_id;
		$one_song = $this->m_artist->get_specified_song($playlist->song_id);
		//print_r($one_song);
		$one_artist = $this->m_artist->get_specified_artist($one_song->artist_id);
		$table_playlist = $table_playlist."<tr><td>".$list.".</td><td>".$one_song->title."</td><td>:".$one_artist->artist_name."</td></tr>";
		//echo $list." .".$one_song->title." : ".$one_artist->artist_name."<br>"; 
		$text_area = $text_area.site_url().'assets/media/mp3/'.$one_song->file.'
		';
		$list++;
		if($playlist->playlist_id!=$id){
			$table_playlist =  $table_playlist."</table>";
			$text_area = $text_area."</textarea>";
			echo '<div id="button_play_playlist_'.$id.'" style="padding:5px;float:left;margin-right:40px;">
				<a style="cursor:pointer;" onclick="play_song();" onmouseover="set_active_playlist('.$id.');">'.image_asset('icon/play.png', '',array('alt'=>'play')).'</a>					
				</div>
			';
			echo '<div id="button_pause_playlist_'.$id.'" style="padding:5px;float:left;margin-right:40px;display:none;">
				<a style="cursor:pointer;" onclick="pause_song();" onmouseover="set_active_playlist('.$id.');">'.image_asset('icon/pause.png', '',array('alt'=>'play')).'</a>					
				</div>
			';
			echo '<div id="button_unpause_playlist_'.$id.'" style="padding:5px;float:left;margin-right:40px;display:none">
				<a style="cursor:pointer;" onclick="unpause_song();" onmouseover="set_active_playlist('.$id.');">'.image_asset('icon/play.png', '',array('alt'=>'play')).'</a>					
				</div>
			';
			echo $table_playlist;
			echo $text_area;
			//echo "</textarea><a href=''>edit playlist</a>";
			echo "<hr><a href='".site_url()."member/profile/playlist_detail/".$playlist->playlist_id."'><h2>Playlist ".$playlist->playlist_name.$small_link."</a></h2>";;
			$table_playlist = "<table>";
			$id=$playlist->playlist_id;
			$list=1;
			$text_area = "<textarea id='songlist_".$playlist->playlist_id."' style='visibility:hidden'>";
		}
		?>
			
	<? } ?>
</div>
