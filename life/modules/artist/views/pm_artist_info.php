<h1 class="profile_name_title"><?= $name; ?></h1>
<div id="tab">
	<ul>
       	<li class="middle"><a href="#tab2">Info</a></li>
    </ul>
</div>
<!--<div id="fans-of"><label>Fans of : </label><?= $fav_artist_string; ?></div>-->
<div class="profile-title">
	<h2>Basic Info</h2>
</div>
<?php //to whomever wrote below code, please use table for tabular data. rgds, ^zy. ?>
<div class="profile-row">
	<label>Page Name</label> :
	<div class="data"><?= $page_name; ?></div>
</div>
<div class="profile-row">
	<label>Artist Name</label> :
	<div class="data"><?= $name; ?></div>
</div>
<div class="profile-row">
	<label>Group Band/Member</label> :
	<div class="data"><?= $gb; ?></div>
</div>
<div class="profile-row">
	<label>Birthday</label> :
	<div class="data"><?= date('d M Y', strtotime($birthday)); ?></div>
</div>
<div class="profile-row">
	<label>Genre</label> :
	<div class="data"><?= $genre_string; ?></div>
</div>


<div class="profile-title">
	<h2>Detail Info</h2>
</div>
<div class="profile-row">
	<label>Record Label</label> :
	<div class="data"><?= $label; ?></div>
</div>
<div class="profile-row">
	<label>Manager</label> :
	<div class="data"><?= $manager; ?></div>
</div>
<div class="profile-row">
	<label>Contact Number</label> :
	<div class="data"><?= $contact; ?></div>
</div>
<div class="profile-row">
	<label>Content Officer</label> :
	<div class="data"><?= $content; ?></div>
</div>
<div class="profile-row">
	<label>Other Link/Website</label> :
	<div class="data"><?= $link; ?></div>
</div>
<div class="profile-row">
	<label>Group/Band Description</label> :
	<div class="data"><?= $description; ?></div>
</div>