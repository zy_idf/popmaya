<script type="text/javascript">
	function add_to_playlist(song_id,playlist_id){
		//alert("masukan lagu " + song_id + " ke playlist " + playlist_id); 
		var ajaxRequest; 

		try{
			// Opera 8.0+, Firefox, Safari
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			// Internet Explorer Browsers
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e){
					// Something went wrong
					alert("Your browser broke!");
					return false;
				}
			}
		}
		
		// Create a function that will receive data sent from the server
		ajaxRequest.onreadystatechange = function(){
			if(ajaxRequest.readyState == 4){
				//alert(ajaxRequest.responseText);
				var new_inner = document.getElementById("list_song_" + song_id);
				var div = document.getElementById("my_playlist");
				div.innerHTML = div.innerHTML + "<li id='my_song_"+song_id + "_" + playlist_id + "'>" + new_inner.innerHTML
				+ '<div class="button-share">'
				+ '<a style="cursor:pointer;" onclick="delete_from__playlist('+song_id+','+playlist_id+');">Remove from playlist</a>'
				+ '</div>';
				+ "</li>";
				
				var child = document.getElementById("add_playlist_button_" + song_id);
				var parent = document.getElementById("container_button_" + song_id);
				parent.removeChild(child);
			}
			
		}
		ajaxRequest.open("POST", "<?= site_url()  ?>member/add_playlist/"+song_id+"/"+playlist_id, true);
		ajaxRequest.send(null);
	}
	
	function delete_from__playlist(song_id,playlist_id){
		//alert("hapus lagu " + song_id + " dari playlist " + playlist_id); 
		var ajaxRequest; 

		try{
			// Opera 8.0+, Firefox, Safari
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			// Internet Explorer Browsers
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e){
					// Something went wrong
					alert("Your browser broke!");
					return false;
				}
			}
		}
		
		// Create a function that will receive data sent from the server
		ajaxRequest.onreadystatechange = function(){
			if(ajaxRequest.readyState == 4){
				//alert(ajaxRequest.responseText);
				 var child = document.getElementById("my_song_" + song_id + "_" + playlist_id);
				 var parent = document.getElementById("my_playlist");
				 parent.removeChild(child);
			}
			
		}
		ajaxRequest.open("POST", "<?= site_url()  ?>member/remove_song_playlist/"+song_id+"/"+playlist_id, true);
		ajaxRequest.send(null);
	}
</script>
<div class="create-form">
	<h2>Edit playlist | <a href="<?= site_url()  ?>artist/playlist/<?= $this->uri->segment(3)?>/<?= $user_id ?>">back to my playlist</a></h2>
	
	<div id="edited_playlist" style="min-height:400px;width:50%;border-right:1px solid #ccc;float:left;margin-left:-20px;">
		<div class="list-left">
			<ul id="my_playlist">
				<? 	$song_count=0;
					foreach($one_playlist as $my_song){ 
					$song_detail = $this->m_member->get_specified_song($my_song->song_id);
					//print_r($song_detail);
					$artist_detail =  $this->m_member->get_specified_artist($song_detail->artist_id);
					//print_r($artist_detail);?>							
					<li id="my_song_<?= $my_song->song_id ?>_<?= $my_song->playlist_id ?>">
						
							<a href='#'><?= image_asset('general/layout/efek_thumb.jpg', '', array('alt'=>'efek', 'class'=>'thumb-list')); ?> </a>
							<h4><a href="#"><?= $song_detail->title ?></a></h4>
							
							<span class="title"><?= $artist_detail->artist_name ?></span>
							<br>
							<div class="button-share">
							  <a style="cursor:pointer;" onclick="delete_from__playlist(<?= $my_song->song_id ?>,<?= $my_song->playlist_id ?>);">Remove from playlist</a>
							</div>
						
					</li>
					
				<? $song_count++;
				   } ?>
			</ul>
		</div>
	</div>
	<div id="song_available" class="list-left" style="width:50%;float:left;">
		<ul>
		<?php //julian,sabtu 17 desember 2011
			foreach ($list_songs as $key => $item) {
			?>

			  <li  style="clear:right;">
				<div id="list_song_<?= $item->s_id?>">
				<a href='#'><?= image_asset('general/layout/efek_thumb.jpg', '', array('alt'=>'efek', 'class'=>'thumb-list')); ?> </a>
				<h4><a href="#"><?= $item->title ?></a></h4>
				<span class="title">Nama Artis</span>
				</div>
				
				
				<div class="button-share" id="container_button_<?= $item->s_id?>">
				  <a id="add_playlist_button_<?= $item->s_id?>" style="cursor:pointer;" onclick="add_to_playlist(<?= $item->s_id ?>,<?= $last_insert_id ?>);">Add to playlist</a>
				  
				  <?= anchor(site_url().'assets/media/mp3/'.$item->file, image_asset('play_ico.png', '', array('alt'=>'play')), array('title'=>'Play', 'class'=>'btn_play')) ?>
				  <a href="#" title="Download"><?= image_asset('download_ico.png', '','alt="download"'); ?></a>
				  
				</div>
				<div class="clear"></div>
			  </li>
			<?php
			}
		 ?>
		 </ul>
	</div>
</div>