<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Artist extends MX_Controller 
{
	var $uid;
	
   	function __construct()
	{
   		parent::__construct();
		
		//tambahan Adi - 3 Jan 2012
		$this->load->helper(array('form', 'url'));
        $this->load->library('image_moo') ;

   		$this->load->library('form_validation');
   		$this->load->model('m_artist', '', TRUE);
   		$this->load->model('member/m_member', '', TRUE);
		$this->load->model('home/m_home', '', TRUE);
		$this->load->model('fans/m_fans', '', TRUE);

		$this->uid=$this->session->userdata('user_id');

   		//$this->output->enable_profiler(TRUE);
   	}

	//------------------------------------------------------------------------------------------------------------------------frontpage
   	function index($page="")
	{	
		
		$data['foot_js']=css_asset('plugin/sliderStyle.css').js_asset('jquery.nivo.slider.pack.js');
	    $data['foot_script']='
			var sics = $(".share_icons"); sics.hide();
		    $(\'#slider\').nivoSlider({speed:5000});

		    $("#tab ul li.middle a").addClass("active").show(); 
 	        $("#tab2").show();
		    ';	      
			
			$this->config->load('my_config');
			$limit_sidebar_friend_list=$this->config->item('limit_sidebar_friend_list');
			
			if($this->uid){
				$row=$this->m_member->get_user_profile($this->uid);
				
				$row2=$this->m_artist->getArtistProfile(1);
				$data['up_uid']=$row->up_uid; // ^zy 4 jan
				$data['user_id']=$this->uid;
				$data['name']=$row->up_name;
				$data['alias']=$row->up_alias;	
				$data['unread_messages']=$this->m_member->get_count_unread_messages($this->uid);		
				
				$data['gender_code']=$row->up_gender;
				$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
				$data['city']=$row->up_city;
				$data['country']=$row->up_country;
				$data['friends']=$this->m_member->get_friends($this->uid, $limit_sidebar_friend_list);
				//load ads data
				$data['ads_data']=$this->m_artist->get_image_ads();
				
				$data2['artist_name']=$row2->artist_name;
			  $data2['title']=$row2->title;
			  $data2['genre']=$this->m_artist->get_genre_name($row2->genre);
			  $data2['artist_id']=1;
			  switch($row2->type){
				case 0 : $data2['type'] = 'Solo';
					break;
				case 1 : $data2['type'] = 'Band';
					break;
				case 2 : $data2['type'] = 'Group';
					break;
				default : $data2['type'] = 'Solo';
					break;
			}
			  
			  
			  /****================================ tambahan ===================================****/ 
				//Adi 26 Dec 2011
/*				$data['unread_messages']=$this->m_member->get_count_unread_messages($this->uid);		
				$data['count_friend_request']=$this->m_member->get_count_friend_request($this->uid);
				$data['count_notification']=$this->m_member->get_count_notification($this->uid);
*/				
				//music sidebar, suggest song drop down menu		
				$fav_genre=$this->m_member->get_fav_genre($this->uid);
				$fav_genre=explode(", ", $fav_genre);
				$genres=$this->m_member->get_genre();
				$i=0;
				$fav_genre_display='';

				if($fav_genre[0]) //have favourite genre
				{
						foreach($genres as $g)
						{
							if($g->g_id==$fav_genre[$i])
							{
								$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
								$i++;
							}
						}	
					}
					else //do not have favourite genre
					{
						foreach($genres as $g)
						{
							$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
							$i++;
						}				
					}	
					$data['suggest_song_option']=$fav_genre_display;

					$genre_id=$fav_genre_display[0]['id'];
					$limit=$this->config->item('limit_sidebar_suggest_song_list');		
					$data['song_by_genre']=$this->m_member->get_song_by_genre($genre_id, $limit);
					
					$data['recommended_track']=$this->m_home->get_recommended_track();
					$new_track_limit=$this->config->item('limit_new_track');
					$data['new_track']=$this->m_home->get_new_track($new_track_limit);
				}
				
			//Firman
				
				//Autthor	: Firman
			//Date		: 2011 Dec 15
			//Desc		: Load fans list and fans page amount	
			$data['artist_list'] = $this->m_artist->get_artist_list($page,$this->uid);
			/* pagination */ 
			$uri_segment = 3;
			if($this->uri->segment($uri_segment)==''){ $offset=0; }else{ $offset=$this->uri->segment($page/10); }
			$data['start_no']= $offset+1;

			$this->load->library('pagination');
			$config['base_url'] = site_url().'artist/index/';
			$data['jml_member']=$config['total_rows'] = $this->m_artist->get_num_artist();
			$config['per_page'] = 10;
			$config['uri_segment'] = $uri_segment;
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			/* pagination.end */
			
				//load ads data
				$data['ads_data']=$this->m_artist->get_image_ads();
				
				/*================================end tambahan ========================================*/
						

			//render view
			$this->template->write('head_title', 'Artist');
			$this->template->write('keywords', '');
	      	$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
		  	$this->template->write_view('sidebar_left', 'pm_side_artist', $data2, '');
			$this->template->write_view('middle_content', 'pm_artist_idx', $data, '');
			$this->template->write_view('sidebar_right', 'pm_artist_side_right', $data, '');
			$this->template->render();			      			

   	}

	function frontpage()
	{
		$data="";
		$data['foot_js']='';
		//render view
		$this->template->write('keyword', ' ?? ');
		$this->template->write('head_title', 'Home');
		$this->template->write_view('middle_content', 'pm_member_frontpage', $data, TRUE);
		$this->template->render();
	}
	//------------------------------------------------------------------------------------------------------------------------end frontpage	
	
	//------------------------------------------------------------------------------------------------------------------------login
   	function login()
	{
		if ($this->ion_auth->logged_in()) //already logged in so no need to access this page
		{			
			redirect("member/profile", 'refresh');
		}
		
		//validation rules
        $this->form_validation->set_rules('email', 'Email', 'trim|prep_for_form|htmlspecialchars|encode_php_tags|required|valid_email|min_length[5]|max_length[50]|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|alpha_numeric|prep_for_form|htmlspecialchars|encode_php_tags|required|min_length[5]|max_length[20]|xss_clean');
		
		//validation message
    	$this->form_validation->set_message('required', '%s is required.');
    	$this->form_validation->set_message('alpha_numeric', 'Only alphanumeric characters are allowed.');
		$this->form_validation->set_message('valid_email', 'Wrong email format.');
    	$this->form_validation->set_message('min_length[5]', 'Min. character is 5 chars.');    	
    	$this->form_validation->set_message('max_length[20]', 'Max. character is 20 chars.');
    	$this->form_validation->set_message('max_length[50]', 'Max. character is 50 chars.');
        
		//error delimiter
		$this->form_validation->set_error_delimiters('', '');
		
		if($this->form_validation->run()==TRUE) //validation success
		{
			//check to see if the user is logging in
			//check for "remember me"
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('email'), $this->input->post('password'), $remember))
			{ 	
				//if the login is successful
				//redirect them back to the home page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("member/profile", 'refresh');
			}
			else
			{ 
				//if the login was un-successful
				//redirect them back to the login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('member/frontpage', 'refresh'); 
			}			
		}
		else //validation failed
		{
			//the user is not logging in so display the login page
			//set the flash data error message if there is one			
			$this->session->set_flashdata('message', validation_errors());
			redirect('member/frontpage');
		}
	}
	
	function login_by_fb()
	{
		$fb_data = $this->session->userdata('fb_data');
		
		if((!$fb_data['uid']) or (!$fb_data['me']))
		{
			redirect('member/frontpage');
		}
		else
		{
			$data['fb_data']=$fb_data;
			
			$this->load->view('pm_member_login_facebook', $data);
		}
	}
	//------------------------------------------------------------------------------------------------------------------------end login

	//------------------------------------------------------------------------------------------------------------------------logout
	function logout()
	{
		//log the user out
		$logout = $this->ion_auth->logout();

		//redirect them back to the page they came from
		$this->session->set_flashdata('message', "Logout success");
		redirect('member/frontpage', 'refresh');
	}
	//------------------------------------------------------------------------------------------------------------------------end logout
	
	//------------------------------------------------------------------------------------------------------------------------forgot password
	function forgot()
	{
		$data="";
		$data['foot_js']='';
		//render view
		$this->template->write('keyword', ' ?? ');
		$this->template->write('head_title', 'Forgot Password');
		$this->template->write_view('middle_content', 'pm_member_forgot', $data, TRUE);
		$this->template->render();		
	}
	
	function forgot_process()
	{
		$this->form_validation->set_rules('email', 'Email', 'xss_clean|required|valid_email|min_length[5]|max_length[50]');
		
		if ($this->form_validation->run() == true)
		{
			//run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($this->input->post('email'));					
			
			if ($forgotten)  //if there were no errors
			{
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect('member/forgot_confirm', 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('member/forgot', 'refresh');
			}			
		}
		else
		{
			//no email entered
			$this->session->set_flashdata('message', validation_errors());
			redirect('member/forgot');			
		}
	}
	
	function forgot_confirm()
	{
		echo "forgot confirm";
		
		$data="";
						
		//render view
		$this->template->write('keyword', ' ?? ');
		$this->template->write('head_title', 'Forgot Password Confirm');
		$this->template->write_view('middle_content', 'pm_member_forgot_confirm', $data, TRUE);
		$this->template->render();				
	}
	
	function reset_password($code='')
	{
		if($code)
		{
			$reset = $this->ion_auth->forgotten_password_complete($code);

			if ($reset)
			{  
				//if the reset worked then send them to the login page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect('member/frontpage', 'refresh');
			}
			else
			{ 
				//if the reset didnt work then send them back to the forgot password page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('member/forgot', 'refresh');
			}
		}
		else
		{
			redirect('member/frontpage', 'refresh');
		}		
	}
	//------------------------------------------------------------------------------------------------------------------------end forgot password
   	
	//------------------------------------------------------------------------------------------------------------------------register
	function register()
	{
		//validation rules
		$this->form_validation->set_rules('fullname', 'Fullname', 'trim|required|prep_for_form|htmlspecialchars|encode_php_tags|xss_clean|min_length[5]|max_length[20]');
    	$this->form_validation->set_rules('email', 'Email', 'trim|required|prep_for_form|htmlspecialchars|encode_php_tags|valid_email|callback_new_email|min_length[5]|max_length[50]|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|alpha_numeric|required|prep_for_form|htmlspecialchars|encode_php_tags|min_length[5]|max_length[20]|xss_clean');
    	
		//validation message
    	$this->form_validation->set_message('required', '%s is required.');
    	$this->form_validation->set_message('alpha_numeric', 'Only alphanumeric characters are allowed.');
		$this->form_validation->set_message('valid_email', 'Wrong email format.');
    	$this->form_validation->set_message('min_length[5]', 'Min. character is 5 chars.');    	
    	$this->form_validation->set_message('max_length[20]', 'Max. character is 20 chars.');
    	$this->form_validation->set_message('max_length[50]', 'Max. character is 50 chars.');
		
		$this->form_validation->set_error_delimiters('','');

		
		if ($this->form_validation->run() == true)
		{
			if($this->input->post('agree')=="agreed")
			{			
				$fullname = $this->input->post('fullname');
				$email = $this->input->post('email');
				$password = $this->input->post('password');

				$additional_data = array('gender' => $this->input->post('gender'),
					'update' => $this->input->post('update')
				);
			
			
				if($this->ion_auth->register($fullname, $password, $email, $additional_data))
				{
					//check to see if we are creating the user
					$this->session->set_flashdata('message', "User Created");
					redirect('member/frontpage', 'refresh');
				}
				else
				{ 
					//display the register user form
					$this->session->set_flashdata('message', $this->ion_auth->errors());
					redirect('member/frontpage', 'refresh'); 
				}
			}
			else
			{
				$this->session->set_flashdata('message', "You must agree with the Term & Service");
				redirect('member/frontpage', 'refresh'); 				
			}
		}
		else
		{
			$this->session->set_flashdata('message', validation_errors());
			redirect('member/frontpage');			
		}
	}
	//------------------------------------------------------------------------------------------------------------------------end register

	//nama : Adi
	//tanggal : 20 Des 2011
	//deskripsi : profile artist page
	//------------------------------------------------------------------------------------------------------------------------profile
	function profile($id='',$optional='')
	{
		if (!$this->ion_auth->logged_in()) //if not logged in
		{
			//redirect them to the login page
			redirect('home', 'refresh');
		}
		else
		{
		$this->config->load('my_config');
			$limit_sidebar_friend_list=$this->config->item('limit_sidebar_friend_list');
			
			
			$data['a_name'] = $this->input->post('name');
			$data['a_category'] = $this->input->post('category');
			$data['user_id']=$this->uid;
			if(!$this->input->post('name')){
				if($id==''){
					//echo 'a';
					$row2=$this->m_artist->getArtistProfile(1);
					$data['artist_id']=1;
					$data2['artist_id']=1;
				}else{
					$row2=$this->m_artist->getArtistProfile($id);
					//echo 'b';
					//echo '--'.$id;
					$data['artist_id']=$id;
					$data2['artist_id']=$id;
				}
			}else{
				$row2=$this->m_artist->getArtistProfile($this->m_artist->save_artist($data));
			}
			
			if($this->input->post('email')){
				$msg = '<html><body><h1><a href="ideafieldproject.com/popmaya">Popmaya</a></h1></body></html>';
				$subject = "join popmaya";
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= 'From: popmaya@example.com' . "\r\n";
				mail($this->input->post('email'), $subject,$msg,$headers);
			}else{
			}
			
			//setting up viewer
			$page_req="pm_artist";
			$data['foot_js']=css_asset('add_style.css').css_asset('plugin/sliderStyle.css').js_asset('jquery.nivo.slider.pack.js').  js_asset('jwysiwyg/jquery.wysiwyg.js').js_asset('jRecorder/jRecorder.js').js_asset('fileuploader.js').js_asset('jPlayer/jquery.jplayer.min.js').js_asset('stream.js');
	      	$data['sound_js'] = js_asset('sound.js');
		$data['foot_script']='
	        var sics = $(".share_icons"); sics.hide();
	        $(\'#slider\').nivoSlider({speed:5000});

	        $("#tab ul li.first a").addClass("active").show(); 
	          	$("#tab1").show();
	        /* ^zy */
			var the_player = $("#song_player");
			/* ^zy */
			 	var the_player = $("#song_player");

			 	the_player.jPlayer({
			 	    ready: function(){
						console.log("player ready."); 	           			
			 	    },
			 	    play: function(){
			 	    	console.log("play file.");	
			 	    },
			 	    volume: "0.4",
			 	    swfPath: "'.site_url().'assets/js/jPlayer",
					cssSelectorAncestor: "#wrapper",
					supplied: "mp3",
					wmode: "window"
				});           	

				var prev_val=0;
			 	$("#wrapper .btn_play").click(function(e){
			 	    the_player.jPlayer("setMedia", {
						mp3: $(this).attr("href")
					});
					the_player.jPlayer("play");
					var nxt_el=$(this).parent().next().find(".ttl_play");
					var ctr=nxt_el.html();
					nxt_el.fadeOut(); 
					var ctr2=parseInt(ctr);
					if(prev_val==0){
						ctr2+=1;
						prev_val=1;
					}
					nxt_el.html(ctr2); nxt_el.fadeIn();	
					return false;
				});

				$(".add_to_playlist").click(function(){
					$.post("'.site_url().'track/add_to_playlist", { track_id: $(this).attr("href") }, function(data) {
   						$(".list-left p.bottom").html();
   						alert(data);
   						$(".list-left p.bottom").html("Data berhasil di tambahkan."); /* harusnya disini load content ajax utk playlist */
 					});
					return false;	
				});
				$(".commentwall").hide();
				$(".open_comment").click(toggleComment);
				function toggleComment(){
					console.log($(this).parent().next().is(":visible"));
					if($(this).parent().next().is(":visible")){
						$(this).parent().next().fadeOut();
					}else{
						$(this).parent().next().fadeIn();
					}					
					return false;
				}
				$(".hover_content").hide();
				$(".hover_share").hover(function() {
    				$(this).next().show();
  				});
  				$(".hover_share").click(function(){ return false; });
  				$(".hover_content").live("mouseleave", function(){ $(this).hide() }); 

			/* ^zy.end */
	      
	      ';	      

			//if($id=='') //my profile
			//{
				$row=$this->m_artist->getUserProfile($this->uid);
			//}
			//else //other member profile
			//{
				//$row=$this->m_artist->getUserProfile($id);
			//}
			//if($id==''){
			//	$row2=$this->m_artist->getArtistProfile(1);
			//}
			//else{
			//	$row2=$this->m_artist->getArtistProfile($this->m_artist->save_artist($data));
			//}
			
						
			//setting data
			$data2['artist_name']=$row2->artist_name;
			$data2['title']=$row2->title;
			$data2['genre']=$this->m_artist->get_genre_name($row2->genre);
			
			switch($row2->type){
				case 0 : $data2['type'] = 'Solo';
					break;
				case 1 : $data2['type'] = 'Band';
					break;
				case 2 : $data2['type'] = 'Group';
					break;
				default : $data2['type'] = 'Solo';
					break;
			}
			
			$data['banner_image'] = $this->m_member->get_image_banner_profile();
			$data['recommended_track']=$this->m_home->get_recommended_track();
			$new_track_limit=$this->config->item('limit_new_track');
			$data['new_track']=$this->m_home->get_new_track($new_track_limit);
			
			$data['name']=$row->up_name;
			$data['alias']=$row->up_alias;	
			$data['unread_messages']=$this->m_member->get_count_unread_messages($this->uid);		

			$data['gender_code']=$row->up_gender;
			$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
			$data['city']=$row->up_city;
			$data['country']=$row->up_country;
			$data['friends']=$this->m_member->get_friends($this->uid, $limit_sidebar_friend_list);
			//load ads data
			$data['ads_data']=$this->m_artist->get_image_ads();
			
			/****================================ tambahan ===================================****/ 
			//Adi 26 Dec 2011
			$data['unread_messages']=$this->m_member->get_count_unread_messages($this->uid);		
			$data['count_friend_request']=$this->m_member->get_count_friend_request($this->uid);
			$data['count_notification']=$this->m_member->get_count_notification($this->uid);
			
			
			//edit 19 jan 2012
			$tmp = $this->m_artist->get_album_song_by_artist($id,1);
			if($tmp) {
				$data['album_song'] = $tmp[0];
				if($data['album_song']){
						$data['song_list']=$this->m_artist->get_song_by_album($data['album_song']->as_id,5);
				}
			}
			else $data['album_song'] = $tmp;
			
			//music sidebar, suggest song drop down menu		
			$fav_genre=$this->m_member->get_fav_genre($this->uid);
			$fav_genre=explode(", ", $fav_genre);
			$genres=$this->m_member->get_genre();
			$i=0;
			$fav_genre_display='';

			if($fav_genre[0]) //have favourite genre
			{
				foreach($genres as $g)
				{
					if($g->g_id==$fav_genre[$i])
					{
						$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
						$i++;
					}
				}	
			}
			else //do not have favourite genre
			{
				foreach($genres as $g)
				{
					$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$i++;
				}				
			}	
			$data['suggest_song_option']=$fav_genre_display;

			$genre_id=$fav_genre_display[0]['id'];
			$limit=$this->config->item('limit_sidebar_suggest_song_list');		
			$data['song_by_genre']=$this->m_member->get_song_by_genre($genre_id, $limit);
			/*================================end tambahan ========================================*/
			
			//Adi 7 Jan 2012
			$type_text=$this->uri->segment(4);
			if($type_text=='status') $type='1';
			else if($type_text=='voice') $type='2';
			else if($type_text=='video') $type='4';
			else if($type_text=='note') $type='5';
			else $type='';
			
			$data['texts']=$this->m_artist->get_status_update_artist($id, $type);
			$data['text_comments']=$this->m_artist->get_status_comment_artist($id);
			
			$template_artist['template'] = 'template_artist.php';
			$template_artist['regions'] = array(
				'keyword',
			   'middle_content',
			   'sidebar_left',
			   'sidebar_right',
			   'profile_menu'
			);
			$template_artist['parser'] = 'parser';
			$template_artist['parser_method'] = 'parse';
			$template_artist['parse_template'] = FALSE;
			$template_artist['regions']['keywords'] = array('content' => array('Popmaya'));
			$template_artist['regions']['header'] = array('content' => array(anchor('', '<h1 class="ir" id="logo">Popmaya</h1>','')));
			$template_artist['regions']['head_title'] = array('content' => array('Popmaya &rsaquo; '));
			$template_artist['regions']['footer'] = array('content' => array('Copyright &copy; 2011 Popmaya. '.anchor(prep_url('www.theideafield.com'), 'POWERED BY THE IDEAFIELD', 'class="web_dev" title="web developent by www.theideafield.com"')));

			$this->template->add_template('template_artist',$template_artist,true);
			$data['ispagephoto'] = false;
			
			//render view
			$this->template->write('head_title', 'Profile');
			$this->template->write('keywords', '');
			$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
		    $this->template->write_view('sidebar_left', 'pm_side_artist_profile', $data2, '');
			if($this->m_artist->artist_member($this->uid,$id)){
				$this->template->write_view('sidebar_right', 'pm_artist_side_profile_right', $id, '');
			}else{
				$this->template->write_view('sidebar_right', 'pm_artist_side_right', $data, '');
			}
			$this->template->write_view('middle_content', 'pm_artist', $data, '');
			$this->template->render();
	      			
		}
	} 
	//------------------------------------------------------------------------------------------------------------------------end profile
	
	function create_playlist($artist_id=''){
		if(isset($_POST['playlist_name'])){
			$playlist_id = $this->m_artist->save_playlist(array('playlist_name'=>$_POST['playlist_name'],'artist_id'=>$artist_id));
			redirect(base_url().'artist/edit_playlist/'.$artist_id.'/'.$playlist_id,'refresh');
		}else{
			redirect(base_url().'artist/playlist/'.$artist_id.'/'.$this->uid,'refresh');
		}
		
		
	}
	
	function edit_playlist($id='',$playlist_id=''){
		if (!$this->ion_auth->logged_in()) //if not logged in
		{
			//redirect them to the login page
			redirect('home', 'refresh');
		}
		else
		{
		$this->config->load('my_config');
			$limit_sidebar_friend_list=$this->config->item('limit_sidebar_friend_list');
			
			
			$data['a_name'] = $this->input->post('name');
			$data['a_category'] = $this->input->post('category');
			$data['user_id']=$this->uid;
			if(!$this->input->post('name')){
				if($id==''){
					//echo 'a';
					$row2=$this->m_artist->getArtistProfile(1);
					$data['artist_id']=1;
					$data2['artist_id']=1;
				}else{
					$row2=$this->m_artist->getArtistProfile($id);
					//echo 'b';
					//echo '--'.$id;
					$data['artist_id']=$id;
					$data2['artist_id']=$id;
				}
			}else{
				$row2=$this->m_artist->getArtistProfile($this->m_artist->save_artist($data));
			}
			
			if($this->input->post('email')){
				$msg = '<html><body><h1><a href="ideafieldproject.com/popmaya">Popmaya</a></h1></body></html>';
				$subject = "join popmaya";
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= 'From: popmaya@example.com' . "\r\n";
				mail($this->input->post('email'), $subject,$msg,$headers);
			}else{
			}
			
			//setting up viewer
			$page_req="pm_artist";
			$data['foot_js']=css_asset('add_style.css').css_asset('plugin/sliderStyle.css').js_asset('jquery.nivo.slider.pack.js').  js_asset('jwysiwyg/jquery.wysiwyg.js').js_asset('jRecorder/jRecorder.js').js_asset('fileuploader.js').js_asset('jPlayer/jquery.jplayer.min.js').js_asset('stream.js');
	      	$data['sound_js'] = js_asset('sound.js');
		$data['foot_script']='
	        var sics = $(".share_icons"); sics.hide();
	        $(\'#slider\').nivoSlider({speed:5000});

	        $("#tab ul li.first a").addClass("active").show(); 
	          	$("#tab1").show();
	        /* ^zy */
			var the_player = $("#song_player");
			/* ^zy */
			 	var the_player = $("#song_player");

			 	the_player.jPlayer({
			 	    ready: function(){
						console.log("player ready."); 	           			
			 	    },
			 	    play: function(){
			 	    	console.log("play file.");	
			 	    },
			 	    volume: "0.4",
			 	    swfPath: "'.site_url().'assets/js/jPlayer",
					cssSelectorAncestor: "#wrapper",
					supplied: "mp3",
					wmode: "window"
				});           	

				var prev_val=0;
			 	$("#wrapper .btn_play").click(function(e){
			 	    the_player.jPlayer("setMedia", {
						mp3: $(this).attr("href")
					});
					the_player.jPlayer("play");
					var nxt_el=$(this).parent().next().find(".ttl_play");
					var ctr=nxt_el.html();
					nxt_el.fadeOut(); 
					var ctr2=parseInt(ctr);
					if(prev_val==0){
						ctr2+=1;
						prev_val=1;
					}
					nxt_el.html(ctr2); nxt_el.fadeIn();	
					return false;
				});

				$(".add_to_playlist").click(function(){
					$.post("'.site_url().'track/add_to_playlist", { track_id: $(this).attr("href") }, function(data) {
   						$(".list-left p.bottom").html();
   						alert(data);
   						$(".list-left p.bottom").html("Data berhasil di tambahkan."); /* harusnya disini load content ajax utk playlist */
 					});
					return false;	
				});
				$(".commentwall").hide();
				$(".open_comment").click(toggleComment);
				function toggleComment(){
					console.log($(this).parent().next().is(":visible"));
					if($(this).parent().next().is(":visible")){
						$(this).parent().next().fadeOut();
					}else{
						$(this).parent().next().fadeIn();
					}					
					return false;
				}
				$(".hover_content").hide();
				$(".hover_share").hover(function() {
    				$(this).next().show();
  				});
  				$(".hover_share").click(function(){ return false; });
  				$(".hover_content").live("mouseleave", function(){ $(this).hide() }); 

			/* ^zy.end */
	      
	      ';	      

			//if($id=='') //my profile
			//{
				$row=$this->m_artist->getUserProfile($this->uid);
			//}
			//else //other member profile
			//{
				//$row=$this->m_artist->getUserProfile($id);
			//}
			//if($id==''){
			//	$row2=$this->m_artist->getArtistProfile(1);
			//}
			//else{
			//	$row2=$this->m_artist->getArtistProfile($this->m_artist->save_artist($data));
			//}
			
						
			//setting data
			$data2['artist_name']=$row2->artist_name;
			$data2['title']=$row2->title;
			$data2['genre']=$this->m_artist->get_genre_name($row2->genre);
			
			switch($row2->type){
				case 0 : $data2['type'] = 'Solo';
					break;
				case 1 : $data2['type'] = 'Band';
					break;
				case 2 : $data2['type'] = 'Group';
					break;
				default : $data2['type'] = 'Solo';
					break;
			}
			
			$data['banner_image'] = $this->m_member->get_image_banner_profile();
			$data['recommended_track']=$this->m_home->get_recommended_track();
			$new_track_limit=$this->config->item('limit_new_track');
			$data['new_track']=$this->m_home->get_new_track($new_track_limit);
			
			$data['name']=$row->up_name;
			$data['alias']=$row->up_alias;	
			$data['unread_messages']=$this->m_member->get_count_unread_messages($this->uid);		

			$data['gender_code']=$row->up_gender;
			$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
			$data['city']=$row->up_city;
			$data['country']=$row->up_country;
			$data['friends']=$this->m_member->get_friends($this->uid, $limit_sidebar_friend_list);
			//load ads data
			$data['ads_data']=$this->m_artist->get_image_ads();
			
			/****================================ tambahan ===================================****/ 
			//Adi 26 Dec 2011
			$data['unread_messages']=$this->m_member->get_count_unread_messages($this->uid);		
			$data['count_friend_request']=$this->m_member->get_count_friend_request($this->uid);
			$data['count_notification']=$this->m_member->get_count_notification($this->uid);
			
			
			//edit 19 jan 2012
			$tmp = $this->m_artist->get_album_song_by_artist($id,1);
			if($tmp) {
				$data['album_song'] = $tmp[0];
				if($data['album_song']){
						$data['song_list']=$this->m_artist->get_song_by_album($data['album_song']->as_id,5);
				}
			}
			else $data['album_song'] = $tmp;
			
			//music sidebar, suggest song drop down menu		
			$fav_genre=$this->m_member->get_fav_genre($this->uid);
			$fav_genre=explode(", ", $fav_genre);
			$genres=$this->m_member->get_genre();
			$i=0;
			$fav_genre_display='';

			if($fav_genre[0]) //have favourite genre
			{
				foreach($genres as $g)
				{
					if($g->g_id==$fav_genre[$i])
					{
						$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
						$i++;
					}
				}	
			}
			else //do not have favourite genre
			{
				foreach($genres as $g)
				{
					$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$i++;
				}				
			}	
			$data['suggest_song_option']=$fav_genre_display;

			$genre_id=$fav_genre_display[0]['id'];
			$limit=$this->config->item('limit_sidebar_suggest_song_list');		
			$data['song_by_genre']=$this->m_member->get_song_by_genre($genre_id, $limit);
			/*================================end tambahan ========================================*/
			
			//Adi 7 Jan 2012
			$type_text=$this->uri->segment(4);
			if($type_text=='status') $type='1';
			else if($type_text=='voice') $type='2';
			else if($type_text=='video') $type='4';
			else if($type_text=='note') $type='5';
			else $type='';
			
			$data['texts']=$this->m_artist->get_status_update_artist($id, $type);
			$data['text_comments']=$this->m_artist->get_status_comment_artist($id);
			
			$template_artist['template'] = 'template_artist.php';
			$template_artist['regions'] = array(
				'keyword',
			   'middle_content',
			   'sidebar_left',
			   'sidebar_right',
			   'profile_menu'
			);
			$template_artist['parser'] = 'parser';
			$template_artist['parser_method'] = 'parse';
			$template_artist['parse_template'] = FALSE;
			$template_artist['regions']['keywords'] = array('content' => array('Popmaya'));
			$template_artist['regions']['header'] = array('content' => array(anchor('', '<h1 class="ir" id="logo">Popmaya</h1>','')));
			$template_artist['regions']['head_title'] = array('content' => array('Popmaya &rsaquo; '));
			$template_artist['regions']['footer'] = array('content' => array('Copyright &copy; 2011 Popmaya. '.anchor(prep_url('www.theideafield.com'), 'POWERED BY THE IDEAFIELD', 'class="web_dev" title="web developent by www.theideafield.com"')));

			$this->template->add_template('template_artist',$template_artist,true);
			$data['ispagephoto'] = false;
			
			
			$data['one_playlist'] = $this->m_artist->get_one_playlist($playlist_id);
			$data['list_songs'] = $this->m_artist->get_all_song();
			$data['last_insert_id'] = $playlist_id;
			
			//render view
			$this->template->write('head_title', 'Profile');
			$this->template->write('keywords', '');
			$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
		    $this->template->write_view('sidebar_left', 'pm_side_artist_profile', $data2, '');
			if($this->m_artist->artist_member($this->uid,$id)){
				$this->template->write_view('sidebar_right', 'pm_artist_side_profile_right', $id, '');
			}else{
				$this->template->write_view('sidebar_right', 'pm_artist_side_right', $data, '');
			}
			$this->template->write_view('middle_content', 'pm_artist_playlist_edit', $data, '');
			$this->template->render();
	      			
		}
	}
	
	
	function playlist($id='',$user_id=''){
		if (!$this->ion_auth->logged_in()) //if not logged in
		{
			//redirect them to the login page
			redirect('home', 'refresh');
		}
		else
		{
		$this->config->load('my_config');
			$limit_sidebar_friend_list=$this->config->item('limit_sidebar_friend_list');
			
			
			$data['a_name'] = $this->input->post('name');
			$data['a_category'] = $this->input->post('category');
			$data['user_id']=$this->uid;
			if(!$this->input->post('name')){
				if($id==''){
					//echo 'a';
					$row2=$this->m_artist->getArtistProfile(1);
					$data['artist_id']=1;
					$data2['artist_id']=1;
				}else{
					$row2=$this->m_artist->getArtistProfile($id);
					//echo 'b';
					//echo '--'.$id;
					$data['artist_id']=$id;
					$data2['artist_id']=$id;
				}
			}else{
				$row2=$this->m_artist->getArtistProfile($this->m_artist->save_artist($data));
			}
			
			if($this->input->post('email')){
				$msg = '<html><body><h1><a href="ideafieldproject.com/popmaya">Popmaya</a></h1></body></html>';
				$subject = "join popmaya";
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= 'From: popmaya@example.com' . "\r\n";
				mail($this->input->post('email'), $subject,$msg,$headers);
			}else{
			}
			
			//setting up viewer
			$page_req="pm_artist";
			$data['foot_js']=css_asset('add_style.css').css_asset('plugin/sliderStyle.css').js_asset('jquery.nivo.slider.pack.js').  js_asset('jwysiwyg/jquery.wysiwyg.js').js_asset('jRecorder/jRecorder.js').js_asset('fileuploader.js').js_asset('jPlayer/jquery.jplayer.min.js').js_asset('stream.js');
	      	$data['sound_js'] = js_asset('sound.js');
		$data['foot_script']='
	        var sics = $(".share_icons"); sics.hide();
	        $(\'#slider\').nivoSlider({speed:5000});

	        $("#tab ul li.first a").addClass("active").show(); 
	          	$("#tab1").show();
	        /* ^zy */
			var the_player = $("#song_player");
			/* ^zy */
			 	var the_player = $("#song_player");

			 	the_player.jPlayer({
			 	    ready: function(){
						console.log("player ready."); 	           			
			 	    },
			 	    play: function(){
			 	    	console.log("play file.");	
			 	    },
			 	    volume: "0.4",
			 	    swfPath: "'.site_url().'assets/js/jPlayer",
					cssSelectorAncestor: "#wrapper",
					supplied: "mp3",
					wmode: "window"
				});           	

				var prev_val=0;
			 	$("#wrapper .btn_play").click(function(e){
			 	    the_player.jPlayer("setMedia", {
						mp3: $(this).attr("href")
					});
					the_player.jPlayer("play");
					var nxt_el=$(this).parent().next().find(".ttl_play");
					var ctr=nxt_el.html();
					nxt_el.fadeOut(); 
					var ctr2=parseInt(ctr);
					if(prev_val==0){
						ctr2+=1;
						prev_val=1;
					}
					nxt_el.html(ctr2); nxt_el.fadeIn();	
					return false;
				});

				$(".add_to_playlist").click(function(){
					$.post("'.site_url().'track/add_to_playlist", { track_id: $(this).attr("href") }, function(data) {
   						$(".list-left p.bottom").html();
   						alert(data);
   						$(".list-left p.bottom").html("Data berhasil di tambahkan."); /* harusnya disini load content ajax utk playlist */
 					});
					return false;	
				});
				$(".commentwall").hide();
				$(".open_comment").click(toggleComment);
				function toggleComment(){
					console.log($(this).parent().next().is(":visible"));
					if($(this).parent().next().is(":visible")){
						$(this).parent().next().fadeOut();
					}else{
						$(this).parent().next().fadeIn();
					}					
					return false;
				}
				$(".hover_content").hide();
				$(".hover_share").hover(function() {
    				$(this).next().show();
  				});
  				$(".hover_share").click(function(){ return false; });
  				$(".hover_content").live("mouseleave", function(){ $(this).hide() }); 

			/* ^zy.end */
	      
	      ';	      

			//if($id=='') //my profile
			//{
				$row=$this->m_artist->getUserProfile($this->uid);
			//}
			//else //other member profile
			//{
				//$row=$this->m_artist->getUserProfile($id);
			//}
			//if($id==''){
			//	$row2=$this->m_artist->getArtistProfile(1);
			//}
			//else{
			//	$row2=$this->m_artist->getArtistProfile($this->m_artist->save_artist($data));
			//}
			
						
			//setting data
			$data2['artist_name']=$row2->artist_name;
			$data2['title']=$row2->title;
			$data2['genre']=$this->m_artist->get_genre_name($row2->genre);
			
			switch($row2->type){
				case 0 : $data2['type'] = 'Solo';
					break;
				case 1 : $data2['type'] = 'Band';
					break;
				case 2 : $data2['type'] = 'Group';
					break;
				default : $data2['type'] = 'Solo';
					break;
			}
			
			$data['banner_image'] = $this->m_member->get_image_banner_profile();
			$data['recommended_track']=$this->m_home->get_recommended_track();
			$new_track_limit=$this->config->item('limit_new_track');
			$data['new_track']=$this->m_home->get_new_track($new_track_limit);
			
			$data['name']=$row->up_name;
			$data['alias']=$row->up_alias;	
			$data['unread_messages']=$this->m_member->get_count_unread_messages($this->uid);		

			$data['gender_code']=$row->up_gender;
			$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
			$data['city']=$row->up_city;
			$data['country']=$row->up_country;
			$data['friends']=$this->m_member->get_friends($this->uid, $limit_sidebar_friend_list);
			//load ads data
			$data['ads_data']=$this->m_artist->get_image_ads();
			
			/****================================ tambahan ===================================****/ 
			//Adi 26 Dec 2011
			$data['unread_messages']=$this->m_member->get_count_unread_messages($this->uid);		
			$data['count_friend_request']=$this->m_member->get_count_friend_request($this->uid);
			$data['count_notification']=$this->m_member->get_count_notification($this->uid);
			
			
			//edit 19 jan 2012
			$tmp = $this->m_artist->get_album_song_by_artist($id,1);
			if($tmp) {
				$data['album_song'] = $tmp[0];
				if($data['album_song']){
						$data['song_list']=$this->m_artist->get_song_by_album($data['album_song']->as_id,5);
				}
			}
			else $data['album_song'] = $tmp;
			
			//music sidebar, suggest song drop down menu		
			$fav_genre=$this->m_member->get_fav_genre($this->uid);
			$fav_genre=explode(", ", $fav_genre);
			$genres=$this->m_member->get_genre();
			$i=0;
			$fav_genre_display='';

			if($fav_genre[0]) //have favourite genre
			{
				foreach($genres as $g)
				{
					if($g->g_id==$fav_genre[$i])
					{
						$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
						$i++;
					}
				}	
			}
			else //do not have favourite genre
			{
				foreach($genres as $g)
				{
					$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$i++;
				}				
			}	
			$data['suggest_song_option']=$fav_genre_display;

			$genre_id=$fav_genre_display[0]['id'];
			$limit=$this->config->item('limit_sidebar_suggest_song_list');		
			$data['song_by_genre']=$this->m_member->get_song_by_genre($genre_id, $limit);
			/*================================end tambahan ========================================*/
			
			//Adi 7 Jan 2012
			$type_text=$this->uri->segment(4);
			if($type_text=='status') $type='1';
			else if($type_text=='voice') $type='2';
			else if($type_text=='video') $type='4';
			else if($type_text=='note') $type='5';
			else $type='';
			
			$data['texts']=$this->m_artist->get_status_update_artist($id, $type);
			$data['text_comments']=$this->m_artist->get_status_comment_artist($id);
			
			$template_artist['template'] = 'template_artist.php';
			$template_artist['regions'] = array(
				'keyword',
			   'middle_content',
			   'sidebar_left',
			   'sidebar_right',
			   'profile_menu'
			);
			$template_artist['parser'] = 'parser';
			$template_artist['parser_method'] = 'parse';
			$template_artist['parse_template'] = FALSE;
			$template_artist['regions']['keywords'] = array('content' => array('Popmaya'));
			$template_artist['regions']['header'] = array('content' => array(anchor('', '<h1 class="ir" id="logo">Popmaya</h1>','')));
			$template_artist['regions']['head_title'] = array('content' => array('Popmaya &rsaquo; '));
			$template_artist['regions']['footer'] = array('content' => array('Copyright &copy; 2011 Popmaya. '.anchor(prep_url('www.theideafield.com'), 'POWERED BY THE IDEAFIELD', 'class="web_dev" title="web developent by www.theideafield.com"')));

			$this->template->add_template('template_artist',$template_artist,true);
			$data['ispagephoto'] = false;
			
			if(strcmp($id,$this->session->userdata['artist_id'])==0){
				$data['is_my_playlist'] = true;
			}else{
				$data['is_my_playlist'] = false;
			}
			$data['user_playlist'] = $this->m_artist->get_artist_playlist($id);
			//print_r($data['user_playlist']);
		
			//render view
			$this->template->write('head_title', 'Profile');
			$this->template->write('keywords', '');
			$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
		    $this->template->write_view('sidebar_left', 'pm_side_artist_profile', $data2, '');
			if($this->m_artist->artist_member($this->uid,$id)){
				$this->template->write_view('sidebar_right', 'pm_artist_side_profile_right', $id, '');
			}else{
				$this->template->write_view('sidebar_right', 'pm_artist_side_right', $data, '');
			}
			$this->template->write_view('middle_content', 'pm_artist_playlist', $data, '');
			$this->template->render();
	      			
		}
	}
	
	
	function become_fan($artist_id=''){
		$data = array(
				'artist_id' => $artist_id,
				'member_id' => $this->uid,
				'f_isactive' => 1				
		);
		$this->m_artist->become_fans($data);
		redirect("artist/","refresh");
	}
	
	function stop_become_fan($artist_id=''){
		$this->m_artist->unfans($artist_id,$this->uid);
		redirect("artist/","refresh");
	}

	function create_artist(){
		if(!$this->input->post('terms')){
			$this->session->set_flashdata('terms', "You must accept the terms");
			redirect($_SERVER['HTTP_REFERER']);
		}else if(!$this->input->post('name')){
			$this->session->set_flashdata('name', "You must fill the name");
			redirect($_SERVER['HTTP_REFERER']);
		}
		else{		
			$data['a_name'] = $this->input->post('name');
			$data['a_type'] = $this->input->post('category');
			$data['user_id']=$this->uid;
			
			redirect('artist/upload_crop/'.$this->m_artist->save_artist($data));
		}
	}
	
	
	//nama : Adi
	//tanggal : 3 Jan 2012
	//deskripsi : create artist - upload & crop photo
	//------------------------------------------------------------------------------------------------------------------------upload crop
	function upload_crop($id='')
	{
		if (!$this->ion_auth->logged_in()) //if not logged in
		{
			//redirect them to the login page
			redirect('member/frontpage', 'refresh');
		}
		else
		{
			$this->config->load('my_config');
			$limit_sidebar_friend_list=$this->config->item('limit_sidebar_friend_list');
			
			/*
			$data['a_name'] = $this->input->post('name');
			$data['a_category'] = $this->input->post('category');
			$data['user_id']=$this->uid;
			if(!$this->input->post('name')){
				//if($id==''){
					redirect('artist','refresh');
				//}else{
				//	$row2=$this->m_artist->getArtistProfile($id);
				//	$data['artist_id']=$id;
				//	$data2['artist_id']=$id;
				//}
			}else{
				$row2=$this->m_artist->getArtistProfile($this->m_artist->save_artist($data));
			}
			*/
			$row2=$this->m_artist->getArtistProfile($id);
			
			if($this->input->post('email')){
				$msg = '<html><body><h1><a href="ideafieldproject.com/popmaya">Popmaya</a></h1></body></html>';
				$subject = "join popmaya";
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= 'From: popmaya@example.com' . "\r\n";
				mail($this->input->post('email'), $subject,$msg,$headers);
			}else{
			}
			
			//setting up viewer
			$page_req="pm_artist";
			$data['foot_js']=css_asset('plugin/sliderStyle.css').js_asset('jquery.nivo.slider.pack.js');
	      	$data['foot_script']='
		        var sics = $(".share_icons"); sics.hide();
		        $(\'#slider\').nivoSlider({speed:5000});
		        
            $("#tab ul li.middle a").addClass("active").show(); 
            $("#tab2").show();  

		      ';	      

			//if($id=='') //my profile
			//{
				$row=$this->m_artist->getUserProfile($this->uid);
			//}
			//else //other member profile
			//{
				//$row=$this->m_artist->getUserProfile($id);
			//}
			//if($id==''){
			//	$row2=$this->m_artist->getArtistProfile(1);
			//}
			//else{
			//	$row2=$this->m_artist->getArtistProfile($this->m_artist->save_artist($data));
			//}
			
						
			//setting data
			$data2['artist_name']=$row2->artist_name;
			$data2['title']=$row2->title;
			$data2['genre']=$this->m_artist->get_genre_name($row2->genre);
			switch($row2->type){
				case 0 : $data2['type'] = 'Solo';
					break;
				case 1 : $data2['type'] = 'Band';
					break;
				case 2 : $data2['type'] = 'Group';
					break;
				default : $data2['type'] = 'Solo';
					break;
			}
			$data2['artist_id']=$row2->ID;
			$data['artist_id']=$row2->ID;
			$data['ispagephoto'] = false;
			
			$data['banner_image'] = $this->m_member->get_image_banner_profile();
			$data['recommended_track']=$this->m_home->get_recommended_track();
			$new_track_limit=$this->config->item('limit_new_track');
			$data['new_track']=$this->m_home->get_new_track($new_track_limit);
			$data['user_id']=$this->uid;
			
			$data['name']=$row->up_name;
			$data['alias']=$row->up_alias;	
			$data['unread_messages']=$this->m_member->get_count_unread_messages($this->uid);		

			$data['gender_code']=$row->up_gender;
			$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
			$data['city']=$row->up_city;
			$data['country']=$row->up_country;
			$data['friends']=$this->m_member->get_friends($this->uid, $limit_sidebar_friend_list);
			//load ads data
			$data['ads_data']=$this->m_artist->get_image_ads();
			
			/****================================ tambahan ===================================****/ 
			//Adi 26 Dec 2011
			$data['unread_messages']=$this->m_member->get_count_unread_messages($this->uid);		
			$data['count_friend_request']=$this->m_member->get_count_friend_request($this->uid);
			$data['count_notification']=$this->m_member->get_count_notification($this->uid);
			
			///edit 19 jan 2012
			$tmp = $this->m_artist->get_album_song_by_artist($id,1);
			if($tmp) {
				$data['album_song'] = $tmp[0];
				if($data['album_song']){
						$data['song_list']=$this->m_artist->get_song_by_album($data['album_song']->as_id,5);
				}
			}
			else $data['album_song'] = $tmp;
			
			//music sidebar, suggest song drop down menu		
			$fav_genre=$this->m_member->get_fav_genre($this->uid);
			$fav_genre=explode(", ", $fav_genre);
			$genres=$this->m_member->get_genre();
			$i=0;
			$fav_genre_display='';

			if($fav_genre[0]) //have favourite genre
			{
				foreach($genres as $g)
				{
					if($g->g_id==$fav_genre[$i])
					{
						$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
						$i++;
					}
				}	
			}
			else //do not have favourite genre
			{
				foreach($genres as $g)
				{
					$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$i++;
				}				
			}	
			$data['suggest_song_option']=$fav_genre_display;

			$genre_id=$fav_genre_display[0]['id'];
			$limit=$this->config->item('limit_sidebar_suggest_song_list');		
			$data['song_by_genre']=$this->m_member->get_song_by_genre($genre_id, $limit);
			/*================================end tambahan ========================================*/	
			
			
			/*****  UPLOAD AND CROP FOTO ********/
			
			$data['upload_path']        = $upload_path          = "assets/images/artist/" ;
			$data['destination_thumbs'] = $destination_thumbs   = "assets/images/artist/thumbnail/" ;
			$data['destination_medium'] = $destination_medium   = "assets/images/artist/medium/" ;

			$size_medium_width  = 300 ;
			$size_medium_height = 300 ;


			$data['large_photo_exists'] = $data['thumb_photo_exists'] = $data['error'] = NULL ;
			$data['thumb_width']        = $thumb_width  = 100 ;
			$data['thumb_height']       = $thumb_height = 100 ;
			
			if (($this->input->post('upload'))) {
				$config['upload_path']  = $upload_path ;
				$config['allowed_types']= 'gif|jpg|png|jpeg';
				$config['max_size']     = '2000';
				$config['max_width']    = '2000';
				$config['max_height']   = '2000';
				$config['file_name'] = $id;

				if(file_exists($upload_path.$id.'.jpg')){
					unlink($upload_path.$id.'.jpg');
				}else if(file_exists($upload_path.$id.'.gif')){
					unlink($upload_path.$id.'.gif');
				}else if(file_exists($upload_path.$id.'.png')){
					unlink($upload_path.$id.'.png');
				}else if(file_exists($upload_path.$id.'.jpeg')){
					unlink($upload_path.$id.'.jpeg');
				}
				
				if(file_exists($destination_thumbs.$id.'.jpg')){
					unlink($destination_thumbs.$id.'.jpg');
				}else if(file_exists($destination_thumbs.$id.'.gif')){
					unlink($destination_thumbs.$id.'.gif');
				}else if(file_exists($destination_thumbs.$id.'.png')){
					unlink($destination_thumbs.$id.'.png');
				}else if(file_exists($destination_thumbs.$id.'.jpeg')){
					unlink($destination_thumbs.$id.'.jpeg');
				}
				
				if(file_exists($destination_medium.$id.'.jpg')){
					unlink($destination_medium.$id.'.jpg');
				}else if(file_exists($destination_medium.$id.'.gif')){
					unlink($destination_medium.$id.'.gif');
				}else if(file_exists($destination_medium.$id.'.png')){
					unlink($destination_medium.$id.'.png');
				}else if(file_exists($destination_medium.$id.'.jpeg')){
					unlink($destination_medium.$id.'.jpeg');
				}
				
				$this->load->library('upload', $config);

				if ($this->upload->do_upload("image")) {
					$data['img']	 = $this->upload->data();

					$resize = $this->image_moo
								->load($upload_path . $data['img']['file_name'])
								->resize($size_medium_width,$size_medium_height)
								->save($destination_medium . $data['img']['file_name']) ;

					if ($resize) {
						list($medium_width,$medium_height) = getimagesize($destination_medium . $data['img']['file_name']);

						$data['img']['image_width']    = $medium_width ;
						$data['img']['image_height']   = $medium_height ;

						// For Auto Selection, we must define X1, X2, Y1, and Y2
						$data['img']['x1']  = ($medium_width/2) - ($thumb_width/2) ;
						$data['img']['x2']  = ($medium_width/2) + ($thumb_width/2) ;
						$data['img']['y1']  = ($medium_height/2) - ($thumb_height/2) ;
						$data['img']['y2']  = ($medium_height/2) + ($thumb_height/2) ;
					}
					
					$data['large_photo_exists']  = "<img src=\"".base_url() . $destination_medium.$data['img']['file_name']."\" alt=\"Medium Image\"/>";
				}
			}
			elseif (($this->input->post('upload_thumbnail'))) {
				$x1 = $this->input->post('x1',TRUE) ;
				$y1 = $this->input->post('y1',TRUE) ;
				$x2 = $this->input->post('x2',TRUE) ;
				$y2 = $this->input->post('y2',TRUE) ;
				$w  = $this->input->post('w',TRUE) ;
				$h  = $this->input->post('h',TRUE) ;

				$file_name                  = $this->input->post('file_name',TRUE) ;
				
				if ($file_name) {
					$this->image_moo
						->load($destination_medium . $file_name)
						->crop($x1,$y1,$x2,$y2)
						->save($destination_thumbs . $file_name)
						->load($destination_thumbs . $file_name)
						->resize($data['thumb_width'],$data['thumb_height'])
						->save($destination_thumbs . $file_name,TRUE) ;


					if ($this->image_moo->errors) {
						$data['error'] = $this->image_moo->display_errors() ;
					}
					else {
					//tambahan adi 11 jan 2012. buat session data artist
					$newdata = array('artist_id'  => $id);
					$this->session->set_userdata($newdata);
			
						$data['thumb_photo_exists'] = "<img src=\"".base_url() . $destination_thumbs . $file_name."\" alt=\"Thumbnail Image\"/>";
						$data['large_photo_exists'] = "<img src=\"".base_url() . $destination_medium . $file_name."\" alt=\"Medium Image\"/>";
						redirect('artist/invite_fans/'.$id);
					}
				}

			}
			$template_crop['template'] = 'template_crop.php';
			$template_crop['regions'] = array(
				'keyword',
			   'middle_content',
			   'sidebar_left',
			   'sidebar_right',
			   'profile_menu'
			);
			$template_crop['parser'] = 'parser';
			$template_crop['parser_method'] = 'parse';
			$template_crop['parse_template'] = FALSE;
			$template_crop['regions']['keywords'] = array('content' => array('Popmaya'));
			$template_crop['regions']['header'] = array('content' => array(anchor('', '<h1 class="ir" id="logo">Popmaya</h1>','')));
			$template_crop['regions']['head_title'] = array('content' => array('Popmaya &rsaquo; '));
			$template_crop['regions']['footer'] = array('content' => array('Copyright &copy; 2011 Popmaya. '.anchor(prep_url('www.theideafield.com'), 'POWERED BY THE IDEAFIELD', 'class="web_dev" title="web developent by www.theideafield.com"')));

			$this->template->add_template('template_crop',$template_crop,true);
		}				
					
		//render view
		$this->template->write('head_title', 'Profile');
		$this->template->write('keywords', '');
		$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
		$this->template->write_view('sidebar_left', 'pm_side_artist_profile', $data2, '');
		if($this->m_artist->artist_member($this->uid,$id)){
			$this->template->write_view('sidebar_right', 'pm_artist_side_profile_right', $id, '');
		}else{
			$this->template->write_view('sidebar_right', 'pm_artist_side_right', $data, '');
		}
		//$this->template->write_view('middle_content', 'pm_artist', $data, '');
		$this->template->write_view('middle_content','pm_upload_crop',$data) ;
		$this->template->render();
				
		
	} 
	//------------------------------------------------------------------------------------------------------------------------end upload crop
	
	//nama : Adi
	//tanggal : 11 Jan 2012
	//deskripsi : create artist - invite fans
	//------------------------------------------------------------------------------------------------------------------------invite fans
	function invite_fans($id='')
	{	
		
		$data['foot_js']=css_asset('plugin/sliderStyle.css').js_asset('jquery.nivo.slider.pack.js');
	    $data['foot_script']='
			var sics = $(".share_icons"); sics.hide();
		    $(\'#slider\').nivoSlider({speed:5000});

		    $("#tab ul li.middle a").addClass("active").show(); 
 	        $("#tab2").show();
		    ';	      
			
			$this->config->load('my_config');
			$limit_sidebar_friend_list=$this->config->item('limit_sidebar_friend_list');
			
			if($this->uid){
				$row=$this->m_member->get_user_profile($this->uid);
				$data['user_id']=$this->uid;
				$data['up_uid']=$row->up_uid; // ^zy 4 jan
				$data['name']=$row->up_name;
				$data['alias']=$row->up_alias;	
				$data['unread_messages']=$this->m_member->get_count_unread_messages($this->uid);		
				$data['count_friend_request']=$this->m_member->get_count_friend_request($this->uid);
				$data['count_notification']=$this->m_member->get_count_notification($this->uid);

				$data['gender_code']=$row->up_gender;
				$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
				$data['city']=$row->up_city;
				$data['country']=$row->up_country;
				$data['friends']=$this->m_member->get_friends($this->uid, $limit_sidebar_friend_list);
			}
			
			if(!isset($this->session->userdata['artist_id']) || $this->session->userdata['artist_id']!=$id){
				redirect('artist');
			}
			
			
			if($id==''){
				redirect('artist');
			}else{
				$row2=$this->m_artist->getArtistProfile($id);
				$data['artist_id']=$id;
				$data2['artist_id']=$id;
			}
			
			//setting data
			$data2['artist_name']=$row2->artist_name;
			$data2['title']=$row2->title;
			$data2['genre']=$this->m_artist->get_genre_name($row2->genre);
			
			switch($row2->type){
				case 0 : $data2['type'] = 'Solo';
					break;
				case 1 : $data2['type'] = 'Band';
					break;
				case 2 : $data2['type'] = 'Group';
					break;
				default : $data2['type'] = 'Solo';
					break;
			}
			
			//edit 19 jan 2012
			$tmp = $this->m_artist->get_album_song_by_artist($id,1);
			if($tmp) {
				$data['album_song'] = $tmp[0];
				if($data['album_song']){
						$data['song_list']=$this->m_artist->get_song_by_album($data['album_song']->as_id,5);
				}
			}
			else $data['album_song'] = $tmp;
			

			//Autthor	: Adi
			//Date		: 2012 Jan 11
			//Desc		: Load member list to invite fans	
			$data['fans']=$this->m_fans->get_member_list_fans($this->uri->segment(4)/10,$id);
			/* pagination */ 
			$uri_segment = 4;
			if($this->uri->segment($uri_segment)==''){ $offset=0; }else{ $offset=$this->uri->segment($uri_segment); }
			$data['start_no']= $offset+1;

			$this->load->library('pagination');
			$config['base_url'] = site_url().'artist/invite_fans/'.$id;
			$data['jml_member']=$config['total_rows'] = $this->m_fans->get_num_fans();
			$config['per_page'] = 10;
			$config['uri_segment'] = $uri_segment;
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			/* pagination.end */
			
			//Suggest Song --> duplicate form member
			//music sidebar, suggest song drop down menu	
			$fav_genre=$this->m_fans->get_fav_genre($this->uid);
			$fav_genre=explode(", ", $fav_genre);
			$fav_artist=$this->m_fans->get_fav_artist($this->uid);
			$fav_artist=explode(", ", $fav_artist);
			
			$genres=$this->m_fans->get_genre();
			$artists=$this->m_fans->get_artist();
			$i=0;
			$fav_genre_display='';
			$fav_genre_string='';
			$fav_artist_display='';
			$fav_artist_string='';

			if($fav_genre[0]) //have favourite genre
			{
				foreach($genres as $g)
				{
					if($g->g_id==$fav_genre[$i])
					{
						$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
						$fav_genre_string .= $g->g_name.',';
						$i++;
					}
				}	
			}
			else //do not have favourite genre
			{
				foreach($genres as $g)
				{
					$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$fav_genre_string .= $g->g_name.',';
					$i++;
				}				
			}	
			  $data['suggest_song_option']=$fav_genre_display;
				$data['fav_genre_string']=$fav_genre_string;
				
			$i=0;	
			if($fav_artist[0]) //have favourite artist
			{
				foreach($artists as $a)
				{
					if($a->ID==$fav_artist[$i])
					{
						$fav_artist_display[$i]=array('id'=>$a->ID, 'name'=>$a->artist_name);
						$fav_artist_string .= $a->artist_name.',';
						$i++;
					}
				}	
			}
			else //do not have favourite genre
			{
				foreach($artists as $a)
				{
					$fav_artist_display[$i]=array('id'=>$a->ID, 'name'=>$a->artist_name);
					$fav_artist_string .= $a->artist_name.',';
					$i++;
				}				
			}	
			
				$data['fav_artist_string']=$fav_artist_string;

			  $genre_id=$fav_genre_display[0]['id'];
			$limit=$this->config->item('limit_sidebar_suggest_song_list');		
			$data['song_by_genre']=$this->m_fans->get_song_by_genre($genre_id, $limit);
			//end duplicate suggest song

			if($this->input->post('submit')){
				
				$data['fans']=$this->m_fans->get_fans_list_criteria($this->uri->segment(3)/10,$this->input->post('gender'),$this->input->post('age1'),$this->input->post('age2'));
			}
			
			$data['ispagephoto'] = false;
		
			
			//load ads data
			$data['ads_data']=$this->m_fans->get_image_ads();
			//render view
			$this->template->write('head_title', 'Artist');
			$this->template->write('keywords', '');
	      	$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
		  	$this->template->write_view('sidebar_left', 'pm_side_artist_profile', $data2, '');
			$this->template->write_view('middle_content', 'pm_artist_invite_fans', $data, '');
			$this->template->write_view('sidebar_right', 'pm_artist_side_profile_right', $id, '');
			$this->template->render();			      			

   	}

	//------------------------------------------------------------------------------------------------------------------------end invite fans
	
	//(adi) 10-1-2012
	function invite_fans_proccess($memberid) {
		$data_insert = array(
				'member_id' => $memberid,
				'artist_id' => $this->session->userdata['artist_id'],
				'f_isactive' => 0
		);
		$query = $this->db->get_where('tr_member_fans',$data_insert);
		if(! $query->row()->mf_id) {
			$this->db->insert('tr_member_fans',$data_insert);
			$data_not = array(
				'n_type' => 10,
				'n_fromartistid' => $this->artist_id,
				'n_foruserid' => $memberid
			);
			$this->db->insert('tr_notification',$data_not);			
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	function cancel_fans_req($fid) {
		$mf = $this->m_artist->get_member_fans($fid);
		
		$this->db->delete('tr_member_fans', array('mf_id' => $fid));
		
		$this->db->delete('tr_notification', array('n_fromartistid' => $mf->artist_id, 'n_foruserid' => $mf->member_id));
		
		//echo "gagal";
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	/* Adi. 11 Jan 2012. use page as artist */
	function use_as_artist($id)
	{
		if($this->m_member->is_my_artist($this->uid, $id))
		{
			$newdata = array('artist_id'  => $id);
			$this->session->set_userdata($newdata);
			
			$this->session->set_flashdata('message', "Successfully use popmaya as artist");			
			redirect($_SERVER['HTTP_REFERER'], 'refresh');						
		}
		else
		{
			$this->session->set_flashdata('message', "Can not use popmaya as this artist");			
			redirect($_SERVER['HTTP_REFERER'], 'refresh');						
		}
	}

	/* Adi. 11 Jan 2012. use as page as member */	
	function use_as_member($id)
	{
		if($this->session->userdata['user_id']==$id)
		{
			if(isset($this->session->userdata['artist_id']))
			{
				$this->session->unset_userdata('artist_id');
			}
			$this->session->set_flashdata('message', "Successfully use popmaya as myself");			
			redirect($_SERVER['HTTP_REFERER'], 'refresh');			
		}
		else
		{
			$this->session->set_flashdata('message', "Can not use popmaya as myself");			
			redirect($_SERVER['HTTP_REFERER'], 'refresh');			
		}
	}
	
	/* Firman : blast email */
	function blastemail($artist_id){
			$fans = $this->m_artist->get_artist_fans($artist_id);
			$artist = $this->m_artist->getArtistProfile($artist_id);
			//print_r($artist);
			foreach($fans as $fan){				
				$user = $this->m_artist->getUserProfile($fan->member_id);
				$this->load->library('email');
				$config['protocol'] = 'sendmail';
				$config['mailtype'] = 'text';
				$config['charset'] = 'utf-8';
				$config['wordwrap'] = TRUE;
				$this->email->initialize($config);
				$this->email->from($artist->artist_name."@popmaya.com", $artist->artist_name);
				$this->email->to($user->ul_email);
				//$this->email->cc('xcho85@gmail.com');
				//$this->email->bcc('them@their-example.com');

				$this->email->subject('Blast email');
				$this->email->message('blast email dummy text');

				$this->email->send();
				//echo "email_send";
			}
			echo "email successfuly sent";
			redirect(site_url()."artist/profile/".$artist_id,"refresh");
	}
	
	/* Firman : send invittion to become fans */
	function invite_people($id='')
	{
		if($_POST['submit']){
			if(empty($_POST['email'])){
				echo "destiantion email cannot be empty!";
				redirect(site_url()."artist/profile/".$id);
			}
			$user = $this->m_artist->getUserProfile($this->uid);
			
			$this->load->library('email');
			$config['protocol'] = 'sendmail';
			$config['mailtype'] = 'text';
			$config['charset'] = 'utf-8';
			$config['wordwrap'] = TRUE;
			$this->email->initialize($config);
			$this->email->from($user->ul_email, $user->up_name);
			$this->email->to($_POST['email']);
			//$this->email->cc('xcho85@gmail.com');
			//$this->email->bcc('them@their-example.com');

			$this->email->subject('Become fans my artist in popmaya');
			$this->email->message('follow this link '.site_url().'artist/profile/'.$id);

			$this->email->send();

			echo "innvitation has been sent";
			redirect(site_url()."artist/profile/".$id);
		}
	}
	
	function email_invitation(){
		$this->template->write_view('middle_content', 'OpenInviter/more_examples/example_list');
	}

	function status_update_artist(){
				echo 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
                $pagesize=10;
                $type=$this->input->get_post('type');
                $updateType=$this->input->post('content_type');
								
                //check if request is from status update post
                if($updateType!==FALSE)
                {
                    //insert status in to database here
                    switch($updateType)
                    {
                        case 'status':
                            $textStatus=$this->input->post('text');
							$who_profile=$this->input->post('who_profile');
                            $contentUpdate=array(
                                        'type'=>'text',
                                        'text'=>$textStatus
                                    );
							$to_id=$who_profile;
							
							//adi 16 jan 2012, insert to database
							if(isset($this->session->userdata['artist_id']) && $this->session->userdata['artist_id']!=$who_profile)
							{
								$member_id='0';
								$artist_id=$this->session->userdata['artist_id'];
							}
							else if(isset($this->session->userdata['artist_id']) && $this->session->userdata['artist_id']==$who_profile)
							{
								$artist_id='0';
								$member_id='0';
							}else{
								$member_id=$this->uid;
								$artist_id='0';
							}
							$this->m_artist->share_status_artist($artist_id, $member_id, $to_id, $textStatus, $this->session->userdata('ip_address'));
							//end adi 16 jan
                            break;
                        case 'video':
                            $videourl=$this->input->post('videourl');
							$who_profile=$this->input->post('who_profile');
                            $contentUpdate=array(
                                        'type'=>'video',
                                        'videourl'=>$videourl
                                   );
							//adi 16 jan, insert to database
							if(isset($this->session->userdata['artist_id']) && $this->session->userdata['artist_id']!=$who_profile)
							{
								$member_id='0';
								$artist_id=$this->session->userdata['artist_id'];
							}
							else if(isset($this->session->userdata['artist_id']) && $this->session->userdata['artist_id']==$who_profile)
							{
								$artist_id='0';
								$member_id='0';
							}else{
								$member_id=$this->uid;
							}
							$this->m_artist->share_video_artist($artist_id, $member_id, $to_id, $textStatus, $this->session->userdata('ip_address'));
							//end adi 16 jan
                            break;
                        case 'audio':
                            $contentUpdate=array(
                                        'type'=>'audio',
                                    );
                            break;
                            break;
                        case 'photo':
                            $album_id=$this->input->post('album_id');
                            //check if album is not exist yet
                            if($album_id===0)
                            {
                                //create new album here and replace album_id with the id from db
                                $album_id=10;
                            }
                            $album_title=$this->input->post('album_title');
                            $filepath=$videourl=$this->input->post('filepath');
                            $desc=$this->input->post('desc');
                            $contentUpdate=array(
                                'type'=>'photo',
                                'photo_url'=>'http://www.starpulseonline.com/images/celebrity/coldplay.jpg',
                                'album_id'=>$album_id,
                                'album_title'=>$album_title,
                                'desc'=>$desc
                            );
                            break;
                        case 'note':
                            $note=$this->input->post('note');
                            $contentUpdate=array(
                                        'type'=>'note',
                                        'note'=>$note
                                    );
							//adi 16 Jan 2012, insert to database
							if(isset($this->session->userdata['artist_id']) && $this->session->userdata['artist_id']!=$who_profile)
							{
								$member_id='0';
								$artist_id=$this->session->userdata['artist_id'];
							}
							else if(isset($this->session->userdata['artist_id']) && $this->session->userdata['artist_id']==$who_profile)
							{
								$artist_id='0';
								$member_id='0';
							}else{
								$member_id=$this->uid;
							}
							
							$this->m_artist->share_note_artist($artist_id, $member_id, $to_id, 'Note', $note, $this->session->userdata('ip_address'));
							//end adi 6 jan 2012						
                            break;
                    }
                    
                }
                
                //check if request not from status update post
                if($type==='latest')
                {
                    $since=$this->input->get_post('since');
                    
                    if($since===FALSE)
                    {
                        //load all latest update from db using page size above
                    }
                    else
                    {
                        //load only latest update since latest id
                        
                        echo json_encode(array('sinceid'=>$since,
                            'status'=>
                            array(
                                //load from database and for each status create an array like below
                                array(
                                    'id'=>0,
                                    'author'=>array(
                                      'id'=>3,
                                      'name'=>'Zufrizal Yordan',
                                      'thumbnail'=>image_asset_url('cd_thumb.jpg'),
                                    ),
                                    'content'=>$contentUpdate
                                )
                            )
                        ));

                    }
                }
                else if($type==='more')
                {
                    $after=$this->input->get('before');

					$this->load->config('my_config');
					$limit_load_more=$this->config->item('limit_load_more');
					
					$walls=$this->m_member->get_more_wall($limit_load_more, $this->user_id);

                    //load all updates after ID paging by page size
					$i=0;
					foreach($walls as $row)
					{
						$path=$this->m_member->get_profile_path($this->user_id);
						
						$status[$i]=array(
                              'author'=>array(
	                              'id'=>$row->memberID,
	                              'name'=>$row->up_name,
	                              'thumbnail'=>$path."/".$row->memberID.".jpg",
                            	),
                            	'content'=>array(
	                                'type'=>'text',
	                                'text'=>$row->info
                            	)
                        );
						$i++;
					}					

					//send to html
                    echo json_encode(array('beforeid'=>$after,
                            'status'=>$status
                        ));
                }
                
                
   	}
	
	function file_upload_artist()
    {
        
        // list of valid extensions, ex. array("jpeg", "xml", "bmp")
        $allowedExtensions = array();
        // max file size in bytes
        $sizeLimit = 10 * 1024 * 1024;
        $this->load->library('QqFileUploader',array('allowedExtensions'=>$allowedExtensions,'sizeLimit'=>$sizeLimit));
        $uploadpath=dirname(__FILE__).'/../../../../assets/media/';
        $result = $this->qqfileuploader->handleUpload($uploadpath);
        // to pass data through iframe you will need to encode all html tags
        echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
    }

	/* Adi. 17 Jan 2012. artist like status artist */
	function artist_like_status($wall_id)
	{
		$this->m_artist->artist_like_status($wall_id, $this->session->userdata['artist_id']);
		redirect($_SERVER['HTTP_REFERER'], 'refresh');
	}
	
	/* Adi. 17 Jan 2012. member like status artist */
	function member_like_status($wall_id)
	{
		$this->m_artist->member_like_status($wall_id, $this->uid);
		redirect($_SERVER['HTTP_REFERER'], 'refresh');
	}
	
	/* Adi 17 Jan 2012. use ase artist page */
	function use_page_as_artist_proccess($id){
		$newdata = array('artist_id'  => $id);
		$this->session->set_userdata($newdata);
		
		redirect('artist/profile/'.$id);
	}
	
	function profile_info($id=''){
		if (!$this->ion_auth->logged_in()) //if not logged in
		{
			//redirect them to the login page
			redirect('home', 'refresh');
		}
		else
		{
			//setting up viewer
			$page_req="pm_profile_idx";
			$data['foot_js']=css_asset('plugin/sliderStyle.css').js_asset('jquery.nivo.slider.pack.js').  js_asset('jwysiwyg/jquery.wysiwyg.js').js_asset('jRecorder/jRecorder.js').js_asset('fileuploader.js').js_asset('jPlayer/jquery.jplayer.min.js');
			$data['foot_script']='
				var sics = $(".share_icons"); sics.hide();
				$(\'#slider\').nivoSlider({speed:5000});

				$("#tab ul li.middle a").addClass("active").show(); 
				$("#tab2").show();
				/* ^zy */
				var the_player = $("#song_player");
				
				the_player.jPlayer({
					ready: function(){
						console.log("player ready."); 	           			
					},
					play: function(){
						console.log("play file.");	
					},
					volume: "0.4",
					swfPath: "'.site_url().'assets/js/jPlayer",
					cssSelectorAncestor: "#wrapper",
					supplied: "mp3",
					wmode: "window"
				});

				var prev_val=0;
				$("#wrapper .btn_play").click(function(e){
					the_player.jPlayer("setMedia", {
						mp3: $(this).attr("href")
					});
					the_player.jPlayer("play");
					var nxt_el=$(this).parent().next().find(".ttl_play");
					var ctr=nxt_el.html();
					nxt_el.fadeOut(); 
					var ctr2=parseInt(ctr);
					if(prev_val==0){
						ctr2+=1;
						prev_val=1;
					}
					nxt_el.html(ctr2); nxt_el.fadeIn();	
					return false;
				});
				/* ^zy.end */
			  ';	      

			if($id=='') //my profile
			{
				$row2=$this->m_artist->getArtistProfile($this->session->userdata['artist_id']);
			}
			else //other member profile
			{
				$row2=$this->m_artist->getArtistProfile($id);
			}
			$data2['name']=$row2->artist_name;
			$data2['page_name']=$row2->page_name;
			$data2['gb']=$row2->gb_member;
			$data2['birthday']=$row2->a_bdate;
			
			$genre=$row2->genre;
			$genre=explode(", ", $genre);
			
			$genre_list=$this->m_member->get_genre();
			
			$genre_string='';
			$genre_display='';
			$i=0;
			
			$len = count($genre);
			
			if($genre[0]) //have genre
			{
				foreach($genre_list as $g)
				{
					if($g->g_id==$genre[$i])
					{
						$genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
						$genre_string .= $g->g_name.',';
						$i++;
					}
					if($i = $len) break;
				}	
			}
			else //do not have favourite genre
			{
				foreach($genre_list as $g)
				{
					$genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$genre_string .= $g->g_name.',';
					$i++;
				}				
			}	
			$data2['genre_display']=$genre_display;
			$data2['genre_string']=$genre_string;
			
			$data2['label']=$row2->record_label;
			$data2['manager']=$row2->manager;
			$data2['contact']=$row2->cnumber;
			$data2['content']=$row2->c_officer;
			$data2['link']=$row2->other_link;
			$data2['description']=$row2->gb_description;
			
			//edit 19 jan 2012
			$tmp = $this->m_artist->get_album_song_by_artist($id,1);
			if($tmp) {
				$data['album_song'] = $tmp[0];
				if($data['album_song']){
						$data['song_list']=$this->m_artist->get_song_by_album($data['album_song']->as_id,5);
				}
			}
			else $data['album_song'] = $tmp;
			
			//setting data
			$data2['artist_name']=$row2->artist_name;
			$data2['artist_id']=$id;
			$data2['title']=$row2->title;
			$data2['genre']=$this->m_artist->get_genre_name($row2->genre);
			
			switch($row2->type){
				case 0 : $data2['type'] = 'Solo';
					break;
				case 1 : $data2['type'] = 'Band';
					break;
				case 2 : $data2['type'] = 'Group';
					break;
				default : $data2['type'] = 'Solo';
					break;
			}
			
			//tambah database
			//ALTER TABLE `tr_member_artist` CHANGE `a_bdate` `a_bdate` DATE NOT NULL DEFAULT '0000-00-00'
			
			$row=$this->m_member->get_user_profile($this->uid);
			
			//setting data
			$data['ispagephoto'] = false;
			$data['user_id']=$this->uid;
			$data['image_path']=$this->m_member->get_profile_path($this->uid);
			$data['name']=$row->up_name;
			$data['alias']=$row->up_alias;	
			$data['phone']=$row->up_mobile;
			$data['birthday']=$row->up_birthday;
			$data['status_code']=$row->up_status;
			$data['status']=($row->up_status==1) ? 'Married' : 'Single';
			$data['unread_messages']=$this->m_member->get_count_unread_messages($this->uid);		
			$data['count_friend_request']=$this->m_member->get_count_friend_request($this->uid);
			$data['count_notification']=$this->m_member->get_count_notification($this->uid);
			
			
			//music sidebar, my song list
		$limit=$this->config->item('limit_sidebar_fave_song_list');		

		//music sidebar, suggest song drop down menu		
		$fav_genre=$this->m_member->get_fav_genre($this->uid);
		$fav_genre=explode(", ", $fav_genre);
		$fav_artist=$this->m_member->get_fav_artist($this->uid);
		$fav_artist=explode(", ", $fav_artist);
		
		$genres=$this->m_member->get_genre();
		$artists=$this->m_member->get_artist();
		$i=0;
		$fav_genre_display='';
		$fav_genre_string='';
		$fav_artist_display='';
		$fav_artist_string='';

		if($fav_genre[0]) //have favourite genre
		{
			foreach($genres as $g)
			{
				if($g->g_id==$fav_genre[$i])
				{
					$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$fav_genre_string .= $g->g_name.',';
					$i++;
				}
			}	
		}
		else //do not have favourite genre
		{
			foreach($genres as $g)
			{
				$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
				$fav_genre_string .= $g->g_name.',';
				$i++;
			}				
		}	
		  $data['suggest_song_option']=$fav_genre_display;
			$data['fav_genre_string']=$fav_genre_string;
			
		$i=0;	
		if($fav_artist[0]) //have favourite artist
		{
			foreach($artists as $a)
			{
				if($a->ID==$fav_artist[$i])
				{
					$fav_artist_display[$i]=array('id'=>$a->ID, 'name'=>$a->artist_name);
					$fav_artist_string .= $a->artist_name.',';
					$i++;
				}
			}	
		}
		else //do not have favourite genre
		{
			foreach($artists as $a)
			{
				$fav_artist_display[$i]=array('id'=>$a->ID, 'name'=>$a->artist_name);
				$fav_artist_string .= $a->artist_name.',';
				$i++;
			}				
		}	
		
		$data['fav_artist_string']=$fav_artist_string;

		$genre_id=$fav_genre_display[0]['id'];
		$limit=$this->config->item('limit_sidebar_suggest_song_list');		
		$data['song_by_genre']=$this->m_member->get_song_by_genre($genre_id, $limit);
		
		$data['recommended_track']=$this->m_home->get_recommended_track();
		$new_track_limit=$this->config->item('limit_new_track');
		$data['new_track']=$this->m_home->get_new_track($new_track_limit);
		
		$data['artist_list']=$this->m_member->get_artist_member($row->up_uid, 3);
		$data['count_my_artist']=$this->m_member->get_count_artist_member($row->up_uid);
		
		$data['recommend_friends']=$this->m_member->get_recommendation($row->up_uid, 5);

		//render view
		$this->template->write('head_title', 'Profile');
		$this->template->write('keywords', '');
		$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
		$this->template->write_view('sidebar_left', 'pm_side_artist_profile', $data2, '');
		$this->template->write_view('middle_content', 'pm_artist_info', $data2, '');
		if($this->m_artist->artist_member($this->uid,$id)){
				$this->template->write_view('sidebar_right', 'pm_artist_side_profile_right', $id, '');
			}else{
				$this->template->write_view('sidebar_right', 'pm_artist_side_right', $data, '');
			}
		$this->template->render();	      			
		}
	}

	function profile_edit()
	{
		if (!$this->ion_auth->logged_in()) //if not logged in
		{
			//redirect them to the login page
			redirect('home', 'refresh');
		}
		if(!isset($this->session->userdata['artist_id'])){
			redirect($_SERVER['HTTP_REFERER'], 'refresh');
		}

   		$this->load->library('form_validation');
		$this->load->helper('form');
		
		//setting up viewer
		$page_req="pm_profile_idx";
		$data['foot_js']=js_asset('jquery.imgareaselect.pack.js');
      	$data['foot_script']="
			function preview(img, selection) {
			    if (!selection.width || !selection.height)
			        return;

			    var scaleX = 100 / selection.width;
			    var scaleY = 100 / selection.height;

			    $('#preview img').css({
			        width: Math.round(scaleX * 300),
			        height: Math.round(scaleY * 300),
			        marginLeft: -Math.round(scaleX * selection.x1),
			        marginTop: -Math.round(scaleY * selection.y1)
			    }); 
			}

			$(function () {
			    $('#photo').imgAreaSelect({ aspectRatio: '1:1', handles: true,
			        fadeSpeed: 200, onSelectChange: preview });
			});	      
		";		
		//setting data
		$row=$this->m_member->get_user_profile($this->uid);
		$row2=$this->m_artist->getArtistProfile($this->session->userdata['artist_id']);
		
		$data2['name']=$row2->artist_name;
			$data2['page_name']=$row2->page_name;
			$data2['gb']=$row2->gb_member;
			$data2['birthday']=$row2->a_bdate;
			
			//birthday
			$birthday=explode("-", $row2->a_bdate);		
			$data2['birth_month']=$birthday[1];
			$data2['birth_date']=$birthday[2];
			$data2['birth_year']=$birthday[0];
			
			$genre=$row2->genre;
			$genre=explode(", ", $genre);
			
			$genre_list=$this->m_member->get_genre();
			
			$data2['genres']=$genre_list;
			
			$genre_string='';
			$genre_display='';
			$i=0;
			
			$len = count($genre);
			
			if($genre[0]) //have genre
			{
				foreach($genre_list as $g)
				{
					if($g->g_id==$genre[$i])
					{
						$genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
						$genre_string .= $g->g_name.',';
						$i++;
					}
					if($i = $len) break;
				}	
			}
			else //do not have favourite genre
			{
				foreach($genre_list as $g)
				{
					$genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$genre_string .= $g->g_name.',';
					$i++;
				}				
			}	
			$data2['genre_display']=$genre_display;
			$data2['genre_string']=$genre_string;
			
			$data2['label']=$row2->record_label;
			$data2['manager']=$row2->manager;
			$data2['contact']=$row2->cnumber;
			$data2['content']=$row2->c_officer;
			$data2['link']=$row2->other_link;
			$data2['description']=$row2->gb_description;
			
			//edit 19 jan 2012
			$tmp = $this->m_artist->get_album_song_by_artist($this->session->userdata['artist_id'],1);
			if($tmp) {
				$data['album_song'] = $tmp[0];
				if($data['album_song']){
						$data['song_list']=$this->m_artist->get_song_by_album($data['album_song']->as_id,5);
				}
			}
			else $data['album_song'] = $tmp;
			
			//setting data
			$data2['artist_name']=$row2->artist_name;
			$data2['artist_id']=$this->session->userdata['artist_id'];
			$data2['title']=$row2->title;
			$data2['genre']=$genre;
			
			$data2['ispagephoto']=false;
			
			switch($row2->type){
				case 0 : $data2['type'] = 'Solo';
					break;
				case 1 : $data2['type'] = 'Band';
					break;
				case 2 : $data2['type'] = 'Group';
					break;
				default : $data2['type'] = 'Solo';
					break;
			}
			
			//tambah database
			//ALTER TABLE `tr_member_artist` CHANGE `a_bdate` `a_bdate` DATE NOT NULL DEFAULT '0000-00-00'
			//ALTER TABLE `tr_member_artist` CHANGE `genre` `genre` VARCHAR( 100 ) NOT NULL 
		
		//sidebar
		$data['user_id']=$this->uid;
		$data['image_path']=$this->m_member->get_profile_path($this->uid);
		$data['name']=$row->up_name;
		$data['alias']=$row->up_alias;

		//Adi 26 Dec 2011
		$data['unread_messages']=$this->m_member->get_count_unread_messages($this->uid);		
		$data['count_friend_request']=$this->m_member->get_count_friend_request($this->uid);
		$data['count_notification']=$this->m_member->get_count_notification($this->uid);		
		
		/*****  UPLOAD AND CROP FOTO ********/
			
			$data['upload_path']        = $upload_path          = "assets/images/artist/" ;
			$data['destination_thumbs'] = $destination_thumbs   = "assets/images/artist/thumbnail/" ;
			$data['destination_medium'] = $destination_medium   = "assets/images/artist/medium/" ;

			$size_medium_width  = 300 ;
			$size_medium_height = 300 ;


			$data['large_photo_exists'] = $data['thumb_photo_exists'] = $data['error'] = NULL ;
			$data['thumb_width']        = $thumb_width  = 100 ;
			$data['thumb_height']       = $thumb_height = 100 ;
			
			$data['img']['file_name'] = '';
			
			if (($this->input->post('upload'))) {
				$config['upload_path']  = $upload_path ;
				$config['allowed_types']= 'gif|jpg|png|jpeg';
				$config['max_size']     = '2000';
				$config['max_width']    = '185';
				$config['max_height']   = '200';
				$config['file_name'] = $id;

				if(file_exists($upload_path.$id.'.jpg')){
					unlink($upload_path.$id.'.jpg');
				}else if(file_exists($upload_path.$id.'.gif')){
					unlink($upload_path.$id.'.gif');
				}else if(file_exists($upload_path.$id.'.png')){
					unlink($upload_path.$id.'.png');
				}else if(file_exists($upload_path.$id.'.jpeg')){
					unlink($upload_path.$id.'.jpeg');
				}
				
				if(file_exists($destination_thumbs.$id.'.jpg')){
					unlink($destination_thumbs.$id.'.jpg');
				}else if(file_exists($destination_thumbs.$id.'.gif')){
					unlink($destination_thumbs.$id.'.gif');
				}else if(file_exists($destination_thumbs.$id.'.png')){
					unlink($destination_thumbs.$id.'.png');
				}else if(file_exists($destination_thumbs.$id.'.jpeg')){
					unlink($destination_thumbs.$id.'.jpeg');
				}
				
				if(file_exists($destination_medium.$id.'.jpg')){
					unlink($destination_medium.$id.'.jpg');
				}else if(file_exists($destination_medium.$id.'.gif')){
					unlink($destination_medium.$id.'.gif');
				}else if(file_exists($destination_medium.$id.'.png')){
					unlink($destination_medium.$id.'.png');
				}else if(file_exists($destination_medium.$id.'.jpeg')){
					unlink($destination_medium.$id.'.jpeg');
				}
				
				$this->load->library('upload', $config);

				if ($this->upload->do_upload("image")) {
					$data['img']	 = $this->upload->data();

					$resize = $this->image_moo
								->load($upload_path . $data['img']['file_name'])
								->resize($size_medium_width,$size_medium_height)
								->save($destination_medium . $data['img']['file_name']) ;

					if ($resize) {
						list($medium_width,$medium_height) = getimagesize($destination_medium . $data['img']['file_name']);

						$data['img']['image_width']    = $medium_width ;
						$data['img']['image_height']   = $medium_height ;

						// For Auto Selection, we must define X1, X2, Y1, and Y2
						$data['img']['x1']  = ($medium_width/2) - ($thumb_width/2) ;
						$data['img']['x2']  = ($medium_width/2) + ($thumb_width/2) ;
						$data['img']['y1']  = ($medium_height/2) - ($thumb_height/2) ;
						$data['img']['y2']  = ($medium_height/2) + ($thumb_height/2) ;
					}
					
					$data['large_photo_exists']  = "<img src=\"".base_url() . $destination_medium.$data['img']['file_name']."\" alt=\"Medium Image\"/>";
				}
			}
			elseif (($this->input->post('upload_thumbnail'))) {
				$x1 = $this->input->post('x1',TRUE) ;
				$y1 = $this->input->post('y1',TRUE) ;
				$x2 = $this->input->post('x2',TRUE) ;
				$y2 = $this->input->post('y2',TRUE) ;
				$w  = $this->input->post('w',TRUE) ;
				$h  = $this->input->post('h',TRUE) ;

				$file_name = $this->input->post('file_name',TRUE) ;
				
				if ($file_name) {
					$this->image_moo
						->load($destination_medium . $file_name)
						->crop($x1,$y1,$x2,$y2)
						->save($destination_thumbs . $file_name)
						->load($destination_thumbs . $file_name)
						->resize($data['thumb_width'],$data['thumb_height'])
						->save($destination_thumbs . $file_name,TRUE) ;


					if ($this->image_moo->errors) {
						$data['error'] = $this->image_moo->display_errors() ;
					}
					else {
					//tambahan adi 11 jan 2012. buat session data artist
					$newdata = array('artist_id'  => $id);
					$this->session->set_userdata($newdata);
			
						$data['thumb_photo_exists'] = "<img src=\"".base_url() . $destination_thumbs . $file_name."\" alt=\"Thumbnail Image\"/>";
						$data['large_photo_exists'] = "<img src=\"".base_url() . $destination_medium . $file_name."\" alt=\"Medium Image\"/>";
						redirect('artist/invite_fans/'.$id);
					}
				}

			}
			$template_crop['template'] = 'template_crop.php';
			$template_crop['regions'] = array(
				'keyword',
			   'middle_content',
			   'sidebar_left',
			   'sidebar_right',
			   'profile_menu'
			);
			$template_crop['parser'] = 'parser';
			$template_crop['parser_method'] = 'parse';
			$template_crop['parse_template'] = FALSE;
			$template_crop['regions']['keywords'] = array('content' => array('Popmaya'));
			$template_crop['regions']['header'] = array('content' => array(anchor('', '<h1 class="ir" id="logo">Popmaya</h1>','')));
			$template_crop['regions']['head_title'] = array('content' => array('Popmaya &rsaquo; '));
			$template_crop['regions']['footer'] = array('content' => array('Copyright &copy; 2011 Popmaya. '.anchor(prep_url('www.theideafield.com'), 'POWERED BY THE IDEAFIELD', 'class="web_dev" title="web developent by www.theideafield.com"')));

			$this->template->add_template('template_crop',$template_crop,true);
			
			/*upload crop end*/
				
		//render view
		$this->template->write('head_title', 'Profile');
		$this->template->write('keywords', '');
      	$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
	  	$this->template->write_view('sidebar_left', 'pm_side_artist_profile', $data2, '');
		$this->template->write_view('middle_content', 'pm_artist_edit', $data2, '');
		$this->template->write_view('sidebar_right', 'pm_artist_side_profile_right', $data2, '');
		$this->template->render();
	}
	
	function profile_edit_process()
	{
   		$this->load->library('form_validation');
		$this->load->helper('form');

		//validation rules
		$this->form_validation->set_rules('page_name', 'Page Name', 'trim|required|prep_for_form|htmlspecialchars|encode_php_tags|xss_clean|min_length[5]|max_length[20]');
		$this->form_validation->set_rules('name', 'Artist Name', 'trim|required|prep_for_form|htmlspecialchars|encode_php_tags|xss_clean|min_length[5]|max_length[20]');
    	
		$this->form_validation->set_rules('label', 'Record Label', 'trim|required|prep_for_form|htmlspecialchars|encode_php_tags|xss_clean');
		$this->form_validation->set_rules('contact', 'Contact Person', 'trim|required|prep_for_form|htmlspecialchars|encode_php_tags|xss_clean');
		$this->form_validation->set_rules('manager', 'Manager', 'trim|required|prep_for_form|htmlspecialchars|encode_php_tags|xss_clean');
		$this->form_validation->set_rules('content', 'Content Officer', 'trim|prep_for_form|htmlspecialchars|encode_php_tags|xss_clean');
		$this->form_validation->set_rules('description', 'Group/Band Description', 'trim|required|prep_for_form|htmlspecialchars|encode_php_tags|xss_clean');
		$this->form_validation->set_rules('link', 'Other Link', 'trim|required|prep_for_form|htmlspecialchars|encode_php_tags|xss_clean');
		
		//validation message
    	$this->form_validation->set_message('required', '%s is required.');
    	$this->form_validation->set_message('alpha_numeric', 'Only alphanumeric characters are allowed.');
		$this->form_validation->set_message('valid_email', 'Wrong email format.');
    	$this->form_validation->set_message('min_length[5]', 'Min. character is 5 chars.');    	
    	$this->form_validation->set_message('max_length[20]', 'Max. character is 20 chars.');
    	$this->form_validation->set_message('max_length[50]', 'Max. character is 50 chars.');
		
		$this->form_validation->set_error_delimiters('','<br />');
		
		if ($this->form_validation->run() == true)
		{					
			$birthday=$this->input->post('birth_year')."-".$this->input->post('birth_month')."-".$this->input->post('birth_date');
			
			$genre=$this->input->post('genre');
			$genres="";
			for($i=0; $i < count($genre); $i++)
			{
				$genres.=$genre[$i].", ";
			}
						
			$data_update=array(
				'artist_name' => $this->input->post('name'),
				'page_name' => $this->input->post('page_name'),
				'gb_member' => $this->input->post('gb'),
				'a_bdate' => $birthday,
				'genre' => $genres,
				'record_label' => $this->input->post('label'),
				'manager' => $this->input->post('manager'),
				'cnumber' => $this->input->post('contact'),				
				'c_officer' => $this->input->post('content'),
				'other_link' => $this->input->post('link'),
				'gb_description' => $this->input->post('description')
				);			
			$this->m_artist->update_artist_profile($data_update, $this->session->userdata['artist_id']);

			$this->session->set_flashdata('message', "Edit artist profile success");
			redirect('artist/profile_edit');						
		}
		else
		{
			$this->session->set_flashdata('message', validation_errors());
			redirect('artist/profile_edit');			
		}
	}
}
?>