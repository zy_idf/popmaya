<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Photo_artist extends MX_Controller 
{
	var $user_id;
	var $limit_sidebar_friend_list;
	
   	function __construct()
	{
   		parent::__construct();

   		$this->load->library('form_validation');
   		$this->load->model('member/m_member', '', TRUE);
   		$this->load->model('home/m_home', '', TRUE);
		$this->load->model('m_artist', '', TRUE);

		$this->user_id=$this->session->userdata('user_id');
		  
		$this->config->load('my_config');
  		$this->limit_sidebar_friend_list=$this->config->item('limit_sidebar_friend_list');
//   		$this->output->enable_profiler(FALSE);
   	}

	function index($id='')
	{
		if (!$this->ion_auth->logged_in()) //if not logged in
		{
			//redirect them to the login page
			redirect('home', 'refresh');
		}
		else if (!isset($this->session->userdata['artist_id'])) //if not use page as artist
		{
			//redirect them to the artist page
			redirect($_SERVER['HTTP_REFERER']);
		}
		else
		{
			//setting up viewer
			$page_req="pm_profile_idx";
			$data['foot_js']=css_asset('plugin/sliderStyle.css').js_asset('jquery.nivo.slider.pack.js').  js_asset('jwysiwyg/jquery.wysiwyg.js').js_asset('jRecorder/jRecorder.js').js_asset('fileuploader.js').js_asset('jPlayer/jquery.jplayer.min.js');
	      	$data['foot_script']='
		        var sics = $(".share_icons"); sics.hide();
		        $(\'#slider\').nivoSlider({speed:5000});

		        $("#tab ul li.middle a").addClass("active").show(); 
 	           	$("#tab2").show();
 	           	/* ^zy */
 	           	var the_player = $("#song_player");
 	           	
 	           	the_player.jPlayer({
 	           		ready: function(){
						console.log("player ready."); 	           			
 	           		},
 	           		play: function(){
 	           			console.log("play file.");	
 	           		},
 	           		volume: "0.4",
 	           		swfPath: "'.site_url().'assets/js/jPlayer",
					cssSelectorAncestor: "#wrapper",
					supplied: "mp3",
					wmode: "window"
				});

				var prev_val=0;
 	           	$("#wrapper .btn_play").click(function(e){
 	           		the_player.jPlayer("setMedia", {
						mp3: $(this).attr("href")
					});
					the_player.jPlayer("play");
					var nxt_el=$(this).parent().next().find(".ttl_play");
					var ctr=nxt_el.html();
					nxt_el.fadeOut(); 
					var ctr2=parseInt(ctr);
					if(prev_val==0){
						ctr2+=1;
						prev_val=1;
					}
					nxt_el.html(ctr2); nxt_el.fadeIn();	
					return false;
		        });
		        /* ^zy.end */
		      ';	

			$row=$this->m_artist->getUserProfile($this->user_id);

			if($id=='') //my artist profile
			{
				$row2=$this->m_artist->getArtistProfile($this->session->userdata['artist_id']);
			}
			else //other artist profile
			{
				$row2=$this->m_artist->getArtistProfile($id);
			}
			
			$data['artist_id']=$this->session->userdata['artist_id'];
			$data['artist_name']=$row2->artist_name;
			$data['title']=$row2->title;
			$data['genre']=$this->m_artist->get_genre_name($row2->genre);
			switch($row2->type){
				case 0 : $data['type'] = 'Solo';
					break;
				case 1 : $data['type'] = 'Band';
					break;
				case 2 : $data['type'] = 'Group';
					break;
				default : $data['type'] = 'Solo';
					break;
			}
			//setting data
			$data['user_id']=$this->user_id;
			$data['image_path']=$this->m_member->get_profile_path($this->user_id);
			$data['name']=$row->up_name;
			$data['alias']=$row->up_alias;	
			$data['unread_messages']=$this->m_member->get_count_unread_messages($this->user_id);		
			$data['count_friend_request']=$this->m_member->get_count_friend_request($this->user_id);
			$data['count_notification']=$this->m_member->get_count_notification($this->user_id);
			
			$data['gender_code']=$row->up_gender;
			$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
			$data['city']=$row->up_city;
			$data['country']=$row->up_country;
			$data['friends']=$this->m_member->get_friends($this->user_id, $this->limit_sidebar_friend_list);
			
			//music sidebar, my song list
  			$limit=$this->config->item('limit_sidebar_fave_song_list');
  			$data['song_list']=$this->m_member->get_fav_song($this->user_id, $limit);		

  			//music sidebar, suggest song drop down menu		
	  		$fav_genre=$this->m_member->get_fav_genre($this->user_id);
	  		$fav_genre=explode(", ", $fav_genre);
	  		$genres=$this->m_member->get_genre();
	  		$i=0;
	  		$fav_genre_display='';

			if($fav_genre[0]) //have favourite genre
			{
				foreach($genres as $g)
				{
					if($g->g_id==$fav_genre[$i])
					{
						$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
						$i++;
					}
				}	
			}
			else //do not have favourite genre
			{
				foreach($genres as $g)
				{
					$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$i++;
				}				
			}
		
			$data['suggest_song_option']=$fav_genre_display;

		    $genre_id=$fav_genre_display[0]['id'];
	  		$limit=$this->config->item('limit_sidebar_suggest_song_list');		
	  		$data['song_by_genre']=$this->m_member->get_song_by_genre($genre_id, $limit);

	  		$data['recommended_track']=$this->m_home->get_recommended_track();
			$new_track_limit=$this->config->item('limit_new_track');
			$data['new_track']=$this->m_home->get_new_track($new_track_limit);
			
			//save album
			if($this->input->post('createalbum')){
				$album = array(
					'artistID' => $data['artist_id'] ,
					'pageID' => 0,
					'articleID' => 0,
					'title' => $this->input->post('title'),
					'image' => 'adam_thumb.jpg',
					'description' => $this->input->post('desc'),
					'status' => 0,
					'post' => 0
				);
				$lastid = $this->m_artist->save_album_artist($album);
				
				$dir = "./assets/images/artist/album/".$lastid."/";
				if(!is_dir($dir)){
					mkdir($dir,0755);
				}
			}
			$data['artist_album'] = $this->m_artist->get_album_artist($data['artist_id']);
			
			//save picture to album
			if($this->input->post("savephoto")){
				//create directory	
				$dir="./assets/images/artist/album/".$this->input->post('album')."/";
				
				$config['upload_path']=$dir;
				$config['allowed_types']='png|jpg|gif|bmp';
				$config['overwrite']=FALSE;
				
				//upload
				$this->load->library('upload', $config);

				if (!$this->upload->do_upload("photo_album")){
					$error=array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('message', $error);
					$data['error']=$error;	
					print_r($error);
					redirect('artist/photo_artist','refresh');
				}
				
				$image_upload = array('upload_data' => $this->upload->data());
				//echo $data['upload_data']['file_name'];
				$add_photo = array(
					'artistID' => $data['artist_id'] ,
					'title' => '',
					'image' => $image_upload['upload_data']['file_name'],
					'description' => $this->input->post('desc'),
					'post' => 0,
					'rate' => 0,					
					'view' => 0,
					'band' => 0,
					'albumID' => $this->input->post('album')
				);
				$this->m_artist->save_photo_artist($add_photo);
				redirect($_SERVER['HTTP_REFERER'], 'refresh');				
			}
			$data['ispagephoto'] = true;
			$data['recommend_friends']=$this->m_member->get_recommendation($this->user_id, 5);
			$data['artist_list']=$this->m_member->get_artist_member($row->up_uid, 3);
			$data['count_my_artist']=$this->m_member->get_count_artist_member($row->up_uid);

   			$data['count_friend_request']=$this->m_member->get_count_friend_request($this->user_id);
			$data['count_notification']=$this->m_member->get_count_notification($this->user_id);
			
			//edit 19 jan 2012
			$tmp = $this->m_artist->get_album_song_by_artist($id,1);
			if($tmp) {
				$data['album_song'] = $tmp[0];
				if($data['album_song']){
						$data['song_list']=$this->m_artist->get_song_by_album($data['album_song']->as_id,5);
				}
			}
			else $data['album_song'] = $tmp;

			//render view
			$this->template->write('head_title', 'Profile');
			$this->template->write('keywords', '');
		    $this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
			$this->template->write_view('sidebar_left', 'pm_side_artist_profile', $data, '');			
			$this->template->write_view('middle_content', 'pm_photo_artist_idx', $data, '');
			$this->template->write_view('sidebar_right', 'pm_artist_side_profile_right', $data, '');
			$this->template->render();
		}
	}
	
	function album($album_name='', $id='')
	{
		if (!$this->ion_auth->logged_in()) //if not logged in
		{
			//redirect them to the login page
			redirect('home', 'refresh');
		}
		else
		{
			$data['ispagephoto'] = false;
			//setting up viewer
			$page_req="pm_profile_idx";
			$data['foot_js']=css_asset('plugin/sliderStyle.css').js_asset('jquery.nivo.slider.pack.js').  js_asset('jwysiwyg/jquery.wysiwyg.js').js_asset('jRecorder/jRecorder.js').js_asset('fileuploader.js').js_asset('jPlayer/jquery.jplayer.min.js');
	      	$data['foot_script']='
		        var sics = $(".share_icons"); sics.hide();
		        $(\'#slider\').nivoSlider({speed:5000});

		        $("#tab ul li.middle a").addClass("active").show(); 
 	           	$("#tab2").show();
 	           	/* ^zy */
 	           	var the_player = $("#song_player");
 	           	
 	           	the_player.jPlayer({
 	           		ready: function(){
						console.log("player ready."); 	           			
 	           		},
 	           		play: function(){
 	           			console.log("play file.");	
 	           		},
 	           		volume: "0.4",
 	           		swfPath: "'.site_url().'assets/js/jPlayer",
					cssSelectorAncestor: "#wrapper",
					supplied: "mp3",
					wmode: "window"
				});

				var prev_val=0;
 	           	$("#wrapper .btn_play").click(function(e){
 	           		the_player.jPlayer("setMedia", {
						mp3: $(this).attr("href")
					});
					the_player.jPlayer("play");
					var nxt_el=$(this).parent().next().find(".ttl_play");
					var ctr=nxt_el.html();
					nxt_el.fadeOut(); 
					var ctr2=parseInt(ctr);
					if(prev_val==0){
						ctr2+=1;
						prev_val=1;
					}
					nxt_el.html(ctr2); nxt_el.fadeIn();	
					return false;
		        });

		        $(".form_photo_comment").submit(function(){
		        	var photo_comment = $(this).children(":first");
		        	var id_foto = $(this).children(":first").next().val();		        	
		        	var par_el = $(this).parent();
		        	// ajax post
		        	if(photo_comment.val().length>2){
		        	
      					$.ajax({
           					type: "POST",
           					url: "'.site_url().'member/photo/photo_comment_process",
           					async: true,
           					cache: false,
           					timeout:50000,
           					data: {req_type: \'ajax\', comment : photo_comment.val(), photo_id : id_foto },
           					success: function(data){
           						// append to list_photo_comment
           						par_el.find(".list_photo_comment").prepend("<li>"+data+"<li>");
           						console.log(data);
           					},
           					error: function(XMLHttpRequest, textStatus, errorThrown){ alert(errorThrown); },
      						});
    				}else{
      					alert("Komentar kurang dari 3 huruf, mohon ulangi.");
      					photo_comment.val(\'\');
    				}

		        	return false;
		        });

		        $(".preview_image").click(function(){
		        	$(".zyBox").fadeIn();
		        });
		        $(".close_zyBox").click(function(){
		        	$(".zyBox").fadeOut();
		        });
		        /* ^zy.end */
		      ';	      

			if($id=='') //my profile
			{
				$row=$this->m_member->get_user_profile($this->user_id);
			}
			else //other member profile
			{
				$row=$this->m_member->get_user_profile($id);
			}
			
			// belom ngambil by user session, ini biar jalan dulu aja
			$row=$this->m_member->get_user_profile($this->user_id);
			
			//setting data
			$data['user_id']=$this->user_id;
			$data['image_path']=$this->m_member->get_profile_path($this->user_id);
			$data['name']=$row->up_name;
			$data['alias']=$row->up_alias;	
			$data['unread_messages']=$this->m_member->get_count_unread_messages($this->user_id);		
			$data['count_friend_request']=$this->m_member->get_count_friend_request($this->user_id);
			$data['count_notification']=$this->m_member->get_count_notification($this->user_id);
			
			$data['gender_code']=$row->up_gender;
			$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
			$data['city']=$row->up_city;
			$data['country']=$row->up_country;
			$data['friends']=$this->m_member->get_friends($this->user_id, $this->limit_sidebar_friend_list);
			
			//music sidebar, my song list
  			$limit=$this->config->item('limit_sidebar_fave_song_list');
  			$data['song_list']=$this->m_member->get_fav_song($this->user_id, $limit);		

  			//music sidebar, suggest song drop down menu		
	  		$fav_genre=$this->m_member->get_fav_genre($this->user_id);
	  		$fav_genre=explode(", ", $fav_genre);
	  		$genres=$this->m_member->get_genre();
	  		$i=0;
	  		$fav_genre_display='';

			if($fav_genre[0]) //have favourite genre
			{
				foreach($genres as $g)
				{
					if($g->g_id==$fav_genre[$i])
					{
						$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
						$i++;
					}
				}	
			}
			else //do not have favourite genre
			{
				foreach($genres as $g)
				{
					$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$i++;
				}				
			}
		
			$data['suggest_song_option']=$fav_genre_display;

		    $genre_id=$fav_genre_display[0]['id'];
	  		$limit=$this->config->item('limit_sidebar_suggest_song_list');		
	  		$data['song_by_genre']=$this->m_member->get_song_by_genre($genre_id, $limit);

	  		$data['recommended_track']=$this->m_home->get_recommended_track();
			$new_track_limit=$this->config->item('limit_new_track');
			$data['new_track']=$this->m_home->get_new_track($new_track_limit);

			$data['user_photo_album']=$this->m_member->get_photo_album($id);
			$data['recommend_friends']=$this->m_member->get_recommendation($this->user_id, 5);

			$data['count_friend_request']=$this->m_member->get_count_friend_request($this->user_id);
			$data['count_notification']=$this->m_member->get_count_notification($this->user_id);
			$data['artist_list']=$this->m_member->get_artist_member($row->up_uid, 3);
			$data['count_my_artist']=$this->m_member->get_count_artist_member($row->up_uid);
			
			//nisa 26 des
			$data['photo_comments']=$this->m_member->get_photo_comment($row->up_uid);
			//end nisa 26 des
   
			//render view
			$this->template->write('head_title', 'Profile');
			$this->template->write('keywords', '');
		    $this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
			$this->template->write_view('sidebar_left', 'pm_side_newbie', $data, '');			
			$this->template->write_view('middle_content', 'pm_photo_list', $data, '');
			$this->template->write_view('sidebar_right', 'general/sidebar_profile_right', $data, '');
			$this->template->render();
		}
		
	}
	
	function change_avatar($photo_id=""){
		if (!$this->ion_auth->logged_in()) //if not logged in
		{
			//redirect them to the login page
			redirect('home', 'refresh');
		}
		else
		{
			$photo = $this->m_member->get_single_photo($photo_id);			
			$source = "./assets/images/album/".$photo[0]->image;
			$dest = "./assets/images/profile/0000/00/00/".$this->user_id.".jpg";
			copy ($source, $dest);
		
			$dest = "./assets/images/profile/0000/00/00/".$this->user_id."_thumb.jpg";
			copy ($source, $dest);
			redirect("member/photo","refresh");
			
		}
	}
	
	/* nisa 26 des. photo like */
	function photo_like_process($photo_id)
	{
		$this->m_member->photo_like($photo_id, $this->user_id);
		redirect($_SERVER['HTTP_REFERER'], 'refresh');
	}
	
	/* nisa 26 des. photo comment */
	function photo_comment_process()
	{
		$this->m_member->photo_comment($this->input->post('comment'), $this->input->post('photo_id'), $this->user_id, $this->session->userdata('ip_address'));
		if($this->input->post('req_type')!='ajax'){
			redirect($_SERVER['HTTP_REFERER'], 'refresh');	
		}else{
			$uname=$this->m_member->get_user_profile($this->user_id)->up_name;
			echo $uname.' - '.$this->input->post('comment');
		}
				
	}
	
	/* nisa 26 des. hapus foto */
	function photo_delete_process($photo_id)
	{
		$this->m_member->photo_delete($photo_id);
		redirect($_SERVER['HTTP_REFERER'], 'refresh');				
	}
	
	/* nisa 26 des. hapus album */
	function album_delete_process($album_id)
	{
		$this->m_member->album_delete($album_id);
		redirect($_SERVER['HTTP_REFERER'], 'refresh');						
	}
	
}