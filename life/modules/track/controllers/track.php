<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Track extends MX_Controller 
{
	var $uid;
	
   	function __construct()
	{
   		parent::__construct();

   		$this->load->library('form_validation');
   		$this->load->model('m_track', '', TRUE);
   		$this->load->model('member/m_member', '', TRUE);


		/*============= tambahan ==================*/
		//Adi 26 Dec 2011
		$this->load->model('home/m_home', '', TRUE);
		$this->uid=$this->session->userdata('user_id');

   		//$this->output->enable_profiler(TRUE);
   	}

   	function add_to_playlist($id=''){ /* zy. masukin ini ke global library or helper */
		if($this->session->userdata('user_id')){
			//$arr=explode("/", $this->input->post('track_id'));
	   		//$song_id=$arr[6]; // kalo yg online, segment 6, kalo local : segment 7
	   		// cek apakah usr dengan song id tsb sudah add to playlist?
	   		$res=$this->db->get_where('tr_fave_song', array('memberID'=>$this->session->userdata('user_id'), 'songID'=>$id));
			if($res->result()){
				$row=$res->row();
		   		if($row->fs_id){
		   			echo 'Lagu sudah ada di playlist utama Anda.';
					redirect(site_url()."track",'refresh');
		   		}
			}else{
				$this->db->insert('tr_fave_song', array('memberID'=>$this->session->userdata('user_id'), 'songID'=>$id));
	   			if($this->db->insert_id()){
	   				echo 'Lagu dengan id '.$id.' berhasil ditambahkan ke playlist Anda.';
					redirect(site_url()."track",'refresh');
	   			}else{
	   				echo 'Lagu gagal ditambahkan ke playlist Anda.';
					redirect(site_url()."track",'refresh');
	   			}
			}
		}else{
			echo 'Anda belum login.';
		}   		
   		
   }

	//------------------------------------------------------------------------------------------------------------------------frontpage
   	function index()
	{	
		
		$data['foot_js']=css_asset('plugin/sliderStyle.css').js_asset('jquery.nivo.slider.pack.js').js_asset('jPlayer/jquery.jplayer.min.js');
	    $data['foot_script']='
			var sics = $(".share_icons"); sics.hide();
		    $(\'#slider\').nivoSlider({speed:5000});

		    $("#tab ul li.middle a").addClass("active").show(); 
 	        $("#tab2").show();
			
			/* ^zy */
			 	var the_player = $("#song_player");

			 	the_player.jPlayer({
			 	    ready: function(){
						console.log("player ready."); 	           			
			 	    },
			 	    play: function(){
			 	    	console.log("play file.");	
			 	    },
			 	    volume: "0.4",
			 	    swfPath: "'.site_url().'assets/js/jPlayer",
					cssSelectorAncestor: "#wrapper",
					supplied: "mp3",
					wmode: "window"
				});           	

				var prev_val=0;
			 	$("#wrapper .btn_play").click(function(e){
			 	    the_player.jPlayer("setMedia", {
						mp3: $(this).attr("href")
					});
					the_player.jPlayer("play");
					var nxt_el=$(this).parent().next().find(".ttl_play");
					var ctr=nxt_el.html();
					nxt_el.fadeOut(); 
					var ctr2=parseInt(ctr);
					if(prev_val==0){
						ctr2+=1;
						prev_val=1;
					}
					nxt_el.html(ctr2); nxt_el.fadeIn();	
					return false;
				});
				
				$(".add_to_playlist").click(function(){
					$.post("'.site_url().'track/add_to_playlist", { track_id: $(this).attr("href") }, function(data) {
   						$(".list-left p.bottom").html();
   						alert(data);
   						$(".list-left p.bottom").html("Data berhasil di tambahkan."); /* harusnya disini load content ajax utk playlist */
 					});
					return false;	
				});

			/* ^zy.end */
			
		    ';	      
			
			$this->config->load('my_config');
			$limit_sidebar_friend_list=$this->config->item('limit_sidebar_friend_list');
			if($this->uid){
				
				$row=$this->m_member->get_user_profile($this->uid);
				$data['user_id']=$this->uid;
				
				$data['name']=$row->up_name;
				$data['alias']=$row->up_alias;	
				$data['unread_messages']=$this->m_member->get_count_unread_messages($this->uid);		
				$data['count_friend_request']=$this->m_member->get_count_friend_request($this->uid);
				$data['count_notification']=$this->m_member->get_count_notification($this->uid);

				$data['gender_code']=$row->up_gender;
				$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
				$data['city']=$row->up_city;
				$data['country']=$row->up_country;
				$data['friends']=$this->m_member->get_friends($this->uid, $limit_sidebar_friend_list);
			}
			
			$data['up_uid']=$row->up_uid;
			/* pagination */ 
        $uri_segment = 4;
        if($this->uri->segment($uri_segment)==''){ $offset=0; }else{ $offset=$this->uri->segment($uri_segment); }
        $data['start_no']= $offset+1;

        $this->load->library('pagination');
        $config['base_url'] = site_url().'track/index/';
        $data['jml_member']=$config['total_rows'] = $this->m_track->count_all('');
        $config['per_page'] = $this->config->item('limit_new_track');
        $config['uri_segment'] = $uri_segment;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        /* pagination.end */
		
		//Suggest Song --> duplicate form member
		//music sidebar, suggest song drop down menu	
		$fav_genre=$this->m_track->get_fav_genre($this->uid);
		$fav_genre=explode(", ", $fav_genre);
		$fav_artist=$this->m_track->get_fav_artist($this->uid);
		$fav_artist=explode(", ", $fav_artist);
		
		$genres=$this->m_track->get_genre();
		$artists=$this->m_track->get_artist();
		$i=0;
		$fav_genre_display='';
		$fav_genre_string='';
		$fav_artist_display='';
		$fav_artist_string='';

		if($fav_genre[0]) //have favourite genre
		{
			foreach($genres as $g)
			{
				if($g->g_id==$fav_genre[$i])
				{
					$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$fav_genre_string .= $g->g_name.',';
					$i++;
				}
			}	
		}
		else //do not have favourite genre
		{
			foreach($genres as $g)
			{
				$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
				$fav_genre_string .= $g->g_name.',';
				$i++;
			}				
		}	
		  $data['suggest_song_option']=$fav_genre_display;
			$data['fav_genre_string']=$fav_genre_string;
			
		$i=0;	
		if($fav_artist[0]) //have favourite artist
		{
			foreach($artists as $a)
			{
				if($a->ID==$fav_artist[$i])
				{
					$fav_artist_display[$i]=array('id'=>$a->ID, 'name'=>$a->artist_name);
					$fav_artist_string .= $a->artist_name.',';
					$i++;
				}
			}	
		}
		else //do not have favourite genre
		{
			foreach($artists as $a)
			{
				$fav_artist_display[$i]=array('id'=>$a->ID, 'name'=>$a->artist_name);
				$fav_artist_string .= $a->artist_name.',';
				$i++;
			}				
		}	
		
			$data['fav_artist_string']=$fav_artist_string;

		  $genre_id=$fav_genre_display[0]['id'];
		$limit=$this->config->item('limit_sidebar_suggest_song_list');		
		$data['song_by_genre']=$this->m_track->get_song_by_genre($genre_id, $limit);
		//end duplicate suggest song

        $data['list_songs']=$this->m_track->get_paged_list($this->config->item('limit_new_track'), $offset, '');
		$data['ads_data']=$this->m_track->get_image_ads();
		
		/****================================ tambahan ===================================****/ 
		//Adi 27 Dec 2011
		$data['unread_messages']=$this->m_member->get_count_unread_messages($this->uid);		
		$data['count_friend_request']=$this->m_member->get_count_friend_request($this->uid);
		$data['count_notification']=$this->m_member->get_count_notification($this->uid);
	
		
		$data['recommended_track']=$this->m_home->get_recommended_track();
		$new_track_limit=$this->config->item('limit_new_track');
		$data['new_track']=$this->m_home->get_new_track($new_track_limit);
			
		/*================================end tambahan ========================================*/
		
			//render view
			$this->template->write('head_title', 'Artist');
			$this->template->write('keywords', '');
	      	$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
		  	$this->template->write_view('sidebar_left', 'pm_track_side', $data, '');
			$this->template->write_view('middle_content', 'pm_track_idx', $data, '');
			$this->template->write_view('sidebar_right', 'pm_track_side_right', $data, '');
			$this->template->render();			      			

   	}


	//tambahan
	//adi 26 dec 2011
	//parameter id song untuk song/track detail
	function detail($id='')
	{	$data['foot_js']=css_asset('plugin/sliderStyle.css').js_asset('jquery.nivo.slider.pack.js').js_asset('jPlayer/jquery.jplayer.min.js');
	    $data['foot_script']='
			var sics = $(".share_icons"); sics.hide();
		    $(\'#slider\').nivoSlider({speed:5000});

		    $("#tab ul li.middle a").addClass("active").show(); 
 	        $("#tab2").show();
			
			/* ^zy */
			 	var the_player = $("#song_player");

			 	the_player.jPlayer({
			 	    ready: function(){
						console.log("player ready."); 	           			
			 	    },
			 	    play: function(){
			 	    	console.log("play file.");	
			 	    },
			 	    volume: "0.4",
			 	    swfPath: "'.site_url().'assets/js/jPlayer",
					cssSelectorAncestor: "#wrapper",
					supplied: "mp3",
					wmode: "window"
				});           	

				var prev_val=0;
			 	$("#wrapper .btn_play").click(function(e){
			 	    the_player.jPlayer("setMedia", {
						mp3: $(this).attr("href")
					});
					the_player.jPlayer("play");
					var nxt_el=$(this).parent().next().find(".ttl_play");
					var ctr=nxt_el.html();
					nxt_el.fadeOut(); 
					var ctr2=parseInt(ctr);
					if(prev_val==0){
						ctr2+=1;
						prev_val=1;
					}
					nxt_el.html(ctr2); nxt_el.fadeIn();	
					return false;
				});
				
				$(".add_to_playlist").click(function(){
					$.post("'.site_url().'track/add_to_playlist", { track_id: $(this).attr("href") }, function(data) {
   						$(".list-left p.bottom").html();
   						alert(data);
   						$(".list-left p.bottom").html("Data berhasil di tambahkan."); /* harusnya disini load content ajax utk playlist */
 					});
					return false;	
				});
				
				$(".hover_content").hide();
				$(".hover_share").hover(function() {
    				$(this).next().show();
  				});
  				$(".hover_share").click(function(){ return false; });
  				$(".hover_content").live("mouseleave", function(){ $(this).hide() }); 

  				$(".hover_share_side").hover(function() {
    				$(this).append("<div class=\"side_hover\">
					<div class="share_box">
						<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
							<a class="addthis_button_preferred_1"></a>
							<a class="addthis_button_preferred_2"></a>
							<a class="addthis_button_preferred_3"></a>
						</div>
					</div>
					</div>");
  				});
  				$(".hover_share_side").click(function(){ return false; });
  				$(".side_hover").live("mouseleave", function(){ $(".side_hover").remove() });


			/* ^zy.end */
			
		    ';	      
			
			$this->config->load('my_config');
			$limit_sidebar_friend_list=$this->config->item('limit_sidebar_friend_list');
			if($this->uid){
				$row=$this->m_member->get_user_profile($this->uid);
				$data['user_id']=$this->uid;
				$data['name']=$row->up_name;
				$data['alias']=$row->up_alias;	

				$data['gender_code']=$row->up_gender;
				$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
				$data['city']=$row->up_city;
				$data['country']=$row->up_country;
				$data['friends']=$this->m_member->get_friends($this->uid, $limit_sidebar_friend_list);
				
				//tambahan
				$data['song_info']=$this->m_track->item_info($id);
				$data['artist_name']=$this->m_track->get_artist_name($id);
				$data['album_name']=$this->m_track->get_album_name($id);
								
				
				/****================================ tambahan ===================================****/ 
				//Adi 26 Dec 2011
				$data['unread_messages']=$this->m_member->get_count_unread_messages($this->uid);		
				$data['count_friend_request']=$this->m_member->get_count_friend_request($this->uid);
				$data['count_notification']=$this->m_member->get_count_notification($this->uid);
				
				//Adi 28 dec 2011
				$data['s_id']=$id;
				$data['song_comment']=$this->m_track->get_song_comment($id);
				$data['like']=$this->m_track->get_song_like($id);
				
				//music sidebar, suggest song drop down menu		
				$fav_genre=$this->m_member->get_fav_genre($this->uid);
				$fav_genre=explode(", ", $fav_genre);
				$genres=$this->m_member->get_genre();
				$i=0;
				$fav_genre_display='';

				if($fav_genre[0]) //have favourite genre
				{
						foreach($genres as $g)
						{
							if($g->g_id==$fav_genre[$i])
							{
								$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
								$i++;
							}
						}	
					}
					else //do not have favourite genre
					{
						foreach($genres as $g)
						{
							$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
							$i++;
						}				
					}	
					$data['suggest_song_option']=$fav_genre_display;

					$genre_id=$fav_genre_display[0]['id'];
					$limit=$this->config->item('limit_sidebar_suggest_song_list');		
					$data['song_by_genre']=$this->m_member->get_song_by_genre($genre_id, $limit);
					
					$data['recommended_track']=$this->m_home->get_recommended_track();
					$new_track_limit=$this->config->item('limit_new_track');
					$data['new_track']=$this->m_home->get_new_track($new_track_limit);
				}
				
				//load ads data
				$data['ads_data']=$this->m_track->get_image_ads();
				
				/*================================end tambahan ========================================*/
			
			//render view
			$this->template->write('head_title', 'Artist');
			$this->template->write('keywords', '');
	      	$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
		  	$this->template->write_view('sidebar_left', 'pm_track_side', $data, '');
			$this->template->write_view('middle_content', 'pm_songlist_tengah', $data, '');
			$this->template->write_view('sidebar_right', 'pm_track_side_right', $data, '');
			$this->template->render();			      			

   	}


	function frontpage()
	{
		$data="";
		$data['foot_js']='';
		//render view
		$this->template->write('keyword', ' ?? ');
		$this->template->write('head_title', 'Home');
		$this->template->write_view('middle_content', 'pm_member_frontpage', $data, TRUE);
		$this->template->render();
	}
	//------------------------------------------------------------------------------------------------------------------------end frontpage	
	
	//------------------------------------------------------------------------------------------------------------------------login
   	function login()
	{
		if ($this->ion_auth->logged_in()) //already logged in so no need to access this page
		{			
			redirect("member/profile", 'refresh');
		}
		
		//validation rules
        $this->form_validation->set_rules('email', 'Email', 'trim|prep_for_form|htmlspecialchars|encode_php_tags|required|valid_email|min_length[5]|max_length[50]|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|alpha_numeric|prep_for_form|htmlspecialchars|encode_php_tags|required|min_length[5]|max_length[20]|xss_clean');
		
		//validation message
    	$this->form_validation->set_message('required', '%s is required.');
    	$this->form_validation->set_message('alpha_numeric', 'Only alphanumeric characters are allowed.');
		$this->form_validation->set_message('valid_email', 'Wrong email format.');
    	$this->form_validation->set_message('min_length[5]', 'Min. character is 5 chars.');    	
    	$this->form_validation->set_message('max_length[20]', 'Max. character is 20 chars.');
    	$this->form_validation->set_message('max_length[50]', 'Max. character is 50 chars.');
        
		//error delimiter
		$this->form_validation->set_error_delimiters('', '');
		
		if($this->form_validation->run()==TRUE) //validation success
		{
			//check to see if the user is logging in
			//check for "remember me"
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('email'), $this->input->post('password'), $remember))
			{ 	
				//if the login is successful
				//redirect them back to the home page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("member/profile", 'refresh');
			}
			else
			{ 
				//if the login was un-successful
				//redirect them back to the login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('member/frontpage', 'refresh'); 
			}			
		}
		else //validation failed
		{
			//the user is not logging in so display the login page
			//set the flash data error message if there is one			
			$this->session->set_flashdata('message', validation_errors());
			redirect('member/frontpage');
		}
	}
	
	function login_by_fb()
	{
		$fb_data = $this->session->userdata('fb_data');
		
		if((!$fb_data['uid']) or (!$fb_data['me']))
		{
			redirect('member/frontpage');
		}
		else
		{
			$data['fb_data']=$fb_data;
			
			$this->load->view('pm_member_login_facebook', $data);
		}
	}
	//------------------------------------------------------------------------------------------------------------------------end login

	//------------------------------------------------------------------------------------------------------------------------logout
	function logout()
	{
		//log the user out
		$logout = $this->ion_auth->logout();

		//redirect them back to the page they came from
		$this->session->set_flashdata('message', "Logout success");
		redirect('member/frontpage', 'refresh');
	}
	//------------------------------------------------------------------------------------------------------------------------end logout
	
	//------------------------------------------------------------------------------------------------------------------------forgot password
	function forgot()
	{
		$data="";
		$data['foot_js']='';
		//render view
		$this->template->write('keyword', ' ?? ');
		$this->template->write('head_title', 'Forgot Password');
		$this->template->write_view('middle_content', 'pm_member_forgot', $data, TRUE);
		$this->template->render();		
	}
	
	function forgot_process()
	{
		$this->form_validation->set_rules('email', 'Email', 'xss_clean|required|valid_email|min_length[5]|max_length[50]');
		
		if ($this->form_validation->run() == true)
		{
			//run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($this->input->post('email'));					
			
			if ($forgotten)  //if there were no errors
			{
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect('member/forgot_confirm', 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('member/forgot', 'refresh');
			}			
		}
		else
		{
			//no email entered
			$this->session->set_flashdata('message', validation_errors());
			redirect('member/forgot');			
		}
	}
	
	function forgot_confirm()
	{
		echo "forgot confirm";
		
		$data="";
						
		//render view
		$this->template->write('keyword', ' ?? ');
		$this->template->write('head_title', 'Forgot Password Confirm');
		$this->template->write_view('middle_content', 'pm_member_forgot_confirm', $data, TRUE);
		$this->template->render();				
	}
	
	function reset_password($code='')
	{
		if($code)
		{
			$reset = $this->ion_auth->forgotten_password_complete($code);

			if ($reset)
			{  
				//if the reset worked then send them to the login page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect('member/frontpage', 'refresh');
			}
			else
			{ 
				//if the reset didnt work then send them back to the forgot password page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('member/forgot', 'refresh');
			}
		}
		else
		{
			redirect('member/frontpage', 'refresh');
		}		
	}
	//------------------------------------------------------------------------------------------------------------------------end forgot password
   	
	//------------------------------------------------------------------------------------------------------------------------register
	function register()
	{
		//validation rules
		$this->form_validation->set_rules('fullname', 'Fullname', 'trim|required|prep_for_form|htmlspecialchars|encode_php_tags|xss_clean|min_length[5]|max_length[20]');
    	$this->form_validation->set_rules('email', 'Email', 'trim|required|prep_for_form|htmlspecialchars|encode_php_tags|valid_email|callback_new_email|min_length[5]|max_length[50]|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|alpha_numeric|required|prep_for_form|htmlspecialchars|encode_php_tags|min_length[5]|max_length[20]|xss_clean');
    	
		//validation message
    	$this->form_validation->set_message('required', '%s is required.');
    	$this->form_validation->set_message('alpha_numeric', 'Only alphanumeric characters are allowed.');
		$this->form_validation->set_message('valid_email', 'Wrong email format.');
    	$this->form_validation->set_message('min_length[5]', 'Min. character is 5 chars.');    	
    	$this->form_validation->set_message('max_length[20]', 'Max. character is 20 chars.');
    	$this->form_validation->set_message('max_length[50]', 'Max. character is 50 chars.');
		
		$this->form_validation->set_error_delimiters('','');

		
		if ($this->form_validation->run() == true)
		{
			if($this->input->post('agree')=="agreed")
			{			
				$fullname = $this->input->post('fullname');
				$email = $this->input->post('email');
				$password = $this->input->post('password');

				$additional_data = array('gender' => $this->input->post('gender'),
					'update' => $this->input->post('update')
				);
			
			
				if($this->ion_auth->register($fullname, $password, $email, $additional_data))
				{
					//check to see if we are creating the user
					$this->session->set_flashdata('message', "User Created");
					redirect('member/frontpage', 'refresh');
				}
				else
				{ 
					//display the register user form
					$this->session->set_flashdata('message', $this->ion_auth->errors());
					redirect('member/frontpage', 'refresh'); 
				}
			}
			else
			{
				$this->session->set_flashdata('message', "You must agree with the Term & Service");
				redirect('member/frontpage', 'refresh'); 				
			}
		}
		else
		{
			$this->session->set_flashdata('message', validation_errors());
			redirect('member/frontpage');			
		}
	}
	//------------------------------------------------------------------------------------------------------------------------end register

	//------------------------------------------------------------------------------------------------------------------------profile
	function profile($id='')
	{
		if (!$this->ion_auth->logged_in()) //if not logged in
		{
			//redirect them to the login page
			redirect('member/frontpage', 'refresh');
		}
		else
		{
			//setting up viewer
			$page_req="pm_profile_idx";
			$data['foot_js']=css_asset('plugin/sliderStyle.css').js_asset('jquery.nivo.slider.pack.js');
	      	$data['foot_script']='
		        var sics = $(".share_icons"); sics.hide();
		        $(\'#slider\').nivoSlider({speed:5000});
		        
            $("#tab ul li.middle a").addClass("active").show(); 
            $("#tab2").show();  

		      ';	      

			if($id=='') //my profile
			{
				$row=$this->m_member->getUserProfile($this->uid);
			}
			else //other member profile
			{
				$row=$this->m_member->getUserProfile($id);
			}
						
			//setting data
			$data['name']=$row->up_name;
			$data['email']=$row->ul_email;
			$data['email_show']=$row->up_emailhide;
			$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
			$data['birthday']=$row->up_birthday;
			$data['birthday_show']=$row->up_birthyearhide;
			$data['website']=$row->up_website;
			$data['city']=$row->up_city;
			$data['country']=$row->up_country;
			$data['timezone']=$row->up_timezonename;
			$data['alias']=$row->up_alias;
			$data['member_since']=$row->up_membersince;

			//render view
			$this->template->write('head_title', 'Profile');
			$this->template->write('keywords', '');
	      	$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
		  	$this->template->write_view('sidebar_left', 'pm_side_newbie', $data, '');
			$this->template->write_view('middle_content', 'pm_profile_idx', $data, '');
			$this->template->write_view('sidebar_right', 'general/sidebar_profile_right', $data, '');
			$this->template->render();
	      			
		}
	} 
	//------------------------------------------------------------------------------------------------------------------------end profile
	
	/* Adi. 28 Des 2011. beri komentar pada song */
	function song_comment_process()
	{
		$this->m_track->song_comment($this->input->post('comment'), $this->input->post('songID'), $this->uid, $this->session->userdata('ip_address'));
		redirect($_SERVER['HTTP_REFERER'], 'refresh');
	}
	
	/* Adi. 28 Des 2011. like song */
	function song_like_process($s_id)
	{
		$this->m_track->song_like($s_id, $this->uid);
		redirect($_SERVER['HTTP_REFERER'], 'refresh');
	}
	
	/*Firman. 17 Januari 2012. Download Song*/
	function download($song_id='',$brand_id=0){
		{	
		
		$data['foot_js']=css_asset('plugin/sliderStyle.css').js_asset('jquery.nivo.slider.pack.js').js_asset('jPlayer/jquery.jplayer.min.js');
	    $data['foot_script']='
			var sics = $(".share_icons"); sics.hide();
		    $(\'#slider\').nivoSlider({speed:5000});

		    $("#tab ul li.middle a").addClass("active").show(); 
 	        $("#tab2").show();
			
			/* ^zy */
			 	var the_player = $("#song_player");

			 	the_player.jPlayer({
			 	    ready: function(){
						console.log("player ready."); 	           			
			 	    },
			 	    play: function(){
			 	    	console.log("play file.");	
			 	    },
			 	    volume: "0.4",
			 	    swfPath: "'.site_url().'assets/js/jPlayer",
					cssSelectorAncestor: "#wrapper",
					supplied: "mp3",
					wmode: "window"
				});           	

				var prev_val=0;
			 	$("#wrapper .btn_play").click(function(e){
			 	    the_player.jPlayer("setMedia", {
						mp3: $(this).attr("href")
					});
					the_player.jPlayer("play");
					var nxt_el=$(this).parent().next().find(".ttl_play");
					var ctr=nxt_el.html();
					nxt_el.fadeOut(); 
					var ctr2=parseInt(ctr);
					if(prev_val==0){
						ctr2+=1;
						prev_val=1;
					}
					nxt_el.html(ctr2); nxt_el.fadeIn();	
					return false;
				});
				
				$(".add_to_playlist").click(function(){
					$.post("'.site_url().'track/add_to_playlist", { track_id: $(this).attr("href") }, function(data) {
   						$(".list-left p.bottom").html();
   						alert(data);
   						$(".list-left p.bottom").html("Data berhasil di tambahkan."); /* harusnya disini load content ajax utk playlist */
 					});
					return false;	
				});

			/* ^zy.end */
			
		    ';	      
			
			$this->config->load('my_config');
			$limit_sidebar_friend_list=$this->config->item('limit_sidebar_friend_list');
			if($this->uid){
				
				$row=$this->m_member->get_user_profile($this->uid);
				$data['user_id']=$this->uid;
				
				$data['name']=$row->up_name;
				$data['alias']=$row->up_alias;	
				$data['unread_messages']=$this->m_member->get_count_unread_messages($this->uid);		
				$data['count_friend_request']=$this->m_member->get_count_friend_request($this->uid);
				$data['count_notification']=$this->m_member->get_count_notification($this->uid);

				$data['gender_code']=$row->up_gender;
				$data['gender']=($row->up_gender=='F') ? 'Female' : 'Male';
				$data['city']=$row->up_city;
				$data['country']=$row->up_country;
				$data['friends']=$this->m_member->get_friends($this->uid, $limit_sidebar_friend_list);
			}
			
			$data['up_uid']=$row->up_uid;
			/* pagination */ 
        $uri_segment = 4;
        if($this->uri->segment($uri_segment)==''){ $offset=0; }else{ $offset=$this->uri->segment($uri_segment); }
        $data['start_no']= $offset+1;

        $this->load->library('pagination');
        $config['base_url'] = site_url().'track/index/';
        $data['jml_member']=$config['total_rows'] = $this->m_track->count_all('');
        $config['per_page'] = $this->config->item('limit_new_track');
        $config['uri_segment'] = $uri_segment;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        /* pagination.end */
		
		//Suggest Song --> duplicate form member
		//music sidebar, suggest song drop down menu	
		$fav_genre=$this->m_track->get_fav_genre($this->uid);
		$fav_genre=explode(", ", $fav_genre);
		$fav_artist=$this->m_track->get_fav_artist($this->uid);
		$fav_artist=explode(", ", $fav_artist);
		
		$genres=$this->m_track->get_genre();
		$artists=$this->m_track->get_artist();
		$i=0;
		$fav_genre_display='';
		$fav_genre_string='';
		$fav_artist_display='';
		$fav_artist_string='';

		if($fav_genre[0]) //have favourite genre
		{
			foreach($genres as $g)
			{
				if($g->g_id==$fav_genre[$i])
				{
					$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
					$fav_genre_string .= $g->g_name.',';
					$i++;
				}
			}	
		}
		else //do not have favourite genre
		{
			foreach($genres as $g)
			{
				$fav_genre_display[$i]=array('id'=>$g->g_id, 'name'=>$g->g_name);
				$fav_genre_string .= $g->g_name.',';
				$i++;
			}				
		}	
		  $data['suggest_song_option']=$fav_genre_display;
			$data['fav_genre_string']=$fav_genre_string;
			
		$i=0;	
		if($fav_artist[0]) //have favourite artist
		{
			foreach($artists as $a)
			{
				if($a->ID==$fav_artist[$i])
				{
					$fav_artist_display[$i]=array('id'=>$a->ID, 'name'=>$a->artist_name);
					$fav_artist_string .= $a->artist_name.',';
					$i++;
				}
			}	
		}
		else //do not have favourite genre
		{
			foreach($artists as $a)
			{
				$fav_artist_display[$i]=array('id'=>$a->ID, 'name'=>$a->artist_name);
				$fav_artist_string .= $a->artist_name.',';
				$i++;
			}				
		}	
		
			$data['fav_artist_string']=$fav_artist_string;

		  $genre_id=$fav_genre_display[0]['id'];
		$limit=$this->config->item('limit_sidebar_suggest_song_list');		
		$data['song_by_genre']=$this->m_track->get_song_by_genre($genre_id, $limit);
		//end duplicate suggest song

        $data['list_songs']=$this->m_track->get_paged_list($this->config->item('limit_new_track'), $offset, '');
		$data['ads_data']=$this->m_track->get_image_ads();
		
		/****================================ tambahan ===================================****/ 
		//Adi 27 Dec 2011
		$data['unread_messages']=$this->m_member->get_count_unread_messages($this->uid);		
		$data['count_friend_request']=$this->m_member->get_count_friend_request($this->uid);
		$data['count_notification']=$this->m_member->get_count_notification($this->uid);
	
		
		$data['recommended_track']=$this->m_home->get_recommended_track();
		$new_track_limit=$this->config->item('limit_new_track');
		$data['new_track']=$this->m_home->get_new_track($new_track_limit);
			
		/*================================end tambahan ========================================*/
		if($brand_id==0){
			/*Firman : get randomize brand */
			$data['brand'] = $this->m_track->get_brand();
			$data['brand'] = $data['brand'][0];
		}else{
			/*Firman : get specified brand */
			//print_r($data['brand']);
			$data['brand'] = $this->m_track->get_specified_brand($brand_id);
			$data['brand'] = $data['brand'][0];
		}
		
		$data['is_like_brand'] = $this->m_track->is_like_brand($data['brand']->brand_id,$this->uid);
		//echo '<hr>'.(int)$data['is_like_brand'];
		
		/* Firman : Download song */
		$data['download_song'] = $this->m_track->get_single_song($song_id);
		$data['download_song'] = $data['download_song'][0];
		
		
			//render view
			$this->template->write('head_title', 'Artist');
			$this->template->write('keywords', '');
	      	$this->template->write_view('profile_menu', 'general/profile_menu', $data, '');
		  	$this->template->write_view('sidebar_left', 'pm_track_side', $data, '');
			$this->template->write_view('middle_content', 'pm_download_track', $data, '');
			$this->template->write_view('sidebar_right', 'pm_track_side_right', $data, '');
			$this->template->render();			      			

   	}
	}
	
	function like_brand($brand_id,$track_id){
		if(!$this->m_track->is_like_brand($brand_id,$this->uid)){
			$this->m_track->like_brand($brand_id,$this->uid);
		}
		redirect('track/download/'.$track_id."/".$brand_id,'refresh');
	}
	
	function download_song($song_id,$brand_id){
		if($this->m_track->is_like_brand($brand_id,$this->uid)){
			//download statement
			$song = $this->m_track->get_single_song($song_id);
			$song = $song[0];print_r($song);
			$this->load->helper('download');
			$name = $song->file;
			$data = site_url()."assets/media/mp3/".$song->file;
			echo $data;
			force_download($name, $data); 
		}else{
			redirect('track/download/'.$song_id,'refresh');
		}
	}
}
?>