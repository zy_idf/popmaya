<div class="form_add">
	<table>
	<?php 
		$attr=array();
		$hidden=array('prev_page'=>current_url());
		echo form_open_multipart('track/manage/upload', $attr, $hidden); 
		echo '<tr><td></td><td><h3>Quick Add Song</h3></td></tr>';
		echo '<tr><td>Judul</td><td>'.form_input(array('name'=>'item_title')).'</td></tr>';
		echo '<tr><td>Genre</td><td>'.form_dropdown('item_genre', array('1'=>'General', '2'=>'Alternative')).'</td></tr>';
		echo '<tr><td>Lama</td><td>'.form_input(array('name'=>'item_length')).'</td></tr>';
		echo '<tr><td>Info</td><td>'.form_textarea(array('name'=>'item_info')).'</td></tr>';
		echo '<tr><td>File</td><td>'.form_upload('userfile', '').'</td></tr>';
		echo '<tr><td></td><td>'.form_submit(array('name'=>'submit', 'value'=>'Submit')).'</td></tr>';
		echo form_close();
	?>
	</table>
</div>
<?= $pagination ?>
<table class="list_song">
	<tr>
		<!--<th></th>--><th>No</th><th>Nama</th><th class="w270">Info</th><th>Lama</th><th>File size</th><th></th>
	</tr>
	<?php
		
		if(count($list_songs)>0){
			$i=$start_no;
			foreach ($list_songs as $items => $item) {
				$cls='';
				if(($i%2)==1){
					$cls='class="odd"';
				}
				echo '
			<tr '.$cls.'>
				<!--<td>'.form_checkbox('cb_'.$i).'</td>-->
				<td>'.$i.'</td>
				<td>'.$item->title.'</td>
				<td>'.$item->post.'</td>
				<td>'.$item->time_length.'</td>
				<td>'.$item->file_size.'</td>
				<td>'.anchor('track/manage/edit/'.$item->s_id, 'Edit').' &middot; '.anchor('track/manage/delete/'.$item->s_id, 'Delete', array('class'=>'delConf')).'</td>
			</tr>
				';
				$i++;
			}
		}
	?>
</table>
