<div class="head-title">
          <span><strong>Song List</strong><?= $jml_member ?></span>
      </div>
      
      <div id="track_list">
          <div class="sort-form">
              <label for="category-music">Sort list by Nama Artis</label>
              <select id="category-music">
                  <option>All</option>
                  <option>Rap</option>
              </select>
          </div>

          <?php 
            foreach ($list_songs as $key => $item) {
          ?>
              <div class="list_box_large">
                <?= image_asset('general/layout/efek_thumb.jpg', '', array('alt'=>'efek', 'class'=>'thumb-box')); ?>
                <h3><a href="<?= site_url().'track/detail/'.$item->s_id ?>"><?= $item->title ?></a></h3>
                <span class="title">Nama Artis</span>
                <div class="button-share">
                  <?= anchor(site_url().'assets/media/mp3/'.$item->file, image_asset('play_ico.png', '', array('alt'=>'play')), array('title'=>'Play', 'class'=>'btn_play')) ?>
                  <!-- tambahan adi 27 dec 2011 - link download music -->
				  <a href="track/download/<?= $item->s_id ?>" title="Download"><img src="assets/images/download_ico.png" alt="download"/></a>
                  <a href="#" title="Share" class="hover_share"><img src="assets/images/share_ico.png" alt="share"/></a>
                  <div class="hover_content" style='display:none;'>
					<!-- <?= image_asset('hover_content.jpg'); ?> -->
					<div class="url_shorten"><span class="url_val">Url shortener here</span><a href="#" class="copy_shorten">Copy</a></div>
					<div class="share_box">
						<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
							<a class="addthis_button_preferred_1"></a>
							<a class="addthis_button_preferred_2"></a>
							<a class="addthis_button_preferred_3"></a>
						</div>
					</div>
				</div>
				  <a href="<?= site_url().'track/add_to_playlist/'.$item->s_id ?>" class="last add_to_playlist" title="Add"><img src="assets/images/add_ico.png" alt="add"/></a>
                </div>
                <div class="lbl_total_play">Total play : <?= anchor('#', '200', 'class="ttl_play"'); ?> | Total download : <?= anchor('#', '32'); ?></div>
              </div>
          <?php
            }
          ?>
          
          <div class="clear"></div>
          <?= $pagination ?>
          <!--
          <div class="pagination">
              <a href="#" class="first">Previous</a>
              <strong>1</strong>
              <a href="#">2</a>
              <a href="#">3</a>
              <a href="#">4</a>
              <a href="#">5</a>
              <a href="#">6</a>
              <a href="#">7</a>
              <a href="#">8</a>
              <a href="#" class="last">Next</a>
          </div>
          -->
          <div class="clear"></div>
      </div>