
<div class="head-title">
	  <span>Download Song</span>
</div>
<div class="head-title" style="display:inline-block;width:100%;padding:10px;">
	 <div class="list_box_large" style="border:none;margin-top:-5px;margin-bottom:-10px;">
		<?= image_asset('artist/thumbnail/'.$download_song->artist_id.'.jpg', '', array('alt'=>'efek', 'class'=>'thumb-box')); ?>
		<h3><a href="<?= site_url().'track/detail/'.$download_song->s_id ?>"><?= $download_song->title ?></a></h3>
		<span class="title"><?= $download_song->artist_name ?></span>
	  </div>
</div>
<div class="head-title">
  <span><strong><?= $brand->brand_name ?></strong></span>
  <?= image_asset('brand/'.$brand->image, '', array('alt'=>'efek', 'class'=>'thumb-box')); ?>
  <p>
  <?= $brand->brand_desc ?>
  </p>
 </div>
<div class="head-title" style="display:inline-block;">
  <span><strong>Term</strong></span><br>
  <p style="width:80%;float:left;clear:left;">
  Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
  Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
  Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
  Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet
  </p>
  <? if(!$this->m_track->is_like_brand($brand->brand_id,$user_id)){ ?>
   <a href="<?= site_url() ?>track/like_brand/<?= $brand->brand_id ?>/<?= $download_song->s_id ?>">
   <?= image_asset('general/btn_like_brand.png', '', array('alt'=>'like', 'style'=>'float:right;')); ?>
	</a>
  <? } ?>
  <? if($this->m_track->is_like_brand($brand->brand_id,$user_id)){ ?>
   
   <a href="<?= site_url() ?>track/download_song/<?= $download_song->s_id ?>/<?= $brand->brand_id ?>" style="width:100%;text-align:center;">
  <?= image_asset('general/btn_download_track.png', '', array('alt'=>'download track','style='=>'margin:0px auto;')); ?>
  </a>
  <? } ?>
</div>