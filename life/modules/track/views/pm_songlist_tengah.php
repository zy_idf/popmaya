<!--<div class="head-title">
          <span><strong>Song List</strong></span>
      </div>-->
      <!-- 
	  Adi 27 Dec 2011
	  tambahan, judul lagu, nama artist, nama album -->
      <div id="track_list" style="margin-top: 0px;">
	  <div class="grid_6" style="margin-top:10px;">
			<div class="grid_2">
				<?= image_asset('album_art.png', '',array('alt'=>'album_art')); ?>
			</div>
			<div class="grid_4" >
				<!-- ini dirubah -->
				<h5><?=$song_info->title?></h5>
				<p style="margin-top:-10px;">
					<strong>Artist&nbsp;:&nbsp;<?=$artist_name?></strong>
					<br />
					<strong>Album&nbsp;:&nbsp;<?=$album_name?></strong>
				</p>
			</div>
			<div class="clear"></div>
			<div class="grid_5">
				<span>Total Download : 25</span>
				<br />
				<div class="button-share" style="margin-bottom: 10px;">
					<?= anchor(site_url().'assets/media/sample.mp3', image_asset('play_ico.png', '', array('alt'=>'play')), array('title'=>'Play', 'class'=>'btn_play')) ?>
					<!-- tambahan adi 28 dec 2011 - link download -->
					<a href=<?=site_url().'member/download_music/sample.mp3'?> title="Download"><img src="<?= site_url().'/assets/images/'; ?>download_ico.png" alt="download"/></a>
					<a href="#" title="Share" class="hover_share"><img src="<?= site_url().'/assets/images/'; ?>share_ico.png" alt="share"/></a>
					<div class="hover_content">
						<!-- <?= image_asset('hover_content.jpg'); ?> -->
						<div class="url_shorten"><span class="url_val">Url shortener here</span><a href="#" class="copy_shorten">Copy</a></div>
						<div class="share_box">
							<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
								<a class="addthis_button_preferred_1"></a>
								<a class="addthis_button_preferred_2"></a>
								<a class="addthis_button_preferred_3"></a>
							</div>
						</div>
					</div>
					<a href="<?= site_url().'track/add_to_playlist/1'?>" class="last add_to_playlist" title="Add"><img src="<?= site_url().'/assets/images' ?>/add_ico.png" alt="add"/></a>
				</div>
				<div class="clear"></div>
			</div>
	</div>
	<div class="clear"></div>
	<div class="clear"></div>
	<div class="grid_7 bg_upload" style="width: 458px;">
		<h5 style="margin:10px 0 0 10px">RBT Code</h5>
		<p style="margin-left:10px;">
			Telkomsel 	: Ketik RING 11700779 kirim ke 1212
			<br />
			3	 	: Ketik 11700779 kirim ke 888
			<br />
			Telkomsel 	: Ketik RING 11700779 kirim ke 1212
			<br />
			3	 	: Ketik 11700779 kirim ke 888
			<br />
			Telkomsel 	: Ketik RING 11700779 kirim ke 1212
			<br />
			3	 	: Ketik 11700779 kirim ke 888
		</p>
			<div>
				<ul class="uibutton-group">
					<li class="grid_2">
						<a href=""><span class="small biruLite" style="margin-left: 20px;">14 minutes ago</span></a>
					</li>
					<li class="grid_1">
						<a href=""><div class="count_see"><span class="small biruLite" style="margin-left: 20px;">20,191</span></div></a>
					</li>
					<li class="grid_1">
						<a href=""><div class="count_comment"><span class="small biruLite" style="margin-left: 16px;">14,578</span></div></a>
					</li>
					<li class="grid_1" style="margin-left: 2px;">
						<a href=""><div class="count_flag"><span class="small biruLite" style="margin-left: 26px;">2,93</span></div></a>
					</li>
					<li class="grid_1" style="margin-right: 27px; margin-top: -5px;">
						<a href="https://twitter.com/share" class="twitter-share-button" data-via="popmaya" data-lang="en">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
					</li>
					<li class="grid_1" style="margin-top: -5px;">
						<!--<iframe src="//www.facebook.com/plugins/like.php?href&amp;send=false&amp;layout=button_count&amp;width=360&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:400px; height:21px;" allowTransparency="true"></iframe>-->
						<!-- tambahan Adi 28 des 2011 - Like song -->
						<?php if($like >= 1) { ?>
						<?= $like; ?> member like this - 
						<?php } ?>
						<?= anchor('track/song_like_process/'.$s_id, 'Like'); ?>
					</li>
				</ul>
			  </div>
	</div>
          <!-- tambahan adi 28 dec 2011 - form comment song -->
		<?= form_open('track/song_comment_process'); ?>
						<?= form_input(array('name'=>'comment','id'=>'comment','value'=>'','maxlength'=>'100','size'=>'60')); ?>
						<?= form_hidden('songID', $s_id); ?>
						<?= form_submit('submit', 'Comment'); ?>
					<?= form_close(); ?>
		<?
			if($song_comment){
				foreach($song_comment as $s){ ?>
					<div class="list_box_large">
						  <?= image_asset('general/layout/efek_thumb.jpg', '', array('alt'=>'efek', 'class'=>'thumb-box')); ?>
						  <a href=<?=site_url().'member/profile/'.$s->up_uid?>><span class="ijoText"><strong><?=$s->up_name?></strong></span></a>
						  <p>
						  <?=$s->comment?>
						  </p>
						  <div class="clear"></div>
						  <div class="grid_7 push_1" style="margin-left:-10px; margin-top:-15px;">
							<ul class="uibutton-group">
								<li class="grid_2">
									<a href=""><span class="small biruLite" style="margin-left: 20px;">14 minutes ago</span></a>
								</li>
								<li class="grid_1" style="margin-left: 2px;">
									<a href=""><div class="count_flag"><span class="small biruLite" style="margin-left: 26px;">2,93</span></div></a>
								</li>
								
							</ul>
						  </div>
					</div>			
		<?		}
			}
		?>
		
          <div class="clear"></div>
          <div class="pagination">
              <a href="#" class="first">Previous</a>
              <strong>1</strong>
              <a href="#">2</a>
              <a href="#">3</a>
              <a href="#" class="last">Next</a>
          </div>
          <div class="clear"></div>
      </div>