<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Notification extends MX_Controller {
    
    function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()){
            redirect('mobile/login');
            exit();
        } 
        $this->uid=$this->session->userdata('user_id');
        $this->load->model('m_member', '', TRUE);
    }
    
    function index()
    {
        $data['notification_list']=$this->m_member->get_count_all_note_list();
        $this->template->write('head_title', 'Notifications');
        $this->template->set_master_template('mobile');
        $this->template->write_view('middle_content', 'notification/pm_mobile_notification',$data,true);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
    
    function view($id)
    {
        $filter=$this->m_member->get_notif($id);
        if($filter === NULL || $filter->n_type != 7)
        {
            redirect('mobile/notification');
            exit();
        }
        if($filter->n_isread==0)
        {
            $this->m_member->update_notif($id);
        }
        $data['notif']=$filter;
        $data['image_path']=$this->m_member->get_profile_path($filter->up_uid);
        $this->template->write('head_title', 'Notifications');
        $this->template->set_master_template('mobile');
        $this->template->write_view('middle_content', 'notification/pm_mobile_view',$data,true);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
}