<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Photo extends MX_Controller {
    
    var $user_id;
    
    function __construct() 
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()){
            redirect('mobile/login');
            exit();
        } 
        $this->user_id=$this->session->userdata('user_id');
        $this->load->model('m_member', '', TRUE);
        $this->load->model('m_photos', '', TRUE);
    }
    
    function index($id=false)
    {
        if(!$id)
        {
            $id=$this->user_id;
        }
        else
        {
            $profile=$this->m_photos->getUserProfile($id);
            if(empty($profile))
            {
                redirect('mobile/home');
                exit ();
            }
            $data['submenu']=$id;
        }
        
        $data['active_menu']='profile';
        $data['album'] = $this->m_photos->get_count_album($id);
        $albums = $this->m_photos->get_user_album($id);
        $untitled=$this->m_photos->get_untitled_photo($id);
        if(!empty($untitled))
        {
            $cvUnt=$this->m_photos->get_photo_cover($id,0);
            $data['countUnt']= $this->m_photos->get_count_photo_album($id,0);
            if(!empty($cvUnt))
            {
                $data['coverUnt']='album/'.$id.'/'.$cvUnt->image;
            }
            else
            {
                $data['coverUnt']='profile/what.jpg';
            }
        }
        $dataAlbums=array();
        if(!empty($albums))
        {
            foreach($albums as $row)
            {
                $cover=$this->m_photos->get_photo_cover($id,$row->ID);
                if(!empty($cover))
                {
                    $coverImg='album/'.$id.'/'.$cover->image;
                }
                else
                {
                    $coverImg='profile/what.jpg';
                }
                $count= $this->m_photos->get_count_photo_album($id,$row->ID);
                $dataAlbums[]=array($row->ID,$row->title,$coverImg,$count);
            }
        }
        else
        {
            $dataAlbums=NULL;
        }
        $imgP=$id;
                
        $data['path']=$imgP;
        $data['albums'] = $dataAlbums;
        $data['untitled_album']=$untitled;
        $this->template->write('head_title', 'Photo Album');
        $this->template->set_master_template('mobile');
        $this->template->write_view('middle_content', 'photo/pm_mobile_photo',$data,true);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
    
    function album_previews($id,$userid=false)
    {
        if(!$userid)
        {
            $userid=$this->user_id;
        }
        else
        {
            $profile=$this->m_photos->getUserProfile($userid);
            if(empty($profile))
            {
                redirect('mobile/home');
                exit ();
            }
            $data['submenu']=$userid;
        }
        
        if($this->input->post('album_cover'))
        {
            $idImg=$this->input->post('album_cover');
            $albID=$this->input->post('albumID');
            $this->m_photos->update_cover($albID);
            $this->m_photos->update_cover_active($idImg,$albID);
        }
        
        if(isset($id))
        {
            if($id=='0')
            {
                $data['count']= $this->m_photos->get_count_photo_album($userid,0);
                $data['albumPhotos']=$this->m_photos->get_photos_album($userid,0);
            }
            else
            {
                $album=$this->m_photos->get_detail_album($id,$userid);            
                if(!empty($album))
                {
                    $data['album']=$album;
                    $data['count']= $this->m_photos->get_count_photo_album($userid,$id);
                    $data['albumPhotos']=$this->m_photos->get_photos_album($userid,$id);
                }
                else
                {
                    redirect('mobile/photo/'.$userid);
                    exit();
                }            
            }
        }
        else
        {
            redirect('mobile/photo');
            exit();
        }
        $data['active_menu']='profile';        
        $data['path'] = $userid;
        $this->template->write('head_title', 'Photo Album Preview');
        $this->template->set_master_template('mobile');
        $this->template->write_view('middle_content', 'photo/pm_mobile_albumprev',$data,true);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
    
    function view($id,$userid=false)
    {
        if(!$userid)
        {
            $userid=$this->user_id;
        }
        else
        {
            $profile=$this->m_photos->getUserProfile($userid);
            if(empty($profile))
            {
                redirect('mobile/home');
                exit ();
            }
            $data['submenu']=$userid;
        }
        
        if($id)
        {
            $photo=$this->m_photos->get_detail_photo($userid,$id);
            if(empty($photo))
            {
                redirect('mobile/photo');
                exit();
            }
            else
            {
                if($photo->memberID == $this->user_id)
                {
                    $this->m_photos->update_notif_photo($id);
                }
                $photo=$photo;
                $like=$this->m_photos->get_count_like_photo($id);
            }
        }
        else
        {
            redirect('mobile/photo');
            exit();
        }
        
        if($this->input->post('post_comment'))
                {
                    $set=array(
                        'photoID'=>$id,
                        'memberID'=>$this->user_id,
                        'comment'=>$this->input->post('post_comment'),
                        'host'=>$_SERVER['REMOTE_ADDR']
                    );
                    $this->m_photos->add_comment($set);
                    if($this->user_id !== $photo->memberID)
                    {
                        $comset=array(
                            'n_type'=>10,
                            'n_itemid'=>$id,
                            'n_fromuserid'=>$this->user_id,
                            'n_foruserid'=>$photo->memberID
                        );
                        $this->m_photos->add_notif($comset);
                    }                
                }
        
        $comStatus=$this->m_photos->get_comment($photo->ID);
                $dataComStatus=array();
                if(!empty($comStatus))
                {
                    foreach($comStatus as $rCom)
                    {                
                        $imgP=$this->m_photos->get_profile_path($rCom->up_uid);
                        if(@$imgP)
                        {
                            $imgPath=$imgP;
                        }
                        else
                        {
                            $imgPath=NULL;
                        }
                        $dataComStatus[]=array($rCom->up_name,$rCom->comment,$imgPath."/".$rCom->up_uid.'_thumb.jpg',$rCom->up_uid,$rCom->ID);
                    }
                }
                else
                {
                    $dataComStatus=NULL;
                }
                
        
        $data['photo']=$photo;
        $data['like']=$like;
        $data['comment']=$dataComStatus;
        $data['path'] = $userid;
        $this->template->write('head_title', 'Photo');
        $this->template->set_master_template('mobile');
        $this->template->write_view('middle_content', 'photo/pm_mobile_detail',$data,true);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
    
    function like($id,$userid=false)
    {
        if(!$userid)
        {
            $userid=$this->user_id;
        }
        else
        {
            $profile=$this->m_photos->getUserProfile($userid);
            if(empty($profile))
            {
                redirect('mobile/home');
                exit ();
            }
        }
        
        if($id)
        {
            $photo=$this->m_photos->get_detail_photo($userid,$id);
            if($photo)
            {
                $valLike=  $this->m_photos->get_like_photo($this->user_id,$photo->ID);
                if(empty($valLike))
                {                    
                    $set=array(
                        'l_member_id'=>$this->user_id,
                        'l_photo_id'=>$photo->ID
                    );
                    $this->m_photos->add_like($set);
                    
                    if($this->user_id !== $photo->memberID)
                    {
                        $comset=array(
                            'n_type'=>11,
                            'n_itemid'=>$id,
                            'n_fromuserid'=>$this->user_id,
                            'n_foruserid'=>$photo->memberID
                        );
                        $this->m_photos->add_notif($comset);
                    }  
                }                
                redirect('mobile/photo/view/'.$id.'/'.$userid, 'refresh');
            }
            else
                redirect('mobile/photo/'.$userid);
        }
        else
            redirect('mobile/photo/'.$userid);
    }
    
    function delete_photo($id)
    {
        if($id)
        {
            $getPost=$this->m_photos->get_own_photo();
            if(!empty($getPost))
            {
                $album=$getPost->albumID;
                $userid=$getPost->memberID;
                $this->m_photos->delete_photo($id);
                    redirect('mobile/photo/album_previews/'.$album.'/'.$userid, 'refresh');
            }
            else
                redirect('mobile/photo');
        }
        else
            redirect('mobile/photo');
    }
    
    function delete_comment($id,$userid=false)
    {
        if(!$userid)
        {
            $userid=$this->user_id;
        }
        else
        {
            $profile=$this->m_photos->getUserProfile($userid);
            if(empty($profile))
            {
                redirect('mobile/home');
                exit ();
            }
        }
        
        if($id)
        {
            $getComment=$this->m_photos->get_own_comment($id);
            if(!empty($getComment))
            {
                $this->m_photos->delete_comment($id);
                redirect('mobile/photo/view/'.$getComment->photoID.'/'.$userid, 'refresh');
            }
            else
                redirect('mobile/photo');
        }
        else
                redirect('mobile/photo');
    }
    
    function upload_photo($id)
    {
        $submit = $this->input->post('submit');
        if ($submit) $id = $this->input->post('albumID');
        $title = $this->input->post('title');
        $description = $this->input->post('description');
        if($id)
        {
            if ($this->input->post('albumID'))
            {
                if (@$_FILES['picture']['name'] != "")
                {
                    $result = $this->_upload('picture',$id);
                }
                if (!@$result['upload_data']['is_error'])
                {
                    $dataPost=array(
                        'memberID'=>$this->user_id,
                        'title'=>$title,
                        'image'=>$result['upload_data']['file_name'],
                        'description'=>$description,
                        'albumID'=>$id
                    );
                    $this->m_photos->add_photo($dataPost);
                    redirect('mobile/photo/album_previews/'.$id.'/'.$this->user_id);
                }
            }
            
            $album=$this->m_photos->get_detail_album($id,$this->user_id);
            if(!empty($album))
            {
                $data['album']=$album;
            }
            else
            {
                redirect('mobile/photo');
                exit();
            }
            $data['active_menu']='profile';
            $data['profile'] = $this->m_photos->getUserProfile($this->user_id);
            $this->template->write('head_title', 'Upload Photo');
            $this->template->set_master_template('mobile');
            $this->template->write_view('middle_content', 'photo/pm_mobile_uploadphoto',$data,true);
            $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
            $this->template->render();
        }
        else
            redirect('mobile/photo');
    }
    
    function _upload($file_name,$id)
    {
        $dir="./assets/images/album/".$this->user_id;
		if(!is_dir($dir)) {mkdir($dir, 0755);}
                
        $config['upload_path']="./assets/images/album/".$this->user_id;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']='300';
        $config['max_width']='950';
        $config['max_height']='950';
        $config['encrypt_name'] = TRUE;
        
        $this->load->library('upload', $config);
        
        if ($this->upload->do_upload($file_name))
        {
            $data = $this->upload->data(); 
            $source             = $dir."/".$data['file_name'] ;
            
            $thumb_config['width']=184;
            $thumb_config['height']=184;
            $thumb_config['master_dim']='width';
            $thumb_config['create_thumb']=TRUE;
            $thumb_config['maintain_ratio']=TRUE;
            $thumb_config['source_image']=$source;
            
            $this->load->library('image_lib', $thumb_config);
            
            if (!$this->image_lib->resize())
            {
                $this->session->set_flashdata('message', "Can not create photo thumbnail");
            }
            else
            {
                $this->session->set_flashdata('message', "Upload profile picture success");
            }        

            return array('upload_data'=>$this->upload->data());
        }
        else
        {
            return array('upload_data'=>array('error_msg'=>$this->upload->display_errors(), 'is_error'=>true));
            $this->session->set_flashdata('message', $this->upload->display_errors());
            redirect('mobile/photo/upload_photo/'.$id);	
        }
    }
    
}
/* End of file photo.php */
/* Location: ./life/modules/mobile/controllers/photo.php */