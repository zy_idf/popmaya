<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MX_Controller {
    
    var $user_id;
    
    function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()){
            redirect('mobile/login');
            exit();
        } 
        $this->user_id=$this->session->userdata('user_id');
        $this->load->model('m_member', '', TRUE);
        $this->load->model('m_wall', '', TRUE);
        $data['active_menu']='home';
        $this->template->set_master_template('mobile');
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->load->library('form_validation');
        $this->load->library('session');
    }
    
    function index()
    {
        $data['notification']=$this->m_member->get_count_unread_note();
        $data['notification_list']=$this->m_member->get_count_unread_note_list();
        $data['photo_challenge']=$this->m_member->get_photo_challenge();
        if($this->input->post('status'))
        {                        
            $set=array(
                'memberID'=>$this->user_id,
                'toMemberID'=>$this->user_id,
                'info'=>$this->input->post('status'),
                'host'=>$_SERVER['REMOTE_ADDR']
            );
            $this->m_wall->add_wall($set);
        }
        
        $inviteEmail=$this->input->post('invite_email');
        if($inviteEmail)
        {
            $cekEmail=$this->m_member->get_email_address($this->input->post('invite_email'));
            if($cekEmail)
            {
                $set=array(
                    'i_userid'=>$this->user_id,
                    'i_email'=>$cekEmail->ul_email
                );
                $toID=$cekEmail->ul_id;
                $this->m_member->add_invite_email($set,$toID);
            }
            else
            {
//                $set=array(
//                    'i_email'=>$this->input->post('invite_email')
//                );
//                $this->m_member->add_invite_email($set);
                $this->session->set_flashdata('invite_errors', 'We can not find the email address.');
                redirect('mobile/home');
            }
        }
        
        $invitePhone=$this->input->post('invite_phone');
        if($invitePhone)
        {
            $cekPhone=$this->m_member->get_phone_number($invitePhone);
            if($cekPhone)
            {
                $set=array(
                    'i_userid'=>$this->user_id,
                    'i_email'=>$cekPhone->ul_email
                );
                $this->m_member->add_invite_email($set);
            }
            else
            {
//                $set=array(
//                    'i_email'=>$this->input->post('invite_email')
//                );
//                $this->m_member->add_invite_email($set);
                $this->session->set_flashdata('invite_errors', 'We can not find the phone number.');
                redirect('mobile/home');
            }
        }
        $data['status']=$this->m_member->get_last_status();
        $data['image_path']=$this->m_member->get_profile_path($data['status']->up_uid);
        $comStatus=$this->m_member->get_comment_status($data['status']->ID);
        $dataComStatus=array();
        if(!empty($comStatus))
        {
            foreach($comStatus as $rCom)
            {                
                $imgP=$this->m_member->get_profile_path($rCom->up_uid);
                if(@$imgP)
                {
                    $imgPath=$imgP;
                }
                else
                {
                    $imgPath=NULL;
                }
                $dataComStatus[]=array($rCom->up_name,$rCom->comment,$imgPath."/".$rCom->up_uid.'_thumb.jpg',$rCom->up_uid,$rCom->ID);
            }
        }
        else
        {
            $dataComStatus=NULL;
        }
        $data['comment_status']=$dataComStatus;
        
        $recommended=$this->m_member->get_a_member();
//        $recommended=$this->m_member->get_recommendation($this->user_id, 5);
//        $dataRec=array();
//        if(!empty($recommended))
//        {
//            foreach($recommended as $rowRec)
//            {
//                $cekFriend=  $this->m_member->get_friend($rowRec->up_uid);
//                if(empty($cekFriend))
//                {
//                    $cekFriendInvited=$this->m_member->get_friend_invited($rowRec->ul_email);
//                    if(empty($cekFriendInvited))
//                    {
//                        $imgP=$this->m_member->get_profile_path('19450923');
//                        if(@$imgP)
//                        {
//                            $imgPath=$imgP;
//                        }
//                        else
//                        {
//                            $imgPath=NULL;
//                        }
//                        $dataRec[]=array($rowRec->up_name,$rowRec->up_city,$imgPath."/".$rowRec->up_uid.'_thumb.jpg',$rowRec->up_uid);
//                    }
//                }
//            }
//        }
//        else
//        {
//            $dataRec=NULL;
//        }
        $data['recommend_friend']=$recommended;
        $this->template->write('head_title', 'Biggest sound based social media');
        $this->template->write_view('middle_content', 'pm_mobile_home',$data);
        $this->template->render();
    }
    
    function tutorial()
    {
        $this->template->set_master_template('mobile');
        $this->template->write('head_title', 'Tutorial');
        $this->template->write_view('middle_content', 'pm_mobile_tutorial');
        $this->template->render();
    }
    
}
/* End of file home.php */
/* Location: ./life/modules/mobile/controllers/home.php */