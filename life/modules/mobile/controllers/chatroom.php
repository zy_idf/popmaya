<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Chatroom extends MX_Controller {
    
    function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()){
            redirect('mobile/login');
            exit();
        } 
        $this->uid=$this->session->userdata('user_id');
        $this->load->model('m_chat', '', TRUE);
    }
    
    function index()
    {
        $data['active_menu']='chatroom';
        $data['rooms']=  $this->m_chat->get_room_list();
        $this->template->set_master_template('mobile');
        $this->template->write('head_title', 'Chat Room');
        $this->template->write_view('middle_content', 'chat/pm_mobile_chatroom',$data);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
    
    function room($id)
    {
        $data['active_menu']='chatroom';
        $room=  $this->m_chat->get_detail($id);
        $data['room'] = $room;
        if($this->input->post('comment_chat'))
        {
            $set=array(
                'c_room_id'=>$room->cr_id,
                'c_sender_id'=>$this->uid,
                'c_mssg'=>  strip_tags($this->input->post('comment_chat')),
                'c_status'=>1
            );
            $this->m_chat->add_chat($set);
        }
        $data['chatlist'] = $this->m_chat->get_chat_list($id);
        $this->template->set_master_template('mobile');
        $this->template->write('head_title', 'Chat Room');
        $this->template->write_view('middle_content', 'chat/pm_mobile_chatlist',$data);
        $this->template->write_view('profile_menu', 'pm_mobile_top',$data,'');
        $this->template->render();
    }
}
/* End of file chatroom.php */
/* Location: ./life/modules/mobile/controllers/chatroom.php */