    <div class="listfren pad1020">
    <?php if(!empty($artistResults)):?>
        <label class="art">Artist Results</label>
        <hr/>
        <ul>
            <?php foreach($artistResults as $rArtist):?>
            <li><?php echo anchor('mobile/artist/profile/'.$rArtist->ID,$rArtist->artist_name);?></li>
            <?php endforeach;?>
        </ul>
        <form method="post" action="<?php echo base_url();?>mobile/artist/search">
            <input type="hidden" name="artist_q" value="<?php echo $key;?>"/>
            <input type="submit" name="search" value="more.." style="border: none;background: none;color: #a4d63f;float: right;cursor: pointer;"/>
        </form>
        <br class="clear"/>
    <?php endif;?>
    <?php if(!empty($friendResults)):?>
        <label class="fren">Friend Results</label>
        <hr/>
        <ul>
            <?php foreach($friendResults as $rFriend):?>
            <li><?php echo anchor('mobile/post/'.$rFriend->up_uid,$rFriend->up_name.' '.$rFriend->up_lastname);?></li>
            <?php endforeach;?>
        </ul>
        <form method="post" action="<?php echo base_url();?>mobile/friend/search">
            <input type="hidden" name="keyword_friend" value="<?php echo $key;?>"/>
            <input type="submit" name="search" value="more.." style="border: none;background: none;color: #a4d63f;float: right;cursor: pointer;"/>
        </form>
        <br class="clear"/>
    <?php endif;?>    
    <?php if(!empty($songResults)):?>
        <label class="song">Song Results</label>
        <hr/>
        <ul>
            <?php foreach($songResults as $rSong):?>
            <li><?php echo $rSong->title;?></li>
            <?php endforeach;?>
        </ul>
        <form method="post" action="<?php echo base_url();?>mobile/track/search">
            <input type="hidden" name="track_q" value="<?php echo $key;?>"/>
            <input type="submit" name="search" value="more.." style="border: none;background: none;color: #a4d63f;float: right;cursor: pointer;"/>
        </form>
        <br class="clear"/>
    <?php endif;?>    
    </div>