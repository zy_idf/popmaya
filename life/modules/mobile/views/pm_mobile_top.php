<div class="pad520 bgx">
		
		<div class="w100">
                    <form class="w100" method="post" action="<?php echo base_url();?>mobile/search">
                            <label style=" color:#474747; float:left; padding-right:10px;"><?php echo $this->session->userdata('user_name');?></label><input type="text" name="serach_q" class="text left" /><input type="submit" class="btnshare right" value="Search" />
			</form>
            <div class="rate">
    			<span>In The Mood</span>
                        <?php for($i=1;$i<=5;$i++):?>
                            <?php echo image_asset('mobile/star.png','',array('alt'=>'mood star'));?>
                        <?php endfor;?>    			
    			<br class="clear"/>
    		</div>
		</div>
        <br class="clear"/>


	</div>
    <div class="pad220 howto">
		<div class="left" style="color:#fff;">Tutorial 1 How To</div>
		<div class="right" style="text-align:right;"><a href="home/tutorial" style="color:#a3d53f; ">More <?php echo image_asset('mobile/r-arrow.png','',array('alt'=>'more'));?></a></div>
        <br class="clear"/>
    </div>
	
	<div class="menu">
		<?php echo anchor('mobile/home', 'Home', (isset($active_menu) && $active_menu==='home')?array('class'=>'active'):''); ?>
		<?php echo anchor('mobile/member/profile', 'Profile', (isset($active_menu) && $active_menu==='profile')?array('class'=>'active'):''); ?>
		<?php echo anchor('mobile/artist', 'Artist', (isset($active_menu) && $active_menu==='artist')?array('class'=>'active'):''); ?>
		<?php echo anchor('mobile/track', 'Track', (isset($active_menu) && $active_menu==='track')?array('class'=>'active'):''); ?>
		<?php echo anchor('mobile/chatroom', 'Chat', (isset($active_menu) && $active_menu==='chatroom')?array('class'=>'active'):''); ?>
                <?php echo anchor('mobile/member/logout', 'Logout'); ?>
            <?php if(isset($submenu)):?>
            <hr/>
            <?php endif;?>
	</div>
        <?php if(isset($submenu)):?>
        <div style="margin-bottom: 5px;">
            <ul style="list-style: none;display: inline;margin-left: 0px;">
                <li style="display: inline;"><?php echo anchor('mobile/profile/'.$submenu,'Info',array('class'=>'info'));?></li>
                <li style="display: inline;"><?php echo anchor('mobile/post/'.$submenu,'Posts',array('class'=>'wall'));?></li>
                <li style="display: inline;"><?php echo anchor('mobile/note/'.$submenu,'Notes',array('class'=>'notes'));?></li>
                <li style="display: inline;"><?php echo anchor('mobile/photo/'.$submenu,'Photos',array('class'=>'image'));?></li>
                <li style="display: inline;"><?php echo anchor('mobile/video/'.$submenu,'Videos',array('class'=>'video'));?></li>
                <li style="display: inline;"><?php echo anchor('mobile/voice/'.$submenu,'Voices',array('class'=>'voice'));?></li>
            </ul>
        </div>
        <?php endif;?>