<div class="note pad520">
    <?php if(isset($album)):?>
        <?php echo anchor('mobile/photo','Album Photo "'.$album->title.'"',array('class'=>'image left'));?><label class="right"><i><?php echo $count;?> photo(s)</i></label><br class="clear"/>
    <?php else:?>
        <?php echo anchor('mobile/photo','Album Photo "Untitled Album"',array('class'=>'image left'));?><label class="right"><i><?php echo $count;?> photo(s)</i></label><br class="clear"/>
    <?php endif;?>
</div>
    <?php if(!empty($albumPhotos)):?>
    <?php if($path == $this->session->userdata('user_id')):?><form method="post"><?php endif;?>
        <?php if(isset($album)):?>
        <input type="hidden" name="albumID" value="<?php echo $album->ID;?>"/>
        <?php else:?>
        <input type="hidden" name="albumID" value="0"/>
        <?php endif;?>
    <div class="listfren prev">
      <ul>
          <?php foreach($albumPhotos as $row):?>
        <li>
            <div class="left "><?php echo anchor('mobile/photo/view/'.$row->ID.'/'.$path,image_asset('album/'.$path.'/'.$row->image,'',array('style'=>'max-width:41px;')));?><br /> <?php if($path == $this->session->userdata('user_id')):?><input type="radio" name="album_cover" value="<?php echo $row->ID;?>" <?php echo ($row->isCover==='1')?'checked="checked"':'';?>/><?php endif;?><label><?php echo $row->title;?></label></div>
        </li>        
        <?php endforeach;?>
      </ul>
    </div>
    <?php if($path == $this->session->userdata('user_id')):?>
    <div class="pad1020">
        <div class="note" style="text-align:center; padding:3px 0;">&nbsp;</div>
        <p>do you want to make selected photo as album cover?</p>
        <br class="clear"/>
        
        <button class="btnok left"></button>
        <?php if(isset($album)):?>
        <?php echo anchor('mobile/photo/upload_photo/'.$album->ID,'Upload Photo',array('class'=>'right'));?>
        <?php else:?>
        <?php echo anchor('mobile/photo/upload_photo/0','Upload Photo',array('class'=>'right'));?>
        <?php endif;?>
        <br class="clear"/>
    </div>
    <?php endif;?>
    <?php if($path == $this->session->userdata('user_id')):?>
    </form>
    <?php endif;?>
    <?php else:?>
    <div class="pad1020">
        <p style="margin: 5px 0;">There are no photos in this album.</p>
        <?php echo ($path == $this->session->userdata('user_id'))?anchor('mobile/photo/upload_photo/'.$album->ID,'Upload Photo',array('class'=>'right')):'';?>
    </div>
    <?php endif;?>