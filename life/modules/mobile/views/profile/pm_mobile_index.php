<div class="pad520 note">
        <label class="info">Info</label>
    </div>
    <div class="pad1020 listfren" style="color:#6d6e71;">
      <div class="left w25">
        <?php echo image_profile($image_path."/".$profile->up_uid.'_thumb.jpg',array('alt'=>$profile->up_uid,'style'=>'max-width:40px;float:left;margin-right:10px;'));?>
      </div>
      <div class="right w70">
        <strong style="color:#000;"><?php echo $profile->up_name;?></strong> <strong><i><?php echo $profile->up_alias;?></i></strong><br/>
        <?php if($profile->up_uid == $this->session->userdata('user_id')):?>
        <?php echo anchor('mobile/profile/edit_profile','Edit Profile');?>
        <?php endif;?>
<!--        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis at massa in erat mollis luctus.</p>-->
      </div>
      <br class="clear" />

      <ul>
          <label class="contact">Contact Info</label>
          <hr />
          <br />

              <li>
                  <label>Profile</label><span><?php echo $profile->up_name;?></span><br />
                  <label>E-mail</label><span><?php echo $profile->ul_email;?></span><br />
<!--                  <label>Yahoo! Messengger</label><input type="text" value="AlamIdf" size="5" /><br />-->
                  <label>Phone</label><span><?php echo $profile->up_mobile;?></span><br />
              </li>

        </ul>

        <ul>
          <label class="basic">Basic Info</label>
          <hr />
          <br />

              <li>
                  <label>Sex</label><span><?php echo ($profile->up_gender=='F') ? 'Female' : 'Male';?></span><br />
                  <label>Birthday</label><span><?php echo $profile->up_birthday;?></span><br />
<!--                  <label>Current City</label><span><?php //echo $profile->up_city;?></span><br />-->
                  <label>Hometown</label><span><?php echo $profile->up_city;?></span><br />
                  <label>Relationship</label><input type="text" value="In a relationship" /><br />
                  <label>Interested in</label><input type="text" value="Another Female" /><br />
                  <label>Languages</label><input type="text" value="Indonesian, Sundanesse" /><br />
                  <label>Religion</label><input type="text" value="Moslem" /><br />
              </li>

        </ul>

<!--        <ul>
          <label class="hobies">Hobby and Likes</label>
          <hr />
          <br />

              <li>
                  <label>Activities</label><input type="text" value="Web Designer" /><br />
                  <label>Interest</label><input type="text" value="Working" /><br />
                  <label>Sports</label><input type="text" value="Golf" size="10" /><br />
                  <label>Music</label><input type="text" value="Dangdut Koplo" size="10" /><br />
                  <label>TV Show</label><input type="text" value="The Walking Dead" /><br />
                  <label>Movies</label><input type="text" value="Drama, Romance" /><br />
                  <label>Books</label><input type="text" value="The Harry Potter" /><br />
                  <label>Quotes</label><input type="text" value="Bring Out The Big Bro" /><br />
                  <label>Bio</label><input type="text" value=" #####" /><br />
              </li>

        </ul>-->

    </div>