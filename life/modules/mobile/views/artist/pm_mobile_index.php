

    <div class="pad1020 bgx">
        <p><?php foreach(range('A', 'Z') as $letter) 
                {
                    echo anchor('mobile/artist/search/'.$letter,$letter.' ');
                } ?></p>
        <form method="post" action="<?php echo base_url();?>mobile/artist/search">
    		<input type="text" name="artist_q" class="text left w100" /><input type="submit" class="btnshare right" value="Search" />
            <br class="clear" />
    	
        <?php if(@$genre):?>
        <ul id="musiclist">
            <?php foreach($genre as $r_genre):?>
            <li><input type="checkbox" value="<?php echo $r_genre->g_id;?>" name="genre[]"/><label><?php echo $r_genre->g_name;?></label></li>
             <?php endforeach;?>
         </ul>
        <?php else:?>
        <p>Genre list is unavailable.</p>
        <?php endif;?>
            <br class="clear" />
        </form>
    </div>
    <div class="note pad520">
        <label>Artist List</label>
    </div>
	<div class="pad1020">
            <?php if(!empty($artist_list)):?>
        <ul class="track">
            <?php foreach($artist_list as $rartist):?>
            <li>
                <div class="left w20"><?php echo anchor('mobile/artist/profile/'.$rartist->ID,image_artist_thumb($rartist->ID.'.jpg', array('alt'=>$rartist->artist_name, 'style'=>'max-width:30px;')));?></div>
                <div class="left w45" style="padding:0 0 0 10px;">
                    <p style="color:#669900; text-transform:uppercase;"><?php echo anchor('mobile/artist/profile/'.$rartist->ID,$rartist->artist_name);?></p>
                    <p style="color:#404040;"><?php echo $rartist->g_name;?></p>                    
                </div>
                <br class="clear" />
            </li>
            <?php endforeach;?>     
        </ul>
        <?php else:?>
        <p>Artist lists are empty.</p>
        <?php endif;?>
	</div>

<!--	<div class="pad520" style="background:#A4D640;">
		<label style="background:url(rf-icon.png) no-repeat center left; padding: 3px 0 3px 40px; color:#fff;">Recomended Friends</label>
	</div>-->
<!--	<br class="clear" />-->
	<?php //if(@$recommended):?>
<!--            <div class="w15 left">
		<img src="inah.jpg" alt="" />
	</div>
	
	<div class="w85 right">
		<b>Inah nurjanah</b>
		<p>Lorem ipsum it dolor is amet consestursipiding alet</p>

		<br class="clear" />
	</div>-->
        <?php //else:?>
<!--        <p style="margin: 10px;">Recommended friend is unavailable.</p>-->
        <?php //endif; ?>