<div class="pad520 note">
        <label class="info">Info</label>
    </div>
    <div class="pad1020 listfren" style="color:#6d6e71;">
      <div class="left w25">
        <?php echo image_artist_thumb($profile->ID.'.jpg', array('alt'=>$profile->artist_name, 'style'=>'max-width:30px;'));?>
      </div>
      <div class="right w70">
        <strong style="color:#000;"><?php echo $profile->artist_name;?></strong>
        <p><?php echo $profile->title;?></p>
      </div>
      <br class="clear" />
      <?php if(!empty($fan)):?>
      <div class="mood pad520 bgx">
		<form method="post">
			<input type="text" class="text" name="status"/><input type="submit" class="btnshare" value="Share" />
		</form>
      </div>
      <?php endif;?>
      <div class="clear"></div>
      <div style="margin: 7px 0;">
          <?php echo anchor('mobile/artist/album/'.$profile->ID,'Album',array('style'=>'background:#a4d63f;color:#fff;padding: 2px;border:1px solid #c0c0c0;margin-right;3px;'));?>
          <?php echo anchor('mobile/artist/gallery/'.$profile->ID,'Gallery',array('style'=>'background:#a4d63f;color:#fff;padding: 2px;border:1px solid #c0c0c0;margin-right;3px;'));?>
          <?php echo anchor('mobile/artist/gallery/'.$profile->ID,'Message',array('style'=>'background:#a4d63f;color:#fff;padding: 2px;border:1px solid #c0c0c0;margin-right;3px;'));?>
          <?php echo anchor('mobile/artist/photo_contest/'.$profile->ID,'Photo Contest',array('style'=>'background:#a4d63f;color:#fff;padding: 2px;border:1px solid #c0c0c0;margin-right;3px;'));?>
      </div>
      <p><?php echo $profile->gb_description;?></p>
      <?php if(!empty($fan)):?>
        <?php echo anchor('mobile/artist/remove_fan/'.$profile->ID,  image_asset('mobile/del-btn.png','',array('style'=>'padding-top: 5px;margin-top:2px;')).' Remove Fan',array('style'=>'color: #E13300;'));?>
      <?php else:?>
        <?php echo anchor('mobile/artist/add_fan/'.$profile->ID,  image_asset('mobile/plus-icon.png','',array('style'=>'padding-top: 5px;margin-top:2px;')).' Add Fan',array('style'=>'color: #99cc33;'));?>
      <?php endif;?>
      <hr/>
      
    </div>