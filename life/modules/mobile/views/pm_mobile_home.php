
	<div class="note pad520">
		<span><?php echo $notification;?></span>Notification (<?php echo $notification;?>)
	</div>
        <?php if(@$notification_list):?>
	<div class="notelist pad520">
		<ul>
                    <?php foreach ($notification_list as $r_note):?>
                        <?php if(($r_note->n_type == 1) || ($r_note->n_type == 2) || ($r_note->n_type == 3)):?>
			<li><?php echo anchor('mobile/post/view/'.$r_note->n_itemid,'<b>'.$r_note->up_name.' '.$r_note->up_lastname.'</b><br/>'.$r_note->nt_name,array('style'=>'color:#000'));?></li>
                        <?php endif;?>
                        <?php if($r_note->n_type == 7):?>
			<li><?php echo anchor('mobile/notification/view/'.$r_note->n_id,'<b>'.$r_note->up_name.' '.$r_note->up_lastname.'</b><br/>'.$r_note->nt_name,array('style'=>'color:#000'));?></li>
                        <?php endif;?>
                        <?php if($r_note->n_type == 8):?>
			<li><?php echo anchor('mobile/friend/request/'.$r_note->n_itemid,'<b>'.$r_note->up_name.' '.$r_note->up_lastname.'</b><br/>'.$r_note->nt_name,array('style'=>'color:#000'));?></li>
                        <?php endif;?>
                        <?php if($r_note->n_type == 9):?>
			<li><?php echo anchor('mobile/message/view/'.$r_note->n_itemid,'<b>'.$r_note->up_name.' '.$r_note->up_lastname.'</b><br/>'.$r_note->nt_name,array('style'=>'color:#000'));?></li>
                        <?php endif;?>
                        <?php if(($r_note->n_type == 10) || ($r_note->n_type == 11)):?>
			<li><?php echo anchor('mobile/photo/view/'.$r_note->n_itemid,'<b>'.$r_note->up_name.' '.$r_note->up_lastname.'</b><br/>'.$r_note->nt_name,array('style'=>'color:#000'));?></li>
                        <?php endif;?>
                    <?php endforeach;?>
                        <li><?php echo anchor('mobile/notification/','See More Notification...');?></li>
		</ul>
	</div>
        <?php endif;?>
	
	<div class="mood pad520 bgx">
		<label>What's your mood today</label>
		<form method="post">
			<input type="text" class="text" name="status"/><input type="submit" class="btnshare" value="Share" />
		</form>
		<span>or 
			<?php echo anchor('mobile/post/image',image_asset('mobile/img-share1.png','',array('alt'=>'share image')));?>
			<?php echo anchor('mobile/post/video',image_asset('mobile/img-share2.png','',array('alt'=>'share video')));?>
			<?php echo anchor('mobile/post/voice',image_asset('mobile/img-share3.png','',array('alt'=>'share voice')));?>
			<?php echo anchor('mobile/post/note',image_asset('mobile/img-share4.png','',array('alt'=>'share note')));?>
		</span>
	</div>
	<br class="clear" />
	<div class="mood pad520 bgx">
		<label>Invite Friends</label>
                
                <div style="color: red;"><?php echo $this->session->flashdata('invite_errors');?></div>
                
                <form method="post">
			<i>By Email</i>
			<input type="text" class="text" name="invite_email"/><input type="submit" class="btnshare" value="Submit" />
			<i>By Phone</i>
			<input type="text" class="text" name="invite_phone"/><input type="submit" class="btnshare" value="Submit" />
			
		</form>
	</div>
	
        <?php if(@$status):?>
	<div class="pad1020">
            <?php echo image_profile($image_path."/".$status->up_uid.'_thumb.jpg', array('alt'=>'profile','style'=>'max-width:30px;float:left;margin-right:10px;'));?>
		<b><?php echo anchor('mobile/post/'.$status->up_uid,$status->up_name.' '.$status->up_lastname);?></b>
                <?php if($status->up_uid == $this->session->userdata('user_id')):?>
                        <?php echo anchor('mobile/post/delete_post/'.$status->ID,image_asset('mobile/del-btn.png','',array('alt'=>'delete')),array('style'=>'float:right;'));?>
                        <?php endif;?>
                <?php echo ($status->title !== NULL)?'<br/><em>'.$status->title.'</em><br/>':'';?>
		<?php if($status->type==3):?>
                    <?php echo image_asset($status->info,'',array('style'=>'max-width:140px;'));?><br/>
                <?php else:?>
                    <p><?php echo $status->info;?></p>
                <?php endif;?>
                <?php echo anchor('mobile/post/view/'.$status->ID,image_asset('mobile/cmn-icon.png','',array('alt'=>'cmn')));?>&nbsp;<?php echo anchor('mobile/post/like/'.$status->ID,image_asset('mobile/like_icon.png','',array('alt'=>'like')).''.$status->like);?>
		
		<br class="clear" />		
                
                <?php if($comment_status !== NULL):?>
                <?php foreach($comment_status as $r_comm):?>
                <div class="pad1020" style="margin-left: 30px;">
			<?php echo image_profile($r_comm[2], array('alt'=>$r_comm[0],'style'=>'max-width:30px;float:left;margin-right:10px;')); ?>		
			<b><?php echo anchor('mobile/post/'.$r_comm[3],$r_comm[0]);?></b><br/>
			<span><?php echo $r_comm[1];?></span>
                        <?php if(($r_comm[3] == $this->session->userdata('user_id')) || ($status->up_uid == $this->session->userdata('user_id'))):?>
                        <?php echo anchor('mobile/post/delete_comment/'.$r_comm[4],image_asset('mobile/del-btn.png','',array('alt'=>'delete')),array('style'=>'float:right;'));?>
                        <?php endif;?>
                        <div class="clear"></div>
		</div>
                <?php endforeach;?>
                <?php endif;?>
	</div>
        <?php endif;?>
	
	<div class="pad520" style="background:#000; color:#fff;">
		<label style="background:url(../assets/images/mobile/camera.jpg) no-repeat center left; padding-left:40px;">PHOTO COMPETITION</label>
	</div>
        <?php if(@$photo_challenge):?>
	<div class="pad1020" style="text-align:center;" >
		<a href=""><img src="competition.png" alt="PHOTO COMPETITION" /></a>
		<br />
		<p style="text-align:justify;"><?php echo $photo_challenge->title;?><br/>
			<i>by:<?php echo $photo_challenge->up_name.' '.$photo_challenge->last-name;?></i>
		</p>
		<p style="text-align:justify;"><?php echo $photo_challenge->description;?></p>
	</div>
	
	<div class="mood pad1020 bgx">
		<label>Add Comment</label>
		<form style="text-align:left;">
			<input type="text" class="text" name="photo_challenge_comment"/>
			<input type="submit" class="btnsend" value="" />
			
		</form>
	</div>        
	
	<div class="pad520">
		<b>Dian Sastro</b>
		<p>Lorem ipsum it dolor is amet consestur sipiding alet</p>
		
		<br class="clear" />
		
		<div class="w15 left">
			<img src="titi.jpg" alt="" />
		</div>
		<div class="w85 right">
			<b>Titi tumiwati</b>
			<p>Lorem ipsum it dolor is amet consestursipiding alet</p>
			<p style="color:#a4d640">15 minute ago</p>
			<a href=""><img src="cmn-icon.png" alt="" /></a>
			<a href=""><img src="fb-icon.png" alt="" /></a>
			<a href=""><img src="tw-icon.png" alt="" /></a>
			<br class="clear" />
		</div>
		
		<br class="clear" />
		<br class="clear" />
		
		<div class="w15 left">
			<img src="titi.jpg" alt="" />
		</div>
		<div class="w85 right">
			<b>Titi tumiwati</b>
			<p>Lorem ipsum it dolor is amet consestursipiding alet</p>
			<p style="color:#a4d640">15 minute ago</p>
			<a href=""><img src="cmn-icon.png" alt="" /></a>
			<a href=""><img src="fb-icon.png" alt="" /></a>
			<a href=""><img src="tw-icon.png" alt="" /></a>
			<br class="clear" />
		</div>
		<br class="clear" />
		
		<a href="" style="color:#719c1c; float:right;">View More <img src="r-arrow.png" alt="" /></a>
		<br class="clear" />
	</div>
        <?php else:?>
        <p style="margin: 10px 20px;">Photo challenge is unavailable.</p>
        <?php endif;?>
	
	<div class="pad520" style="background:#A4D640;">
		<label style="background:url(../assets/images/mobile/rf-icon.png) no-repeat center left; padding:3px 0 3px 40px;">Recommended Friends</label>
	</div>
	<?php if($recommend_friend !== NULL):?>
        <?php //foreach($recommend_friends as $rRec):?>
        <div class="pad1020">
                <?php echo image_profile($image_path."/".$recommend_friend->up_uid.'_thumb.jpg', array('alt'=>$recommend_friend->up_uid,'style'=>'max-width:30px;float:left;margin-right:10px;')); ?>
                <b><?php echo anchor('mobile/post/'.$recommend_friend->up_uid,$recommend_friend->up_name);?></b><br/>
		<span><?php echo (!empty($recommend_friend->up_city))?$recommend_friend->up_city:'&nbsp;';?></span>
                <?php echo anchor('mobile/friend/add_friend/'.$recommend_friend->up_uid,image_asset('mobile/plus-icon.png','',array('alt'=>'add')),array('style'=>'float:right;'));?>
                <div class="clear"></div>
	</div>
        <?php //endforeach;?>
        <?php else:?>
        <p style="margin: 10px;">Recommended friend is unavailable.</p>
        <?php endif; ?>
