<hr />
        <div style="text-align:center;margin-bottom: 10px;">
          <button class="btnnew">New</button>
          <button class="btnrecom">Recommended</button>
        </div>

    <div class="pad1020 bgx">
        <p><?php foreach(range('A', 'Z') as $letter) 
                {
                    echo anchor('mobile/track/search/'.$letter,$letter.' ');
                } ?></p>
        <form method="post" action="<?php echo base_url();?>mobile/track/search">
    		<input type="text" name="track_q" class="text left w100" /><input type="submit" class="btnshare right" value="Search" />
            <br class="clear" />
    	
       <?php if(@$genre):?>
        <ul id="musiclist">
            <?php foreach($genre as $r_genre):?>
            <li><input type="checkbox" value="<?php echo $r_genre->g_id;?>" /><label><?php echo $r_genre->g_name;?></label></li>
             <?php endforeach;?>
         </ul>
        <?php else:?>
        <p>Genre list is unavailable.</p>
        <?php endif;?>
            <br class="clear" />
        </form>
    </div>
    <div class="note pad520">
        <label class="result">Search Track By Keyword "<?php echo $key;?>"</label>
    </div>
    <div class="pad1020">
        <?php if(!empty($tracks)):?>
        <ul class="track">
            <?php foreach($tracks as $rtrack):?>
            <li>
                <div class="left w20"><?php echo image_asset('mobile/sid.jpg');?></div>
                <div class="left w45" style="padding:0 0 0 10px;">
                    <p style="color:#669900; text-transform:uppercase;"><?php echo $rtrack[0];?></p>
                    <p style="color:#404040;"><?php echo $rtrack[1];?></p>
                    <p><span style="color:#99cc33;"><?php echo $rtrack[3];?></span> <span style="color:#669900;"> Like </span><span style="padding:0 0 0 10px;"> | </span> <a href="" style="padding:0 0 0 10px;"><?php echo image_asset('mobile/fren.png','',array('alt'=>'add'));?></a></p>
                </div>
                <div class="left w30">
                    <span>Total Play <?php echo $rtrack[2];?></span>
                </div>
                <br class="clear" />
            </li>
            <?php endforeach;?>     
        </ul>
        <?php else:?>
        <p>Track list is empty.</p>
        <?php endif;?>
    </div>