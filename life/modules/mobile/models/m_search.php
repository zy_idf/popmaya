<?php
class M_Search extends CI_Model 
{
	
    function get_artist_result($key)
    {
        $query=$this->db->select('ID,artist_name')
                        ->from('tr_member_artist')
                        ->like('artist_name',$key)
                        ->limit(3)
                        ->get();
            return $query->result();
    }
    
    function get_friend_result($key)
    {
        $query=$this->db->select('tr_user_profile.up_uid, tr_user_profile.up_name, tr_user_profile.up_lastname')
                        ->from('tr_user_profile')
                        ->join('tr_friend', 'tr_friend.f_friendid=tr_user_profile.up_uid')
                        ->where('tr_friend.f_userid',  $this->session->userdata('user_id'))
                        ->like('tr_user_profile.up_name',$key)
                        ->or_like('tr_user_profile.up_lastname',$key)
                        ->limit(3)
                        ->get();
            return $query->result();
    }
    
    function get_song_result($key)
    {
        $query=$this->db->select('title')
                        ->from('tr_song')
                        ->like('title',$key)
                        ->limit(3)
                        ->get();
            return $query->result();
    }
    
}
?>