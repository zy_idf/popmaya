<?php
class M_Friends extends CI_Model 
{
    
        function getUserProfile($id)
	{
		$query=$this->db->select('tr_user_profile.*, tr_user_login.ul_email')
				->from('tr_user_profile')
				->join('tr_user_login', 'ul_id=up_uid')
				->where('up_uid', $id)
				->get();
		return $query->row();
	}
        
        function add_user_invite($input)
        {
            $this->db->insert('tr_user_invited', $input);
            return mysql_insert_id();
        }
        
        function add_invite_email($input,$toID)
        {
            $invite=$this->add_user_invite($input);
            $data=array(
                'n_type'=>8,
                'n_itemid'=>$invite,
                'n_fromuserid'=>$this->session->userdata('user_id'),
                'n_foruserid'=>$toID
            );
            $this->db->insert('tr_notification', $data);
        }
    
        function get_profile_path($id)
	{
		$query=$this->db->get_where('tr_user_login', array('ul_id'=>$id));
		$row=$query->row();
                if(isset($row->ul_createdon))
                {
                    $row=substr($row->ul_createdon, 0, 10);
                }
                else
                    $row=NULL;
		$path=str_replace('-', '/', $row);
		return $path;
	}
        
    /*
         * friend's model
         */
        function get_all_friends()
        {
            $query=$this->db->select('tr_friend.*, tr_user_profile.up_name, tr_user_profile.up_city')
			->from('tr_friend')
                        ->join('tr_user_profile', 'up_uid=f_friendid')
			->where('tr_friend.f_userid', $this->session->userdata('user_id'))
                        ->where('tr_friend.f_isactive',1)
                        ->get();
            return $query->result();
        }
        
        function get_all_friend_by($letter)
        {
            $query=$this->db->select('tr_friend.*, tr_user_profile.up_name, tr_user_profile.up_city')
			->from('tr_friend')
                        ->join('tr_user_profile', 'up_uid=f_friendid')
			->where('tr_friend.f_userid', $this->session->userdata('user_id'))
                        ->where('tr_friend.f_isactive',1)
                        ->like('tr_user_profile.up_name',$letter,'after')
                        ->get();
            return $query->result();
        }
        
        function get_all_friend_search($key)
        {
            $query=$this->db->select('tr_friend.*, tr_user_profile.up_name, tr_user_profile.up_city')
			->from('tr_friend')
                        ->join('tr_user_profile', 'up_uid=f_friendid')
			->where('tr_friend.f_userid', $this->session->userdata('user_id'))
                        ->where('tr_friend.f_isactive',1)
                        ->like('tr_user_profile.up_name',$key)
                        ->get();
            return $query->result();
        }
        
        function get_count_friend()
	{
		$count=$this->db->select('*')
			->from('tr_friend')
			->where('f_userid', $this->session->userdata('user_id'))
			->where('f_isactive', "1")
			->count_all_results();
		
		return $count;
	}
        
        function get_friend($id)
        {
            $query=$this->db->select('*')
                            ->from('tr_friend')
                            ->where('f_userid',$this->session->userdata('user_id'))
                            ->where('f_friendid',$id)
                            ->get();
            return $query->row();
        }
        
        function get_friend_invited($email)
        {
            $query=$this->db->select('*')
                            ->from('tr_user_invited')
                            ->where('i_userid',$this->session->userdata('user_id'))
                            ->where('i_email',$email)
                            ->get();
            return $query->row();
        }
        
        
        function get_friend_request($id)
        {
            $query=$this->db->select('tr_friend.*, tr_user_profile.up_uid, tr_user_profile.up_name, tr_user_profile.up_lastname, tr_notification.n_id, dr_notification_type.nt_name')
                            ->from('tr_friend')
                            ->join('tr_notification', 'tr_notification.n_itemid=tr_friend.f_id')
                            ->join('tr_user_profile', 'up_uid=tr_friend.f_userid')
                            ->join('dr_notification_type', 'nt_id=tr_notification.n_type')
                            ->where('tr_friend.f_id',$id)
                            ->where('tr_friend.f_isactive',0)
                            ->where('tr_notification.n_foruserid',$this->session->userdata('user_id'))
                            ->get();
            return $query->row();
        }
        
        function cek_friend($id,$toId)
        {
            $query=$this->db->select('*')
                            ->from('tr_friend')
                            ->where('f_userid',$id)
                            ->where('f_friendid',$toId)
                            ->get();
            return $query->row();
        }
        
        function add_request_friend($id)
        {
            $cek=$this->getUserProfile($id);
            if(!empty($cek))
            {
                $val1=  $this->cek_friend($this->session->userdata('user_id'), $id);
                if(empty($val))
                {
                    $val2=  $this->cek_friend($id,$this->session->userdata('user_id'));
                    if(empty($va2))
                    {
                        $set=array(
                            'f_userid'=>$this->session->userdata('user_id'),
                            'f_friendid'=>$cek->up_uid,
                            'f_isactive'=>0
                        );
                        $addID=$this->add_friend($set);

                        $data=array(
                        'n_type'=>8,
                        'n_itemid'=>$addID,
                        'n_fromuserid'=>$this->session->userdata('user_id'),
                        'n_foruserid'=>$cek->up_uid
                        );
                        $this->db->insert('tr_notification', $data);
                    }
                }
            }
        }
        
        function add_friend($post)
        {
            $this->db->insert('tr_friend', $post);
            return mysql_insert_id();            
        }
        
        function add_approve_friend($post,$toID,$id,$n_id)
        {
            $add=array(
                'f_isactive'=>1
            );
            $this->db->where('f_id', $id);
		$this->db->update('tr_friend', $add);
            
            $this->db->insert('tr_friend', $post);
            $data=array(
                'n_type'=>7,
                'n_fromuserid'=>$this->session->userdata('user_id'),
                'n_foruserid'=>$toID
            );
            $this->db->insert('tr_notification', $data);
            $this->db->delete('tr_notification',array('n_id'=>$n_id));
        }
        
        function rejected_friend_request($id,$n_id)
        {                        
            $this->db->delete('tr_friend',array('f_id'=>$id));
            $this->db->delete('tr_notification',array('n_id'=>$n_id));
        }
        
        function pending_friend_request()
        {
            $up=array(
                'n_isread'=>1
            );
            $this->db->where('n_id', $id);
		$this->db->update('tr_notification', $up);
        }
        
        function remove_friend($id)
        {
            $this->db->delete('tr_friend',array('f_userid'=>$id,'f_friendid'=>$this->session->userdata('user_id')));
            $this->db->delete('tr_friend',array('f_userid'=>$this->session->userdata('user_id'),'f_friendid'=>$id));
        }
        
        function get_recommendation($id, $limit)
	{
            //$offset=  $this->get_random_recomended();
            $this->db->select('a.*, c.ul_email');
	    $this->db->from('tr_user_profile a');
            $this->db->join('tr_user_login c', 'a.up_uid = c.ul_id', 'left');
            $this->db->where('a.up_uid !=',$this->session->userdata('user_id'));
		$this->db->limit(10);
		$query = $this->db->get();
	    return $query->result();	
	}
        
        function get_random_recomended()
        {
            $query=$this->db->count_all_results('tr_user_profile');
            $offset = rand(0, $query - 1);
            return $offset;
        }
}