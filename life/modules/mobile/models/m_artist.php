<?php
class M_Artist extends CI_Model 
{
	function getUserProfile($id)
	{
		$query=$this->db->select('tr_user_profile.*, tr_user_login.ul_email')
				->from('tr_user_profile')
				->join('tr_user_login', 'ul_id=up_uid')
				->where('up_uid', $id)
				->get();
		return $query->row();
	}
        
        function get_genre_list()
        {
            $query=$this->db->select('*')
                        ->from('tr_genre')
                        ->order_by('g_name','asc')
                        ->get();
            return $query->result();
        }
        
        function get_detail_artist($id)
        {
            $query=$this->db->select('a.*, b.g_name')
                            ->from('tr_member_artist a')
                            ->join('tr_genre b', 'b.g_id=a.genre')
                            ->where('a.ID',$id)
                            ->get();
                    return $query->row();
        }
        
        function get_artist_list()
        {
            $query=$this->db->select('tr_member_artist.*, tr_genre.g_name')
			->from('tr_member_artist')
                        ->join('tr_genre', 'g_id=genre')
                        ->get();
            return $query->result();
        }
        
        function get_all_artists_by($letter)
        {
            $query=$this->db->select('tr_member_artist.*, tr_genre.g_name')
			->from('tr_member_artist')
                        ->join('tr_genre', 'g_id=genre')
                        ->like('tr_member_artist.artist_name',$letter,'after')
                        ->get();
            return $query->result();
        }
        
        function get_all_artists_search($letter,$array)
        {
            if(!empty($array))
            {
                $query=$this->db->select('tr_member_artist.*, tr_genre.g_name')
                            ->from('tr_member_artist')
                            ->join('tr_genre', 'g_id=genre')
                            ->like('tr_member_artist.artist_name',$letter)
                            ->where_in('tr_member_artist.genre',$array)
                            ->get();
            }
            else
            {
                $query=$this->db->select('tr_member_artist.*, tr_genre.g_name')
                            ->from('tr_member_artist')
                            ->join('tr_genre', 'g_id=genre')
                            ->like('tr_member_artist.artist_name',$letter)
                            ->get();
            }
            return $query->result();
        }
        
        function get_artist_activity($id)
        {
            $query=$this->db->select('*')
                            ->from('tr_wall_artist')
                            ->where('artistID',$id)
                            ->get();
            return $query->result();
        }
        
        function get_track_list()
        {
            $query=$this->db->select('tr_song.*, tr_user_profile.up_name, tr_user_profile.up_lastname')
			->from('tr_song')
                        ->join('tr_user_profile', 'up_uid=memberID')
                        ->get();
            return $query->result();
        }
        
        function get_fan($id)
        {
            $user=$this->session->userdata('user_id');
            $query=$this->db->select('*')
                            ->from('tr_member_fans')
                            ->where('member_id',$user)
                            ->where('artist_id',$id)
                            ->get();
            return $query->row();
        }
        
        function add_fans($data)
        {
            $this->db->insert('tr_member_fans',$data);
        }
        
        function remove_fans($id)
        {
            $user=$this->session->userdata('user_id');
            $this->db->delete('tr_member_fans',array('member_id'=>$user,'artist_id'=>$id));
        }
}
?>