<?php
class M_Track extends CI_Model 
{
	function getUserProfile($id)
	{
		$query=$this->db->select('tr_user_profile.*, tr_user_login.ul_email')
				->from('tr_user_profile')
				->join('tr_user_login', 'ul_id=up_uid')
				->where('up_uid', $id)
				->get();
		return $query->row();
	}
        
        function get_genre_list()
        {
            $query=$this->db->select('*')
                        ->from('tr_genre')
                        ->order_by('g_name','asc')
                        ->get();
            return $query->result();
        }
        
        function get_mysong_list($id)
        {
            $query=$this->db->select('tr_fave_song.*, tr_song.*, tr_member_artist.ID as artistID, tr_member_artist.artist_name')
			->from('tr_fave_song')
                        ->join('tr_song', 'tr_song.s_id=tr_fave_song.songID')
                        ->join('tr_member_artist', 'tr_member_artist.ID=tr_song.artist_id')
                        ->where('tr_fave_song.memberID', $id)
                        ->get();
            return $query->result();
        }
        
        function get_track_list()
        {
            $query=$this->db->select('tr_song.*, tr_member_artist.ID as artistID, tr_member_artist.artist_name')
			->from('tr_song')
                        ->join('tr_member_artist', 'tr_member_artist.ID=tr_song.artist_id')
                        ->get();
            return $query->result();
        }
        
        function get_all_track_by($letter)
        {
            $query=$this->db->select('tr_song.*, tr_member_artist.ID as artistID, tr_member_artist.artist_name')
			->from('tr_song')
                        ->join('tr_member_artist', 'tr_member_artist.ID=tr_song.artist_id')
                        ->like('tr_song.title',$letter,'after')
                        ->get();
            return $query->result();
        }
        
        function get_all_track_search($key,$array)
        {
            if(!empty($array))
            {
            $query=$this->db->select('tr_song.*, tr_member_artist.ID as artistID, tr_member_artist.artist_name')
			->from('tr_song')
                        ->join('tr_member_artist', 'tr_member_artist.ID=tr_song.artist_id')
                        ->like('tr_song.title',$key)
                        ->where_in('tr_song.genreID',$array)
                        ->get();
            }
            else
            {
                $query=$this->db->select('tr_song.*, tr_member_artist.ID as artistID, tr_member_artist.artist_name')
			->from('tr_song')
                        ->join('tr_member_artist', 'tr_member_artist.ID=tr_song.artist_id')
                        ->like('tr_song.title',$key)
                        ->get();
            }
            return $query->result();
        }
}
?>