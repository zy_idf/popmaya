<?php
class M_Photos extends CI_Model 
{
        function getUserProfile($id)
	{
		$query=$this->db->select('tr_user_profile.*, tr_user_login.ul_email')
				->from('tr_user_profile')
				->join('tr_user_login', 'ul_id=up_uid')
				->where('up_uid', $id)
				->get();
		return $query->row();
	}    
        
        function get_profile_path($id)
	{
		$query=$this->db->get_where('tr_user_login', array('ul_id'=>$id));
		$row=$query->row();
		$row=substr($row->ul_createdon, 0, 10);
		$path=str_replace('-', '/', $row);
		return $path;
	}
        
        function get_count_album($id)
	{
		$count=$this->db->from('tr_album')
			->where('memberID', $id)
			->count_all_results();		
		return $count;
	}
        
        function get_user_album($id)
        {
            $query=$this->db->select('*')
                            ->from('tr_album')
                            ->where('memberID', $id)
                            ->get();
            return $query->result();
        }
        
        function get_photo_cover($user,$albID)
        {
            $query=$this->db->select('image')
                            ->from('tr_photo')
                            ->where('memberID',$user)
                            ->where('albumID',$albID)
                            ->where('isCover',1)
                            ->get();
            return $query->row();
        }
        
        function get_count_photo_album($user,$albID)
	{
		$count=$this->db->from('tr_photo')
			->where('memberID', $user)
                        ->where('albumID',$albID)
			->count_all_results();		
		return $count;
	}
        
        function get_photos_album($user,$albID)
        {
            $query=$this->db->select('*')
                            ->from('tr_photo')
                            ->where('memberID',$user)
                            ->where('albumID',$albID)
                            ->get();
            return $query->result();
        }
        
        function get_detail_album($id,$user)
        {
            $query=$this->db->select('*')
                            ->from('tr_album')
                            ->where('memberID',$user)
                            ->where('ID',$id)
                            ->get();
            return $query->row();
        }
        
        function get_detail_photo($user,$id)
        {
            $query=$this->db->select('*')
                            ->from('tr_photo')
                            ->where('memberID',$user)
                            ->where('ID',$id)
                            ->get();
            return $query->row();
        }
        
        function get_untitled_photo($user)
        {
            $query=$this->db->select('ID')
                            ->from('tr_photo')
                            ->where('memberID',$user)
                            ->where('albumID',0)
                            ->get();
            return $query->result();
        }
        
        function get_own_photo()
        {
            $query=$this->db->select('*')
                            ->from('tr_photo')
                            ->where('memberID',$this->session->userdata('user_id'))
                            ->get();
            return $query->row();
        }
        
        function get_count_like_photo($id)
        {
            $count=$this->db->from('tr_photo_like')
                        ->where('l_photo_id',$id)
			->count_all_results();		
		return $count;
        }
        
        function get_like_photo($id,$pid)
        {
            $query=$this->db->select('*')
                            ->from('tr_photo_like')
                            ->where('l_member_id',$id)
                            ->where('l_photo_id',$pid)
                            ->get();
            return $query->row();
        }
        
        function get_comment($id)
        {
            $query=$this->db->select('tr_photo_comment.ID, tr_photo_comment.comment, tr_user_profile.up_uid, tr_user_profile.up_name, tr_user_profile.up_lastname')
                        ->from('tr_photo_comment')
                        ->join('tr_user_profile', 'tr_user_profile.up_uid=tr_photo_comment.memberID')
                        ->where('tr_photo_comment.photoID', $id)
                        ->order_by('tr_photo_comment.ID','asc')
                        ->get();
            return $query->result();
        }
        
        function get_own_comment($id)
        {
            $query=$this->db->select('tr_photo_comment.*, tr_photo.memberID as memID, tr_photo.albumID')
                            ->from('tr_photo_comment')
                            ->join('tr_photo','tr_photo.ID=tr_photo_comment.photoID')
                            ->where('tr_photo_comment.ID',$id)
                            ->where('tr_photo_comment.memberID',$this->session->userdata('user_id'))
                            ->or_where('tr_photo_comment.memberID',$this->session->userdata('user_id'))
                            ->get();
            return $query->row();
        }
        
        function add_photo($post)
        {
            $this->db->insert('tr_photo', $post);
        }
        
        function add_comment($post)
        {
            $this->db->insert('tr_photo_comment', $post);
        }
        
        function add_like($post)
        {
            $this->db->insert('tr_photo_like', $post);
        }

        function add_notif($post)
        {
            $this->db->insert('tr_notification', $post);
        }
        
        function update_cover($id)
        {
            $set=array(
                    'isCover'=>0
                );
            $this->db->where('albumID', $id);
                    $this->db->update('tr_photo', $set);
        }
        
        function update_cover_active($id,$alb)
        {
            $set=array(
                    'isCover'=>1
                );
            $this->db->where('ID', $id);
            $this->db->where('albumID', $alb);
                    $this->db->update('tr_photo', $set);
        }
        
        function update_notif_photo($id)
        {
            $notif=array(
                    'n_isread'=>1
                );
            $this->db->where('n_itemid', $id);
            $this->db->where('n_foruserid',$this->session->userdata('user_id'));
            $this->db->where('n_type', 10);
            $this->db->or_where('n_type', 11);
                    $this->db->update('tr_notification', $notif);
        }
        
        function delete_photo($id)
        {
            $this->db->delete('tr_photo_comment',array('photoID'=>$id));
            $this->db->delete('tr_photo_like',array('l_photo_id'=>$id));
            $this->db->delete('tr_notification',array('n_itemid'=>$id,'n_type'=>'10'));
            $this->db->delete('tr_notification',array('n_itemid'=>$id,'n_type'=>'11'));
            $this->db->delete('tr_photo',array('ID'=>$id));
        }
        
        function delete_comment($id)
        {
            $this->db->delete('tr_photo_comment',array('ID'=>$id));
            //$this->db->delete('tr_notification',array('n_id'=>$n_id));        
        }
}