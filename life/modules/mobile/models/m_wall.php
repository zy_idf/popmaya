<?php
class M_Wall extends CI_Model 
{
    //protected $table        = 'tr_wall_item';
    const STATUS=1;
    const VOICE=2;
    const VIDEO=4;
    const IMAGE=3;
    const NOTE=5;

    function getUserProfile($id)
	{
		$query=$this->db->select('tr_user_profile.*, tr_user_login.ul_email')
				->from('tr_user_profile')
				->join('tr_user_login', 'ul_id=up_uid')
				->where('up_uid', $id)
				->get();
		return $query->row();
	}
        
    function get_profile_path($id)
    {
            $query=$this->db->get_where('tr_user_login', array('ul_id'=>$id));
            $row=$query->row();
            $row=substr($row->ul_createdon, 0, 10);
            $path=str_replace('-', '/', $row);
            return $path;
    }
        
    function add_wall($post)
    {
         $this->db->insert('tr_wall_item', $post);
    }
    
    function add_comment($post)
    {
        $this->db->insert('tr_wall_item_comment', $post);
    }
    
    function add_notif($post)
    {
        $this->db->insert('tr_notification', $post);
    }
    
    function add_like_post($post)
    {
        $this->db->insert('tr_wall_item_like', $post);
    }
    
    function add_photo($post)
    {
        $this->db->insert('tr_photo', $post);
        return mysql_insert_id();
    }
    
    function get_post_detail_type($id,$type)
    {
        $query=$this->db->select('tr_wall_item.ID, tr_wall_item.title, tr_wall_item.info, tr_wall_item.type, tr_wall_item.like, tr_wall_item.memberID, tr_user_profile.up_uid, tr_user_profile.up_name, tr_user_profile.up_lastname')
                        ->from('tr_wall_item')
                        ->join('tr_user_profile', 'up_uid=memberID')
                        ->where('tr_wall_item.ID', $id)
                        ->where('tr_wall_item.type', $type)
                        //->where('memberID',$this->session->userdata('user_id'))
                        ->get();
            return $query->row();
    }
    
    function get_post_detail($id)
    {
        $query=$this->db->select('tr_wall_item.ID, tr_wall_item.title, tr_wall_item.info, tr_wall_item.type, tr_wall_item.like, tr_wall_item.memberID, tr_user_profile.up_uid, tr_user_profile.up_name, tr_user_profile.up_lastname')
                        ->from('tr_wall_item')
                        ->join('tr_user_profile', 'up_uid=memberID')
                        ->where('tr_wall_item.ID', $id)
                        ->get();
            return $query->row();
    }
    
    function get_detail_photo($user,$id)
    {
        $query=$this->db->select('*')
                        ->from('tr_photo')
                        ->where('memberID',$user)
                        ->where('ID',$id)
                        ->get();
        return $query->row();
    }
    
    function get_notif($type,$item,$forid)
    {
        $query=$this->db->select('*')
                        ->from('tr_notification')
                        ->where('n_type',$type)
                        ->where('n_itemid',$item)
                        ->where('n_fromuserid',$this->session->userdata('user_id'))
                        ->where('n_foruserid',$forid)
                        ->get();
        return $query->row();
    }
    
    function get_like_post($id,$wid)
    {
        $query=$this->db->select('*')
                        ->from('tr_wall_item_like')
                        ->where('l_member_id',$id)
                        ->where('l_wall_id',$wid)
                        ->get();
        return $query->row();
    }
    
    function update_like_post($id,$like)
    {
        $this->db->where('ID', $id);
		$this->db->update('tr_wall_item', $like);
    }
    
    function update_notif_post($id)
    {
        $notif=array(
                'n_isread'=>1
            );
        $this->db->where('n_itemid', $id);
        $this->db->where('n_foruserid',$this->session->userdata('user_id'));
        $this->db->where('n_type', 1);
        $this->db->or_where('n_type', 2);
        $this->db->or_where('n_type', 3);
		$this->db->update('tr_notification', $notif);
    }
    
    function get_own_post($id)
    {
        $query=$this->db->select('*')
                        ->from('tr_wall_item')
                        ->where('memberID',$this->session->userdata('user_id'))
                        ->get();
        return $query->row();
    }
    
    function get_comment_status($id)
        {
            $query=$this->db->select('tr_wall_item_comment.ID, tr_wall_item_comment.comment, tr_user_profile.up_uid, tr_user_profile.up_name, tr_user_profile.up_lastname')
                        ->from('tr_wall_item_comment')
                        ->join('tr_user_profile', 'up_uid=memberID')
                        ->where('wall_itemID', $id)
                        ->order_by('tr_wall_item_comment.ID','asc')
                        ->get();
            return $query->result();
        }
        
        function get_count_comment($id)
        {
            $count=$this->db->from('tr_wall_item_comment')
			->where('wall_itemID', $id)
//                        ->where('type',$type)
			->count_all_results();
		
		return $count;
        }
    
    function delete_post($id)
    {
        $this->db->delete('tr_wall_item_comment',array('wall_itemID'=>$id));
        $this->db->delete('tr_wall_item',array('ID'=>$id));
    }
    
    function get_own_comment($id)
    {
        $query=$this->db->select('tr_wall_item_comment.*, tr_wall_item.memberID, tr_wall_item.type')
                        ->from('tr_wall_item_comment')
                        ->join('tr_wall_item','tr_wall_item.ID=tr_wall_item_comment.wall_itemID')
                        ->where('tr_wall_item_comment.ID',$id)
                        ->where('tr_wall_item_comment.memberID',$this->session->userdata('user_id'))
                        ->or_where('tr_wall_item.memberID',$this->session->userdata('user_id'))
                        ->get();
        return $query->row();
    }
    
    function get_all_wall_type($id,$type)
    {
        $query=$this->db->select('tr_wall_item.ID, tr_wall_item.title, tr_wall_item.info, tr_wall_item.file, tr_wall_item.like, tr_user_profile.up_uid, tr_user_profile.up_name, tr_user_profile.up_lastname')
                        ->from('tr_wall_item')
                        ->join('tr_user_profile', 'up_uid=memberID')
                        ->where('tr_wall_item.toMemberID',$id)
                        ->where('tr_wall_item.type',$type)
                        ->order_by('tr_wall_item.ID','desc')
                        ->get();
            return $query->result();
    }
    
    function get_all_wall($id)
    {
        $query=$this->db->select('tr_wall_item.ID, tr_wall_item.title, tr_wall_item.info, tr_wall_item.type, tr_wall_item.like, tr_user_profile.up_uid, tr_user_profile.up_name, tr_user_profile.up_lastname')
                        ->from('tr_wall_item')
                        ->join('tr_user_profile', 'up_uid=memberID')
                        ->where('tr_wall_item.memberID',$id)
                        ->order_by('tr_wall_item.ID','desc')
                        ->get();
            return $query->result();
    }
    
    function delete_comment($id)
    {
        $this->db->delete('tr_wall_item_comment',array('ID'=>$id));
        //$this->db->delete('tr_notification',array('n_id'=>$n_id));        
    }
}