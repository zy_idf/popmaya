<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

/*
Nama: Nisa
Tanggal: 21 Nov 2011
Deskripsi: Home controller
*/

class Home extends MX_Controller 
{
	var $user_id;
	
	function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->model('m_home', '', TRUE);

		$this->user_id=$this->session->userdata('user_id');

		//$this->output->enable_profiler(TRUE);
	}

	//------------------------------------------------------------------------------------------------------------------------homepage
	/*
	Nama: Nisa
	Tanggal: 21 Nov 2011
	Deskripsi: Halaman depan website, menampilkan login, register, dan front page dari situs
	*/	
	function index()
	{		
		if (!$this->ion_auth->logged_in()) //not logged in
		{
			$this->config->load('my_config');
			
			//facebook
			$fb_appid=$this->config->item('facebook_app_id');
			$fb_secret=$this->config->item('facebook_app_secret');			
			$data['fb_appid']=$fb_appid;
			$data['fb_secret']=$fb_secret;
			
			//updated
			$data['updated_artist']=$this->m_home->get_updated_artist();
			//$hour=('Y-m-d h:i:s', $data['updated_artist']->timestamp);
			$data['updated_artist_hour']=0;//date_sub($hour, now());
			
			//bottom
			$data['recommended_track']=$this->m_home->get_recommended_track();
			$new_track_limit=$this->config->item('limit_new_track');
			$data['new_track']=$this->m_home->get_new_track($new_track_limit);

			$data['slide_home']=$this->m_home->slide_home(); // ^zy
			$data['benefit_artist']=$this->m_home->get_benefit('artist'); // ^zy
			$data['benefit_brand']=$this->m_home->get_benefit('brand'); // ^zy
			$data['benefit_fan']=$this->m_home->get_benefit('fan'); // ^zy


			$this->load->view('general', $data);
		}
		else //logged in
		{
			//redirect to the profile (my profile) page
			redirect("member/profile", 'refresh');
		}
	}
	//------------------------------------------------------------------------------------------------------------------------end homepage	
	
	//------------------------------------------------------------------------------------------------------------------------register
	function register()
	{
		//validation rules
		$this->form_validation->set_rules('fullname', 'Fullname', 'trim|required|prep_for_form|htmlspecialchars|encode_php_tags|xss_clean|min_length[5]|max_length[20]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|prep_for_form|htmlspecialchars|encode_php_tags|valid_email|callback_new_email|min_length[5]|max_length[50]|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|alpha_numeric|required|prep_for_form|htmlspecialchars|encode_php_tags|min_length[5]|max_length[20]|xss_clean');

		//validation message
		$this->form_validation->set_message('required', '%s is required.');
		$this->form_validation->set_message('alpha_numeric', 'Only alphanumeric characters are allowed.');
		$this->form_validation->set_message('valid_email', 'Wrong email format.');
		$this->form_validation->set_message('min_length[5]', 'Min. character is 5 chars.');    	
		$this->form_validation->set_message('max_length[20]', 'Max. character is 20 chars.');
		$this->form_validation->set_message('max_length[50]', 'Max. character is 50 chars.');

		$this->form_validation->set_error_delimiters('','');


		if ($this->form_validation->run() == true)
		{
			if($this->input->post('agree')=="agreed")
			{			
				$fullname = $this->input->post('fullname');
//				$last_name = $this->input->post('last_name');
				$email = $this->input->post('email');
				$password = $this->input->post('password');

				$additional_data = array('gender' => $this->input->post('gender'),'lastname' => $this->input->post('last_name'),
					'update' => $this->input->post('update')
				);


				if($this->ion_auth->register($fullname, $password, $email, $additional_data))
				{
					//check to see if we are creating the user
					$this->session->set_flashdata('register_error', "User Created, Please check your email.");
					redirect('home', 'refresh');
				}
				else
				{ 
					//display the register user form
					$this->session->set_flashdata('register_error', $this->ion_auth->errors());
					redirect('home', 'refresh'); 
				}
			}
			else
			{
				$this->session->set_flashdata('register_error', "You must agree with the Term & Service");
				redirect('home', 'refresh'); 				
			}
		}
		else
		{
			$this->session->set_flashdata('register_error', validation_errors());
			redirect('home');			
		}
	}
	
	function activate($id, $code=false)
	{
		if ($code !== false)
		{
			$activation = $this->ion_auth->activate($id, $code);
		}
		else if ($this->ion_auth->is_admin())
		{
			$activation = $this->ion_auth->activate($id);
		}

		if ($activation)
		{
			//redirect them to the auth page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect('home', 'refresh');
		}
		else
		{
			//redirect them to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect('home/forgot', 'refresh');
		}
	}	
	//------------------------------------------------------------------------------------------------------------------------end register
	
	//------------------------------------------------------------------------------------------------------------------------login
	function login()
	{
		if ($this->ion_auth->logged_in()) //already logged in so no need to access this page
		{			
			redirect("member/profile", 'refresh');
		}

		//validation rules
	    $this->form_validation->set_rules('email', 'Email', 'trim|prep_for_form|htmlspecialchars|encode_php_tags|required|valid_email|min_length[5]|max_length[50]|xss_clean');
	    $this->form_validation->set_rules('password', 'Password', 'trim|alpha_numeric|prep_for_form|htmlspecialchars|encode_php_tags|required|min_length[5]|max_length[20]|xss_clean');

		//validation message
		$this->form_validation->set_message('required', '%s is required.');
		$this->form_validation->set_message('alpha_numeric', 'Only alphanumeric characters are allowed.');
		$this->form_validation->set_message('valid_email', 'Wrong email format.');
		$this->form_validation->set_message('min_length[5]', 'Min. character is 5 chars.');    	
		$this->form_validation->set_message('max_length[20]', 'Max. character is 20 chars.');
		$this->form_validation->set_message('max_length[50]', 'Max. character is 50 chars.');

		//error delimiter
		$this->form_validation->set_error_delimiters('', '');

		if($this->form_validation->run()==TRUE) //validation success
		{
			//check to see if the user is logging in
			//check for "remember me"
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('email'), $this->input->post('password'), $remember))
			{ 	
				//if the login is successful
				//redirect them back to the home page
				$this->session->set_flashdata('login_error', $this->ion_auth->messages());
				redirect("member/profile", 'refresh');
			}
			else
			{ 
				//if the login was un-successful
				//redirect them back to the login page
				$this->session->set_flashdata('login_error', $this->ion_auth->errors());
				redirect('home/wrong', 'refresh'); 
			}			
		}
		else //validation failed
		{
			//the user is not logging in so display the login page
			//set the flash data error message if there is one			
			$this->session->set_flashdata('login_error', validation_errors());
			redirect('home/wrong', 'refresh'); 
		}
	}
	
	function facebook_login()
	{
		parse_str($_SERVER['QUERY_STRING'],$_GET);

		if(isset($_GET['code'])) 
		{
			$this->load->library("curl");

			$this->config->load('my_config');
			$fb_appid=$this->config->item('facebook_app_id');
			$fb_secret=$this->config->item('facebook_app_secret');

			// buat url untuk mengambil token
			$url = 'https://graph.facebook.com/oauth/access_token?client_id='.$fb_appid.'&redirect_uri='.base_url()."member/facebook_login".'&client_secret='.$fb_secret.'&code='.$_GET['code'];

			// ambil token lewat curl
			$token_data = $this->curl->simple_get($url);

			// ambil kode token saja, dengan regular expression
			// arti tanda ([^&]+) adalah:
			// ambil semua karakter asal bukan tanda &
			preg_match("/access_token=([^&]+)/",$token_data,$token);

			// kode token ada di variabel token[1]
			$access_token = $token[1];

			// pengambilan token selesai, sekarang ambil userid, nama
			$uri = 'https://graph.facebook.com/me?access_token='.$access_token;
			$data = $this->curl->simple_get($uri);

			// decode data
			$fb = json_decode($data);
			$fb_id = $fb->id;

			// ambil nama dan foto pengguna
			$fb_userdata = $this->curl->simple_get("https://graph.facebook.com/".$fb_id."?fields=name,picture,gender,email,id&access_token=".$access_token);

			//echo "https://graph.facebook.com/".$fb_id."?fields=name,picture&access_token=".$access_token;

			$fb_user = json_decode($fb_userdata);

			$data = array();
			//$data['fbuser'] = array('id'=>$fb_id, 'avatar'=> $fb_user->picture, 'nama'=> $fb_user->name, 'gender'=> $fb_user->gender, 'id'=> $fb_user->id, 'email'=> $fb_user->email);

			$name = $fb_user->name;
			$email = $fb_user->email;
			$fbid = $fb_user->id;
			$gender = strtoupper(substr($fb_user->gender,0,1));
			$pwd = '';

			$uid = $this->m_member->fb_id_exists($fbid);
			if($uid == FALSE) 
			{
				$this->save_register($name,$email,$pwd,$gender,1,$fbid,urlencode($fb_user->picture),'','');
				$this->session->set_userdata('fbwall',$name);
			} 
			else 
			{
				$this->session->set_userdata('uloginid', $uid);

				$ipaddress	= $this->input->ip_address();
				$browser	= $this->input->user_agent();
				$location 	= $this->m_member->get_geolocation($ipaddress);

				$this->m_front->add_session($uid,$ipaddress,$browser,$location,$session);
				$this->session->set_userdata('ulogin_id_session',$session);

			}
			redirect('member/profile', 'refresh');
		}
		elseif(isset($_GET['error_reason'])) 
		{
			// untuk menangkap user yang klik "Dont Allow" atau "Cancel di Facebook"
			// buat variabel untuk ditampilkan di view
			$this->index();
		}

	}

	function twitter_login()
	{
		$tokens['access_token'] = NULL;
		$tokens['access_token_secret'] = NULL;

		// GET THE ACCESS TOKENS
		$oauth_tokens = $this->session->userdata('twitter_oauth_tokens');

		if ( $oauth_tokens !== FALSE ) $tokens = $oauth_tokens;

		$this->load->library('twitter');

		$this->config->load('my_config');
		$tw_key=$this->config->item('twitter_key');
		$tw_key_secret=$this->config->item('twitter_secret');

		$auth = $this->twitter->oauth($tw_key, $tw_key_secret, $tokens['access_token'], $tokens['access_token_secret']);

		if (isset($auth['access_token']) && isset($auth['access_token_secret']) )
		{
			// SAVE THE ACCESS TOKENS

			$this->session->set_userdata('twitter_oauth_tokens', $auth);

			if ( isset($_GET['oauth_token']) )
			{
				$uri = $_SERVER['REQUEST_URI'];
				$parts = explode('?', $uri);

				// Now we redirect the user since we've saved their stuff!
				header('Location: '.$parts[0]);
				return;
			}
		}
	}
	
	function save_register($name,$email,$pwd,$gender,$sendupdate,$fbid='',$picfb='',$twid='',$twpic='') 
	{
		$twimg 		= urldecode($twpic);
	    $ipaddress	= $this->input->ip_address();
	    $location 	= $this->m_member->get_geolocation($ipaddress);
	    $browser	= $this->input->user_agent();

	    $this->load->library('randomized');
	    $vericode = $this->randomized->random_generator(10);

	    $uid = $this->m_member->save_new_member($name,$email,$pwd,$gender,$sendupdate,$location,$vericode,$fbid,$twid);
	    $this->session->set_userdata('uloginid', $uid);
	    $this->m_member->add_session($uid,$ipaddress,$browser,$location,$session);
	    $this->send_mail($name,$email,$vericode);
	    $this->session->set_userdata('ulogin_id_session',$session);
	
	    if($fbid != '') 
		{
            $this->save_pic_fb($uid,$fbid,$picfb);
	    }
	    if($twimg != '') 
		{
            $this->save_pic_fb($uid,$twid,$twimg);
	    }
	}	

	function sendmail($name,$email,$code) 
	{
		$this->load->model('m_sendmail');
		$link = $this->config->item('base_url').'verify/email/'.$code;
		$cat = 'verify email';
		$from = $this->m_sendmail->get_mailfrom($cat);
		$to = $email;
		$subject = $this->m_sendmail->get_mailsubject($cat);
		$body = $this->m_sendmail->get_mailbody($cat);
		$search  = array('[name]', '[link]');
		$replace = array($name, $link);
		$body= str_replace($search, $replace, $body);
		$mailsent = $this->m_sendmail->send($from,$to,$subject,$body);
	}

	function savepicfb($uid,$fbid,$link) 
	{
		$link = urldecode($link);
		$picname = $uid.'_'.$fbid.'.jpg';
		$contents= file_get_contents($link);

		//original				
		$savefile = fopen('assets/images/profile/'.$picname, 'w');
		fwrite($savefile, $contents);
		fclose($savefile);

		//public
		$savefile2 = fopen('assets/images/profile/'.$picname, 'w');
		fwrite($savefile2, $contents);
		fclose($savefile2);

		//thumbnail
		$savefile3 = fopen('assets/images/profile/'.$picname, 'w');
		fwrite($savefile3, $contents);
		fclose($savefile3);

		$this->m_member->save_pic_from_fb($uid,$picname);
	}
	//------------------------------------------------------------------------------------------------------------------------end login
	
	//------------------------------------------------------------------------------------------------------------------------logoout
	function logout()
	{
		//log the user out
		$logout = $this->ion_auth->logout();

		//redirect them back to the page they came from
		$this->session->set_flashdata('message', "Logout success");
		redirect('home', 'refresh');
	}	
	//------------------------------------------------------------------------------------------------------------------------end logout
	
	//------------------------------------------------------------------------------------------------------------------------forgot password
	function wrong()
	{
		//facebook
		$fb_appid=$this->config->item('facebook_app_id');
		$fb_secret=$this->config->item('facebook_app_secret');			
		$data['fb_appid']=$fb_appid;
		$data['fb_secret']=$fb_secret;
		
        $this->load->view('login/wrong_pass', $data);        
	}	
	
	function forgot()
	{
        //facebook
		$fb_appid=$this->config->item('facebook_app_id');
		$fb_secret=$this->config->item('facebook_app_secret');			
		$data['fb_appid']=$fb_appid;
		$data['fb_secret']=$fb_secret;
		
        $this->load->view('login/forgot', $data);
    }
		
	function forgot_process()
	{
		$this->form_validation->set_rules('email', 'Email', 'xss_clean|required|valid_email|min_length[5]|max_length[50]');
		
		if ($this->form_validation->run() == true)
		{
			//run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($this->input->post('email'));					
			
			if ($forgotten)  //if there were no errors
			{
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect('home/forgot', 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('home/forgot', 'refresh');
			}			
		}
		else
		{
			//no email entered
			$this->session->set_flashdata('message', validation_errors());
			redirect('home/forgot');			
		}
	}
	
	function reset_password($code='')
	{
		if($code)
		{
			$reset = $this->ion_auth->forgotten_password_complete($code);

			if ($reset)
			{  
				//if the reset worked then send them to the login page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect('home', 'refresh');
			}
			else
			{ 
				//if the reset didnt work then send them back to the forgot password page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('home/forgot', 'refresh');
			}
		}
		else
		{
			redirect('home', 'refresh');
		}		
	}
	//------------------------------------------------------------------------------------------------------------------------end forgot password
}
?>