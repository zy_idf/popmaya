<!DOCTYPE html>
<html lang="en">
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title><?= $head_title ?></title>
  <link  href="<?= site_url() ?>assets/images/favicon.png" rel="shortcut icon" type="image/x-icon" />
  <meta name="description" content="popmaya � sosial network musik indonesia" />
  <meta name="keywords" content="<?= $keywords ?>" />
  <meta name="author" content="www.theideafield.com" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=xa-4efad9765532c16c"></script>
  			
 
  <?= css_asset('reset.css'); ?>
  <?= css_asset('text.css'); ?>
  <?= css_asset('grid.css'); ?>
  <?= css_asset('style.css'); ?>
  <?= css_asset('jwysiwyg/jquery.wysiwyg.css'); ?>
  <?= css_asset('fileuploader.css'); ?>
</head>
<body>
	
    <div id="header">
        <div class="container_16 main-header">
            <div class="grid_3 logo"><h1><a href="<?= site_url(); ?>"><?= image_asset('pop_maya_logo.png', '',array('alt'=>'Popmaya')) ?></a></h1></div>
            <div class="grid_8" id="nav">
                <ul>
                <?php 
                  $act1=$act2=$act3='';
                  $act=array('class'=>'active');
                  switch($this->uri->segment(1)){
                    case 'artist' : $act1=$act; break;
                    case 'fans' : $act2=$act; break;
                    case 'track' : $act3=$act; break;
                  }
                ?>
                    <li><?= anchor(site_url().'artist', 'Artist', $act1); ?></li>
                    <!--<li><?= anchor(site_url().'artist', 'Brand' ); ?></li>-->
                    <li><?= anchor(site_url().'fans', 'Fans', $act2); ?></li>
                    <li><?= anchor(site_url().'track', 'Songs', $act3); ?></li>
                </ul>               
 
    			<!-- Nisa. 22 Des 2011. Search -->
                <form action="<?= base_url(); ?>member/search" method="post" id="form">
                    <input type="text" id="searchbox" name="searchbox" title="searchbox" placeholder="Search" class="text-input" />
                    <input type="image" src="<?= image_asset_url('search_ico.png') ?>" alt="Search" class="submit" />
                </form>    
                <div id="ajax_search">
                </div> 
				<!-- end Nisa -->
				
            </div>
            <div class="grid_4 profile-menu">
                <?= profile_menu_top() ?>
            </div>
        </div>
        <div class="clear"></div>
    </div>
<div class="container_16" id="wrapper">  
  <div class="grid_3" id="left-column">
   	<?= $sidebar_left ?>
    <br class="clear"/>
  </div>
  <!-- end .grid_3 -->
  <div class="grid_8" id="middle-column">
     <?= $middle_content ?>
     <br class="clear"/>
  </div>
  <div class="grid_4" id="right-column">
   	<?= $sidebar_right ?>
  </div>
  <!-- end .grid_13 -->
  
    <!-- end .grid_16 -->
    <div class="clear">&nbsp;</div>
</div>
<div id="song_player"></div>
<div style="display:block; height:20px">&nbsp;</div>
<?= js_asset('jRecorder/jquery.min.js');?>
<?= js_asset('custom.js');?>
<?php if($this->session->userdata('user_id')){ echo css_asset('chat.min.css').js_asset('chat.min.js'); } /* ^zy */ ?>
<!-- tambah js untuk csspopup by gilangaramadan 13Des11 -->
<?= js_asset('csspopup.js');?>
<?php if(isset($foot_js)): ?>
	<?= $foot_js ?>
<?php endif;?>
  
<script type="text/javascript">
   window.updateurl="<?php echo site_url("artist/status_update_artist");?>";
   window.profileurl="<?php echo site_url("artist/profile");?>";
   window.jsUrl="<?php echo js_asset_url('');?>";
   window.uploadurl="<?php echo site_url("artist/file_upload_artist");?>";
   $(window).load(function(){
      <?= $foot_script ?>
   });   
</script>
<!-- end .container_16 -->
</body></html>