<!DOCTYPE html>
<html lang="en">
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>popmaya</title>
  <link  href="<?= site_url() ?>assets/images/favicon.png" rel="shortcut icon" type="image/x-icon" />
  <meta name="description" content="popmaya » sosial network musik indonesia" />
  <meta name="keywords" content="Biggest sound based social media" />
  <meta name="author" content="www.theideafield.com" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  
  <!-- Firman : add share library -->
  
  <?= css_asset('reset.css'); ?>
  <?= css_asset('text.css'); ?>
  <?= css_asset('grid12.css'); ?>
  <?= css_asset('style.css'); ?>
  <?= css_asset('tango/skin.css'); ?>
</head>

<body class="general">
<div id="header_general">
  <div class="container_12 main-header">
      <div class="grid_4 logo">
          <a href="<?= site_url(); ?>"><?= image_asset('/general/pop_maya_logo.png', '',array('alt'=>'Popmaya', 'width'=>'70%')) ?></a>
      </div>
      <div class="grid_4 pad_t_10">
		<!--?= image_asset('general/layout/soc_login-.jpg'); ?-->
		<div class="tit">Login With </div>
		<!-- social network login -->
		<?php
	    $fb_target=base_url()."home/facebook_login";
	    $fb_login="http://www.facebook.com/connect/uiserver.php?method=permissions.request&app_id=".$fb_appid."&display=page&redirect_uri=".urlencode($fb_target)."&perms=read_stream,publish_stream,email,offline_access&response_type=code&fbconnect=1&from_login=1";
		?>
		<a href="<?php echo $fb_login; ?>"><div class="home-facebook"></div></a>
		<!--<?= anchor($fb_login, "Facebook"); ?>-->
		
		<a href="<?php echo base_url().'home/twitter_login'; ?>"><div class="home-twitter"></div></a>
		<!--<?= anchor(base_url()."home/twitter_login", "Twitter"); ?>-->
		<!-- end social network login -->		
		
	  </div>
      <div class="grid_4 pad_t_10"> 

		<?= form_open(base_url()."home/login"); ?>

			<?= form_input(array('class'=>"input-text",'id'=>"email", 'name'=>"email", 'placeholder'=>"Email", 'tabindex'=>'1')); ?>
			<?= form_password(array('class'=>"input-text",'id'=>"password", 'name'=>"password", 'placeholder'=>"Password", 'tabindex'=>'2')); ?>

			<!--<?= form_submit('signin', "Login", array('tabindex'=>'3', 'class'=>"login-home")); ?>-->
			<input type="submit" name="signin" value="Login" class="login-home" tabindex="3" />
			
			<span class="remember_me">
			<?= form_checkbox(array('name'=>"remember", 'id'=>"remember", 'value'=>true, 'checked'=>false, 'title'=>"Remember Me", 'tabindex'=>'4')); ?> Keep me sign in
			<?= anchor(base_url()."home/forgot", "Forgot Password", array('class'=>'forgot_password')); ?>
			</span>

		<?= form_close(); ?>
		<!-- end login -->

      </div>      
  </div>
  <div class="clear"></div>
</div>

<?php 
  
  // refactored
  if($this->session->flashdata('message')):
  echo '<div class="container_12">
    <div class="grid_12 warn_mssg">'.$this->session->flashdata('message').'</div><div class="clear"></div>
    </div>';
  endif;
  
  if($this->session->flashdata('login_error')):
   echo '<div class="container_12">
    <div class="grid_12 warn_mssg">'.$this->session->flashdata('login_error').'</div><div class="clear"></div>
    </div>';
  endif;

  if($this->session->flashdata('register_error')):
    echo '<div class="container_12">
    <div class="grid_12 warn_mssg">'.$this->session->flashdata('register_error').'</div><div class="clear"></div>
    </div>';
  endif;

?>

<div class="container_12">  
  <div class="grid_6">
      <!--?= image_asset('general/layout/join_now.jpg'); ?-->
	  <!--div class="home-title">
	
		<h3>Pop Maya is Bigest Sound Based Social Media</h3>
		<h1>Join now and its free!!</h1>
	
	  </div-->
	
	<ul class="tab_home">	
		<li>
				<a href="#" class="tooltip" title="<?= $benefit_artist->benefit_description;?>">
				<div class="tab_artist"></div>
				</a>
		</li>	
		<li >
				<a href="#" class="tooltip" title="<?= $benefit_brand->benefit_description;?>">
				<div class="tab_brand"></div>
				</a>
		</li>
		<li >
				<a href="#" class="tooltip" title="<?= $benefit_fan->benefit_description;?>">
				<div class="tab_fans"></div>
				</a>
		</li>
  </ul>

  <div class="benefit_content"></div>

      <span class="updated_title"><?= image_asset('general/layout/ttl_updated.jpg'); ?></span>

      <ul class="home_updates">
        <li>
          <?php
            if(file_exists('./assets/images/profile/0000/00/00/'.$updated_artist->up_uid.'.jpg')){
                echo image_asset('profile/0000/00/00/'.$updated_artist->up_uid.'.jpg', '', array('class'=>'profile_img'));
            }else{
              echo image_asset('profile/0000/00/00/profile_th.jpg');
            }
          ?>
          <div class="update_info">
            <span class="update_time"><?= $updated_artist_hour; ?> hours ago</span>
            <h4><?= anchor('member/profile/'.$updated_artist->up_uid, $updated_artist->up_name); ?></h4>
            <p><?= nl2br($updated_artist->info); ?>
            </p>
            <div class="brief_info">
              <?= image_asset('general/layout/dami_album.jpg'); ?>
              <div class="brief_detail">
                <h5>TOS KAPENDAK</h5>
                <p><?= nl2br('dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, ....'); ?></p>
              </div>
			     <div class="clear"></div>
			  			  
        <div>
				<ul class="uibutton-group">
					<li class="grid_1">
						<a href="#" class="not_login"><div class="count_see"><span class="small biruLite" style="margin-left: 20px;">20,191</span></div></a>
					</li>
					<li class="grid_1">
						<a href="" class="not_login"><div class="count_comment"><span class="small biruLite" style="margin-left: 16px;">14,578</span></div></a>
					</li>
					<li class="grid_1" style="margin-left: 2px;">
						<a href="" class="not_login"><div class="count_flag"><span class="small biruLite" style="margin-left: 26px;">2,93</span></div></a>
					</li>
					<li class="grid_1" style="margin-right: 27px; margin-top: -5px;">
						<a href="https://twitter.com/share" class="twitter-share-button" data-via="popmaya" data-lang="en">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
					</li>
					<li class="grid_1" style="margin-top: -5px;">
						<iframe src="//www.facebook.com/plugins/like.php?href&amp;send=false&amp;layout=button_count&amp;width=360&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:400px; height:21px;" allowTransparency="true"></iframe>
					</li>
				</ul>
			  </div>
            </div>
          </div>
        </li>
      </ul>

  </div>
  <div class="grid_6">
    <div class="join_popmaya">
	  <div id="slider">
        <ul id="sliderContent">
			<?php
				foreach($slide_home as $row){ // refactored ?>				
					<li class="sliderImage">
						<a href=""><?= image_asset('banner/home/'.$row->image); ?></a>
						<span class="top"><strong><?= $row->title ?></strong><br />Lorem ipsum dolor sit amet</span>
					</li>
			<?	
				}?>
        </ul>
    </div>

	<!-- register -->
	<?php
	$attr=array('class'=>'form_reg_home');
	$hidden=array('prev_page'=>current_url());
	?>
	<?= form_open(base_url()."home/register", $attr, $hidden); ?>
	
		

		<?= form_label('First name')?>
		<?= form_input(array('id'=>"fullname", 'name'=>"fullname", 'placeholder'=>"First name", 'tabindex'=>'5')); ?> <br />

    <?= form_label('Last name')?>
    <?= form_input(array('id'=>"lastname", 'name'=>"last_name", 'placeholder'=>"Last name", 'tabindex'=>'6')); ?> <br />
		
		<?= form_label('Email')?>
		<?= form_input(array('id'=>"email", 'name'=>"email", 'placeholder'=>"Email", 'tabindex'=>'7')); ?> <br />
		
		<?= form_label('Password')?>		
		<?= form_password(array('id'=>"password", 'name'=>"password", 'placeholder'=>"Password", 'tabindex'=>'8')); ?> <br />

		<?= form_label("Gender", 'gender')?>
		<?= form_radio(array('name'=>"gender", 'id'=>"gender", 'checked'=>true, 'title'=>"Male", 'tabindex'=>'9')); ?> Male
		<?= form_radio(array('name'=>"gender", 'id'=>"gender", 'checked'=>false, 'title'=>"Female", 'tabindex'=>'10')); ?> Female <br />

		<div class="cb_join">
		<?= form_checkbox(array('name'=>"agree", 'id'=>"agree", 'checked'=>true, 'value'=>'agreed', 'title'=>"I agree with the Term & Service", 'tabindex'=>'11')); ?> I agree with the <?= anchor(base_url()."member/toc", "terms and conditions");?><br />
		<!--<?= form_checkbox(array('name'=>"update", 'id'=>"update", 'checked'=>false, 'value'=>'1', 'title'=>"Send me newsletter", 'tabindex'=>'12')); ?> Send me newsletter-->
		</div>
		
		<?= form_submit((array('name'=>'submit', 'class'=>'btn_join', 'value'=>'Join'))); ?>

	<?= form_close(); ?>
	<!-- end register -->		

      <br class="clear"/> 
    </div>
    
  </div>
  <div class="clear">&nbsp;</div>
</div>

<div id="home_spacer">&nbsp;</div>

<div class="container_12">  
  <div class="grid_12">
      <div class="track-select">
          <span class="set-recommend active">Recommended Track</span>
          <span class="set-new">New Tracks</span>
      </div>
      <div id="recommended-tracks">
 <ul class="jcarousel-skin-tango carousel">
      <?php foreach($recommended_track as $row) { ?>
      <li>
          <?= image_asset('cd_thumb.jpg','',array('class'=>'thumb')); ?>
          <div class="info">
          <?php $artist_name = 'superlooooooooooong band name'; ?>
              <div class="artist_name"><a href="#" title="<?= $artist_name; ?>">
              <?php if(strlen($artist_name) > 15) echo substr($artist_name, 0, 15) . '&hellip;'; else echo $artist_name; ?></a>
              </div>
              <div class="song_title"><?= $row->title;?></div>
              <div class="play_count">Total Play: 23</div>
              <div class="action">
                  <a href="#" class="not_login"><div class="bottom_play"></div></a>
			<a href="#" class="not_login"><div class="bottom_download"></div></a>
              </div>
          </div>
      </li>
     <?php } ?>
  </ul>
      </div>
      <div id="new-tracks">
          <ul class="jcarousel-skin-tango carousel">
          <?php foreach($new_track as $row) { ?>
          <li>
              <?= image_asset('efek_thumb.jpg','',array('class'=>'thumb')); ?>
              <div class="info">
                <?php $artist_name='Efek Rumah Kaca'; ?>
                  <div class="artist_name"><a href="#" title="<?= $artist_name; ?>"><?php if(strlen($artist_name) > 15) echo substr($artist_name, 0, 15) . '&hellip;'; else echo $artist_name; ?></a>
                  </div>

                  <div class="song_title"><?= $row->title;?></div>
                  <div class="play_count">Total Play: 23</div>
                  <div class="action">
                    <a href="#" class="not_login"><div class="bottom_play"></div></a>
					<a href="#" class="not_login"><div class="bottom_download"></div></a>
                  </div> 
              </div>
          </li>
         <?php } ?>
      </ul>
      </div>
  
  </div>
  <div class="clear">&nbsp;</div>
</div>

<div class="container_12">  
  <div class="grid_12">&nbsp;</div>
  <div class="clear">&nbsp;</div>
</div>  
<div class="bg_footer">
<div class="container_12">  
  <div class="grid_3">Popmaya &copy; 2011</div>
  <div class="grid_9 align_r">
    <?= anchor('en', 'English(US)'); ?> &middot; 
    <?= anchor('mobile', 'Mobile'); ?> &middot; 
    <?= anchor('search/friends', 'Find Friends'); ?> &middot; 
    <?= anchor('page/badges', 'Badges'); ?> &middot;
    <?= anchor('member/list', 'People'); ?> &middot; 
    <?= anchor('page/about', 'About'); ?> &middot; 
    <?= anchor('page/advertising', 'Advertising'); ?> &middot; 
    <?= anchor('page/create_page', 'Create a page'); ?> &middot; 
    <?= anchor('page/developer', 'Developers'); ?> &middot; 
    <?= anchor('page/career', 'Careers'); ?> &middot; 
    <?= anchor('page/privacy', 'Privacy'); ?> &middot; 
    <?= anchor('page/terms', 'Terms'); ?> &middot; 
    <?= anchor('page/help', 'Help'); ?> 
  </div>
  <div class="clear">&nbsp;</div>
</div> 
</div>

<!--div class="container_12">  
  <div class="grid_12">&nbsp;</div>
  <div class="clear">&nbsp;</div>
</div--> 
<?= js_asset('jquery-1.7.min.js');?>
<?= js_asset('jquery.jcarousel.min.js');?>
<?= js_asset('s3Slider.js');?>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('.carousel').jcarousel({
            visible:5,
            scroll:5,
            auto:3,
            animation:'slow',
            wrap:'last'
        });
        $('#new-tracks').hide();
        $('.track-select span').click(function(){
           $('.track-select span').removeClass('active');
           $(this).addClass('active');
           if($(this).hasClass('set-recommend'))
           {
               $('#recommended-tracks').show();
               $('#new-tracks').hide();
           }
           else
           {
                $('#recommended-tracks').hide();
                $('#new-tracks').show();
           }
               
        });

        jQuery('.not_login').click(function(){ alert('Mohon login terlebih dahulu.'); return false; });

        $('#slider').s3Slider({
            timeOut: 3000
        });
    });
   $(window).load(function(){
      <?= isset($foot_script)?$foot_script:''; ?>
   });   
</script>

<!-- untuk tooltip di bawah sini -->
<script type="text/javascript">
 
$(document).ready(function() {
    
    /* ^zy */
    var bc = $('.benefit_content');

    $('.tooltip').hover(function(){
      bc.html($(this).attr('title'));
      bc.slideDown();
    });
    
    $('.tooltip').mouseout(function(){
      bc.html('');
      bc.slideUp('fast');
    });
    /* ^zy.end */
 
});
 
</script>

<!-- end .container_12 -->
<br class="clear"/>
</body></html>