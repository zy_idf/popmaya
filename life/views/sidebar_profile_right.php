<div class="create-form">
    <h2>CREATE BRAND OR ARTIST</h2>
    <form method="post" action="#">
        <div class="styled-select">
            <select title="Select one">
              <option>Category</option>
              <option>Blue</option>
              <option>Red</option>
              <option>Green</option>
              <option>Yellow</option>
              <option>Brown</option>
            </select>
        </div>
        <input type="checkbox" id="terms"/><label for="terms">I Agree to PMY terms</label>
        <input type="text" name="name" placeholder="Name" title="name" class="text-input"/>
       
		<!-- di sini bagian pop up by gilangaramadan 13Des11-->
		<div id="blanket" style="display:none;"></div>
		<div id="popUpDiv" style="display:none;">
			<a href="#" onclick="popup('popUpDiv')">Click Me To Close</a>
			<span class="ijoText"><h6>MY PHOTO</h6></span>
			<hr />
			<p>
				Lorem ipsum dolor sit amet, apertius ingens ad suis. Coram te finis puellam materia effigie scit in deinde cupis auras sed quod eam in. A patriam ei Taliarchum in lucem in fuerat se est in.
			</p>
			<?= image_asset('test_crop.jpg', '',array('alt'=>'test_crop')); ?>
			<div class="clear"></div>
			<br />
			<div class="push_2 grid_2">
				<a href="#"><?= image_asset('save_thumb_btn.png', '',array('alt'=>'save_thumb', 'style'=>'margin-left: 20px;')); ?></a>
			</div>
			<div class="clear"></div>
			<br />
			<div class="grid_6 bg_upload" style="width: 400px; padding-bottom: 5px; padding-top: 5px;">
				<p align="center">Lorem ipsum dolor sit amet Rheni sedes collocavit at ipsum. </p>
				<div class="clear"></div>
				<div id="file_browse_wrapper" class="push_2" style="margin-left: 20px; margin-top: -10px;" >
					<input type='file' id='file_browse' />
				</div>
			</div>
			<div class="clear"></div>
			<br />
			<div class="push_2 grid_7">
				<a href="#"><?= image_asset('save_btn.png', '',array('alt'=>'save')); ?></a>
				<a href="#"><?= image_asset('skip_btn.png', '',array('alt'=>'skip')); ?></a>
			</div>
		</div>
		
		<a href="#" type="file" onclick="popup('popUpDiv')"><?= image_asset('create_btn.png', '',array('alt'=>'create_artist')); ?></a>
		<!--<input type="submit" value="Create" class="submit" onclick="popup('popUpDiv')"/>-->
		<!-- endPopup -->
	</form>
</div>
<div class="divider">&nbsp;</div>
<div class="sidebar-right">       
    <div class="group-page">
      <span>Artist Page (1)</span>
      <?= image_asset('karinding_thumb.jpg', '',array('alt'=>'karinding')); ?>
      <span class="link"><a href="#">Amazing Karinding'ers</a></span>
      <div class="clear"></div>
      <span><?= anchor('artist', 'See more', array('class'=>'more', 'title'=>'See more')); ?></span>            
    </div>
</div>
<div class="divider">&nbsp;</div>
<script type="text\javascript">
	function PopupCenter(pageURL, title,w,h) {
    var left = (screen.width/2)-(w/2);
    var top = (screen.height/2)-(h/2);
    var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}
</script>
<div class="sidebar-right">
    <div class="friend-page">
        Invite People by:</p>
        <ul>
            <li><?= anchor('#', 'Google', array('class'=>'google')); ?></li>
            <li><?= anchor('#', 'Yahoo', array('class'=>'yahoo')); ?></li>
        </ul>
        
        <form action="#" method="post" id="submit_email">
            <label for="email">or type an email:</label>
            <input type="text" name="email" placeholder="email" class="text-input" id="email"/>
            <input type="submit" name="submit" value="Submit" class="submit"/>
        </form>
    </div>
    <div class="list-right">
        <h3>Recommended Friends</h3>
        <ul>
            <li>
               <?= image_asset('pham_thumb.jpg', '',array('alt'=>'Linkin Park', 'class'=>'thumb-list')); ?>
                <h4><a href="#">Phamella</a></h4>
                <span>Bandung</span>
                <span class="link"><a href="#">Add as Friend</a></span>
            </li>
            <li>
               <?= image_asset('des_thumb.jpg', '',array('alt'=>'Mariah Carey', 'class'=>'thumb-list')); ?>
                <h4><a href="#">Desdes</a></h4>
                <span>Bandung</span>
                <span class="link"><a href="#">Add as Friend</a></span>
            </li>
            <li>
               <?= image_asset('rian_thumb.jpg', '',array('alt'=>'Dave Grohl', 'class'=>'thumb-list')); ?>
                <h4><a href="#">Rian</a></h4>
                <span>Bandung</span>
                <span class="link"><a href="#">Add as Friend</a></span>
            </li>
        </ul>
    </div>          
</div>