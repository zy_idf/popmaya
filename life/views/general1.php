<!DOCTYPE html>
<html lang="en">
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>popmaya</title>
  <link  href="<?= site_url() ?>assets/images/favicon.png" rel="shortcut icon" type="image/x-icon" />
  <meta name="description" content="popmaya » sosial network musik indonesia" />
  <meta name="keywords" content="Biggest sound based social media" />
  <meta name="author" content="www.theideafield.com" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <?= css_asset('reset.css'); ?>
  <?= css_asset('text.css'); ?>
  <?= css_asset('grid12.css'); ?>
  <?= css_asset('style.css'); ?>
  <?= css_asset('tango/skin.css'); ?>
</head>
<body class="general">
<div id="header_general">
  <div class="container_12 main-header">
      <div class="grid_4 logo">
        <h1>
          <a href="<?= site_url(); ?>"><?= image_asset('/general/pop_maya_logo.png', '',array('alt'=>'Popmaya')) ?></a>
        </h1>
      </div>
      <div class="grid_4 pad_t_10">
		<!--?= image_asset('general/layout/soc_login-.jpg'); ?-->
		<div class="tit">Login With </div>
		<!-- social network login -->
		<?php
	    $fb_target=base_url()."member/facebook_login";
	    $fb_login="http://www.facebook.com/connect/uiserver.php?method=permissions.request&app_id=".$fb_appid."&display=page&redirect_uri=".urlencode($fb_target)."&perms=read_stream,publish_stream,email,offline_access&response_type=code&fbconnect=1&from_login=1";
		?>
		<a href="<?php echo $fb_login; ?>"><div class="home-facebook"></div></a>
		<!--<?= anchor($fb_login, "Facebook"); ?>-->
		<a href="<?php echo base_url().'member/twitter_login'; ?>"><div class="home-twitter"></div></a>
		<!--<?= anchor(base_url()."member/twitter_login", "Twitter"); ?>-->
		<!-- end social network login -->		
		
	  </div>
      <div class="grid_4 pad_t_10"> 
	
		<!-- login -->
		<?php if($this->session->flashdata('login_error')):?>
			<?= $this->session->flashdata('login_error'); ?>
		<?php endif;?>

		<?= form_open(base_url()."member/login"); ?>

			<?= form_input(array('id'=>"email", 'name'=>"email", 'placeholder'=>"Email", 'tabindex'=>'1')); ?>
			<?= form_password(array('id'=>"password", 'name'=>"password", 'placeholder'=>"Password", 'tabindex'=>'2')); ?>

			<!--<?= form_submit('signin', "Login", array('tabindex'=>'3', 'class'=>"login-home")); ?>-->
			<input type="submit" name="signin" value="Login" class="login-home" tabindex="3" />
			
			<span class="remember_me">
			<?= form_checkbox(array('name'=>"remember", 'id'=>"remember", 'value'=>true, 'checked'=>false, 'title'=>"Remember Me", 'tabindex'=>'4')); ?> Keep me sign in
			<?= anchor(base_url()."member/forgot", "Forgot Password", array('class'=>'forgot_password')); ?>
			</span>

		<?= form_close(); ?>
		<!-- end login -->

      </div>      
  </div>
  <div class="clear"></div>
</div>
<div class="container_12">  
  <div class="grid_6">
      <!--?= image_asset('general/layout/join_now.jpg'); ?-->
	  <div class="home-title">
		<h3>Pop Maya is Bigest Sound Based Social Media</h3>
		<h1>Join now and its free!!</h1>
	  </div>
      <ul class="tab_home">
		<li >
			<a href="#" rel="tooltip" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus accumsan sollicitudin dui eget tincidunt. Sed non dignissim lectus. Ut quam quam, lobortis at hendrerit id, sagittis ac urna. Morbi auctor nunc eget diam bibendum pellentesque. Sed vel sem in nulla posuere vehicula ut in arcu. Curabitur in lectus justo. Curabitur vel tellus in sapien blandit scelerisque ac quis mi.
						Sed erat elit, tincidunt at congue in, ornare ac nibh. Nunc ac urna rhoncus risus fringilla tempor eget non tortor. Maecenas nec suscipit sem. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus vel felis ac mauris facilisis laoreet ac a risus. Vestibulum at pretium sem. Donec pulvinar, turpis sit amet porttitor mattis, nisl diam eleifend dui, eget tincidunt velit lectus in diam. In pellentesque hendrerit felis. Nullam luctus, quam eu vehicula malesuada, augue nulla iaculis nulla, non posuere massa erat vel quam. Vivamus in lacus sed lacus sollicitudin tempus non at arcu.">
			<div class="tab_artist"></div>
			</a>
		</li>
		<li >
			<a href="#" rel="tooltip" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus accumsan sollicitudin dui eget tincidunt. Sed non dignissim lectus. Ut quam quam, lobortis at hendrerit id, sagittis ac urna. Morbi auctor nunc eget diam bibendum pellentesque. Sed vel sem in nulla posuere vehicula ut in arcu. Curabitur in lectus justo. Curabitur vel tellus in sapien blandit scelerisque ac quis mi.
						Sed erat elit, tincidunt at congue in, ornare ac nibh. Nunc ac urna rhoncus risus fringilla tempor eget non tortor. Maecenas nec suscipit sem. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus vel felis ac mauris facilisis laoreet ac a risus. Vestibulum at pretium sem. Donec pulvinar, turpis sit amet porttitor mattis, nisl diam eleifend dui, eget tincidunt velit lectus in diam. In pellentesque hendrerit felis. Nullam luctus, quam eu vehicula malesuada, augue nulla iaculis nulla, non posuere massa erat vel quam. Vivamus in lacus sed lacus sollicitudin tempus non at arcu.">
			<div class="tab_brand"></div>
			</a>
		</li>
		<li >
			<a href="#" rel="tooltip" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus accumsan sollicitudin dui eget tincidunt. Sed non dignissim lectus. Ut quam quam, lobortis at hendrerit id, sagittis ac urna. Morbi auctor nunc eget diam bibendum pellentesque. Sed vel sem in nulla posuere vehicula ut in arcu. Curabitur in lectus justo. Curabitur vel tellus in sapien blandit scelerisque ac quis mi.
						Sed erat elit, tincidunt at congue in, ornare ac nibh. Nunc ac urna rhoncus risus fringilla tempor eget non tortor. Maecenas nec suscipit sem. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus vel felis ac mauris facilisis laoreet ac a risus. Vestibulum at pretium sem. Donec pulvinar, turpis sit amet porttitor mattis, nisl diam eleifend dui, eget tincidunt velit lectus in diam. In pellentesque hendrerit felis. Nullam luctus, quam eu vehicula malesuada, augue nulla iaculis nulla, non posuere massa erat vel quam. Vivamus in lacus sed lacus sollicitudin tempus non at arcu.">
			<div class="tab_fans"></div>
			</a>
		</li>
        <!--
		<li>
			<?= anchor('#', image_asset('general/layout/tab_artist.png'), 
			array('rel'=>'tooltip', 
			'title' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus accumsan sollicitudin dui eget tincidunt. Sed non dignissim lectus. Ut quam quam, lobortis at hendrerit id, sagittis ac urna. Morbi auctor nunc eget diam bibendum pellentesque. Sed vel sem in nulla posuere vehicula ut in arcu. Curabitur in lectus justo. Curabitur vel tellus in sapien blandit scelerisque ac quis mi.
						Sed erat elit, tincidunt at congue in, ornare ac nibh. Nunc ac urna rhoncus risus fringilla tempor eget non tortor. Maecenas nec suscipit sem. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus vel felis ac mauris facilisis laoreet ac a risus. Vestibulum at pretium sem. Donec pulvinar, turpis sit amet porttitor mattis, nisl diam eleifend dui, eget tincidunt velit lectus in diam. In pellentesque hendrerit felis. Nullam luctus, quam eu vehicula malesuada, augue nulla iaculis nulla, non posuere massa erat vel quam. Vivamus in lacus sed lacus sollicitudin tempus non at arcu. Etiam ullamcorper, leo pretium bibendum molestie, magna massa blandit mi, et sollicitudin lorem ligula vel quam. ')); 
			?>
		</li>
        <li>
			<?= anchor('#', image_asset('general/layout/tab_fans_off.png'),
			array('rel'=>'tooltip',
			'title' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus accumsan sollicitudin dui eget tincidunt. Sed non dignissim lectus. Ut quam quam, lobortis at hendrerit id, sagittis ac urna. Morbi auctor nunc eget diam bibendum pellentesque. Sed vel sem in nulla posuere vehicula ut in arcu. Curabitur in lectus justo. Curabitur vel tellus in sapien blandit scelerisque ac quis mi.
						Sed erat elit, tincidunt at congue in, ornare ac nibh. Nunc ac urna rhoncus risus fringilla tempor eget non tortor. Maecenas nec suscipit sem. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus vel felis ac mauris facilisis laoreet ac a risus. Vestibulum at pretium sem. Donec pulvinar, turpis sit amet porttitor mattis, nisl diam eleifend dui, eget tincidunt velit lectus in diam. In pellentesque hendrerit felis. Nullam luctus, quam eu vehicula malesuada, augue nulla iaculis nulla, non posuere massa erat vel quam. Vivamus in lacus sed lacus sollicitudin tempus non at arcu. Etiam ullamcorper, leo pretium bibendum molestie, magna massa blandit mi, et sollicitudin lorem ligula vel quam. ')); 
			?>
		</li>
        <li>
			<?= anchor('#', image_asset('general/layout/tab_brand_off.png'),
			array('rel'=>'tooltip',
			'title' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus accumsan sollicitudin dui eget tincidunt. Sed non dignissim lectus. Ut quam quam, lobortis at hendrerit id, sagittis ac urna. Morbi auctor nunc eget diam bibendum pellentesque. Sed vel sem in nulla posuere vehicula ut in arcu. Curabitur in lectus justo. Curabitur vel tellus in sapien blandit scelerisque ac quis mi.
						Sed erat elit, tincidunt at congue in, ornare ac nibh. Nunc ac urna rhoncus risus fringilla tempor eget non tortor. Maecenas nec suscipit sem. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus vel felis ac mauris facilisis laoreet ac a risus. Vestibulum at pretium sem. Donec pulvinar, turpis sit amet porttitor mattis, nisl diam eleifend dui, eget tincidunt velit lectus in diam. In pellentesque hendrerit felis. Nullam luctus, quam eu vehicula malesuada, augue nulla iaculis nulla, non posuere massa erat vel quam. Vivamus in lacus sed lacus sollicitudin tempus non at arcu. Etiam ullamcorper, leo pretium bibendum molestie, magna massa blandit mi, et sollicitudin lorem ligula vel quam. ')); 
			?>
		</li>
		-->
      </ul>
      <span class="updated_title"><?= image_asset('general/layout/ttl_updated.jpg'); ?></span>

      <ul class="home_updates">
        <li>
          <?= image_asset('general/layout/profile_th.jpg'); ?>
          <div class="update_info">
            <span class="update_time">5 hours ago</span>
            <h4><?= anchor('member/profile/rihanna', 'Rihanna'); ?></h4>
            <p><?= nl2br('Hey Poppers..!!
This is my new album “Tos Kapendak Alamatna”
Check this out..!! available in iTunes store and many more store in your country...enjoy it :)'); ?>
            </p>
            <div class="brief_info">
              <?= image_asset('general/layout/dami_album.jpg'); ?>
              <div class="brief_detail">
                <h5>TOS KAPENDAK</h5>
                <p><?= nl2br('dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, ....'); ?></p>
              </div>
              <?= image_asset('general/layout/action_bar.jpg', '', array('class'=>'no_border')); ?>
            </div>
          </div>
        </li>
      </ul>

  </div>
  <div class="grid_6">
    <div class="join_popmaya">
      <?= image_asset('general/layout/banner_right.jpg'); ?>

	<!-- register -->
	<?php
	$attr=array('class'=>'form_reg_home');
	$hidden=array('prev_page'=>current_url());
	?>
	<?= form_open(base_url()."member/register", $attr, $hidden); ?>
	
		<?php if($this->session->flashdata('register_error')):?>
			<?= $this->session->flashdata('register_error'); ?>
			<br />
		<?php endif;?>

		<?= form_label('Fullname')?>
		<?= form_input(array('id'=>"fullname", 'name'=>"fullname", 'placeholder'=>"Fullname", 'tabindex'=>'5')); ?> <br />
		
		<?= form_label('Email')?>
		<?= form_input(array('id'=>"email", 'name'=>"email", 'placeholder'=>"Email", 'tabindex'=>'6')); ?> <br />
		
		<?= form_label('Password')?>		
		<?= form_password(array('id'=>"password", 'name'=>"password", 'placeholder'=>"Password", 'tabindex'=>'7')); ?> <br />

		<?= form_label("Gender", 'gender')?>
		<?= form_radio(array('name'=>"gender", 'id'=>"gender", 'checked'=>true, 'title'=>"Male", 'tabindex'=>'8')); ?> Male
		<?= form_radio(array('name'=>"gender", 'id'=>"gender", 'checked'=>false, 'title'=>"Female", 'tabindex'=>'9')); ?> Female <br />

		<div class="cb_join">
		<?= form_checkbox(array('name'=>"agree", 'id'=>"agree", 'checked'=>false, 'value'=>'agreed', 'title'=>"I agree with the Term & Service", 'tabindex'=>'10')); ?> I agree with the <?= anchor(base_url()."member/toc", "terms and conditions");?><br />
		<?= form_checkbox(array('name'=>"update", 'id'=>"update", 'checked'=>false, 'value'=>'1', 'title'=>"Send me newsletter", 'tabindex'=>'11')); ?> Send me newsletter
		</div>
		
		<?= form_submit((array('name'=>'submit', 'class'=>'btn_join', 'value'=>'Join'))); ?>

	<?= form_close(); ?>
	<!-- end register -->		

      <br class="clear"/> 
    </div>
    
  </div>
  <div class="clear">&nbsp;</div>
</div>

<div class="container_12">  
  <div class="grid_12">&nbsp;</div>
  <div class="clear">&nbsp;</div>
</div>  

<div class="container_12">  
  <div class="grid_12">
      <div class="track-select">
          <span class="set-recommend active">Recommended Track</span>
          <span class="set-new">New Tracks</span>
      </div>
      <div id="recommended-tracks">
 <ul class="jcarousel-skin-tango carousel">
      <?php for($i=0;$i<20;$i++):?>
      <li>
          <?= image_asset('cd_thumb.jpg','',array('class'=>'thumb')); ?>
          <div class="info">
              <div class="artist_name">Coldplay</div>
              <div class="song_title">Yellow</div>
              <div class="play_count">Total Play: 23</div>
              <div class="action">
                  <?= image_asset('btn_play.jpg'); ?>
                  <?= image_asset('btn_download.jpg'); ?>
              </div>
          </div>
      </li>
     <?php endfor;?>
  </ul>
      </div>
      <div id="new-tracks">
          <ul class="jcarousel-skin-tango carousel">
          <?php for($i=0;$i<20;$i++):?>
          <li>
              <?= image_asset('efek_thumb.jpg','',array('class'=>'thumb')); ?>
              <div class="info">
                 <div class="artist_name">Efek Rumah Kaca</div>
                  <div class="song_title">Kenakalan Remaja di Era Informatika</div>
                  <div class="play_count">Total Play: 23</div>
                  <div class="action">
                      <?= image_asset('btn_play.jpg'); ?>
                      <?= image_asset('btn_download.jpg'); ?>
                  </div> 
              </div>
          </li>
         <?php endfor;?>
      </ul>
      </div>
  
  </div>
  <div class="clear">&nbsp;</div>
</div>

<div class="container_12">  
  <div class="grid_12">&nbsp;</div>
  <div class="clear">&nbsp;</div>
</div>  
<div class="bg_footer">
<div class="container_12">  
  <div class="grid_3">Popmaya &copy; 2011</div>
  <div class="grid_9 align_r">
    <?= anchor('en', 'English(US)'); ?> &middot; 
    <?= anchor('mobile', 'Mobile'); ?> &middot; 
    <?= anchor('search/friends', 'Find Friends'); ?> &middot; 
    <?= anchor('page/badges', 'Badges'); ?> &middot;
    <?= anchor('member/list', 'People'); ?> &middot; 
    <?= anchor('page/about', 'About'); ?> &middot; 
    <?= anchor('page/advertising', 'Advertising'); ?> &middot; 
    <?= anchor('page/create_page', 'Create a page'); ?> &middot; 
    <?= anchor('page/developer', 'Developers'); ?> &middot; 
    <?= anchor('page/career', 'Careers'); ?> &middot; 
    <?= anchor('page/privacy', 'Privacy'); ?> &middot; 
    <?= anchor('page/terms', 'Terms'); ?> &middot; 
    <?= anchor('page/help', 'Help'); ?> 
  </div>
  <div class="clear">&nbsp;</div>
</div> 
</div>

<div class="container_12">  
  <div class="grid_12">&nbsp;</div>
  <div class="clear">&nbsp;</div>
</div> 
<?= js_asset('jquery-1.7.min.js');?>
<?= js_asset('jquery.jcarousel.min.js');?>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('.carousel').jcarousel({
            visible:5,
            scroll:5,
            auto:3,
            animation:'slow',
            wrap:'last'
        });
        $('#new-tracks').hide();
        $('.track-select span').click(function(){
           $('.track-select span').removeClass('active');
           $(this).addClass('active');
           if($(this).hasClass('set-recommend'))
           {
               $('#recommended-tracks').show();
               $('#new-tracks').hide();
           }
           else
           {
                $('#recommended-tracks').hide();
                $('#new-tracks').show();
           }
               
        });
    });
   $(window).load(function(){
      <?= isset($foot_script)?$foot_script:''; ?>
   });   
</script>

<!-- untuk tooltip di bawah sini -->
<script type="text/javascript">
 
$(document).ready(function() {
 
    //Select all anchor tag with rel set to tooltip
    $('a[rel=tooltip]').mouseover(function(e) {
         
        //Grab the title attribute's value and assign it to a variable
        var tip = $(this).attr('title');   
         
        //Remove the title attribute's to avoid the native tooltip from the browser
        $(this).attr('title','');
         
        //Append the tooltip template and its value
        $(this).append('<div id="tooltip"><div class="tipHeader"></div><div class="tipBody">' + tip + '</div><div class="tipFooter"></div></div>');    
         
        //Set the X and Y axis of the tooltip
        $('#tooltip').css('top', e.pageY + 10 );
        $('#tooltip').css('left', e.pageX + 20 );
         
        //Show the tooltip with faceIn effect
        $('#tooltip').fadeIn('500');
        $('#tooltip').fadeTo('10',1);
         
    }).mousemove(function(e) {
     
        //Keep changing the X and Y axis for the tooltip, thus, the tooltip move along with the mouse
        $('#tooltip').css('top', e.pageY + 10 );
        $('#tooltip').css('left', e.pageX + 20 );
         
    }).mouseout(function() {
     
        //Put back the title attribute's value
        $(this).attr('title',$('.tipBody').html());
     
        //Remove the appended tooltip template
        $(this).children('div#tooltip').remove();
         
    });
 
});
 
</script>
<!-- end .container_12 -->
<br class="clear"/>
</body></html>