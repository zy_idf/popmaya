<?php if($this->session->userdata('user_id')){ ?>
<div class="profile-right">
	 <?php if($this->session->userdata('user_id')==$user_id) { ?>
     	<div class="profile-dropmenu"><?= anchor('member/profile/'.$user_id, $name); ?>
	 <?php } else { ?>
		<div class="profile-dropmenu"><?= anchor('member/profile/'.$my_user_id, $my_name); ?>
	 <?php } ?>
         <ul>
    <?php if($this->session->userdata('group')=='admin'){ ?>
		<li><?= anchor(site_url().'track/manage', 'Track CMS'); ?></li>
		<li><?= anchor(site_url().'admin/manage', 'Admin CMS'); ?></li>
    <?php } ?>
             <li><?= anchor(site_url().'member/use_as_page', 'Use as Page'); ?></li>
             <li><?= anchor(site_url().'member/profile', 'Profile'); ?></li>
             <li><a href="#">Setting</a></li>
             <li><?= anchor('member/profile_edit', 'Edit Profile'); ?></li>
             <li><?= anchor('home/logout', 'Logout'); ?></li>
         </ul>
     </div><span>&nbsp;|&nbsp;</span>
		<span class="noti_container">
			<a href="<?= base_url()?>member/friend_request">
				<?= image_asset('star_ico.png', '', array('alt'=>'favorite')) ?>
				 <span class="noti_bubble">
					<!-- nisa 25 Des friend request notification -->
					<?php if($count_friend_request) { ?>
						<?= $count_friend_request; ?>
					<?php } ?>					
				</span>        		
			</a>
		</span>
         <span class="noti_container">
             <a href="<?= site_url().'member/message/box/inbox' ?>"><?= image_asset('message_ico.png', '', array('alt'=>'notification')) ?></a>
             <span class="noti_bubble">
				<?php if($unread_messages>0) {?>
					<?= $unread_messages; ?>
				<?php } ?>
			</span>
         </span>   
     <span class="noti_container">
		<a href="<?= base_url()?>member/notification">
			<?= image_asset('circle_ico.png', '', array('alt'=>'refresh')) ?>
		</a>
		 <span class="noti_bubble">
			<!-- nisa 25 Des self notification -->
			<?php if($count_notification) { ?>
				<?= $count_notification; ?>
			<?php } ?>			
		</span>        		
	</span>
 </div>
<?php } ?>