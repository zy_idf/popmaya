<div class="create-form">
    <h2>CREATE ARTIST</h2>
    <form method="post" action="<?=site_url()?>artist/create_artist">
        <div class="styled-select">
            <select title="Select one" name="category">
              <option value="0">Artist</option>
              <option value="1">Band</option>
              <option value="2">Group</option>
            </select>
        </div>
        <input type="checkbox" name="terms" id="terms"/><label for="terms">I Agree to PMY terms</label>
        <input type="text" name="name" placeholder="Name" title="name" class="text-input"/>
		
		<? if($this->session->flashdata('terms')){ ?>
			<span> ERROR! You Must accept the Terms! </span>
			</br>
		<?}else if($this->session->flashdata('name')){?>
			<span> ERROR! You Must fill the name! </span>
			</br>
		<?}?>
       
		<!-- di sini bagian pop up by gilangaramadan 13Des11-->
		<!--
		<div id="blanket" style="display:none;"></div>
		<div id="popUpDiv" style="display:none;">
			<a href="#" onclick="popup('popUpDiv')">Click Me To Close</a>
			<span class="ijoText"><h6>MY PHOTO</h6></span>
			<hr />
			<p>
				Lorem ipsum dolor sit amet, apertius ingens ad suis. Coram te finis puellam materia effigie scit in deinde cupis auras sed quod eam in. A patriam ei Taliarchum in lucem in fuerat se est in.
			</p>
			<?= image_asset('test_crop.jpg', '',array('alt'=>'test_crop')); ?>
			<div class="clear"></div>
			<br />
			<div class="push_2 grid_2">
				<a href="#"><?= image_asset('save_thumb_btn.png', '',array('alt'=>'save_thumb', 'style'=>'margin-left: 20px;')); ?></a>
			</div>
			<div class="clear"></div>
			<br />
			<div class="grid_6 bg_upload" style="width: 400px; padding-bottom: 5px; padding-top: 5px;">
				<p align="center">Lorem ipsum dolor sit amet Rheni sedes collocavit at ipsum. </p>
				<div class="clear"></div>
				<div id="file_browse_wrapper" class="push_2" style="margin-left: 20px; margin-top: -10px;" >
					<input type='file' id='file_browse' />
				</div>
			</div>
			<div class="clear"></div>
			<br />
			<div class="push_2 grid_7">
				<a href="#"><?= image_asset('save_btn.png', '',array('alt'=>'save')); ?></a>
				 <a href="#">
				<?//= image_asset('skip_btn.png', '',array('alt'=>'skip')); ?>
				</a> 				<input type="submit" value="Skip" />
				<input type="submit" value="Skip" />
			</div>
		</div>-->
		
		<!--<a href="<?=site_url().'artist/create_artist'?>" type="submit"><?= image_asset('create_btn.png', '',array('alt'=>'create_artist')); ?></a>-->
		<input type="submit" value="Create" class="submit"/>
		<!-- endPopup -->
	</form>
</div>
<div class="divider">&nbsp;</div>
<div class="sidebar-right">       
    <div class="group-page">
      <span>Artist Page (<?= $count_my_artist;?>)</span>
      <!-- Edited by Adi, 2 Jan 2012 -->
	  <ul>
		<?foreach($artist_list as $a){?>
		<li>
			<?= anchor("artist/profile/".$a->ID, image_artist_thumb($a->ID, array('alt'=>$a->artist_name, 'class'=>'thumb-list'))); ?>
			<a><?= anchor("artist/profile/".$a->ID, $a->artist_name); ?></a>
			</br>
			<div class="clear"></div>
		</li>
		<?}?>		  
		  <span><?= anchor('artist', 'See more', array('class'=>'more', 'title'=>'See more')); ?></span>            
	  </ul>
	  <!-- end edited -->            
    </div>
</div>
<div class="divider">&nbsp;</div>
<div class="sidebar-right">
    <div class="friend-page">
        Invite People by:</p>
        <ul>
            <li><?= anchor('#', 'Google', array('class'=>'google')); ?></li>
            <li><?= anchor('#', 'Yahoo', array('class'=>'yahoo')); ?></li>
        </ul>
        
        <form action="#" method="post" id="submit_email">
            <label for="email">or type an email:</label>
            <input type="text" name="email" placeholder="email" class="text-input" id="email"/>
            <input type="submit" name="submit" value="Submit" class="submit"/>
        </form>
    </div>
    <div class="list-right">
        <h3>Recommended Friends</h3>
        <ul>
			<?php
				foreach($recommend_friends as $item){
			?>
					<li>
	                 	<?= anchor("member/profile/".$item->up_uid, image_profile_thumb($item->up_uid.'.jpg', array('alt'=>$item->up_name, 'class'=>'thumb-list'))); ?>
	                  	<h4><?= anchor("member/profile/".$item->up_uid, $item->up_name); ?></h4>
		                <span><?= $item->up_city; ?></span> 
		                <span class="link"><?= anchor('member/add_friend/'.$item->up_uid, 'Add Friend'); ?></span>
		            </li>
			<?php
				}
			?>
        </ul>
    </div>          
</div>
<?php
	if($this->session->userdata('user_id')){
?>
	<div id="chat_room">
		<h4><a href="<?= site_url().'chat'; ?>/chat" class="openChatRooms">Chat Room</a></h4>
		<ul class="chatRoomList">
			<li><a href="#" title="No room">No Room Available <span>0</span></a></li>
		</ul><!-- / -->
		<input type="hidden" name="lastUpdated" class="lastUpdated" value="" />
	</div>
<?php		
	}
?>