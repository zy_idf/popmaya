$(document).ready(function() {
	var cRootUrl = 'http://localhost:8888/ideafield/popmaya/';

    function popmaya_stream(){

        $.ajax({
            type: "POST",
            url: cRootUrl + "member/asyncro/get_stream",
            async: true,
            cache: false,
            timeout:50000,
            success: function(data){
                console.log(data);      
                // add to second list on status-list
                $(".status-list").prepend(data);
                setTimeout(popmaya_stream, 3000);
            },
            error: function(XMLHttpRequest, textStatus, errorThrown){
                alert(errorThrown);
                setTimeout(popmaya_stream, 10000);
            },
        });
    };

    popmaya_stream();

});